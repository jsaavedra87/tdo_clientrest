package com.tdo.integration.ws;

public class ExchangeRateProducer {
	
	public static String getExchangeRateSOAPXmlContent() {
		String SOAPRequest = "\r\n" + 
				"<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.dgie.banxico.org.mx\">\r\n" + 
				"   <soapenv:Header/>\r\n" + 
				"   <soapenv:Body>\r\n" + 
				"      <ws:tiposDeCambioBanxico soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>\r\n" + 
				"   </soapenv:Body>\r\n" + 
				"</soapenv:Envelope>";
		
		return SOAPRequest;
	}

}

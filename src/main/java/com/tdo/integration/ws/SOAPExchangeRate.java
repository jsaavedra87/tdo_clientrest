//package com.tdo.integration.ws;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.tdo.integration.services.HTTPRequestService;;
//
//@Service("SOAPExchangeRate")
//public class SOAPExchangeRate {
//
//	@Autowired
//	HTTPRequestService HTTPRequestService;
//	
//	public String getExchangeRate() {
//		
//		String response = null;
//		
//		try {
//			
//			response = HTTPRequestService.httpXmlRequest("http://www.banxico.org.mx/DgieWSWeb/DgieWS?invoke=",
//														  ExchangeRateProducer.getExchangeRateSOAPXmlContent(), 
//														  null);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		return response;
//	}
//
//}

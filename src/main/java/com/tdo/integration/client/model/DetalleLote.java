package com.tdo.integration.client.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class DetalleLote {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private BigInteger idVenta;
    private int articuloId;
    private String articuloClave;
    private String loteNumero; 
    private Date loteFechaFabricacion; 
    private Date loteFechaCaducidad;
    private BigDecimal loteExistenciaInicial; 
    private BigDecimal loteExistenciaActual; 
    private int loteStatus;
    private BigDecimal cantidad;
    private Integer paquete;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public int getArticuloId() {
		return articuloId;
	}
	public void setArticuloId(int articuloId) {
		this.articuloId = articuloId;
	}
	public String getArticuloClave() {
		return articuloClave;
	}
	public void setArticuloClave(String articuloClave) {
		this.articuloClave = articuloClave;
	}
	public String getLoteNumero() {
		return loteNumero;
	}
	public void setLoteNumero(String loteNumero) {
		this.loteNumero = loteNumero;
	}
	public Date getLoteFechaFabricacion() {
		return loteFechaFabricacion;
	}
	public void setLoteFechaFabricacion(Date loteFechaFabricacion) {
		this.loteFechaFabricacion = loteFechaFabricacion;
	}
	public Date getLoteFechaCaducidad() {
		return loteFechaCaducidad;
	}
	public void setLoteFechaCaducidad(Date loteFechaCaducidad) {
		this.loteFechaCaducidad = loteFechaCaducidad;
	}
	public BigDecimal getLoteExistenciaInicial() {
		return loteExistenciaInicial;
	}
	public void setLoteExistenciaInicial(BigDecimal loteExistenciaInicial) {
		this.loteExistenciaInicial = loteExistenciaInicial;
	}
	public BigDecimal getLoteExistenciaActual() {
		return loteExistenciaActual;
	}
	public void setLoteExistenciaActual(BigDecimal loteExistenciaActual) {
		this.loteExistenciaActual = loteExistenciaActual;
	}
	public int getLoteStatus() {
		return loteStatus;
	}
	public void setLoteStatus(int loteStatus) {
		this.loteStatus = loteStatus;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getPaquete() {
		return paquete;
	}
	public void setPaquete(Integer paquete) {
		this.paquete = paquete;
	}
	
}

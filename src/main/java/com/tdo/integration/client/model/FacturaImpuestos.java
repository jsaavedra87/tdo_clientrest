package com.tdo.integration.client.model;

import java.math.BigDecimal;
import java.math.BigInteger;


public class FacturaImpuestos {
	
	private static final long serialVersionUID = 1L;
	private int id;	
	private BigInteger idVenta;	
	private BigInteger idFactura;
	private Integer idImpuesto;	
	private BigDecimal totalImpuesto;
	private BigDecimal subtotal;
	private String tipoFactor;
	private String nombreImpuesto;
	private BigDecimal porcentajeImpuesto;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public BigInteger getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(BigInteger idFactura) {
		this.idFactura = idFactura;
	}
	public Integer getIdImpuesto() {
		return idImpuesto;
	}
	public void setIdImpuesto(Integer idImpuesto) {
		this.idImpuesto = idImpuesto;
	}
	public BigDecimal getTotalImpuesto() {
		return totalImpuesto;
	}
	public void setTotalImpuesto(BigDecimal totalImpuesto) {
		this.totalImpuesto = totalImpuesto;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public String getTipoFactor() {
		return tipoFactor;
	}
	public void setTipoFactor(String tipoFactor) {
		this.tipoFactor = tipoFactor;
	}
	public String getNombreImpuesto() {
		return nombreImpuesto;
	}
	public void setNombreImpuesto(String nombreImpuesto) {
		this.nombreImpuesto = nombreImpuesto;
	}
	public BigDecimal getPorcentajeImpuesto() {
		return porcentajeImpuesto;
	}
	public void setPorcentajeImpuesto(BigDecimal porcentajeImpuesto) {
		this.porcentajeImpuesto = porcentajeImpuesto;
	}

}

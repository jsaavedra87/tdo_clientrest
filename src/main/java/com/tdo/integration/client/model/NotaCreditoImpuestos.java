package com.tdo.integration.client.model;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NotaCreditoImpuestos {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private BigInteger idNotaCredito;
	private Integer idImpuesto;
	
	private BigDecimal totalImpuesto;
	private BigDecimal subTotal;
	private String tipoFactor;
	private String nombreImpuesto;
	private BigDecimal porcentajeImpuesto;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public BigInteger getIdNotaCredito() {
		return idNotaCredito;
	}
	public void setIdNotaCredito(BigInteger idNotaCredito) {
		this.idNotaCredito = idNotaCredito;
	}
	public Integer getIdImpuesto() {
		return idImpuesto;
	}
	public void setIdImpuesto(Integer idImpuesto) {
		this.idImpuesto = idImpuesto;
	}
	public BigDecimal getTotalImpuesto() {
		return totalImpuesto;
	}
	public void setTotalImpuesto(BigDecimal totalImpuesto) {
		this.totalImpuesto = totalImpuesto;
	}
	public BigDecimal getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}
	public String getTipoFactor() {
		return tipoFactor;
	}
	public void setTipoFactor(String tipoFactor) {
		this.tipoFactor = tipoFactor;
	}
	public String getNombreImpuesto() {
		return nombreImpuesto;
	}
	public void setNombreImpuesto(String nombreImpuesto) {
		this.nombreImpuesto = nombreImpuesto;
	}
	public BigDecimal getPorcentajeImpuesto() {
		return porcentajeImpuesto;
	}
	public void setPorcentajeImpuesto(BigDecimal porcentajeImpuesto) {
		this.porcentajeImpuesto = porcentajeImpuesto;
	}
}

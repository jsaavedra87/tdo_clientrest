package com.tdo.integration.client.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class NotaCredito {

	private static final long serialVersionUID = 1L;
	private int id;
	private String transactionId;
	private BigInteger idResumenCaja;
	private BigInteger idVenta;
	private BigInteger idNotaCredito;
	private BigInteger idFactura;
	private BigInteger idTicket;
	private BigInteger idNotaVenta;
	private Integer idCaja;
	private Date fechaNC;
	private Date fechaFactura;
	private BigInteger folio;
	private BigInteger folioNC;
	private String serieFolio;
	private String emisor;
	private String rfcEmisor;
	private String receptor;
	private String rfcReceptor;
	private String moneda;
	private String accNbr;
	private String siteNbr;
	private BigDecimal tipoCambio;
	private BigDecimal subTotal;
	private BigDecimal descuento;
	private BigDecimal total;
	private BigDecimal monedaSubtotal; 
	private BigDecimal monedaDescuento; 
	private BigDecimal monedaTotal;
	private BigDecimal monedaCambio;	
	private String formaPago;
	private String metodoPago;
	private String uuid;
	private String usoCfdi;	
	private String sucursal;
	private Empresa empresa;
	private Boolean devolucionAplicada;
	private Integer status;
	private String descripcionCaja;
	private String nombreVendedor;
	private BigInteger idVentaAplicada;
	private Integer precioCliente;
	
	private List<NotaCreditoImpuestos> notaCreditoImpuestos;

	private List<Articulo> articulo;
	
	//JAvila: Articulos del kit
	List<Articulo> articuloPaquete;
	
	//JAvila: Articulos del kit
	List<DetalleLote> detalleLotePaquete;

	private List<ArticuloImpuesto> articuloImpuesto;
	
	private List<DetalleLote> detalleLote;
	
	private List<TipoPago> tipoPago;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
	public BigInteger getIdResumenCaja() {
		return idResumenCaja;
	}
	public void setIdResumenCaja(BigInteger idResumenCaja) {
		this.idResumenCaja = idResumenCaja;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public BigInteger getIdNotaCredito() {
		return idNotaCredito;
	}
	public void setIdNotaCredito(BigInteger idNotaCredito) {
		this.idNotaCredito = idNotaCredito;
	}
	public BigInteger getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(BigInteger idFactura) {
		this.idFactura = idFactura;
	}
	public BigInteger getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(BigInteger idTicket) {
		this.idTicket = idTicket;
	}	
	public BigInteger getIdNotaVenta() {
		return idNotaVenta;
	}
	public void setIdNotaVenta(BigInteger idNotaVenta) {
		this.idNotaVenta = idNotaVenta;
	}
	public Integer getIdCaja() {
		return idCaja;
	}
	public void setIdCaja(Integer idCaja) {
		this.idCaja = idCaja;
	}	
	public Date getFechaNC() {
		return fechaNC;
	}
	public void setFechaNC(Date fechaNC) {
		this.fechaNC = fechaNC;
	}
	public Date getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public BigInteger getFolio() {
		return folio;
	}
	public void setFolio(BigInteger folio) {
		this.folio = folio;
	}
	public BigInteger getFolioNC() {
		return folioNC;
	}
	public void setFolioNC(BigInteger folioNC) {
		this.folioNC = folioNC;
	}
	public String getSerieFolio() {
		return serieFolio;
	}
	public void setSerieFolio(String serieFolio) {
		this.serieFolio = serieFolio;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getRfcEmisor() {
		return rfcEmisor;
	}
	public void setRfcEmisor(String rfcEmisor) {
		this.rfcEmisor = rfcEmisor;
	}
	public String getReceptor() {
		return receptor;
	}
	public void setReceptor(String receptor) {
		this.receptor = receptor;
	}
	public String getRfcReceptor() {
		return rfcReceptor;
	}
	public void setRfcReceptor(String rfcReceptor) {
		this.rfcReceptor = rfcReceptor;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public BigDecimal getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}	
	public BigDecimal getMonedaSubtotal() {
		return monedaSubtotal;
	}
	public void setMonedaSubtotal(BigDecimal monedaSubtotal) {
		this.monedaSubtotal = monedaSubtotal;
	}
	public BigDecimal getMonedaDescuento() {
		return monedaDescuento;
	}
	public void setMonedaDescuento(BigDecimal monedaDescuento) {
		this.monedaDescuento = monedaDescuento;
	}
	public BigDecimal getMonedaTotal() {
		return monedaTotal;
	}
	public void setMonedaTotal(BigDecimal monedaTotal) {
		this.monedaTotal = monedaTotal;
	}
	public BigDecimal getMonedaCambio() {
		return monedaCambio;
	}
	public void setMonedaCambio(BigDecimal monedaCambio) {
		this.monedaCambio = monedaCambio;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getMetodoPago() {
		return metodoPago;
	}
	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getUsoCfdi() {
		return usoCfdi;
	}
	public void setUsoCfdi(String usoCfdi) {
		this.usoCfdi = usoCfdi;
	}
	public List<NotaCreditoImpuestos> getNotaCreditoImpuestos() {
		return notaCreditoImpuestos;
	}
	public void setNotaCreditoImpuestos(List<NotaCreditoImpuestos> notaCreditoImpuestos) {
		this.notaCreditoImpuestos = notaCreditoImpuestos;
	}
	public List<Articulo> getArticulo() {
		return articulo;
	}
	public void setArticulo(List<Articulo> articulo) {
		this.articulo = articulo;
	}
	public List<ArticuloImpuesto> getArticuloImpuesto() {
		return articuloImpuesto;
	}
	public void setArticuloImpuesto(List<ArticuloImpuesto> articuloImpuesto) {
		this.articuloImpuesto = articuloImpuesto;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public String getAccNbr() {
		return accNbr;
	}
	public void setAccNbr(String accNbr) {
		this.accNbr = accNbr;
	}
	public String getSiteNbr() {
		return siteNbr;
	}
	public void setSiteNbr(String siteNbr) {
		this.siteNbr = siteNbr;
	}
	public List<DetalleLote> getDetalleLote() {
		return detalleLote;
	}
	public void setDetalleLote(List<DetalleLote> detalleLote) {
		this.detalleLote = detalleLote;
	}
	public List<Articulo> getArticuloPaquete() {
		return articuloPaquete;
	}
	public void setArticuloPaquete(List<Articulo> articuloPaquete) {
		this.articuloPaquete = articuloPaquete;
	}
	public List<DetalleLote> getDetalleLotePaquete() {
		return detalleLotePaquete;
	}
	public void setDetalleLotePaquete(List<DetalleLote> detalleLotePaquete) {
		this.detalleLotePaquete = detalleLotePaquete;
	}	
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public Boolean getDevolucionAplicada() {
		return devolucionAplicada;
	}
	public void setDevolucionAplicada(Boolean devolucionAplicada) {
		this.devolucionAplicada = devolucionAplicada;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getDescripcionCaja() {
		return descripcionCaja;
	}
	public void setDescripcionCaja(String descripcionCaja) {
		this.descripcionCaja = descripcionCaja;
	}
	public String getNombreVendedor() {
		return nombreVendedor;
	}
	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}
	public BigInteger getIdVentaAplicada() {
		return idVentaAplicada;
	}
	public void setIdVentaAplicada(BigInteger idVentaAplicada) {
		this.idVentaAplicada = idVentaAplicada;
	}
	public List<TipoPago> getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(List<TipoPago> tipoPago) {
		this.tipoPago = tipoPago;
	}
	public Integer getPrecioCliente() {
		return precioCliente;
	}
	public void setPrecioCliente(Integer precioCliente) {
		this.precioCliente = precioCliente;
	}
	
	
}

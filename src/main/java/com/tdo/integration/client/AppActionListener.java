package com.tdo.integration.client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.dto.integration.dto.ArLockBoxImportC;
import com.dto.integration.dto.ArticulosJson;
import com.dto.integration.dto.ClientesJson;
import com.dto.integration.dto.CreditDTO;
import com.dto.integration.dto.CreditNoteResponseDTO;
import com.dto.integration.dto.CustomerDTO;
import com.dto.integration.dto.GLInterface;
import com.dto.integration.dto.InventoryTransactionDTO;
import com.dto.integration.dto.InventoryTransactionV1;
import com.dto.integration.dto.RaInterface;
import com.dto.integration.dto.SalesDTO;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.client.localmodel.Sucursal;
import com.tdo.integration.client.localmodel.Udc;
import com.tdo.integration.services.CashMovsService;
import com.tdo.integration.services.ClientLocalService;
import com.tdo.integration.services.CreditNoteService;
import com.tdo.integration.services.CreditService;
import com.tdo.integration.services.CxCService;
import com.tdo.integration.services.GLService;
import com.tdo.integration.services.InventoryMonitorService;
import com.tdo.integration.services.InventoryService;
import com.tdo.integration.services.InventoryTransactionService;
import com.tdo.integration.services.NewCustomerService;
import com.tdo.integration.services.NewItemService;
import com.tdo.integration.services.PaymentService;
import com.tdo.integration.services.RESTService;
import com.tdo.integration.services.SalesService;
import com.tdo.integration.services.UDCService;

public class AppActionListener implements ActionListener {


	private final static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Properties props;
	JTextArea ta = null;
	
	@Autowired
	GLService glService;
	
	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	RESTService restService;
	
	@Autowired
	CxCService cxCService;
	
	@Autowired
	SalesService salesService;

	@Autowired
	PaymentService paymentService;
	
	@Autowired
	ClientLocalService clientLocalService;
	
	@Autowired
	CashMovsService cashMovsService;
	
	@Autowired
	NewCustomerService newCustomerService;
	
	@Autowired
	InventoryTransactionService inventoryTransactionService;
		
	@Autowired
	CreditService creditService;
	
	@Autowired 
	UDCService udcService;
	
	@Autowired 
	CreditNoteService creditNoteService;
	
	@Autowired
	NewItemService newItemService;
	
	@Autowired
	InventoryMonitorService inventoryMonitorService;
	
	Sucursal sucursal;

	public void actionPerformed(ActionEvent e) {
		
		String command = e.getActionCommand();
		
		if(command.equals("EXIT")) {
			int input = JOptionPane.showConfirmDialog(null, "Desea cerrar la aplicaci�n?");
			if (input == 0) {
				System.exit(WindowConstants.EXIT_ON_CLOSE);
			}
		}
		
		sucursal = clientLocalService.getBranch();
		if(sucursal != null) {
			if (command.equals("SEND_SALES")) {
				
				String value = JOptionPane.showInputDialog("Num cierre de caja:");
				if(value != null) {
					if(!"".equals(value)) {
						sendSales(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}
				
			}else if (command.equals("SEND_INVENTORY")) {
				
				String value = JOptionPane.showInputDialog("Num cierre de caja:");
				if(value != null) {
					if(!"".equals(value)) {
						sendInventory(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}
				

			}else if (command.equals("SEND_CXC")) {
				
				String value = JOptionPane.showInputDialog("Num cierre de caja:");
				if(value != null) {
					if(!"".equals(value)) {
						sendCxC(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}

			}else if (command.equals("REC_CUST")) {
				
				String value = JOptionPane.showInputDialog("Cliente");
				if(value != null) {
					if(!"".equals(value)) {
						sendGetCustomers();
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}
				
			}else if (command.equals("REC_ITEM")) {
				
				String value = JOptionPane.showInputDialog("Item");
				if(value != null) {
					if(!"".equals(value)) {
						sendGetItems();
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}
				
			}else if (command.equals("SEND_REMOTE_SALES")) {
				
				String value = JOptionPane.showInputDialog("Num cierre:");
				if(value != null) {
					if(!"".equals(value)) {
						sendSalesToCentralized(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}
			}else if (command.equals("SEND_CASH_MOVS")) {
				
				String value = JOptionPane.showInputDialog("Num cierre:");
				if(value != null) {
					if(!"".equals(value)) {
						sendCashMovs(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}
			}else if (command.equals("SEND_NEW_CUST")) {
				
				String value = JOptionPane.showInputDialog("Num cliente:");
				if(value != null) {
					if(!"".equals(value)) {
						sendNewCustomer(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}
				
			} else if (command.equals("GET_INV_TR")) {
				
				String value = JOptionPane.showInputDialog("Sucursal Transf: CDI001/2019-01-01_00:00:00/2019-01-01_23:59:59");
				if(value != null) {
					if(!"".equals(value)) {
						getInventoryTransfersControled(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}
			} else if (command.equals("SEND_CUST_CREDIT")) {
				String value = JOptionPane.showInputDialog("Num cliente:");
				if(value != null) {
					if(!"".equals(value)) {
						sendCustomerCredit(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}
				
			} else if (command.equals("SEND_PAYMENT")) {
				String value = JOptionPane.showInputDialog("Num cierre:");
				if(value != null) {
					if(!"".equals(value)) {
						sendCustomerPayment(value);
					}else {
						JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
					}
				}else {}
				
			}
		}else {
			if (command.equals("SAVE_BRANCH")) {
				saveBranch();
			}else {
				JOptionPane.showMessageDialog(null, "Debe de registrar una sucursal antes de enviar la informaci�n.");
			}

		}
	}
	
	@Scheduled(cron= "${cronSalesGL}")
	private void startSendSalesGL() {
		Udc udc = null;		
		BigInteger consecutivo = null;
		BigInteger ultimoCC = null; 
		
		try {
			sucursal = clientLocalService.getBranch();
			udc = udcService.searchBySystemAndKey(AppConstants.VENTAS_SYSTEM_GL, AppConstants.VENTAS_KEY_GL);
			consecutivo = new BigInteger(udc.getUdcValue());
			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			System.out.println("PROCESANDO GL CC: " + consecutivo);
			glService.startGLProcess(sucursal.getSucursal(), consecutivo.intValue(), props.getProperty("restGLService"));

			ultimoCC = salesService.getLastId();
			if(consecutivo.intValue() <= ultimoCC.intValue()) {
				udc.setUdcValue(String.valueOf(consecutivo.intValue() + 1));
				udcService.saveUdc(udc);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de GL: " + e.getMessage());
		}
	}
	
	@Scheduled(cron= "${cronSalesAR}")
	private void startSendSalesCxC() {
		Udc udc = null;
		BigInteger consecutivo = null;
		BigInteger ultimoCC = null;
		
		try {
			
			sucursal = clientLocalService.getBranch();
			udc = udcService.searchBySystemAndKey(AppConstants.VENTAS_SYSTEM_AR, AppConstants.VENTAS_KEY_AR);
			consecutivo = new BigInteger(udc.getUdcValue());
			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}			
			
			System.out.println("PROCESANDO CXC CC: " + consecutivo);
			cxCService.startCXCProcess(sucursal.getSucursal(), consecutivo.intValue(), props.getProperty("restARService"), props.getProperty("restARFindService"));

			ultimoCC = salesService.getLastId();
			if(consecutivo.intValue() <= ultimoCC.intValue()) {
				udc.setUdcValue(String.valueOf(consecutivo.intValue() + 1));
				udcService.saveUdc(udc);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de AR: " + e.getMessage());
		}
	}

	@Scheduled(cron= "${cronSalesInventory}")
	private void startSendInventory() {
		boolean processed = false;
		Udc udc = null;
		String lastDate = "";
		String fromDate = "";
		String toDate = "";
		String newDate = "";
		
		try {
			sucursal = clientLocalService.getBranch();
			udc = udcService.searchBySystemAndKey(AppConstants.VENTAS_SYSTEM_INVENTORY, AppConstants.VENTAS_KEY_INVENTORY);			
			lastDate = udc.getUdcValue();
			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			if(!"".equals(lastDate)) {
				fromDate = lastDate;
			} else {
				fromDate = dateFormat.format(new Date());
			}
			toDate = dateFormat.format(new Date());
			newDate = toDate;

			//Delay
			fromDate = dateFormat.format(InventoryService.dateCalculateMinutes(fromDate, "yyyy-MM-dd HH:mm:ss", -1)); //(-1 minuto)
			toDate = dateFormat.format(InventoryService.dateCalculateMinutes(toDate, "yyyy-MM-dd HH:mm:ss", -1)); //(-1 minuto)
			
			System.out.println("PROCESANDO INVENTARIO POR FECHA NUEVO: " + fromDate + " - " + toDate);
			inventoryService.startInventoryProcess(sucursal.getSucursal(), fromDate, toDate, props.getProperty("restInventoryService"));
			
			newDate = dateFormat.format(InventoryService.dateCalculateSeconds(newDate, "yyyy-MM-dd HH:mm:ss", 1)); //(+1 segundo) Nueva fecha de inicio			
			processed = true;
		}catch(Exception e){
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de env�o de Inventarios: " + e.getMessage());
		}
		
		if(processed) {				
			udc.setUdcValue(newDate);
			udcService.saveUdc(udc);
		}
	}	

	@Scheduled(cron= "${cronCashMovs}")
	private void startSendCashMovs() {
		Udc udc = null;		
		BigInteger consecutivo = null;
		BigInteger ultimoCC = null;
		
		try {
			sucursal = clientLocalService.getBranch();
			udc = udcService.searchBySystemAndKey(AppConstants.VENTAS_SYSTEM_CASH_MOV, AppConstants.VENTAS_KEY_CASH_MOV);
			consecutivo = new BigInteger(udc.getUdcValue());
			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			System.out.println("PROCESANDO MOVIMIENTOS DE CAJA CC: " + consecutivo);
			cashMovsService.startCashMovsProcess(sucursal.getSucursal(), consecutivo.intValue(), props.getProperty("restGLService"));
			
			ultimoCC = salesService.getLastId();
			if(consecutivo.intValue() <= ultimoCC.intValue()) {
				udc.setUdcValue(String.valueOf(consecutivo.intValue() + 1));
				udcService.saveUdc(udc);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Movimientos de Caja: " + e.getMessage());
		}
	}
	
	@Scheduled(cron= "${cronSalesCentralized}")
	private void startSendSalesToCentralized() {
		Udc udc = null;		
		BigInteger consecutivo = null;
		BigInteger ultimoCC = null;
		
		try {
			sucursal = clientLocalService.getBranch();
			udc = udcService.searchBySystemAndKey(AppConstants.VENTAS_SYSTEM_CENTRALIZED, AppConstants.VENTAS_KEY_CENTRALIZED);
			consecutivo = new BigInteger(udc.getUdcValue());
			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			System.out.println("PROCESANDO ENVIO DE VENTAS BD CENTRALIZADA CC: " + consecutivo);
			salesService.startCentralizedSalesProcess(sucursal.getSucursal(), consecutivo.intValue(), props.getProperty("restSalesService"));
			
			ultimoCC = salesService.getLastId();
			if(consecutivo.intValue() <= ultimoCC.intValue()) {
				udc.setUdcValue(String.valueOf(consecutivo.intValue() + 1));
				udcService.saveUdc(udc);
			}			
		} catch (Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Env�o de Ventas hacia la BD Centralizada: " + e.getMessage());
		}
	}

	////////////@Scheduled(cron= "${cronCustomerPayment}")
	private void startSendCustomerPayment() {
		Udc udc = null;
		BigInteger consecutivo = null;
		BigInteger ultimoCC = null;
		
		try {
			sucursal = clientLocalService.getBranch();
			udc = udcService.searchBySystemAndKey(AppConstants.VENTAS_SYSTEM_PAYMENT, AppConstants.VENTAS_KEY_PAYMENT);
			consecutivo = new BigInteger(udc.getUdcValue());
			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			System.out.println("PROCESANDO ABONOS CC: " + consecutivo);			
			List<ArLockBoxImportC> list = paymentService.createArLockBoxImportC(sucursal.getSucursal(), AppConstants.REQUEST_TYPE_CC, new String[] { consecutivo.toString() });
			
			if(list != null && !list.isEmpty()) {
				String response = restService.sendRequestObject(props.getProperty("restCustomerPaymentService"), list);
				showMessage("Respuesta del env�o de Abonos para el CC" + consecutivo + ": \r" + response);
			}
			
			ultimoCC = salesService.getLastId();
			if(consecutivo.intValue() <= ultimoCC.intValue()) {
				udc.setUdcValue(String.valueOf(consecutivo.intValue() + 1));
				udcService.saveUdc(udc);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Abonos: " + e.getMessage());
		}
	}
		
	////////////@Scheduled(cron= "${cronCustomerCredit}")
	private void startSendCustomerCredit() {
		Udc udc = null;
		
		try {
			sucursal = clientLocalService.getBranch();
			udc = udcService.searchBySystemAndKey(AppConstants.VENTAS_SYSTEM_CREDIT, AppConstants.VENTAS_KEY_CREDIT);
			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			System.out.println("PROCESANDO L�MITES DE CR�DITO.");
			List<CreditDTO> creditos = creditService.getCustomerCreditSicar(sucursal.getSucursal(), "");
			String res =  restService.sendRequestList(props.getProperty("restCustomerCredit"), creditos);
			creditService.updateCustomerSicar(res);
			
		}catch(Exception e){
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de L�mites de Cr�dito: " + e.getMessage());
		}
	}
	
	@Scheduled(cron= "${cronUdcValue}")
	private void updateUdcValue() {
		try {

			Udc udc = udcService.searchBySystemAndKey(AppConstants.UDC_TIPO_CAMBIO, AppConstants.UDC_TIPO_CAMBIO_KEY);			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			List<Udc> udcList =  restService.sendRequestUdcByUdcKey(props.getProperty("restGetUdcList"), udc.getUdckey());
			System.out.println("PROCESANDO TIPO DE CAMBIO.");
			
			if(udcList!=null) {
				Udc currentExchangeRate = udcList.get(0);
				udc.setUdcValue(currentExchangeRate.getUdcValue());
				System.out.println("About to update udc: "+ AppConstants.UDC_TIPO_CAMBIO + ":" + AppConstants.UDC_TIPO_CAMBIO_KEY + " with new exchangeRate: "+udc.getUdcValue());
				udcService.updateMonedaUdc(udc);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Tipo de Cambio: " + e.getMessage());
		}
	}
	
	@Scheduled(cron= "${cronInventoryTransfers}")
	private void getInventoryTransfers() {
		SimpleDateFormat formatGMT = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");

		boolean processed = false;
		Udc udc = udcService.searchBySystemAndKey(AppConstants.INV_TRANSACTIONS_SYSTEM, AppConstants.INV_TRANSACTIONS_KEY);
		Udc udcBranch = udcService.searchBySystemAndKey(AppConstants.BRANCH_SYSTEM, AppConstants.BRANCH_KEY);
		
		if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
			return;
		}

		String lastDate = "";
		String newLastDate = "";
		
		try {
			
			sucursal = clientLocalService.getBranch();
			lastDate = udc.getUdcValue();
			lastDate = lastDate.substring(0, 10) + "_00:00:00";		
			String suc = udcBranch.getUdcValue();			
			String value = suc + "/" + lastDate;

			newLastDate = formatGMT.format(new Date());
			System.out.println("\nPROCESANDO TRANSFERENCIAS DE INVENTARIO: " + value + " - " + udc.getUdcValue());			
			List<InventoryTransactionDTO> items = restService.sendRequestInventoryTrasfersOracle(props.getProperty("restInvTransfers"), value);
			
			if(items != null) {
				showMessage("SE RECUPERARON " + items.size() + " MOVIMIENTOS DE INVENTARIO: \r");
				if(items.size() > 0) {
					String res = inventoryTransactionService.newInventoryTransactionsRecord(items, props, suc, lastDate);
					showMessage(res);
				}
			}else {
				showMessage("El proceso ha concluido. No se detectaron transacciones con el c�digo " + suc);
			}
			inventoryTransactionService.updateSicarPurchaseCostItem();
			processed = true;	
		}catch(Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Transferencia de Inventario: " + e.getMessage());
		}
		
		if(processed) {
			udc.setUdcValue(newLastDate);
			udcService.saveUdc(udc);
		}

	}

	@Scheduled(cron= "${cronUpdateInventoryTransfers}")
	private void updateInventoryTransfers() {
		Udc udc = udcService.searchBySystemAndKey(AppConstants.INV_TRANSACTIONS_SYSTEM, AppConstants.INV_TRANSACTIONS_KEY);
		Udc udcBranch = udcService.searchBySystemAndKey(AppConstants.BRANCH_SYSTEM, AppConstants.BRANCH_KEY);
		
		if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
			return;
		}

		String lastDate = "";
		
		try {
			
			sucursal = clientLocalService.getBranch();
			lastDate = udc.getUdcValue();
			lastDate = lastDate.substring(0, 10) + "_00:00:00";		
			String suc = udcBranch.getUdcValue();			
			String value = suc + "/" + lastDate;

			System.out.println("\nACTUALIZACI�N DE COSTOS: " + value + " - " + udc.getUdcValue());			
			List<InventoryTransactionDTO> items = restService.sendRequestInventoryTrasfersOracle(props.getProperty("restInvTransfers"), value);
			
			if(items != null) {
				showMessage("SE RECUPERARON " + items.size() + " MOVIMIENTOS DE INVENTARIO: \r");
				if(items.size() > 0) {
					String res = inventoryTransactionService.updateInventoryTransactionsRecord(items, props, suc, lastDate);
					showMessage(res);
				}
			}else {
				showMessage("El proceso ha concluido. No se detectaron transacciones con el c�digo " + suc);
			}
			inventoryTransactionService.updateSicarPurchaseCostItem();
		}catch(Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Actualizaci�n Transferencia de Inventario: " + e.getMessage());
		}		

	}
	
	@Scheduled(cron= "${cronGetCustomers}")
	private void sendGetCustomers() {
		Udc udc = udcService.searchBySystemAndKey(AppConstants.CUSTOMER_SYSTEM, AppConstants.CUSTOMER_KEY);
		Udc udcBranch = udcService.searchBySystemAndKey(AppConstants.BRANCH_SYSTEM, AppConstants.BRANCH_KEY);
		boolean success = false;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		String currentDateTime = format.format(new Date());
		
		try {
			
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			String value = udc.getUdcValue();
			String organizationCode = udcBranch.getUdcValue();
			value = value.substring(0, 10) + "T00:00:00.000Z";
			
			System.out.println("PROCESANDO NUEVOS CLIENTES POR FECHA (ORACLE-SICAR): " + value);
			List<ClientesJson> clientes = restService.sendRequestCustomerOracle(props.getProperty("restCustomerOracleService"), value.trim());
			String res = "";
			if(clientes != null) {
				if(clientes.isEmpty()) {
					showMessage("El proceso ha concluido. No se detectaron clientes a partir de la fecha: " + value);
				} else {
					System.out.println("PROCESANDO NUEVOS CLIENTES POR FECHA (ORACLE-SICAR): " + clientes.size() + " CLIENTE(S) RECUPERADO(S).");
					for(ClientesJson o : clientes) {
						//Valida si existe cliente y obtiene Id del Cliente
						if(!newCustomerService.customerExist(o)) {
							if(AppConstants.CEDI_CODE.equals(organizationCode)) {								
								res = restService.insertSicar(props.getProperty("restSicarCustomer"), o);								
								if(res != null && !res.isEmpty()) {
									showMessage(res);
								}
								showMessage("El proceso ha concluido.Se registro el cliente " + o.getNombre() + " - " + o.getCurp());															
								
								//Valida si existe cliente y obtiene Id del Cliente
								newCustomerService.customerExist(o);
							}
						}
						newCustomerService.saveOrUpdateCustomerSiteAccountClienteJSon(o);					
					}
				}
				success = true;
			}else {
			   	showMessage("El proceso ha concluido. No se detectaron clientes a partir de la fecha: " + value);
			   	success = true;
			}
			inventoryTransactionService.updateSicarDesglosaIEPS();
		}catch(Exception e){
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Env�o de Clientes Oracle-Sicar: " + e.getMessage());
		}
		
		if(success) {
			udc.setUdcValue(currentDateTime);
			udcService.saveUdc(udc);
		}	
	}
	
	@Scheduled(cron= "${cronNewCustomers}")
	private void sendNewCustomer() {
		boolean success = false;
		Udc udc = udcService.searchBySystemAndKey(AppConstants.CUSTOMER_SICAR_SYSTEM, AppConstants.CUSTOMER_SICAR_KEY);		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		String lastDate = "";
		
		try {
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			lastDate = udc.getUdcValue();
			lastDate = lastDate.substring(0, 10) + " 00:00:00";

			System.out.println("PROCESANDO NUEVOS CLIENTES POR FECHA (SICAR-ORACLE): " + lastDate);
			List<CustomerDTO> list = newCustomerService.getNewCustomer(lastDate);
			String res = restService.sendRequestList(props.getProperty("restNewCustomers"), list);
			showMessage(res);
			
			lastDate = format.format(new Date());
			success = true;
		}catch(Exception e){
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Env�o de Clientes Sicar-Oracle: " + e.getMessage());
		}
		
		if(success) {
			udc.setUdcValue(lastDate);
			udcService.saveUdc(udc);
		}
	}
	
	@Scheduled(cron= "${cronGetItems}")
	private void sendGetItems() {
	
		Udc udc = udcService.searchBySystemAndKey(AppConstants.ITEM_MASTER_SYSTEM, AppConstants.ITEM_MASTER_KEY);
		Udc udcBranch = udcService.searchBySystemAndKey(AppConstants.BRANCH_SYSTEM, AppConstants.BRANCH_KEY);
		boolean success = false;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		String currentDateTime = format.format(new Date());
		
		try {
			if(udc.getUdcStatus() != null && !udc.getUdcStatus().equals(AppConstants.STATUS_ON)) {
				return;
			}
			
			String date = udc.getUdcValue();
			String organizationCode = udcBranch.getUdcValue();                                                                                               
			String value = new StringBuffer().append(organizationCode).append("/").append(date).toString();
			
			if(AppConstants.CEDI_CODE.equals(organizationCode)) {
				System.out.println("PROCESANDO NUEVOS ARTICULOS POR FECHA: " + value);
				StringBuilder message = new StringBuilder("");
				List<ArticulosJson> articulos = restService.sendRequestItemOracle(props.getProperty("restItemOracleService"), value.trim(), message);
				
				showMessage("SendRequestItemOracle: " + message);
			    String res = "";
				if(articulos != null) {
					if(articulos.size() > 0) {
				    	for(ArticulosJson o : articulos) {
							if(o != null) {
				    		res = restService.insertSicar(props.getProperty("restSicarItem"), o);
				    		showMessage("El proceso ha concluido para el art�culo: " + o.getArticulo().getDescripcion() + ". Resultado: " + res);			    	
							}
				    	}
				    	newItemService.updateItemLot();
					}
			    	if(articulos.isEmpty()) {
						showMessage("El proceso ha concluido. No se detectaron art�culos con el c�digo " + value);
					}else {
						showMessage(res);
					}
			    	success = true;
			    }else {
			    	showMessage("El proceso ha concluido. No se detectaron art�culos con el c�digo " + value);
			    	success = true;
			    }
			}
		}catch(Exception e){
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Env�o de Art�culos Oracle-Sicar: " + e.getMessage());
		}
		
		if(success) {
			udc.setUdcValue(currentDateTime);
			udcService.saveUdc(udc);
		}
	}
	
	@Scheduled(cron= "${cronInventoryMonitor}")
	private void inventoryMonitor() {		

		try {
			Udc udcBranch = udcService.searchBySystemAndKey(AppConstants.BRANCH_SYSTEM, AppConstants.BRANCH_KEY);
			sucursal = clientLocalService.getBranch();
			
			System.out.println("MONITOREO DE INVENTARIO.");
			inventoryMonitorService.startInventoryMonitor(sucursal.getId(), sucursal.getSucursal(), udcBranch.getUdcValue(), props.getProperty("restInventoryMonitorService"));

		} catch (Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar el proceso de monitoreo de inventarios: " + e.getMessage());			
		}
		
	}

	////@Scheduled(fixedDelay=300000, initialDelay=750000)
	private void updateCreditNoteResponses() {
		
		System.out.println("PROCESANDO NOTAS DE CREDITO POR DESCUENTO.");
		List<CreditNoteResponseDTO> respList =  restService.getTxtResponses(props.getProperty("restGetTxtResp"));
		
		try {
			System.out.println(respList.size());
			creditNoteService.updateCreditNoteTxtResponse(respList);
		} catch (Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al ejecutar la integraci�n de Actualizaci�n de Notas de Cr�dito: " + e.getMessage());
		}
	}
	
	private void sendNewCustomer(String value) {		
		try {
			List<CustomerDTO> list = newCustomerService.getNewCustomer(value);
			String res = restService.sendRequestList(props.getProperty("restNewCustomers"), list);
			newCustomerService.updateCustomer(res);
			showMessage(res);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	private void sendCustomerCredit(String value) {
		try {
			List<CreditDTO> creditos = creditService.getCustomerCreditSicar(sucursal.getSucursal(), value);
			String res =  restService.sendRequestList(props.getProperty("restCustomerCredit"), creditos);
			creditService.updateCustomerSicar(res);
			showMessage(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void sendCashMovs(String value) {
		
		try {
			GLInterface glInterface = cashMovsService.createCashTransactionsInterface(sucursal.getSucursal(), AppConstants.REQUEST_TYPE_CC, new String[] { value });
			String res = restService.sendRequestObject(props.getProperty("restGLService"), glInterface);
			showMessage(res);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void sendSalesToCentralized(String value) {
		try {
			SalesDTO sales = salesService.getSalesDTO(sucursal.getSucursal(), AppConstants.REQUEST_TYPE_CC, new String[] { value });
			String res = restService.sendRequestObject(props.getProperty("restSalesService"), sales);
			showMessage(res);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private void sendCustomerPayment(String value) {
		try {
			List<ArLockBoxImportC> list = paymentService.createArLockBoxImportC(sucursal.getSucursal(), AppConstants.REQUEST_TYPE_CC, new String[] { value});
			String res = restService.sendRequestObject(props.getProperty("restCustomerPaymentService"), list);
			showMessage(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setProperties(Properties props, JTextArea ta ) {
		this.props = props;
		this.ta = ta;
	}
	
	private void sendSales(String value) {
		try {
			GLInterface glInterface = glService.createGLInterface(sucursal.getSucursal(), AppConstants.REQUEST_TYPE_CC, new String[] { value }, null);
			String res = restService.sendRequestObject(props.getProperty("restGLService"), glInterface);
			showMessage(res);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void sendInventory(String value) {
		try {
			InventoryTransactionV1 inv = inventoryService.createInventoryInterface(sucursal.getSucursal(), AppConstants.REQUEST_TYPE_CC, new String[] { value });
			String res = restService.sendRequestObject(props.getProperty("restInventoryService"), inv);
			showMessage(res);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void sendCxC(String value) {
		try {
			RaInterface raInterface = cxCService.createRaInterfaceLinesAll(sucursal.getSucursal(), AppConstants.REQUEST_TYPE_CC, new String[] { value }, null, props.getProperty("restARFindService"));
			String res = restService.sendRequestObject(props.getProperty("restARService"), raInterface);
			showMessage(res);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void getInventoryTransfersControled(String inValue) {		
		String suc = "";
		String initDateString = "";
		
		try {			
			String[] stringValues = inValue.split("/");

			if(stringValues.length < 2) {
				showMessage("Datos insuficientes: " + inValue);
			} else {
				suc = stringValues[0];
				initDateString = stringValues[1].substring(0, 10) + "_00:00:00";
				String value = suc + "/" + initDateString;

				showMessage("PROCESANDO TRANSFERENCIAS DE INVENTARIO: " + value);				
				List<InventoryTransactionDTO> items = restService.sendRequestInventoryTrasfersOracle(props.getProperty("restInvTransfers"), value);
				
				if(items != null) {
					showMessage("SE RECUPERARON " + items.size() + " MOVIMIENTOS DE INVENTARIO: \r");
					if(items.size() > 0) {
						for(InventoryTransactionDTO dto : items) {
							showMessage(new StringBuilder().append("\r")
									.append("\r[ItemId: ").append(dto.getItemId()).append("]")
									.append("\r[ItemName: ").append(dto.getItemName()).append("]")
									.append("\r[ItemDescripcion: ").append(dto.getItemDescription()).append("]")
									.append("\r[OrganizationCode: ").append(dto.getInventoryOrganizationCode()).append("]")
									.append("\r[OrganizationName: ").append(dto.getInventoryOrganizationName()).append("]")
									.append("\r[Location: ").append(dto.getInventoryLocation()).append("]")
									.append("\r[LotNumber: ").append(dto.getLotNumber()).append("]")
									.append("\r[ItemType: ").append(dto.getItemType()).append("]")
									.append("\r[Quantity: ").append(dto.getQuantity()).append("]")
									.append("\r[TransactionId: ").append(dto.getTransactionId()).append("]")
									.append("\r[TransactionType: ").append(dto.getTransactionType()).append("]")
									.append("\r[TransactionDate: ").append(dto.getTransactionDate()).append("]")
									.append("\r[CreationDate: ").append(dto.getCreationDate()).append("]")
									.append("\r[ExpirationDate: ").append(dto.getExpirationDate()).append("]")
									.append("\r[Uom: ").append(dto.getUom()).append("]")
									.append("\r\r")
									.toString()
									);
						}
							
						String res = inventoryTransactionService.newInventoryTransactionsRecord(items, props, suc, initDateString);
						showMessage(res);
					}
				}else {
					showMessage("El proceso ha concluido. No se detectaron transacciones con el c�digo " + suc);
				}					
			}
		}catch(Exception e) {
			e.printStackTrace();
			showMessage("Ocurri� un error al registrar los movimientos de inventario: " + e.getMessage());
		}		
	}
	
	private void showMessage(String message) {
        try {
    		JTextPane jtp = new JTextPane();
    	    Document doc = jtp.getDocument();
    	    SimpleAttributeSet attrs = new SimpleAttributeSet();
            StyleConstants.setUnderline(attrs, true);
            StyleConstants.setForeground(attrs, Color.BLUE);
			doc.insertString(doc.getLength(), message, attrs);
			
			jtp.setSize(new Dimension(480, 10));
		    jtp.setPreferredSize(new Dimension(480, jtp.getPreferredSize().height));
		    //JOptionPane.showMessageDialog(null, jtp, "Title", JOptionPane.INFORMATION_MESSAGE);
		    String log = ta.getText();
			ta.setText(log + "\n" + message);
			System.out.println(message);
		} catch (Exception e) {
			e.printStackTrace();
			ta.setText(e.getMessage() + "\n" + message);
		}
	}
	
	private  void saveBranch() {
		String value = JOptionPane.showInputDialog("Sucursal:");
		if(value != null) {
			if(!"".equals(value)) {
				Sucursal s = new Sucursal();
				s.setId(0);
				s.setSucursal(value);
				clientLocalService.saveBranch(s);
				JOptionPane.showMessageDialog(null, "La nueva sucursal ha sido registrada.");
			}else {
				JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
			}
		}else {}
	}

}


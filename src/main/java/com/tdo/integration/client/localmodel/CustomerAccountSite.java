package com.tdo.integration.client.localmodel;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CLIENTSITEACCOUNT")
public class CustomerAccountSite {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)	
	private int id;
	private Integer customerId;
	private BigInteger customerCloudId;
	private String siteNumber;
	private String accountNumber;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}		
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public BigInteger getCustomerCloudId() {
		return customerCloudId;
	}
	public void setCustomerCloudId(BigInteger customerCloudId) {
		this.customerCloudId = customerCloudId;
	}
	public String getSiteNumber() {
		return siteNumber;
	}
	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}

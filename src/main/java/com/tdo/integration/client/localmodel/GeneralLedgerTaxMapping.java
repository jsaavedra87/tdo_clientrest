package com.tdo.integration.client.localmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GENERALLEDGERTAXMAPPING")
public class GeneralLedgerTaxMapping {

	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String taxCodeSICAR;
	private String lineTypeRILA;
	private String taxCodeOracle;
	private String regime;
	private String taxCodeAlt;
	private String taxCodeStatus;
	private String taxCodeRate;
	private String taxCodeJurisdiction;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTaxCodeSICAR() {
		return taxCodeSICAR;
	}

	public void setTaxCodeSICAR(String taxCodeSICAR) {
		this.taxCodeSICAR = taxCodeSICAR;
	}

	public String getLineTypeRILA() {
		return lineTypeRILA;
	}

	public void setLineTypeRILA(String lineTypeRILA) {
		this.lineTypeRILA = lineTypeRILA;
	}

	public String getTaxCodeOracle() {
		return taxCodeOracle;
	}

	public void setTaxCodeOracle(String taxCodeOracle) {
		this.taxCodeOracle = taxCodeOracle;
	}

	public String getRegime() {
		return regime;
	}

	public void setRegime(String regime) {
		this.regime = regime;
	}

	public String getTaxCodeAlt() {
		return taxCodeAlt;
	}

	public void setTaxCodeAlt(String taxCodeAlt) {
		this.taxCodeAlt = taxCodeAlt;
	}

	public String getTaxCodeStatus() {
		return taxCodeStatus;
	}

	public void setTaxCodeStatus(String taxCodeStatus) {
		this.taxCodeStatus = taxCodeStatus;
	}

	public String getTaxCodeRate() {
		return taxCodeRate;
	}

	public void setTaxCodeRate(String taxCodeRate) {
		this.taxCodeRate = taxCodeRate;
	}

	public String getTaxCodeJurisdiction() {
		return taxCodeJurisdiction;
	}

	public void setTaxCodeJurisdiction(String taxCodeJurisdiction) {
		this.taxCodeJurisdiction = taxCodeJurisdiction;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
   

}

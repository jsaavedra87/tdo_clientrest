package com.tdo.integration.client.localmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BUSINESSUNITMAPPING")
public class BusinessUnitMapping {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String cuentaEntidadSICAR;
	private String nombreEntidadSICAR;
	private String businessUnit;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getCuentaEntidadSICAR() {
		return cuentaEntidadSICAR;
	}
	public void setCuentaEntidadSICAR(String cuentaEntidadSICAR) {
		this.cuentaEntidadSICAR = cuentaEntidadSICAR;
	}
	public String getNombreEntidadSICAR() {
		return nombreEntidadSICAR;
	}
	public void setNombreEntidadSICAR(String nombreEntidadSICAR) {
		this.nombreEntidadSICAR = nombreEntidadSICAR;
	}
	public String getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

}

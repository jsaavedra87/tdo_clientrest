package com.tdo.integration.client;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class RestConnection {
	
	private static URL url;
	@SuppressWarnings("unused")
	private static HttpURLConnection conn;
	private static Map<String, HttpURLConnection> restMap = new HashMap<String, HttpURLConnection>();
	
	public static void initConnection(Properties props){
		try {
	  		url = new URL(props.getProperty("restInventoryService"));
	  		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json");
	  		restMap.put("restInventoryService", conn);
	  		
	  		url = new URL(props.getProperty("restARService"));
	  		conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json");
	  		restMap.put("restARService", conn);
	  		
	  		url = new URL(props.getProperty("restGLService"));
	  		conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json");
	  		conn.setAllowUserInteraction(true);
	  		restMap.put("restGLService", conn);
	  		
	  		url = new URL(props.getProperty("restCustomerService"));
	  		conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json");
	  		restMap.put("restCustomerService", conn);
	  		
	  	 } catch (Exception e){
	  		 e.printStackTrace();
	  	 }
	}
	
	public static Map<String, HttpURLConnection> getConnection(){
		return restMap;
	}
	

}

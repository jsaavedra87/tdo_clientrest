package com.tdo.integration.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.dto.integration.dto.InventoryTransactionDTO;

import com.tdo.integration.client.model.Articulo;

@Repository("inventorySicarDao")
@Transactional 
public class InventorySicarDao {
	
	DataSource dataSource;
	Connection conn = null;
	
private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}


	@SuppressWarnings("unchecked")
	public void registerAverageCost(InventoryTransactionDTO o) {

			Session session = sessionFactory.getCurrentSession();
			String sql = "SELECT e.art_id AS articuloId, " +
					"  e.clave AS articuloClave, " +
					"  e.precio1 AS articuloPrecio1, " +
					"  e.precio2 AS articuloPrecio2, " +
					"  e.precio3 AS articuloPrecio3, " +
					"  e.margen1 AS ventaDetallePrecioSinIva, " +
					"  e.margen2 AS ventaDetallePrecioConIva, " +
					"  e.margen3 AS ventaDetalleImporteSinIva, " +
					"  e.margen4 AS ventaDetalleImporteConIva, " +
					"  e.precioCompra AS precioCompra " +
					" FROM articulo e" +					
					" WHERE e.art_id = :art_id";
			
			SQLQuery query = session.createSQLQuery(sql);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
			query.setParameter("art_id", o.getItemId());
			List<Articulo> articulos = query.list();
			
			for(Articulo a : articulos) {				
				o.setItemId(a.getArticuloId());					
				double itemCost = o.getAverageCost();
				
				if(itemCost > 0) {	
					double um1 = a.getVentaDetallePrecioSinIva().doubleValue();
					double um2 = a.getVentaDetallePrecioConIva().doubleValue();
					double um3 = a.getVentaDetalleImporteSinIva().doubleValue();
					double um4 = a.getVentaDetalleImporteConIva().doubleValue();
					
					double pv1 =0;
					double pv2 =0;
					double pv3 =0;
					double pv4 =0;
					
					if(um1 > 0) {
						pv1 = itemCost + (itemCost * (um1/100));
					}
						
					if(um2 > 0) {
						pv2 = itemCost + (itemCost * (um2/100));
					}
						
					if(um3 > 0) {
						pv3 = itemCost + (itemCost * (um3/100));
					}
					
					if(um4 > 0) {
						pv4 = itemCost + (itemCost * (um4/100));
					}
					
					pv1 =  Math.round(1000 * pv1) / 1000d;
					pv2 =  Math.round(1000 * pv2) / 1000d;
					pv3 =  Math.round(1000 * pv3) / 1000d;
					pv4 =  Math.round(1000 * pv4) / 1000d;
					
					sql = "UPDATE articulo set precioCompra = :precioCompra, preCompraProm = :preCompraProm, " + 
					      "precio1 = :precio1, precio2 = :precio2, precio3 = :precio3, precio4 = :precio4, lote = :lote " +
						  "WHERE art_id = :art_id";
					query = session.createSQLQuery(sql);
					query.setParameter("precioCompra", itemCost);
					query.setParameter("preCompraProm", itemCost);
					query.setParameter("precio1", pv1);
					query.setParameter("precio2", pv2);
					query.setParameter("precio3", pv3);
					query.setParameter("precio4", pv4);
					query.setParameter("lote", 1);
					query.setParameter("art_id", a.getArticuloId());
					query.executeUpdate();	
				}
			}
	}
	
	@SuppressWarnings("unchecked")
	public int getItemId(String item) {
		    int itemId = 0;
			Session session = sessionFactory.getCurrentSession();
			String sql = "SELECT e.art_id AS articuloId, " +
					"  e.clave AS articuloClave, " +
					"  e.precio1 AS articuloPrecio1, " +
					"  e.precio2 AS articuloPrecio2, " +
					"  e.precio3 AS articuloPrecio3, " +
					"  e.margen1 AS ventaDetallePrecioSinIva, " +
					"  e.margen2 AS ventaDetallePrecioConIva, " +
					"  e.margen3 AS ventaDetalleImporteSinIva " +
					" FROM articulo e" +					
					" WHERE e.clave = :item";
			
			SQLQuery query = session.createSQLQuery(sql);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
			query.setParameter("item", item);
			List<Articulo> articulos = query.list();
			if(articulos != null) {
				if(articulos.size() > 0) {
					itemId = articulos.get(0).getArticuloId().intValue();
				}
			}
			
		return itemId;
	}
	
	@SuppressWarnings("unchecked")
	public Articulo getArticulo(String item) {
			Session session = sessionFactory.getCurrentSession();
			String sql = "SELECT e.art_id AS articuloId, " +
					"  e.clave AS articuloClave, " +
					"  e.descripcion AS articuloDescripcion, " +
					"  e.precio1 AS articuloPrecio1, " +
					"  e.precio2 AS articuloPrecio2, " +
					"  e.precio3 AS articuloPrecio3, " +
					"  e.margen1 AS ventaDetallePrecioSinIva, " +
					"  e.margen2 AS ventaDetallePrecioConIva, " +
					"  e.margen3 AS ventaDetalleImporteSinIva, " +
					"  e.precioCompra AS precioCompra " +
					" FROM articulo e" +					
					" WHERE e.clave = :item";
			
			SQLQuery query = session.createSQLQuery(sql);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
			query.setParameter("item", item);
			List<Articulo> articulos = query.list();
			if(articulos != null) {
				if(articulos.size() > 0) {
					return articulos.get(0);
				}
			}
			
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public double getItemQuatity(int itemId) {
			double quantity = 0;
			Session session = sessionFactory.getCurrentSession();
			String sql = "SELECT e.art_id AS articuloId, " +
					"  e.existencia AS articuloPrecio1 " +
					" FROM articulo e" +					
					" WHERE e.art_id = :art_id";
			
			SQLQuery query = session.createSQLQuery(sql);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
			query.setParameter("art_id", itemId);
			List<Articulo> articulos = query.list();
			if(articulos != null) {
				if(articulos.size() > 0) {
					for(Articulo o : articulos) {
						quantity = quantity + o.getArticuloPrecio1().doubleValue();
					}
				}
			}
			
		return quantity;
		
	}
	
	@Transactional
	public void insertLot(InventoryTransactionDTO o) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Session session = sessionFactory.getCurrentSession();
		
		String sql = "INSERT INTO lote (numLote, fechaFab, fechaCad, exisInicial, exisActual, status, art_id) " +
			         "VALUES(:numLote, :fechaFab, :fechaCad, :exisInicial, :exisActual, :status, :artId)";
		
		Query qry = session.createSQLQuery(sql);
		int res = 0;
		int recordsInserted = 0;
			if(o.getItemId() != 0) {
				
				String fechaFab = "";
				String fechaCad = "";
				if(o.getCreationDate() != null) {
					fechaFab = sdf.format(o.getCreationDate().toGregorianCalendar().getTime());
				}else {
					GregorianCalendar gcal = (GregorianCalendar) GregorianCalendar.getInstance();
				    fechaFab = sdf.format(gcal.getTime());
				}
				
				if(o.getExpirationDate() != null) {
					fechaCad = sdf.format(o.getExpirationDate().toGregorianCalendar().getTime());
				}else {
					GregorianCalendar gcal = (GregorianCalendar) GregorianCalendar.getInstance();
					gcal.add((GregorianCalendar.YEAR), 20);
					fechaCad = sdf.format(gcal.getTime());
				}
				
				
				qry.setParameter("numLote", o.getLotNumber());
				qry.setParameter("fechaFab", fechaFab);
				qry.setParameter("fechaCad", fechaCad);
				qry.setParameter("exisInicial",o.getQuantity());
				qry.setParameter("exisActual", o.getQuantity());
				qry.setParameter("status", '1');
				qry.setParameter("artId", o.getItemId());
				res = qry.executeUpdate();
				recordsInserted = recordsInserted + res;
				
				double qty = getItemQuatity(o.getItemId());
				qty = qty + o.getQuantity();
				sql = "UPDATE articulo SET existencia = :existencia WHERE art_id = :art_id";
				qry = session.createSQLQuery(sql);
				qry.setParameter("existencia", qty);
				qry.setParameter("art_id", o.getItemId());
				qry.executeUpdate();
		}
		System.out.println("Lotes registrados: " + recordsInserted);
	}
	
	@Transactional
	public void updateLotQuantity(int itemId, int lotId, double newQty, double oldQty, double itemQty) {
		Session session = sessionFactory.getCurrentSession();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		
		String sql = "UPDATE lote SET exisActual = :exisActual, status = :status WHERE lot_id = :lot_id";
		Query qry = session.createSQLQuery(sql);
		qry.setParameter("exisActual", newQty);
		qry.setParameter("lot_id", lotId);
		qry.setParameter("status", 1);
		qry.executeUpdate();

		sql = "INSERT INTO ajusteinventario (fecha, comentario) " +
			  "VALUES(:fecha, :comentario)";
		qry = session.createSQLQuery(sql);
		qry.setParameter("fecha", dateFormat.format(date));
		qry.setParameter("comentario", "Ajuste interfaz CLOUD");
		qry.executeUpdate();

		BigInteger result = (BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult();
		
		sql = "INSERT INTO ajusteinventarioarticulo (ain_id, art_id, exisAnterior, exisActual) " +
			  "VALUES(:ain_id, :art_id, :exisAnterior, :exisActual)";
			qry = session.createSQLQuery(sql);
			qry.setParameter("ain_id", result.intValue());
			qry.setParameter("art_id", itemId);
			qry.setParameter("exisAnterior", Math.abs(oldQty));
			qry.setParameter("exisActual", newQty);
			qry.executeUpdate();
			
		sql = "INSERT INTO ajustelote (fecha, exisAnterior, exisActual, lot_id) " +
			  "VALUES(:fecha, :exisAnterior, :exisActual, :lot_id)";
			qry = session.createSQLQuery(sql);
			qry.setParameter("fecha", dateFormat.format(date));
			qry.setParameter("exisAnterior", Math.abs(oldQty));
			qry.setParameter("exisActual", newQty);
			qry.setParameter("lot_id", lotId);
			qry.executeUpdate();

			double qty = getItemQuatity(itemId);
			qty = qty + itemQty;
			sql = "UPDATE articulo SET existencia = :existencia WHERE art_id = :art_id";
			qry = session.createSQLQuery(sql);
			qry.setParameter("existencia", qty);
			qry.setParameter("art_id", itemId);
			qry.executeUpdate();
	}
	
	
	
	@SuppressWarnings("unchecked")
	public int getLotId(String lotNumber, int itemId) {
		
		int lotId = 0;
		Session session = sessionFactory.getCurrentSession();
		String sql = "SELECT lot_id from lote WHERE numLote = :numLote AND art_id = :art_id ORDER BY lot_id ASC";
		SQLQuery query = session.createSQLQuery(sql);
		query.setParameter("numLote", lotNumber);
		query.setParameter("art_id", itemId);
		List<BigInteger> lotes = query.list();
		if(lotes != null) {
			if(lotes.size() > 0) {
				lotId = lotes.get(0).intValue();
			}
		}
		return lotId;
	}
	
	@SuppressWarnings("unchecked")
	public int getLotIdByStatus(String lotNumber, int itemId, int status) {
		
		int lotId = 0;
		Session session = sessionFactory.getCurrentSession();
		String sql = "SELECT lot_id from lote WHERE numLote = :numLote AND art_id = :art_id AND status = :status ORDER BY lot_id ASC";
		SQLQuery query = session.createSQLQuery(sql);
		query.setParameter("numLote", lotNumber);
		query.setParameter("art_id", itemId);
		query.setParameter("status", status);
		List<BigInteger> lotes = query.list();
		if(lotes != null) {
			if(lotes.size() > 0) {
				lotId = lotes.get(0).intValue();
			}
		}
		return lotId;
	}	

	@SuppressWarnings("unchecked")
	public List<BigInteger> getListLotId(String lotNumber, int itemId, int status) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "SELECT lot_id from lote WHERE numLote = :numLote AND art_id = :art_id AND status = :status ORDER BY lot_id ASC";
		SQLQuery query = session.createSQLQuery(sql);
		query.setParameter("numLote", lotNumber);
		query.setParameter("art_id", itemId);
		query.setParameter("status", status);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public double getLotQty(int lotNumber) {

		double quantity = 0;
		Session session = sessionFactory.getCurrentSession();
		String sql = "SELECT exisActual from lote WHERE lot_id = :lot_id";
		SQLQuery query = session.createSQLQuery(sql);
		query = session.createSQLQuery(sql);
		query.setParameter("lot_id", lotNumber);
		List<BigDecimal> qList = query.list();

		if(qList != null) {
			if(qList.size() > 0) {
				quantity = qList.get(0).doubleValue();
			}
		}
		return quantity;
	}
	
	public void setDataSource(DataSource dataSource) {
		try {
			this.dataSource = dataSource;
			this.conn = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Actualiza el estatus de 'actualizado' para que la nube sicar replique el cambio
	 * 
	 * */
	public void updateSicarNubeAct(int itemId) {
		Session session = sessionFactory.getCurrentSession();
		 
		String sql = "SELECT IFNULL(MAX(act_id), 0) FROM nubeact WHERE entidad = 'Inventario' AND id = :art_id";
		Query qry = session.createSQLQuery(sql);
		qry.setParameter("art_id", itemId);
		BigInteger lastInvId = (BigInteger) qry.uniqueResult();
		
		sql = "SELECT IFNULL(MAX(act_id), 0) FROM nubeact WHERE entidad = 'Articulo' AND id = :art_id";
		qry = session.createSQLQuery(sql);
		qry.setParameter("art_id", itemId);
		BigInteger lastArtId = (BigInteger) qry.uniqueResult();			
		
		sql = "UPDATE nubeact SET actualizado = 0 WHERE act_id IN (:act_id_inv, :act_id_art)";
		qry = session.createSQLQuery(sql);
		qry.setParameter("act_id_inv", lastInvId);
		qry.setParameter("act_id_art", lastArtId);
		qry.executeUpdate();
	}
	
	/*
	 * Actualiza el precio de compra de los articulos
	 * 
	 * */
	public void updateSicarPurchaseCostItem() {
		Session session = sessionFactory.getCurrentSession();
		
		String sql = "UPDATE  articulo SET precompraprom = preciocompra WHERE preciocompra <> precompraprom";
		Query qry = session.createSQLQuery(sql);
		qry.executeUpdate();			
	}
	
	/*
	 * Actualiza el precio de compra de los articulos
	 * 
	 * */
	public void updateSicarDesglosaIEPS() {
		Session session = sessionFactory.getCurrentSession();
		
		String sql = "UPDATE cliente SET desglosarIEPS = 1 WHERE desglosarIEPS = 0;";
		Query qry = session.createSQLQuery(sql);
		qry.executeUpdate();			
	}
	
	public void updateSicarItemLot() {
		Session session = sessionFactory.getCurrentSession();

		String sql = "UPDATE articulo SET lote=1 where lote=0 AND tipo<>2 AND status = 1 AND servicio = 0";
		Query qry = session.createSQLQuery(sql);
		qry.executeUpdate();			
	}	
	
		
	
/*
 * Updates UUID in sicar.notascredito when credit note is processed by the PAC
 */
	public void updateUUIDIntoSicarNotaCredito(String uuid, String folioNC) {
		Session session = this.sessionFactory.getCurrentSession();
		String sql = "UPDATE NOTACREDITO SET uuid=:uuid where folioNC=:folioNC";
		Query query = session.createSQLQuery(sql);
		query.setParameter("uuid", uuid);
		query.setParameter("folioNC", folioNC);
		query.executeUpdate();
	}

}

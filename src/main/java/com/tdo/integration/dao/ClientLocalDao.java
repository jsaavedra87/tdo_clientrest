package com.tdo.integration.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.tdo.integration.client.localmodel.BusinessUnitMapping;
import com.tdo.integration.client.localmodel.CustomerAccountSite;
import com.tdo.integration.client.localmodel.FileTransferControl;
import com.tdo.integration.client.localmodel.GeneralLedgerMapping;
import com.tdo.integration.client.localmodel.GeneralLedgerTaxMapping;
import com.tdo.integration.client.localmodel.InventoryTransferControl;
import com.tdo.integration.client.localmodel.Sucursal;
import com.tdo.integration.client.localmodel.Udc;

@Repository("clientLocalDao")
@Transactional 
public class ClientLocalDao {
	
	DataSource dataSource;
	Connection conn = null;
    private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public void setDataSource(DataSource dataSource) {
		try {
			this.dataSource = dataSource;
			this.conn = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public CustomerAccountSite getCustomerAccountSite(int customerId){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CustomerAccountSite.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("customerId", customerId))
				);
		criteria.addOrder(Order.asc("id"));
		return (CustomerAccountSite) criteria.uniqueResult();
	}	
	
	@SuppressWarnings("unchecked")
	public List<GeneralLedgerMapping> getSalesAccounts(String sucursal){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GeneralLedgerMapping.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branch", sucursal))
				.add(Restrictions.ne("accountType", "INVENTARIO"))
				);
		criteria.addOrder( Order.asc("sequence"));
		return (List<GeneralLedgerMapping>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralLedgerMapping> getGlSalesAccounts(String sucursal, String glType){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GeneralLedgerMapping.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branch", sucursal))
				.add(Restrictions.eq("interfaceType", glType))
				.add(Restrictions.ne("accountType", "INVENTARIO"))
				);
		criteria.addOrder( Order.asc("sequence"));
		return (List<GeneralLedgerMapping>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralLedgerMapping> getInventoryAccounts(String sucursal){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GeneralLedgerMapping.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branch", sucursal))
				.add(Restrictions.eq("accountType", "INVENTARIO"))
				);
		criteria.addOrder( Order.asc("sequence"));
		return (List<GeneralLedgerMapping>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<GeneralLedgerMapping> getInventoryAccountsDev(String sucursal){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GeneralLedgerMapping.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branch", sucursal))
				.add(Restrictions.eq("accountType", "INVENTARIODEV"))
				);
		criteria.addOrder( Order.asc("sequence"));
		return (List<GeneralLedgerMapping>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralLedgerMapping> getTaxSalesAccounts(String sucursal, String glType){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GeneralLedgerMapping.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branch", sucursal))
				.add(Restrictions.eq("interfaceType", glType))
				.add(Restrictions.ne("taxCodeReference", ""))
				);
		criteria.addOrder( Order.asc("sequence"));
		return (List<GeneralLedgerMapping>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<GeneralLedgerTaxMapping> getTaxMapping(){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GeneralLedgerTaxMapping.class);
		return (List<GeneralLedgerTaxMapping>) criteria.list();		
	}
	
	@SuppressWarnings("unchecked")
	public List<BusinessUnitMapping> getBusinessUnitMapping(){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(BusinessUnitMapping.class);
		return (List<BusinessUnitMapping>) criteria.list();
	}

	public void saveBranch(Sucursal s) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(s);
	}
	
	@SuppressWarnings("unchecked")
	public Sucursal getBranch(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Sucursal s = null;
		Criteria cr = session.createCriteria(Sucursal.class);
		List<Sucursal> results = cr.list();
		if(results != null) {
			if(!results.isEmpty())
				s = results.get(0);
		}
		return s;
	}
	
	@SuppressWarnings("unchecked")
	public Udc getUdcByUdcKey(String udcKey) throws Exception{
		
		try {
			
		Session session = this.sessionFactory.getCurrentSession();
		Udc udc = null;
		Criteria cr = session.createCriteria(Udc.class);
		cr.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("udckey", udcKey)));
	
		List<Udc> results = cr.list();
		if(results != null) {
			if(!results.isEmpty())
				udc = results.get(0);
		}
		return udc;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Udc searchBySystemAndKey(String udcSystem, String udcKey){
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Udc.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("udcSystem", udcSystem.trim()))
				.add(Restrictions.like("udckey", udcKey.trim()))
				);
	   List<Udc> list =  criteria.list();
	   if(list != null){
		   if(!list.isEmpty())
			   return list.get(0);
		   else 
			   return null;
	   }else{
		   return null;
	   }
	   
	}
	
	public void saveOrUpdateUdc(Udc udc) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(udc);
	}
	
	@SuppressWarnings("unchecked")
	public Sucursal getBranch() {
		Session session = this.sessionFactory.getCurrentSession();
		Sucursal s = null;
		Criteria cr = session.createCriteria(Sucursal.class);
		List<Sucursal> results = cr.list();
		if(results != null) {
			if(!results.isEmpty())
				s = results.get(0);
		}
		return s;
	}
	
	@SuppressWarnings("unchecked")
	public List<InventoryTransferControl> getInventoryTransferControlByDate(Date transactionDate){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(InventoryTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.ge("lastUpdate", transactionDate))
				);
		criteria.addOrder(Order.desc("lastUpdate"));
		return (List<InventoryTransferControl>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public void saveOrUpdateInventoryTransferControl(InventoryTransferControl itc) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(InventoryTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("transactionId", itc.getTransactionId()))
				);
		List<InventoryTransferControl> lstITC = (List<InventoryTransferControl>) criteria.list();
		
		if(lstITC != null && lstITC.size() > 0) {
			InventoryTransferControl o = lstITC.get(0);
			o.setAverageCost(itc.getAverageCost());
			o.setLastUpdate(itc.getLastUpdate());
			o.setStatus(itc.getStatus());	
			session.update(o);
		} else {
			session.save(itc);
		}
	}

	@SuppressWarnings("unchecked")
	public void updateInventoryTransferControl(InventoryTransferControl itc) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(InventoryTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("transactionId", itc.getTransactionId()))
				);
		List<InventoryTransferControl> lstITC = (List<InventoryTransferControl>) criteria.list();
		
		if(lstITC != null && lstITC.size() > 0) {
			InventoryTransferControl o = lstITC.get(0);
			o.setAverageCost(itc.getAverageCost());
			o.setLastUpdate(itc.getLastUpdate());
			o.setStatus(itc.getStatus());	
			session.update(o);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<FileTransferControl> searchFileTransferControlByIdCC(String transactionType, int  idCC){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("transactionType", transactionType))
				.add(Restrictions.eq("idCC", idCC))
				);
		return (List<FileTransferControl>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<FileTransferControl> searchFileTransferControlByFromDate(String transactionType, Date fromDate){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("transactionType", transactionType))
				.add(Restrictions.eq("fromDate", fromDate))
				);
		return (List<FileTransferControl>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<FileTransferControl> searchFileTransferControlByDateRange(String transactionType, Date fromDate, Date toDate){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("transactionType", transactionType))
				.add(Restrictions.eq("fromDate", fromDate))
				.add(Restrictions.eq("toDate", toDate))
				);
		return (List<FileTransferControl>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<FileTransferControl> searchFileTransferControlByStatus(String transactionType, String[] status){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("transactionType", transactionType))
				.add(Restrictions.in("status", status))
				);
		return (List<FileTransferControl>) criteria.list();
	}
		
	@SuppressWarnings("unchecked")
	public List<FileTransferControl> searchFileTransferControlByDateRangeAndStatus(String transactionType, Date fromDate, Date toDate, String[] status){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("transactionType", transactionType))
				.add(Restrictions.between("fromDate", fromDate, toDate))
				.add(Restrictions.in("status", status))
				);
		criteria.addOrder(Order.desc("toDate"));
		return (List<FileTransferControl>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public void saveOrUpdateFileTransferControl(FileTransferControl ftc) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branchId", ftc.getBranchId()))
				.add(Restrictions.eq("idCC", ftc.getIdCC()))
				.add(Restrictions.eq("fromDate", ftc.getFromDate()))
				.add(Restrictions.eq("transactionType", ftc.getTransactionType()))
				);
		criteria.addOrder(Order.desc("toDate"));
		List<FileTransferControl> lstFTC = (List<FileTransferControl>) criteria.list();
		if(lstFTC != null && lstFTC.size() > 0) {
			FileTransferControl o = lstFTC.get(0);
			o.setToDate(ftc.getToDate());
			o.setLastUpdate(ftc.getLastUpdate());
			o.setStatus(ftc.getStatus());
			o.setLog(ftc.getLog());
			session.update(o);
		} else {
			session.save(ftc);
		}
	}
	
	@SuppressWarnings("unchecked")
	public CustomerAccountSite getCustomerSiteAccounById(int customerId) {
		CustomerAccountSite csa = null;
		
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CustomerAccountSite.class);		
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("customerId", customerId))
				);
		criteria.addOrder(Order.desc("customerId"));
		List<CustomerAccountSite> lstCustomer = (List<CustomerAccountSite>) criteria.list();
		
		if(lstCustomer != null && lstCustomer.size() > 0) {
			csa = lstCustomer.get(0);
		}
		
		return csa;
	}
	
	@SuppressWarnings("unchecked")
	public void saveOrUpdateCustomerSiteAccoun(CustomerAccountSite csa) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CustomerAccountSite.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("customerId", csa.getCustomerId()))
				);
		criteria.addOrder(Order.desc("customerId"));
		List<CustomerAccountSite> lstCustomer = (List<CustomerAccountSite>) criteria.list();
		
		if(lstCustomer != null && lstCustomer.size() > 0) {
			CustomerAccountSite o = lstCustomer.get(0);
			o.setAccountNumber(csa.getAccountNumber());
			o.setSiteNumber(csa.getSiteNumber());			
			session.update(o);
		} else {
			session.save(csa);
		}
	}	
}

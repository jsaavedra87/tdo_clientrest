package com.tdo.integration.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.dto.integration.dto.ClientesJson;
import com.dto.integration.dto.CreditDTO;
import com.dto.integration.dto.CustomerDTO;
import com.dto.integration.dto.PaymentDTO;
import com.dto.integration.dto.SalesDTO;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.client.model.Abono;
import com.tdo.integration.client.model.Articulo;
import com.tdo.integration.client.model.ArticuloImpuesto;
import com.tdo.integration.client.model.CancelacionVenta;
import com.tdo.integration.client.model.Cliente;
import com.tdo.integration.client.model.Credito;
import com.tdo.integration.client.model.DetalleLote;
import com.tdo.integration.client.model.Empresa;
import com.tdo.integration.client.model.Factura;
import com.tdo.integration.client.model.FacturaImpuestos;
import com.tdo.integration.client.model.MovimientosCaja;
import com.tdo.integration.client.model.NotaCredito;
import com.tdo.integration.client.model.NotaCreditoAbono;
import com.tdo.integration.client.model.NotaCreditoImpuestos;
import com.tdo.integration.client.model.Sales;
import com.tdo.integration.client.model.TipoPago;

@Repository("restClientDao")
@Transactional 
public class RestClientDao {
	DataSource dataSource;
	Connection conn = null;
	private final String CC_SALIDA_TEXTO = "SALIDA POR CORTE DE CAJA";	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@SuppressWarnings({ "unchecked" })
	public SalesDTO getSales(String sucursal, String requestType, String[] params){
		
		SalesDTO salesDTO = new SalesDTO();
		Session session = sessionFactory.getCurrentSession();
		List<BigInteger> rowInitList = null;
		List<Sales> rows = null;
		Empresa emp = null;
		SQLQuery query = null;
		String sqlInit = "";
		int idCorteCaja = 0;
		String fromDate = "";
		String toDate = "";

		if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			idCorteCaja = Integer.valueOf(params[0]);
			
		} else if(AppConstants.REQUEST_TYPE_TIME.equals(requestType)) {
			fromDate = params[0];
			toDate = params[1];
		}
		
		if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			sqlInit = "SELECT distinct ven_id " + 
					"from venta a " + 
					"join resumencortecaja b " + 
					"on a.rcc_id = b.rcc_id " + 
					"join cortecaja c " + 
					"on b.cor_id = c.cor_id and a.caj_id = c.caj_id " + 
					"where b.rcc_id = " + idCorteCaja + " and a.total > 0 ";
			
		} else {
			sqlInit = " select distinct a.ven_id " +
					"	from historial h " +
					"   join venta a on h.id = a.ven_id " +
					"	where h.tabla = 'Venta'  " +
					"	and h.movimiento = 0 " +
					"   and h.fecha between '" + fromDate + "' and '" + toDate + "'" +
					"	and a.total > 0; ";
		}
		
		query = session.createSQLQuery(sqlInit);
		rowInitList = query.list();
		
		if(rowInitList != null && !rowInitList.isEmpty()) {
			List<Integer> ids = new ArrayList<Integer>();
			for(BigInteger s : rowInitList) {
				ids.add(s.intValue());
			}
			
		String sql = "SELECT a.emp_id AS empId, " + 
					   "a.nombre  AS nombre, " + 
				       "a.nombreFiscal  AS nombreFiscal, " + 
				       "a.cuenta  AS cuenta, " +
				       "a.rfc  AS rfc " +
				       "FROM empresa a " ;
			
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Empresa.class));
			List<Empresa> empresa = query.list();
			
			if(empresa != null) {			
				emp = empresa.get(0);
				
				sql = "SELECT c.cli_id AS idCliente, " +
						" IFNULL(e.nub_id, 0) AS clienteNubeId," +
						" replace(c.nombre, ',', '_') AS nombreCliente, " +
						" c.curp AS curp, " +
						" c.diasCredito AS diasCredito, " + 
						" c.clave AS claveCliente, " +
						" c.precio AS precioCliente, " +
						" v.nombre AS nombreVendedor, " +
						" b.tic_id AS idTicket, " +
						" n.not_id AS idNotaVenta, " +
						" a.ven_id AS idVenta,  " + 
						" a.fecha AS ventaFecha,  " + 
						" d.nombre AS ventaCaja, " +
						" a.subtotal AS ventaSubtotal,  " + 
						" a.descuento AS ventaDescuento,  " + 
						" a.total AS ventaTotal,  " +
						" a.cambio AS cambio, " +
						" a.status AS ventaStatus,  " +						
						" a.monAbr AS ventaMoneda,  " + 
						" a.monTipoCambio AS ventaTipoCambio, " +
						" a.monSubtotal AS monedaSubtotal,  " + 
						" a.monDescuento AS monedaDescuento,  " + 
						" a.monTotal AS monedaTotal,  " +
						" a.monCambio AS monedaCambio, " +
						" a.rcc_id AS idResumenCaja, " +
						" a.can_caj_id AS idCancelaCaja, " +
						" a.can_rcc_id AS idCancelaResumenCaja " +						
					    " FROM venta a  " +
					    " LEFT JOIN ticket b " +
					    " ON a.tic_id IS NOT NULL " +
					    " AND a.tic_id = b.tic_id " +
					    " LEFT JOIN nota n " +
					    " ON n.not_id IS NOT NULL " +
					    " AND a.not_id = n.not_id " +
					    " LEFT JOIN cliente c " +
					    " ON (b.cli_id = c.cli_id OR n.cli_id = c.cli_id) " +
					    " LEFT JOIN nubeid e " +
					    " ON c.cli_id = e.id AND e.entidad = 'Cliente' " +
				        " LEFT JOIN caja d " + 
				        " ON a.caj_id = d.caj_id " +				         
				        " LEFT JOIN vendedor v  " + 
				        " ON a.vnd_id = v.vnd_id " + 				         
						" WHERE a.ven_id in (:vList) " +				         
						" ORDER BY a.ven_id; ";

				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Sales.class));
				query.setParameterList("vList", ids);
				rows = query.list();
				
				sql = "SELECT " +
						" a.ven_id AS idVenta, " +						
						" c.cli_id AS clienteId, " +											
						" replace(c.nombre, ',', '_') AS clienteNombre,  " +
						" c.rfc AS clienteRFC, " +
						" c.curp AS clienteCURP, " +
						" c.domicilio AS clienteDomicilio, " +
						" c.colonia AS clienteColonia, " +
						" c.noExt AS clienteNoExt, " +
						" c.noInt AS clienteNoInt, " +
						" c.localidad AS clienteLocalidad, " +
						" c.ciudad AS clienteCiudad, " +
						" c.estado AS clienteEstado, " +
						" c.pais AS clientePais, " +
						" c.codigoPostal AS clienteCodigoPostal, " +
						" c.limite AS clienteLimite, " +
						" c.diasCredito AS clienteDiasCredito, " +
						" c.clave AS clienteClave " +
						" FROM venta a   " +
						" LEFT JOIN ticket b " +
						" ON a.tic_id IS NOT NULL " +
						" AND a.tic_id = b.tic_id " +
						" LEFT JOIN nota n " +
						" ON n.not_id IS NOT NULL " +
						" AND a.not_id = n.not_id " +
						" LEFT JOIN remision r " +
						" ON r.rem_id IS NOT NULL " +
						" AND a.rem_id = r.rem_id " +						
						" LEFT JOIN cliente c " +
						" ON (b.cli_id = c.cli_id  OR n.cli_id = c.cli_id OR r.cli_id = c.cli_id) " +
						" WHERE a.ven_id in (:vList) " +
						" ORDER BY a.ven_id; ";

				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Cliente.class));
				query.setParameterList("vList", ids);
				List<Cliente> cliList= query.list(); 
				
				sql = " SELECT a.ven_id AS idVenta, 					" +
						"  d.art_id AS articuloId, 						" +
						"  d.clave AS articuloClave, 					" +
						"  replace(d.descripcion, ',', '_') AS articuloDescripcion, " +
						"  g.nombre AS categoria, 						" +
						"  h.nombre AS departamento, 					" +
						"  e.precio1 AS articuloPrecio1, 				" +
						"  e.precio2 AS articuloPrecio2, 				" +
						"  e.precio3 AS articuloPrecio3, 				" +
						"  d.cantidad AS ventaDetalleCantidad, 			" +
						"  d.unidad AS ventaDetalleUnidad, 				" +
						"  d.preciosin AS ventaDetallePrecioSinIva, 	" +
						"  d.preciocon AS ventaDetallePrecioConIva, 	" +
						"  d.importesin AS ventaDetalleImporteSinIva, 	" +
						"  d.importecon AS ventaDetalleImporteConIva, 	" +
						"  CASE WHEN TRUNCATE(a.total, 0) = TRUNCATE((SELECT SUM(d.importeCon)				" +
						"											FROM detallev d							" +
						"											WHERE d.ven_id = a.ven_id				" +
						"											GROUP BY d.ven_id), 0) 					" +
						"  THEN																				" +
						"  		(d.importeCon)/ (1 + (IFNULL((SELECT MAX(i.impuesto)						" + 
						"									FROM detallevimpuesto i							" + 
						"                                   WHERE i.ven_id = a.ven_id						" + 
						"                                   AND i.art_id = e.art_id), 0)/100))				" +						
						"  ELSE																				" +
						"  		(d.importeNorCon-d.descTotal)/ (1 + (IFNULL((SELECT MAX(i.impuesto)			" + 
						"													FROM detallevimpuesto i			" + 
						"                                   				WHERE i.ven_id = a.ven_id		" + 
						"                                   				AND i.art_id = e.art_id), 0)/100))	" +
						"  END AS ventaDetalleImporteSinImp, 												" +
						"  CASE WHEN TRUNCATE(a.total, 0) = TRUNCATE((SELECT SUM(d.importeCon)				" +
						"											FROM detallev d							" +
						"											WHERE d.ven_id = a.ven_id				" +
						"											GROUP BY d.ven_id), 0) 					" +
						"  THEN																				" +
						"  		(d.importeCon)-((d.importeCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto)			" + 
						"													FROM detallevimpuesto i			" + 
						"                                                   WHERE i.ven_id = a.ven_id		" + 
						"                                                   AND i.art_id = e.art_id), 0)/100)))					" +
						"  ELSE																									" +
						"  		(d.importeNorCon-d.descTotal)-((d.importeNorCon-d.descTotal)/ (1 +(IFNULL((SELECT MAX(i.impuesto)	" + 
						"																FROM detallevimpuesto i					" + 
						"                                                                WHERE i.ven_id = a.ven_id				" + 
						"                                                                AND i.art_id = e.art_id), 0)/100)))	" +
						"  END AS ventaDetalleImpuesto,																			" +
						"  d.monpreciosin AS monedaDetallePrecioSinIva, 														" +
						"  d.monpreciocon AS monedaDetallePrecioConIva, 														" +
						"  d.monimportesin AS monedaDetalleImporteSinIva, 														" +
						"  d.monimportecon AS monedaDetalleImporteConIva, 														" +
						"  CASE WHEN TRUNCATE(IFNULL(a.monTotal,0), 0) = TRUNCATE((SELECT SUM(IFNULL(d.monImporteCon,0))		" +
						"															FROM detallev d								" +
						"															WHERE d.ven_id = a.ven_id					" +
						"															GROUP BY d.ven_id), 0) 						" +
						"  THEN																									" +
						"  (d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto)												" + 
						"									FROM detallevimpuesto i												" + 
						"                                   WHERE i.ven_id = a.ven_id 											" + 
						"                                   AND i.art_id = e.art_id), 0)/100))									" +
						"  ELSE																									" +
						"  (d.monImporteNorCon-(d.descTotal/a.monTipoCambio))/ (1 +(IFNULL((SELECT MAX(i.impuesto)				" + 
						"																	FROM detallevimpuesto i				" + 
						"                                   								WHERE i.ven_id = a.ven_id 			" + 
						"                                   								AND i.art_id = e.art_id), 0)/100))	" +
						"  END AS monedaDetalleImporteSinImp, 																	" +
						"  CASE WHEN TRUNCATE(IFNULL(a.monTotal,0), 0) = TRUNCATE((SELECT SUM(IFNULL(d.monImporteCon,0))		" +
						"															FROM detallev d								" +
						"															WHERE d.ven_id = a.ven_id					" +
						"															GROUP BY d.ven_id), 0) 						" +
						"  THEN																									" +
						"  d.monImporteCon-((d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) 								" + 
						"													FROM detallevimpuesto i 							" + 
						"                                                   WHERE i.ven_id = a.ven_id 							" + 
						"                                                   AND i.art_id = e.art_id), 0)/100))) 				" +
						"  ELSE																									" +
						"  d.monImporteNorCon-(d.descTotal/a.monTipoCambio)-((d.monImporteNorCon-(d.descTotal/a.monTipoCambio)) " +
						"													/ (1 +(IFNULL((SELECT MAX(i.impuesto) 				" + 
						"																 FROM detallevimpuesto i 				" + 
						"                                                                WHERE i.ven_id = a.ven_id 				" + 
						"                                                                AND i.art_id = e.art_id), 0)/100))) 	" +
						"  END AS monedaDetalleImpuesto,																		" +
						"  d.ncr_id AS notaCreditoId, " +
						"  f.folioNC AS folioNC, " +
						"  f.folio AS folio, " +
						"  d.descTotal AS descuentoTotal, " +
						"  d.precioCompra AS precioCompra, " +
						"  d.importecompra AS importeCompra, " +
						"  e.lote AS lote, " +
						"  d.tipo AS tipo " +
						" FROM venta a  " +
						"  JOIN detallev d " +
						"  ON a.ven_id = d.ven_id" +
						"  LEFT JOIN articulo e" +
						"  ON d.art_id = e.art_id" +
						"  LEFT JOIN notacredito f" +
						"  ON d.ncr_id = f.ncr_id" +
						"  LEFT JOIN categoria g " +
						"  ON e.cat_id = g.cat_id " +
						"  LEFT JOIN departamento h " +
						"  ON g.dep_id = h.dep_id " +
						" WHERE a.ven_id in (:vList) " +
						" and d.ncr_id is null " +
						" ORDER BY a.ven_id, d.art_id;"; 
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
				query.setParameterList("vList", ids);
				List<Articulo> artList= query.list();
				
				//JAvila: Obtiene los articulos y lotes del kit
				sql = " 	SELECT a.ven_id AS idVenta, " + 
						"		  q.art_id AS articuloId,  " + 
						"		  q.clave AS articuloClave,  " + 
						"		  replace(q.descripcion, ',', '_') AS articuloDescripcion,  " + 
						"		  q.precio1 AS articuloPrecio1,  " + 
						"		  q.precio2 AS articuloPrecio2,  " + 
						"		  q.precio3 AS articuloPrecio3,  " + 
						"		  b.cantidad AS ventaDetalleCantidad, " + 
						"         q.lote AS lote, " +
						"		  b.unidad AS ventaDetalleUnidad, " + 
						"		  q.precioCompra AS precioCompra, " +
						"		  b.paquete AS paquete, " +
						"		  a.tipo AS tipo " +
						"	FROM detallev a" + 
						"	JOIN detallep b on a.art_id = b.paquete and a.ven_id=b.ven_id " + 
						"	JOIN articulo q on b.articulo = q.art_id " + 
						"	WHERE  a.ven_id in (:vList);"; 
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
				query.setParameterList("vList", ids);
 				List<Articulo> artKitList= query.list();
				
				sql = "SELECT a.ven_id        AS idVenta,  " + 
						"	   d.articulo        AS articuloId,   " + 
						"	   d.clave         AS articuloClave,    " + 
						"	   l.numlote       AS loteNumero,  " + 
						"	   l.fechafab      AS loteFechaFabricacion,   " + 
						"	   l.fechacad      AS loteFechaCaducidad,   " + 
						"	   l.exisinicial   AS loteExistenciaInicial,   " + 
						"	   l.exisactual    AS loteExistenciaActual,  " + 
						"	   l.status        AS loteStatus,	  " + 
						"	   d.paquete AS paquete, " +
						"	   k.cantidad      AS cantidad  " + 
						"	   FROM   venta a   " + 
						"	   JOIN detallep d  ON a.ven_id = d.ven_id " + 
						"	   LEFT JOIN detalleplote k  ON a.ven_id = k.ven_id " + 
						"			   AND d.articulo = k.articulo " + 
						"	   LEFT JOIN lote l ON k.lot_id = l.lot_id " + 
						"			  AND d.articulo = l.art_id  " + 
						"	   WHERE   l.numlote is not null and a.ven_id in (:vList) " + 
						"	   ORDER BY a.ven_id, d.articulo; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(DetalleLote.class));
				query.setParameterList("vList", ids);
				List<DetalleLote> lotListKit= query.list();
				//JAvila: Fin
				
				sql = "SELECT a.ven_id        AS idVenta, " + 
					       "d.art_id        AS articuloId, " + 
					       "d.clave         AS articuloClave, " +  
					       "h.nombre        AS ventaDetalleImpuestoNombre, " + 
					       "h.impuesto      AS ventaDetalleImpuestoValor, " + 
					       "h.total			AS ventaDetalleImpuestoTotal " +
					       "FROM   venta a " + 
					       "JOIN detallev d " + 
					         "ON a.ven_id = d.ven_id " + 
					       "LEFT JOIN articulo e " + 
					              "ON d.art_id = e.art_id " + 
					       "LEFT JOIN detallevimpuesto h " + 
					              "ON a.ven_id = h.ven_id " + 
								  "AND d.art_id = h.art_id " +					              
						   "WHERE  a.ven_id in (:vList) " + 
						   "AND h.total >= 0 " +
						   "ORDER BY a.ven_id, d.art_id;"; 
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(ArticuloImpuesto.class));
				query.setParameterList("vList", ids);
 				List<ArticuloImpuesto> artInvList= query.list();
				
				sql = "SELECT a.ven_id      AS idVenta, " + 
						   "j.nombre        AS tipoPagoNombre, " + 
					       "j.abr           AS tipoPagoClave, " + 
					       "i.total         AS ventaTipoPagoTotal " +
					       "FROM   venta a " + 
						   "LEFT JOIN ventatipopago i " + 
					              "ON a.ven_id = i.ven_id " + 
					       "LEFT JOIN tipopago j " + 
					              "ON i.tpa_id = j.tpa_id " + 
					       "WHERE  a.ven_id in (:vList) " +
					       "ORDER BY a.ven_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
				query.setParameterList("vList", ids);
				List<TipoPago> pagoList= query.list();
				
//				sql = " SELECT a.ven_id			AS idVenta, " +
//						" '" + AppConstants.TIPO_PAGO_MONEDERO_NOMBRE + "'  AS tipoPagoNombre, " +
//						" '" + AppConstants.TIPO_PAGO_MONEDERO_CLAVE + "'   AS tipoPagoClave,  " +
//						" ((SELECT SUM(x.importeNorCon) FROM detallev x WHERE x.ven_id = a.ven_id GROUP BY x.ven_id)  - d.importeCon - d.descTotal)	AS ventaTipoPagoTotal " +
//						" FROM venta a " +
//						" JOIN detallev d " +
//						" 	ON a.ven_id = d.ven_id " +
//						" WHERE  a.ven_id in (:vList) " +
//						" AND (d.importeNorCon - d.importeCon - d.descTotal) > 0 " +
//						" GROUP BY a.ven_id " +
//						" ORDER BY a.ven_id; ";
//				
//				query = session.createSQLQuery(sql);
//				query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
//				query.setParameterList("vList", ids);
//				List<TipoPago> pagoMonList= query.list();				
				
				sql = "SELECT a.ven_id        AS idVenta, " +
					       "d.art_id        AS articuloId, " + 
					       "d.clave         AS articuloClave, " +  
					       "l.numlote       AS loteNumero, " +
					       "l.fechafab      AS loteFechaFabricacion, " + 
					       "l.fechacad      AS loteFechaCaducidad,  " +
					       "l.exisinicial   AS loteExistenciaInicial, " + 
					       "l.exisactual    AS loteExistenciaActual, " +
					       "l.status        AS loteStatus,	 " +
					       "k.cantidad      AS cantidad " +
						   "FROM   venta a  " +
					       "JOIN detallev d  " +
					         "ON a.ven_id = d.ven_id  " +
						   "LEFT JOIN detallevlote k " + 
					              "ON a.ven_id = k.ven_id  " +
					               "AND d.art_id = k.art_id " + 
					       "LEFT JOIN lote l " + 
					              "ON k.lot_id = l.lot_id " + 
					              "AND d.art_id = l.art_id " + 
						   "WHERE   l.numlote is not null and a.ven_id in (:vList) " +
						   "ORDER BY a.ven_id, d.art_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(DetalleLote.class));
				query.setParameterList("vList", ids);
				List<DetalleLote> lotList= query.list();
							
				sql = "select a.ven_id as idVenta,c.fcf_id as idFactura, a.tic_id as idTicket, a.not_id as idNotaVenta, a.caj_id as idCaja, " + 
						"       c.fechaCert as fechaFactura, c.folio as folio, c.serieFolio as serieFolio, " + 
						"       c.nombreE as emisor, c.rfcE as rfcEmisor, c.nombreC as receptor, c.rfcC as rfcReceptor, " + 
						"       c.monAbr as moneda, c.monTipoCambio as tipoCambio, c.subtotal as subTotal, " + 
						"       c.descuento as descuento, c.total as total, c.formaPago as formaPago, c.metodoPago as metodoPago, " + 
						"       c.uuid as uuid, c.usoCfdi as usoCfdi " + 
						"       FROM venta a " + 
						"       JOIN facturacfdiven b on a.ven_id = b.ven_id " + 
						"       JOIN facturacfdi c on b.fcf_id = c.fcf_id " + 
						" WHERE  a.ven_id in (:vList) " +
						" ORDER BY a.ven_id, c.fcf_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Factura.class));
				query.setParameterList("vList", ids);
				List<Factura> factList= query.list();
				
				sql = "select a.ven_id as idVenta, c.fcf_id as idFactura, d.imp_id as idImpuesto, " + 
						"       d.total as totalImpuesto, d.subtotal as subtotal, d.tipoFactor as tipoFactor, " + 
						"       d.nombreImp as nombreImpuesto, d.valor as porcentajeImpuesto " + 
						"       FROM venta a " + 
						"       JOIN facturacfdiven b on a.ven_id = b.ven_id " + 
						"       JOIN facturacfdi c on b.fcf_id = c.fcf_id " + 
						"       JOIN facturacfdiimp d on c.fcf_id = d.fcf_id " + 
						" WHERE  a.ven_id in (:vList) " +
						" ORDER BY a.ven_id, c.fcf_id, d.imp_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(FacturaImpuestos.class));
				query.setParameterList("vList", ids);
				List<FacturaImpuestos> factImpList= query.list();
				
				List<BigInteger> ncList = new ArrayList<BigInteger>();
				
				//NOTA DE CREDITO POR VENTA en la misma transacci�n
				
				if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
					sql = "select c.rcc_id as idResumenCaja, a.ven_id as idVenta,c.fcf_id as idFactura, a.tic_id as idTicket, a.not_id as idNotaVenta, a.caj_id as idCaja, " + 
							"       c.ncr_id as idNotaCredito, e.nombre as descripcionCaja, v.nombre as nombreVendedor, c.fecha as fechaNC, c.folio as folio, c.serieFolio as serieFolio, " + 
							"       c.nombreE as emisor, c.rfcE as rfcEmisor, c.nombreC as receptor, c.rfcC as rfcReceptor, " + 
							"       c.folioNC as folioNC, c.monAbr as moneda, c.monTipoCambio as tipoCambio, c.subtotal as subTotal, " + 
							"       c.descuento as descuento, c.total as total, c.formaPago as formaPago, c.metodoPago as metodoPago, " +
							"		c.monSubtotal AS monedaSubtotal, c.monDescuento AS monedaDescuento, c.monTotal AS monedaTotal, c.monCambio AS monedaCambio, " +
							"       c.uuid as uuid, c.usoCfdi as usoCfdi, c.devAplicada as devolucionAplicada, f.ven_id as idVentaAplicada, c.status as status, l.precio as precioCliente " + 
							"       FROM venta a  " + 
							"       JOIN notacredito c on (a.tic_id = c.tic_id OR a.not_id = c.not_id) and c.rcc_id = " + idCorteCaja + 
							"       LEFT JOIN caja e on c.caj_id = e.caj_id " +
							"		LEFT JOIN detallev f on c.ncr_id = f.ncr_id " +
					        " 		LEFT JOIN vendedor v on c.vnd_id = v.vnd_id  " +
					        " 		LEFT JOIN cliente l on c.cli_id = l.cli_id " +
							" WHERE  a.ven_id in (:vList) " +
							" ORDER BY a.ven_id, c.ncr_id; ";
				} else {
					sql = "select c.rcc_id as idResumenCaja, a.ven_id as idVenta,c.fcf_id as idFactura, a.tic_id as idTicket, a.not_id as idNotaVenta, a.caj_id as idCaja, " + 
							"       c.ncr_id as idNotaCredito, e.nombre as descripcionCaja, v.nombre as nombreVendedor, c.fecha as fechaNC, c.folio as folio, c.serieFolio as serieFolio, " + 
							"       c.nombreE as emisor, c.rfcE as rfcEmisor, c.nombreC as receptor, c.rfcC as rfcReceptor, " + 
							"       c.folioNC as folioNC, c.monAbr as moneda, c.monTipoCambio as tipoCambio, c.subtotal as subTotal, " + 
							"       c.descuento as descuento, c.total as total, c.formaPago as formaPago, c.metodoPago as metodoPago, " +
							"		c.monSubtotal AS monedaSubtotal, c.monDescuento AS monedaDescuento, c.monTotal AS monedaTotal, c.monCambio AS monedaCambio, " +
							"       c.uuid as uuid, c.usoCfdi as usoCfdi, c.devAplicada as devolucionAplicada, f.ven_id as idVentaAplicada, c.status as status, l.precio as precioCliente " + 
							"       FROM venta a  " + 
							"       JOIN notacredito c on (a.tic_id = c.tic_id OR a.not_id = c.not_id) " +
							"		JOIN historial h on c.ncr_id = h.id " +
							"       LEFT JOIN caja e on c.caj_id = e.caj_id " +
							"		LEFT JOIN detallev f on c.ncr_id = f.ncr_id " +
							" 		LEFT JOIN vendedor v on c.vnd_id = v.vnd_id  " +
							" 		LEFT JOIN cliente l on c.cli_id = l.cli_id " +
							" WHERE a.ven_id in (:vList) " +
						 	"	AND h.tabla = 'NotaCredito' " +
							"	AND h.movimiento = 0 " +							
							" 	AND h.fecha BETWEEN '" + fromDate + "' AND '" + toDate + "' " +
							" ORDER BY a.ven_id, c.ncr_id; ";										
				}
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(NotaCredito.class));
				query.setParameterList("vList", ids);
				List<NotaCredito> ncTicketList= query.list();
				
				if(!ncTicketList.isEmpty()) {
					for(NotaCredito o : ncTicketList) {
						ncList.add(o.getIdNotaCredito());
					}
				}
				
				// NOTA DE CREDITO POR FACTURA en la misma transacci�n:
				sql = " select d.rcc_id as idResumenCaja, a.ven_id as idVenta, d.fcf_id as idFactura, a.tic_id as idTicket, a.not_id as idNotaVenta, a.caj_id as idCaja, " +
						"		d.ncr_id as idNotaCredito, e.nombre as descripcionCaja, v.nombre as nombreVendedor, d.fecha as fechaNC, c.fechaCert as fechaFactura, c.folio as folio, c.serieFolio as serieFolio, " +
						"		d.nombreE as emisor, d.rfcE as rfcEmisor, d.nombreC as receptor, d.rfcC as rfcReceptor, " +
						"		d.folioNC as folioNC, d.monAbr as moneda, d.monTipoCambio as tipoCambio, d.subtotal as subTotal, " +
						"		d.descuento as descuento, d.total as total, d.formaPago as formaPago, d.metodoPago as metodoPago, " +
						"		d.monSubtotal AS monedaSubtotal, d.monDescuento AS monedaDescuento, d.monTotal AS monedaTotal, d.monCambio AS monedaCambio, " +
						"		d.uuid as uuid, d.usoCfdi as usoCfdi, d.devAplicada as devolucionAplicada, f.ven_id as idVentaAplicada, d.status as status, l.precio as precioCliente " + 
						"       FROM venta a " + 
						"       JOIN facturacfdiven b on a.ven_id = b.ven_id " + 
						"       JOIN facturacfdi c on b.fcf_id = c.fcf_id " + 
						"       JOIN notacredito d on c.fcf_id = d.fcf_id " +
						"       LEFT JOIN caja e on d.caj_id = e.caj_id " +
						"		LEFT JOIN detallev f on d.ncr_id = f.ncr_id " +
						" 		LEFT JOIN vendedor v on d.vnd_id = v.vnd_id  " +
						" 		LEFT JOIN cliente l on d.cli_id = l.cli_id " +
						" WHERE  a.ven_id in (:vList) " +
						" ORDER BY a.ven_id, d.ncr_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(NotaCredito.class));
				query.setParameterList("vList", ids);
				List<NotaCredito> ncFactList= query.list();
				
				if(!ncFactList.isEmpty()) {
					for(NotaCredito o : ncFactList) {
						ncList.add(o.getIdNotaCredito());
					}
				}
				
				List<NotaCreditoImpuestos> ncImpList = null;
				List<TipoPago> pagoNCList = null;
				List<TipoPago> pagoCreditoNCList = null;
				List<Articulo> ncArtList = null;
				List<Articulo> ncArtKitList = null;
				List<ArticuloImpuesto> ncArtImpList = null;
				List<DetalleLote> ncLotKitList = null;
				List<DetalleLote> ncLotList = null;
				if(!ncList.isEmpty()) {
					List<Integer> idsNc = new ArrayList<Integer>();
					for(BigInteger n : ncList) {
						idsNc.add(n.intValue());
					}
					
					sql = "select a.ncr_id as idNotaCredito, a.imp_id as idImpuesto, " + 
							"       a.total as totalImpuesto, a.subtotal as subTotal, a.tipoFactor as tipoFactor, " + 
							"       a.nombreImp as nombreImpuesto, a.valor as porcentajeImpuesto " + 
							"       FROM notacreditoimp a " + 
							" WHERE  a.ncr_id in (:ncList) " +
							" ORDER BY a.ncr_id; ";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(NotaCreditoImpuestos.class));
					query.setParameterList("ncList", idsNc);
					ncImpList= query.list();
					
					sql = " SELECT a.ncr_id      AS idNotaCredito, " +
							" j.nombre        AS tipoPagoNombre, " +
							" j.abr           AS tipoPagoClave, " +
							" i.total         AS ventaTipoPagoTotal " +
							" FROM   notacredito a " +
							" LEFT JOIN notacreditotipopago i " +
							"       ON a.ncr_id = i.ncr_id " +
							" LEFT JOIN tipopago j " +
							"       ON i.tpa_id = j.tpa_id " +
							" WHERE  a.ncr_id in (:ncList) " +
							" ORDER BY a.ncr_id;";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
					query.setParameterList("ncList", idsNc);
					pagoNCList= query.list();
					
					sql = " SELECT c.ncr_id      AS idNotaCredito, " +
							" p.nombre        AS tipoPagoNombre, " +
							" p.abr           AS tipoPagoClave, " +
							" c.total         AS ventaTipoPagoTotal " +
							" FROM creditoclientenotcre c " +
							" LEFT JOIN tipopago p ON p.tpa_id = 3 " +
							" WHERE  c.ncr_id in (:ncList) " +
							" ORDER BY c.ncr_id;";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
					query.setParameterList("ncList", idsNc);
					pagoCreditoNCList= query.list();
					
					sql = " SELECT a.ven_id AS idVenta, " +
							"  d.art_id AS articuloId, " +
							"  d.clave AS articuloClave, " +
							"  replace(d.descripcion, ',', '_') AS articuloDescripcion, " +
							"  g.nombre AS categoria, " +
							"  h.nombre AS departamento, " +
							"  e.precio1 AS articuloPrecio1, " +
							"  e.precio2 AS articuloPrecio2, " +
							"  e.precio3 AS articuloPrecio3, " +
							"  d.cantidad AS ventaDetalleCantidad, " +
							"  d.unidad AS ventaDetalleUnidad, " +
							"  d.preciosin AS ventaDetallePrecioSinIva, " +
							"  d.preciocon AS ventaDetallePrecioConIva, " +
							"  d.importesin AS ventaDetalleImporteSinIva, " +
							"  d.importecon AS ventaDetalleImporteConIva, " +
							"  (d.importeCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) " + 
							"									FROM detallenimpuesto i " + 
							"                                   WHERE i.ncr_id = f.ncr_id " +
							"                                   AND i.ven_id = a.ven_id " + 
							"                                   AND i.art_id = e.art_id), 0)/100)) AS ventaDetalleImporteSinImp, " +
							"	d.importeCon-((d.importeCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) " + 
							"												FROM detallenimpuesto i " + 
							"                                               WHERE i.ncr_id = f.ncr_id " +
							"                                               AND i.ven_id = a.ven_id " +
							"                                               AND i.art_id = e.art_id), 0)/100))) AS ventaDetalleImpuesto, " +						
							"  d.monpreciosin AS monedaDetallePrecioSinIva, " +
							"  d.monpreciocon AS monedaDetallePrecioConIva, " +
							"  d.monimportesin AS monedaDetalleImporteSinIva, " +
							"  d.monimportecon AS monedaDetalleImporteConIva, " +
							"  (d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) " + 
							"									FROM detallenimpuesto i " + 
							"                                   WHERE i.ncr_id = f.ncr_id " +
							"                                   AND i.ven_id = a.ven_id " + 
							"                                   AND i.art_id = e.art_id), 0)/100)) AS monedaDetalleImporteSinImp, " +
							"	d.monImporteCon-((d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) " + 
							"																FROM detallenimpuesto i " + 
							"                                                                WHERE i.ncr_id = f.ncr_id " +
							"                                                                AND i.ven_id = a.ven_id " +
							"                                                                AND i.art_id = e.art_id), 0)/100))) AS monedaDetalleImpuesto, " +							
							"  d.ncr_id AS notaCreditoId, " +
							"  f.folioNC AS folioNC, " +
							"  f.folio AS folio, " +
							"  d.descTotal AS descuentoTotal, " +
							"  d.precioCompra AS precioCompra, " +
							"  e.lote AS lote, " +
							"  d.importecompra AS importeCompra, " +
							"  e.tipo AS tipo " +
							" FROM venta a  " +
							"  JOIN detallen d " +
							"  ON a.ven_id = d.ven_id" +
							"  LEFT JOIN articulo e" +
							"  ON d.art_id = e.art_id" +
							"  LEFT JOIN notacredito f" +
							"  ON d.ncr_id = f.ncr_id" +
							"  LEFT JOIN categoria g " +
							"  ON e.cat_id = g.cat_id " +
							"  LEFT JOIN departamento h " +
							"  ON g.dep_id = h.dep_id " +
							" WHERE  d.ncr_id in (:ncList) " +
							" AND (d.dv_ven_id is not null OR d.dv_art_id is not null) " +
							" ORDER BY a.ven_id, d.art_id;";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
					query.setParameterList("ncList", idsNc);
					ncArtList = query.list();

					//Articulos por descuento
					sql = " SELECT c.ven_id AS idVenta,  " + 
							"   d.art_id AS articuloId,  " + 
							"   d.clave AS articuloClave,  " + 
							"	replace(d.descripcion, ',', '_') AS articuloDescripcion, " +
							"	g.nombre AS categoria, " +
							"	h.nombre AS departamento, " +
							"   (e.precio1 * a.total)/c.total AS articuloPrecio1,  " +
							"   (e.precio2 * a.total)/c.total AS articuloPrecio2,  " +
							"   (e.precio3 * a.total)/c.total AS articuloPrecio3,  " +
							"   d.cantidad AS ventaDetalleCantidad,  " +
							"   d.unidad AS ventaDetalleUnidad,  " +
							"   (d.preciosin * a.total)/c.total AS ventaDetallePrecioSinIva,  " +
							"   (d.preciocon * a.total)/c.total AS ventaDetallePrecioConIva,  " + 
							"   (d.importesin * a.total)/c.total AS ventaDetalleImporteSinIva,  " +
							"   (d.importecon * a.total)/c.total AS ventaDetalleImporteConIva,  " +
							"  CASE WHEN TRUNCATE(c.total, 0) = TRUNCATE((SELECT SUM(x.importeCon)												" +
							"											FROM detallev x															" +
							"											WHERE x.ven_id = c.ven_id												" +
							"											GROUP BY x.ven_id), 0) 													" +
							"  THEN																												" +
							"  		ROUND(((d.importeCon)/ (1 + (IFNULL((SELECT MAX(i.impuesto)													" + 
							"									FROM detallevimpuesto i															" + 
							"                                   WHERE i.ven_id = d.ven_id														" + 
							"                                   AND i.art_id = d.art_id), 0)/100)) * a.total)/c.total, 3)						" +						
							"  ELSE																												" +
							"  		ROUND(((d.importeNorCon-d.descTotal)/ (1 + (IFNULL((SELECT MAX(i.impuesto)									" + 
							"													FROM detallevimpuesto i											" + 
							"                                   				WHERE i.ven_id = d.ven_id										" + 
							"                                   				AND i.art_id = d.art_id), 0)/100)) * a.total)/c.total, 3)		" +
							"  END AS ventaDetalleImporteSinImp, 																				" +
							"  CASE WHEN TRUNCATE(c.total, 0) = TRUNCATE((SELECT SUM(x.importeCon)												" +
							"											FROM detallev x															" +
							"											WHERE x.ven_id = c.ven_id												" +
							"											GROUP BY x.ven_id), 0) 													" +
							"  THEN																												" +
							"  		ROUND((((d.importeCon)-((d.importeCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto)									" + 
							"													FROM detallevimpuesto i											" + 
							"                                                   WHERE i.ven_id = d.ven_id										" + 
							"                                                   AND i.art_id = d.art_id), 0)/100)))) * a.total)/c.total, 3)		" +
							"  ELSE																												" +
							"  		ROUND((((d.importeNorCon-d.descTotal)-((d.importeNorCon-d.descTotal)/ (1 +(IFNULL((SELECT MAX(i.impuesto)	" + 
							"																FROM detallevimpuesto i								" + 
							"                                                                WHERE i.ven_id = d.ven_id							" + 
							"                                                                AND i.art_id = d.art_id), 0)/100)))) * a.total)/c.total, 3)	" +
							"  END AS ventaDetalleImpuesto,																						" +
							"   (d.monpreciosin * a.total)/c.total AS monedaDetallePrecioSinIva,  " +
							"   (d.monpreciocon * a.total)/c.total AS monedaDetallePrecioConIva,  " + 
							"   (d.monimportesin * a.total)/c.total AS monedaDetalleImporteSinIva,  " +
							"   (d.monimportecon * a.total)/c.total AS monedaDetalleImporteConIva,  " +
							"  CASE WHEN TRUNCATE(IFNULL(c.monTotal,0), 0) = TRUNCATE((SELECT SUM(IFNULL(d.monImporteCon,0))		" +
							"															FROM detallev d								" +
							"															WHERE d.ven_id = c.ven_id					" +
							"															GROUP BY d.ven_id), 0) 						" +
							"  THEN																									" +
							"  ROUND(((d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto)										" + 
							"									FROM detallevimpuesto i												" + 
							"                                   WHERE i.ven_id = d.ven_id 											" + 
							"                                   AND i.art_id = d.art_id), 0)/100))* a.total)/c.total, 3)			" +
							"  ELSE																									" +
							"  ROUND(((d.monImporteNorCon-(d.descTotal/a.monTipoCambio))/ (1 +(IFNULL((SELECT MAX(i.impuesto)		" + 
							"																	FROM detallevimpuesto i				" + 
							"                                   								WHERE i.ven_id = d.ven_id 			" + 
							"                                   								AND i.art_id = d.art_id), 0)/100))* a.total)/c.total, 3)	" +
							"  END AS monedaDetalleImporteSinImp, 																	" +
							"  CASE WHEN TRUNCATE(IFNULL(c.monTotal,0), 0) = TRUNCATE((SELECT SUM(IFNULL(d.monImporteCon,0))		" +
							"															FROM detallev d								" +
							"															WHERE d.ven_id = c.ven_id					" +
							"															GROUP BY d.ven_id), 0) 						" +
							"  THEN																									" +
							"  ROUND(((d.monImporteCon-((d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) 						" + 
							"													FROM detallevimpuesto i 							" + 
							"                                                   WHERE i.ven_id = d.ven_id 							" + 
							"                                                   AND i.art_id = d.art_id), 0)/100)))) * a.total)/c.total, 3)	" +
							"  ELSE																									" +
							"  ROUND(((d.monImporteNorCon-(d.descTotal/a.monTipoCambio)-((d.monImporteNorCon-(d.descTotal/a.monTipoCambio)) " +
							"													/ (1 +(IFNULL((SELECT MAX(i.impuesto) 				" + 
							"																 FROM detallevimpuesto i 				" + 
							"                                                                WHERE i.ven_id = d.ven_id 				" + 
							"                                                                AND i.art_id = d.art_id), 0)/100))))* a.total)/c.total, 3)	" +
							"  END AS monedaDetalleImpuesto,																		" +
							"   a.ncr_id AS notaCreditoId,  " +
							"   a.folioNC AS folioNC,  " +
							"   a.folio AS folio, " +
							"   (d.descTotal * a.total)/c.total AS descuentoTotal,  " + 
							"   (d.precioCompra * a.total)/c.total AS precioCompra,  " + 
							"   e.lote AS lote,  " + 
							"   (d.importecompra * a.total)/c.total AS importeCompra,  " + 
							"   e.tipo AS tipo " +
							"  FROM notacredito a  " + 
							"  JOIN detallen b  " + 
							" 	ON a.ncr_id = b.ncr_id  " + 
							"  JOIN venta c  " + 
							" 	ON b.ven_id = c.ven_id  " + 
							"  JOIN detallev d  " + 
							" 	ON c.ven_id = d.ven_id  " + 
							"  LEFT JOIN articulo e  " + 
							"   ON d.art_id = e.art_id  " +
							"  LEFT JOIN categoria g " +
							"  ON e.cat_id = g.cat_id " +
							"  LEFT JOIN departamento h " +
							"  ON g.dep_id = h.dep_id " +
							"  WHERE a.ncr_id in (:ncList) " + 
							"  AND dv_ven_id is null  " + 
							"  AND dv_art_id is null " + 
							"  ORDER BY idVenta, articuloId;";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
					query.setParameterList("ncList", idsNc);

					List<Articulo> adList = query.list();
					if(adList != null && adList.size() > 0) {
						if(ncArtList == null) {
							ncArtList = new ArrayList<Articulo>();
						}
						for(Articulo ad : adList) {
							ad.setArticuloDescuento(true);
							ncArtList.add(ad);
						}						
					}
										
					sql = "SELECT a.ven_id    AS idVenta, " + 
							" h.ncr_id		  AS notaCreditoId, " +
							" h.art_id        AS articuloId, " + 
							" b.clave         AS articuloClave, " + 
							" h.nombre        AS ventaDetalleImpuestoNombre, " + 
							" h.impuesto      AS ventaDetalleImpuestoValor, " + 
							" h.total		  AS ventaDetalleImpuestoTotal " + 
							" FROM   venta a " + 
							" LEFT JOIN detallenimpuesto h " + 
							"      ON a.ven_id = h.ven_id " + 
							" LEFT JOIN articulo b " + 
							"      ON h.art_id = b.art_id " + 
							" WHERE  h.ncr_id in (:ncList) " +
							" AND h.total >= 0 " +
							" ORDER BY a.ven_id, h.art_id;"; 
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(ArticuloImpuesto.class));
					query.setParameterList("ncList", idsNc);
					ncArtImpList = query.list();
					
					//Impuesto Articulos por descuento
					sql = "  SELECT c.ven_id AS idVenta, " +
							" a.ncr_id		  AS notaCreditoId, " +
							" d.art_id        AS articuloId, " + 
							" d.clave		  AS articuloClave, " + 
							" e.nombre        AS ventaDetalleImpuestoNombre, " + 
							" e.impuesto      AS ventaDetalleImpuestoValor, " + 
							" (e.total * a.total)/c.total AS ventaDetalleImpuestoTotal " + 
							" FROM notacredito a " + 
							" JOIN detallen b " + 
							"	 ON a.ncr_id = b.ncr_id " + 
							" JOIN venta c " + 
							"	 ON b.ven_id = c.ven_id " + 
							" JOIN detallev d " + 
							"    ON c.ven_id = d.ven_id " + 
							" JOIN detallevimpuesto e " + 
							"     ON d.ven_id = e.ven_id " + 
							"     AND d.art_id = e.art_id " + 
							" WHERE  a.ncr_id in (:ncList) " + 
							"   AND dv_ven_id is null  " + 
							"   AND dv_art_id is null  " +
							" 	AND e.total >= 0 " +
							" ORDER BY idVenta, articuloId;";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(ArticuloImpuesto.class));
					query.setParameterList("ncList", idsNc);
					List<ArticuloImpuesto> aidList = query.list();
					if(aidList != null && aidList.size() > 0) {
						if(ncArtImpList == null) {
							ncArtImpList = new ArrayList<ArticuloImpuesto>(); 
						}
						ncArtImpList.addAll(aidList);
					}
					
					sql = "SELECT v.ven_id      AS idVenta, " +
						       "d.art_id        AS articuloId, " + 
						       "d.clave         AS articuloClave, " +  
						       "l.numlote       AS loteNumero, " +
						       "l.fechafab      AS loteFechaFabricacion, " + 
						       "l.fechacad      AS loteFechaCaducidad,  " +
						       "l.exisinicial   AS loteExistenciaInicial, " + 
						       "l.exisactual    AS loteExistenciaActual, " +
						       "l.status        AS loteStatus,	 " +
						       "k.cantidad      AS cantidad " +
							   "FROM notacredito a  " +
						       "JOIN detallen d  " +
						         "ON a.ncr_id = d.ncr_id  " +
							   "LEFT JOIN detallenlote k " + 
						              "ON a.ncr_id = k.ncr_id  " +
						               "AND d.art_id = k.art_id " + 
						       "LEFT JOIN lote l " + 
						              "ON k.lot_id = l.lot_id " + 
						              "AND d.art_id = l.art_id " + 
						       " JOIN venta v on (a.tic_id = v.tic_id OR a.not_id = v.not_id) " +       
							   "WHERE   l.numlote is not null and a.ncr_id in (:ncList) " +
							   "ORDER BY a.ncr_id, d.art_id; ";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(DetalleLote.class));
					query.setParameterList("ncList", idsNc);
					ncLotList= query.list();
					
					
					//JAvila: Obtiene los articulos y lotes del kit
					sql = " 	SELECT a.ven_id AS idVenta, " + 
							"		  q.art_id AS articuloId,  " + 
							"		  q.clave AS articuloClave,  " + 
							"		  replace(q.descripcion, ',', '_') AS articuloDescripcion,  " + 
							"		  q.precio1 AS articuloPrecio1,  " + 
							"		  q.precio2 AS articuloPrecio2,  " + 
							"		  q.precio3 AS articuloPrecio3,  " + 
							"		  b.cantidad AS ventaDetalleCantidad, " + 
							"         q.lote AS lote, " +
							"		  b.unidad AS ventaDetalleUnidad, " + 
							"		  q.precioCompra AS precioCompra, " +
							"		  b.paquete AS paquete, " +
							"		  a.tipo AS tipo " +
							"	FROM detallev a" + 
							"	JOIN detallepaqn b on a.art_id = b.paquete and a.ven_id=b.ven_id " + 
							"	JOIN articulo q on b.articulo = q.art_id " + 
							"	WHERE b.ncr_id in (:ncList);";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
					query.setParameterList("ncList", idsNc);
					ncArtKitList = query.list();
					
					sql = "SELECT a.ven_id         AS idVenta,  " + 
							"	   d.articulo      AS articuloId,   " + 
							"	   d.clave         AS articuloClave,    " + 
							"	   l.numlote       AS loteNumero,  " + 
							"	   l.fechafab      AS loteFechaFabricacion,   " + 
							"	   l.fechacad      AS loteFechaCaducidad,   " + 
							"	   l.exisinicial   AS loteExistenciaInicial,   " + 
							"	   l.exisactual    AS loteExistenciaActual,  " + 
							"	   l.status        AS loteStatus,	  " + 
							"	   d.paquete AS paquete, " +
							"	   k.cantidad      AS cantidad  " + 
							"	   FROM   venta a   " + 
							"	   JOIN detallepaqn d  ON a.ven_id = d.ven_id " + 
							"	   LEFT JOIN detallepaqnlote k  ON a.ven_id = k.ven_id " + 
							"			   AND d.articulo = k.articulo " + 
							"	   LEFT JOIN lote l ON k.lot_id = l.lot_id " + 
							"			  AND d.articulo = l.art_id " + 
							"	   WHERE   l.numlote is not null and d.ncr_id in (:ncList) " + 
							"	   ORDER BY d.ncr_id, d.articulo; ";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(DetalleLote.class));
					query.setParameterList("ncList", idsNc);
					ncLotKitList= query.list();
					//JAvila: Fin
					
					//Impuesto Obtiene los articulos del kit por descuento
					sql = "	SELECT a.ven_id AS idVenta, " + 
							"		  n.ncr_id AS notaCreditoId, " +
							"		  q.art_id AS articuloId,  " + 
							"		  q.clave AS articuloClave,  " + 
							"		  replace(q.descripcion, ',', '_') AS articuloDescripcion,  " + 
							"		  (q.precio1 * n.total)/c.total AS articuloPrecio1,  " + 
							"		  (q.precio2 * n.total)/c.total AS articuloPrecio2,  " + 
							"		  (q.precio3 * n.total)/c.total AS articuloPrecio3,  " + 
							"		  b.cantidad AS ventaDetalleCantidad, " + 
							"         q.lote AS lote, " + 
							"		  b.unidad AS ventaDetalleUnidad, " + 
							"		  (q.precioCompra * n.total)/c.total AS precioCompra, " + 
							"		  b.paquete AS paquete, " + 
							"		  a.tipo AS tipo " + 
							"	FROM notacredito n " + 
							"   JOIN detallen d on n.ncr_id = d.ncr_id " + 
							"   JOIN detallev a on d.ven_id = a.ven_id " + 
							"   JOIN detallep b on a.art_id = b.paquete and a.ven_id=b.ven_id " + 
							"	JOIN articulo q on b.articulo = q.art_id " + 
							"   JOIN venta c on d.ven_id = c.ven_id " + 
							"	WHERE n.ncr_id in (:ncList) " + 
							"   AND d.dv_ven_id is null " + 
							"	AND d.dv_art_id is null";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
					query.setParameterList("ncList", idsNc);
					List<Articulo> adNcList = query.list();
					if(adNcList != null && adNcList.size() > 0) {
						if(ncArtKitList == null) {
							ncArtKitList = new ArrayList<Articulo>();
						}
						for(Articulo ad : adNcList) {
							ad.setArticuloDescuento(true);
							ncArtKitList.add(ad);
						}						
					}
				}
				

				Cliente cli = null;
				List<Articulo> aList = null;
				List<Articulo> aKitList = null;
				List<ArticuloImpuesto> aiList = null;
				List<TipoPago> tpList = null;
				List<DetalleLote> dlList = null;
				Factura fact = null;
				NotaCredito nc = null;
				List<FacturaImpuestos> fiList = null;
				List<NotaCreditoImpuestos> ncrList = null;
				List<Articulo> ancList = null;
				List<ArticuloImpuesto> aincList = null;
				List<DetalleLote> aLotList = null;
				List<DetalleLote> aLotKitList = null;

				for(Sales s : rows) {
					
					s.setSucursal(sucursal);
					s.setEmpresa(emp);
					
					for(Cliente o : cliList) {
						if(o.getClienteId() != null && s.getIdCliente() != null) {
							if(o.getClienteId().intValue() == s.getIdCliente().intValue()) {							
								cli = o;
								break;
							}
						}
					}					
					s.setCliente(cli);
					
					aList = new ArrayList<Articulo>();
					for(Articulo o :  artList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							aList.add(o);
						}
					}
					s.setArticulo(aList);

					//JAvila: Obtiene los articulos y lotes del kit
					aKitList = new ArrayList<Articulo>();
					for(Articulo o :  artKitList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							aKitList.add(o);
						}
					}
					
					if(aKitList.size() > 0) {
						s.setArticuloPaquete(aKitList);
					}
										
					aLotKitList = new ArrayList<DetalleLote>();
					for(DetalleLote o :  lotListKit) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							aLotKitList.add(o);
						}
					}
					s.setDetalleLotePaquete(aLotKitList);
					//JAvila: FIN
					
					aiList = new ArrayList<ArticuloImpuesto>();
					for(ArticuloImpuesto o :  artInvList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							aiList.add(o);
						}
					}
					s.setArticuloImpuesto(aiList);
					
					tpList = new ArrayList<TipoPago>();
					if(pagoList != null && !pagoList.isEmpty()) {
						for(TipoPago o :  pagoList) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								tpList.add(o);
							}
						}	
					}
//					if(pagoMonList != null && !pagoMonList.isEmpty()) {
//						for(TipoPago o :  pagoMonList) {
//							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
//								tpList.add(o);
//							}
//						}	
//					}
					s.setTipoPago(tpList);
					
					dlList = new ArrayList<DetalleLote>();
					for(DetalleLote o :  lotList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							dlList.add(o);
						}
					}
					s.setDetalleLote(dlList);
					
					fiList = new ArrayList<FacturaImpuestos>();
					for(Factura o :  factList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							for(FacturaImpuestos fi : factImpList) {
								if(o.getIdFactura().intValue() == fi.getIdFactura().intValue()) {
									fiList.add(fi);
								}
							}
							o.setSucursal(sucursal);
							o.setFacturaImpuestos(fiList);
							fact = o;
							break;
						}
					}
					s.setFactura(fact);
										
					nc = null;
					if(!ncTicketList.isEmpty()) {
						for(NotaCredito o :  ncTicketList) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								o.setSucursal(sucursal);
								
								if(ncArtList != null) {
									ancList = new ArrayList<Articulo>();
									for(Articulo onc : ncArtList) {
										if(onc.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											ancList.add(onc);
										}
									}
									o.setArticulo(ancList);
								}
								
								//J.Avila: detalle de articulos de Kit
								if(ncArtKitList != null) {
									ancList = new ArrayList<Articulo>();
									for(Articulo onc : ncArtKitList) {
										if(onc.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											ancList.add(onc);
										}
									}
									if(ancList.size() > 0) {
										o.setArticuloPaquete(ancList);
									}									
								}
								//J.Avila: Fin
								
								List<TipoPago> tpNCList = new ArrayList<TipoPago>();
								if(pagoNCList != null && !pagoNCList.isEmpty()) {
									for(TipoPago tp :  pagoNCList) {
										if(tp.getIdNotaCredito() != null && o.getIdNotaCredito() != null) {
											if(tp.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
												tpNCList.add(tp);
											}	
										}
									}
								}
								if(pagoCreditoNCList != null && !pagoCreditoNCList.isEmpty()) {
									for(TipoPago tp :  pagoCreditoNCList) {
										if(tp.getIdNotaCredito() != null && o.getIdNotaCredito() != null) {
											if(tp.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
												tpNCList.add(tp);
											}	
										}
									}
								}
								o.setTipoPago(tpNCList);

								if(ncArtImpList != null) {
									aincList = new ArrayList<ArticuloImpuesto>();
									for(ArticuloImpuesto onc : ncArtImpList) {
										if(onc.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											aincList.add(onc);
										}
									}
									o.setArticuloImpuesto(aincList);
								}
								
								if(ncImpList != null && ncImpList.size() > 0) {
									ncrList = new ArrayList<NotaCreditoImpuestos>();
									for(NotaCreditoImpuestos nci : ncImpList) {
										if(nci.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
											ncrList.add(nci);
										}
									}
								}
								o.setNotaCreditoImpuestos(ncrList);
								
								if(ncLotList != null) {
									aLotList = new ArrayList<DetalleLote>();
									for(DetalleLote ncl :  ncLotList) {
										if(ncl.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											aLotList.add(ncl);
										}
									}
								}								
								o.setDetalleLote(aLotList);
								
								//J.Avila: Detalle del lote de los kits
								if(ncLotKitList != null) {
									aLotList = new ArrayList<DetalleLote>();
									for(DetalleLote ncl :  ncLotKitList) {									
										if(ncl.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											aLotList.add(ncl);
										}
									}
								}
								o.setDetalleLotePaquete(aLotList);
								//J.Avila:Fin
								
								nc = o;
								break;
							}
						}
					}
					
					if(!ncFactList.isEmpty()) {
						for(NotaCredito o :  ncFactList) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								o.setSucursal(sucursal);
								if(ncArtList != null) {
									ancList = new ArrayList<Articulo>();
									for(Articulo onc : ncArtList) {
										if(onc.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											ancList.add(onc);
										}
									}
									o.setArticulo(ancList);
								}
								
								//J.Avila: detalle de articulos de Kit
								if(ncArtKitList != null) {
									ancList = new ArrayList<Articulo>();
									for(Articulo onc : ncArtKitList) {
										if(onc.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											ancList.add(onc);
										}
									}
									if(ancList.size() > 0) {
										o.setArticuloPaquete(ancList);
									}									
								}
								//J.Avila: Fin

								if(ncArtImpList != null) {
									aincList = new ArrayList<ArticuloImpuesto>();
									for(ArticuloImpuesto onc : ncArtImpList) {
										if(onc.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											aincList.add(onc);
										}
									}
									o.setArticuloImpuesto(aincList);
								}
								
								if(ncImpList != null && ncImpList.size() > 0) {
									if(ncrList == null) {
										ncrList = new ArrayList<NotaCreditoImpuestos>();
									}									
									for(NotaCreditoImpuestos nci : ncImpList) {
										if(nci.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
											ncrList.add(nci);
										}
									}									
								}
								o.setNotaCreditoImpuestos(ncrList);
								
								if(ncLotList != null) {
									aLotList = new ArrayList<DetalleLote>();
									for(DetalleLote ncl :  ncLotList) {									
										if(ncl.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											aLotList.add(ncl);
										}
									}
								}
								o.setDetalleLote(aLotList);
								
								//J.Avila: Detalle del lote de los kits
								
								if(ncLotKitList != null) {
									aLotList = new ArrayList<DetalleLote>();
									for(DetalleLote ncl :  ncLotKitList) {									
										if(ncl.getIdVenta().intValue() == o.getIdVenta().intValue()) {
											aLotList.add(ncl);
										}
									}
								}
								o.setDetalleLotePaquete(aLotList);
								//J.Avila:Fin
								
								List<TipoPago> tpNCList = new ArrayList<TipoPago>();
								if(pagoNCList != null && !pagoNCList.isEmpty()) {
									for(TipoPago tp :  pagoNCList) {
										if(tp.getIdNotaCredito() != null && o.getIdNotaCredito() != null) {
											if(tp.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
												tpNCList.add(tp);
											}	
										}
									}
								}
								if(pagoCreditoNCList != null && !pagoCreditoNCList.isEmpty()) {
									for(TipoPago tp :  pagoCreditoNCList) {
										if(tp.getIdNotaCredito() != null && o.getIdNotaCredito() != null) {
											if(tp.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
												tpNCList.add(tp);
											}	
										}
									}
								}
								o.setTipoPago(tpNCList);								
								nc = o;
								break;
							}
						}
					}
					
					s.setNotaCredito(nc);
				}
			}
			salesDTO.setSales(rows);
		}
			
			//Obtiene cancelaciones
			List<CancelacionVenta> cvList = null;
			String sql = "";
			
			if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			    sql = "select '" + sucursal + "' as sucursal, a.ven_id as idVenta, a.tic_id as idTicket, a.not_id as idNotaVenta, a.can_rcc_id as idCancelaResumen," + 
			    	"	a.can_caj_id as idCancelaCaja, c.nombre as cancelaCaja, a.subtotal as subTotal, a.total as total, h.fecha as fechaCancelacion " + 
			    	"	from venta a " + 
			    	"	join resumencortecaja b on a.can_rcc_id = b.rcc_id " +
			    	"	join historial h on a.ven_id = h.id and h.tabla = 'Venta' and h.movimiento = 2 " +
			    	"	join caja c on a.can_caj_id = c.caj_id " +
			    	"	where a.can_rcc_id = " + idCorteCaja + " and a.total > 0; ";
			} else {
			    sql = "SELECT distinct '" + sucursal + "' as sucursal, a.ven_id as idVenta, a.tic_id as idTicket, a.not_id as idNotaVenta, a.can_rcc_id as idCancelaResumen, " +
			    	"	a.can_caj_id as idCancelaCaja, a.subtotal as subTotal, a.total as total, h.fecha as fechaCancelacion " +
			    	"	from historial h" +
			    	"   join venta a on h.id = a.ven_id" +
			    	"	where h.tabla = 'Venta' " +
			    	"	and h.movimiento = 2" +
			    	"   and h.fecha between '" + fromDate + "' and '" + toDate + "' " +
			    	"	and a.total > 0;";
			}
			
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(CancelacionVenta.class));
			cvList = query.list();
			salesDTO.setCancelaciones(cvList);
			
			if(cvList != null) {
				
				sql = "SELECT a.emp_id AS empId, " + 
						   "a.nombre  AS nombre, " + 
					       "a.nombreFiscal  AS nombreFiscal, " + 
					       "a.cuenta  AS cuenta, " +
					       "a.rfc  AS rfc " +
					       "FROM empresa a " ;
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Empresa.class));
				List<Empresa> cmp = query.list();
				
			List<Integer> ids = new ArrayList<Integer>();			
				for(CancelacionVenta s : cvList) {
					ids.add(s.getIdVenta().intValue());
				}
				
				if(!ids.isEmpty()) {
					sql = "SELECT c.cli_id AS idCliente, " +
							" IFNULL(e.nub_id, 0) AS clienteNubeId," +
							" replace(c.nombre, ',', '_') AS nombreCliente, " + 
							" c.diasCredito AS diasCredito, " + 
							" c.clave AS claveCliente, " +
							" c.precio AS precioCliente, " +
							" v.nombre AS nombreVendedor, " +
							" a.tic_id AS idTicket, " +
							" a.not_id AS idNotaVenta, " +
							" a.ven_id AS idVenta,  " + 
							" a.fecha AS ventaFecha,  " + 
							" d.nombre AS ventaCaja, " +							
							" a.subtotal AS ventaSubtotal,  " + 
							" a.descuento AS ventaDescuento,  " + 
							" a.total AS ventaTotal,  " + 
							" a.cambio AS cambio, " +
							" a.status AS ventaStatus,  " + 
							" a.monAbr AS ventaMoneda,  " + 
							" a.monTipoCambio AS ventaTipoCambio, " +
							" a.monSubtotal AS monedaSubtotal,  " + 
							" a.monDescuento AS monedaDescuento,  " + 
							" a.monTotal AS monedaTotal,  " +
							" a.monCambio AS monedaCambio, " +														
							" a.rcc_id AS idResumenCaja, " +
							" a.can_caj_id AS idCancelaCaja, " +
							" a.can_rcc_id AS idCancelaResumenCaja " +
						    " FROM venta a  " +
						    " LEFT JOIN ticket b " +  
						    "  ON b.tic_id IS NOT NULL " +
						    "  AND a.tic_id = b.tic_id " +
						    " LEFT JOIN nota n " +
						    "  ON n.not_id IS NOT NULL " +
						    "  AND a.not_id = n.not_id " +
							" LEFT JOIN remision r " +
							" ON r.rem_id IS NOT NULL " +
							" AND a.rem_id = r.rem_id " +
						    " LEFT JOIN cliente c " +
						    "  ON (b.cli_id = c.cli_id OR n.cli_id = c.cli_id OR r.rem_id = c.cli_id) " +
						    " LEFT JOIN nubeid e " +
						    " ON c.cli_id = e.id AND e.entidad = 'Cliente' " +						    
					        " LEFT JOIN caja d " +
					        "  ON a.caj_id = d.caj_id " +
					        " LEFT JOIN vendedor v  " +
					        "  ON a.vnd_id = v.vnd_id  " +
							" WHERE a.ven_id in (:vList) " +
							" ORDER BY a.ven_id; ";

					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(Sales.class));
					query.setParameterList("vList", ids);
					rows = query.list();
					
					sql = "SELECT " +
							" a.ven_id AS idVenta, " +
							" c.cli_id AS clienteId, " +
							" replace(c.nombre, ',', '_') AS clienteNombre,  " +
							" c.rfc AS clienteRFC, " +
							" c.curp AS clienteCURP, " +
							" c.domicilio AS clienteDomicilio, " +
							" c.colonia AS clienteColonia, " +
							" c.noExt AS clienteNoExt, " +
							" c.noInt AS clienteNoInt, " +
							" c.localidad AS clienteLocalidad, " +
							" c.ciudad AS clienteCiudad, " +
							" c.estado AS clienteEstado, " +
							" c.pais AS clientePais, " +
							" c.codigoPostal AS clienteCodigoPostal, " +
							" c.limite AS clienteLimite, " +
							" c.diasCredito AS clienteDiasCredito, " +
							" c.clave AS clienteClave " +
							"FROM venta a   " +
						    " LEFT JOIN ticket b " +  
						    "  ON b.tic_id IS NOT NULL " +
						    "  AND a.tic_id = b.tic_id " +
						    " LEFT JOIN nota n " +
						    "  ON n.not_id IS NOT NULL " +
						    "  AND a.not_id = n.not_id " +
							" LEFT JOIN remision r " +
							" ON r.rem_id IS NOT NULL " +
							" AND a.rem_id = r.rem_id " +
						    " LEFT JOIN cliente c " +
						    "  ON (b.cli_id = c.cli_id OR n.cli_id = c.cli_id OR r.cli_id = c.cli_id) " +
							" WHERE a.ven_id in (:vList)  " +
							" ORDER BY a.ven_id; ";

					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(Cliente.class));
					query.setParameterList("vList", ids);
					List<Cliente> cliList= query.list(); 					
					
					sql = " SELECT a.ven_id AS idVenta, " +
							"  d.art_id AS articuloId, " +
							"  d.clave AS articuloClave, " +
							"  replace(d.descripcion, ',', '_') AS articuloDescripcion, " +
							"  g.nombre AS categoria, " +
							"  h.nombre AS departamento, " +
							"  e.precio1 AS articuloPrecio1, " +
							"  e.precio2 AS articuloPrecio2, " +
							"  e.precio3 AS articuloPrecio3, " +
							"  d.cantidad AS ventaDetalleCantidad, " +
							"  d.unidad AS ventaDetalleUnidad, " +
							"  d.preciosin AS ventaDetallePrecioSinIva, " +
							"  d.preciocon AS ventaDetallePrecioConIva, " +
							"  d.importesin AS ventaDetalleImporteSinIva, " +
							"  d.importecon AS ventaDetalleImporteConIva, " +
							"  CASE WHEN TRUNCATE(a.total, 0) = TRUNCATE((SELECT SUM(d.importeCon)				" +
							"											FROM detallev d							" +
							"											WHERE d.ven_id = a.ven_id				" +
							"											GROUP BY d.ven_id), 0) 					" +
							"  THEN																				" +
							"  		(d.importeCon)/ (1 + (IFNULL((SELECT MAX(i.impuesto)						" + 
							"									FROM detallevimpuesto i							" + 
							"                                   WHERE i.ven_id = a.ven_id						" + 
							"                                   AND i.art_id = e.art_id), 0)/100))				" +						
							"  ELSE																				" +
							"  		(d.importeNorCon-d.descTotal)/ (1 + (IFNULL((SELECT MAX(i.impuesto)			" + 
							"													FROM detallevimpuesto i			" + 
							"                                   				WHERE i.ven_id = a.ven_id		" + 
							"                                   				AND i.art_id = e.art_id), 0)/100))	" +
							"  END AS ventaDetalleImporteSinImp, 												" +
							"  CASE WHEN TRUNCATE(a.total, 0) = TRUNCATE((SELECT SUM(d.importeCon)				" +
							"											FROM detallev d							" +
							"											WHERE d.ven_id = a.ven_id				" +
							"											GROUP BY d.ven_id), 0) 					" +
							"  THEN																				" +
							"  		(d.importeCon)-((d.importeCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto)			" + 
							"													FROM detallevimpuesto i			" + 
							"                                                   WHERE i.ven_id = a.ven_id		" + 
							"                                                   AND i.art_id = e.art_id), 0)/100)))					" +
							"  ELSE																									" +
							"  		(d.importeNorCon-d.descTotal)-((d.importeNorCon-d.descTotal)/ (1 +(IFNULL((SELECT MAX(i.impuesto)	" + 
							"																FROM detallevimpuesto i					" + 
							"                                                                WHERE i.ven_id = a.ven_id				" + 
							"                                                                AND i.art_id = e.art_id), 0)/100)))	" +
							"  END AS ventaDetalleImpuesto,																			" +							
							"  d.monpreciosin AS monedaDetallePrecioSinIva, " +
							"  d.monpreciocon AS monedaDetallePrecioConIva, " +
							"  d.monimportesin AS monedaDetalleImporteSinIva, " +
							"  d.monimportecon AS monedaDetalleImporteConIva, " +
							"  CASE WHEN TRUNCATE(IFNULL(a.monTotal,0), 0) = TRUNCATE((SELECT SUM(IFNULL(d.monImporteCon,0))		" +
							"															FROM detallev d								" +
							"															WHERE d.ven_id = a.ven_id					" +
							"															GROUP BY d.ven_id), 0) 						" +
							"  THEN																									" +
							"  (d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto)												" + 
							"									FROM detallevimpuesto i												" + 
							"                                   WHERE i.ven_id = a.ven_id 											" + 
							"                                   AND i.art_id = e.art_id), 0)/100))									" +
							"  ELSE																									" +
							"  (d.monImporteNorCon-(d.descTotal/a.monTipoCambio))/ (1 +(IFNULL((SELECT MAX(i.impuesto)				" + 
							"																	FROM detallevimpuesto i				" + 
							"                                   								WHERE i.ven_id = a.ven_id 			" + 
							"                                   								AND i.art_id = e.art_id), 0)/100))	" +
							"  END AS monedaDetalleImporteSinImp, 																	" +
							"  CASE WHEN TRUNCATE(IFNULL(a.monTotal,0), 0) = TRUNCATE((SELECT SUM(IFNULL(d.monImporteCon,0))		" +
							"															FROM detallev d								" +
							"															WHERE d.ven_id = a.ven_id					" +
							"															GROUP BY d.ven_id), 0) 						" +
							"  THEN																									" +
							"  d.monImporteCon-((d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) 								" + 
							"													FROM detallevimpuesto i 							" + 
							"                                                   WHERE i.ven_id = a.ven_id 							" + 
							"                                                   AND i.art_id = e.art_id), 0)/100))) 				" +
							"  ELSE																									" +
							"  d.monImporteNorCon-(d.descTotal/a.monTipoCambio)-((d.monImporteNorCon-(d.descTotal/a.monTipoCambio)) " +
							"													/ (1 +(IFNULL((SELECT MAX(i.impuesto) 				" + 
							"																 FROM detallevimpuesto i 				" + 
							"                                                                WHERE i.ven_id = a.ven_id 				" + 
							"                                                                AND i.art_id = e.art_id), 0)/100))) 	" +
							"  END AS monedaDetalleImpuesto,																		" +
							"  d.ncr_id AS notaCreditoId, " +
							"  f.folioNC AS folioNC, " +	
							"  f.folio AS folio, " +
							"  d.descTotal AS descuentoTotal, " +
							"  d.precioCompra AS precioCompra, " +
							"  e.lote AS lote, " +
							"  d.importecompra AS importeCompra, " +
							"  e.tipo AS tipo " +
							" FROM venta a  " +
							"  JOIN detallev d " +
							"  ON a.ven_id = d.ven_id" +
							"  LEFT JOIN articulo e" +
							"  ON d.art_id = e.art_id" +
							"  LEFT JOIN notacredito f" +
							"  ON d.ncr_id = f.ncr_id" +
							"  LEFT JOIN categoria g " +
							"  ON e.cat_id = g.cat_id " +
							"  LEFT JOIN departamento h " +
							"  ON g.dep_id = h.dep_id " +
							"  WHERE a.ven_id in (:vList) " +
							//" and d.ncr_id is null " +
							"  ORDER BY a.ven_id, d.art_id;"; 
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
					query.setParameterList("vList", ids);
					List<Articulo> artList= query.list();
					
					//JAvila: Obtiene los articulos y lotes del kit
					sql = " 	SELECT a.ven_id AS idVenta, " + 
							"		  q.art_id AS articuloId,  " + 
							"		  q.clave AS articuloClave,  " + 
							"		  replace(q.descripcion, ',', '_') AS articuloDescripcion,  " + 
							"		  q.precio1 AS articuloPrecio1,  " + 
							"		  q.precio2 AS articuloPrecio2,  " + 
							"		  q.precio3 AS articuloPrecio3,  " + 
							"		  b.cantidad AS ventaDetalleCantidad, " + 
							"         q.lote AS lote, " +
							"		  b.unidad AS ventaDetalleUnidad, " + 
							"		  q.precioCompra AS precioCompra, " +
							"		  b.paquete AS paquete, " +
							"		  a.tipo AS tipo " +
							"	FROM detallev a" + 
							"	JOIN detallep b on a.art_id = b.paquete and a.ven_id=b.ven_id " + 
							"	JOIN articulo q on b.articulo = q.art_id " + 
							"	WHERE  a.ven_id in (:vList);"; 
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
					query.setParameterList("vList", ids);
					List<Articulo> artKitList= query.list();
					
					sql = "SELECT a.ven_id         AS idVenta,  " + 
							"	   d.articulo      AS articuloId,   " + 
							"	   d.clave         AS articuloClave,    " + 
							"	   l.numlote       AS loteNumero,  " + 
							"	   l.fechafab      AS loteFechaFabricacion,   " + 
							"	   l.fechacad      AS loteFechaCaducidad,   " + 
							"	   l.exisinicial   AS loteExistenciaInicial,   " + 
							"	   l.exisactual    AS loteExistenciaActual,  " + 
							"	   l.status        AS loteStatus,	  " + 
							"	   d.paquete       AS paquete, " +
							"	   k.cantidad      AS cantidad  " + 
							"	   FROM   venta a   " + 
							"	   JOIN detallep d  ON a.ven_id = d.ven_id " + 
							"	   LEFT JOIN detalleplote k  ON a.ven_id = k.ven_id " + 
							"			   AND d.articulo = k.articulo " + 
							"	   LEFT JOIN lote l ON k.lot_id = l.lot_id " + 
							"			  AND d.articulo = l.art_id " + 
							"	   WHERE   l.numlote is not null and a.ven_id in (:vList) " + 
							"	   ORDER BY a.ven_id, d.articulo; ";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(DetalleLote.class));
					query.setParameterList("vList", ids);
					List<DetalleLote> lotListKit= query.list();
					//JAvila: Fin
					
					sql = "SELECT a.ven_id      AS idVenta, " + 
						       "d.art_id        AS articuloId, " + 
						       "d.clave			AS articuloClave, " +  
						       "h.nombre        AS ventaDetalleImpuestoNombre, " + 
						       "h.impuesto      AS ventaDetalleImpuestoValor, " + 
						       "h.total			AS ventaDetalleImpuestoTotal " +
						       "FROM   venta a " + 
						       "JOIN detallev d " + 
						         "ON a.ven_id = d.ven_id " + 
						       "LEFT JOIN articulo e " + 
						              "ON d.art_id = e.art_id " + 
						       "LEFT JOIN detallevimpuesto h " + 
						              "ON a.ven_id = h.ven_id " + 
									  "AND d.art_id = h.art_id " + 
							   "WHERE  a.ven_id in (:vList) " +
							   " AND h.total >= 0 " +
							   "ORDER BY a.ven_id, d.art_id;"; 
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(ArticuloImpuesto.class));
					query.setParameterList("vList", ids);
					List<ArticuloImpuesto> artInvList= query.list();
					
					sql = "SELECT a.ven_id        AS idVenta, " + 
							   "j.nombre        AS tipoPagoNombre, " + 
						       "j.abr           AS tipoPagoClave, " + 
						       "i.total         AS ventaTipoPagoTotal " +
						       "FROM   venta a " + 
							   "LEFT JOIN ventatipopago i " + 
						              "ON a.ven_id = i.ven_id " + 
						       "LEFT JOIN tipopago j " + 
						              "ON i.tpa_id = j.tpa_id " + 
						       "WHERE  a.ven_id in (:vList) " +
						       "ORDER BY a.ven_id; ";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
					query.setParameterList("vList", ids);
					List<TipoPago> pagoList= query.list();
					
//					sql = " SELECT a.ven_id			AS idVenta, " +
//							" '" + AppConstants.TIPO_PAGO_MONEDERO_NOMBRE + "'  AS tipoPagoNombre, " +
//							" '" + AppConstants.TIPO_PAGO_MONEDERO_CLAVE + "'   AS tipoPagoClave,  " +
//							" ((SELECT SUM(x.importeNorCon) FROM detallev x WHERE x.ven_id = a.ven_id GROUP BY x.ven_id)  - d.importeCon - d.descTotal)	AS ventaTipoPagoTotal " +
//							" FROM venta a " +
//							" JOIN detallev d " +
//							" 	ON a.ven_id = d.ven_id " +
//							" WHERE  a.ven_id in (:vList) " +
//							" AND (d.importeNorCon - d.importeCon - d.descTotal) > 0 " +
//							" GROUP BY a.ven_id " +
//							" ORDER BY a.ven_id; ";
//					
//					query = session.createSQLQuery(sql);
//					query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
//					query.setParameterList("vList", ids);
//					List<TipoPago> pagoMonList= query.list();
					
					sql = "SELECT a.ven_id        AS idVenta, " +
						       "d.art_id        AS articuloId, " + 
						       "d.clave         AS articuloClave, " +  
						       "l.numlote       AS loteNumero, " +
						       "l.fechafab      AS loteFechaFabricacion, " + 
						       "l.fechacad      AS loteFechaCaducidad,  " +
						       "l.exisinicial   AS loteExistenciaInicial, " + 
						       "l.exisactual    AS loteExistenciaActual, " +
						       "l.status        AS loteStatus,	 " +
						       "k.cantidad      AS cantidad " +
							   "FROM   venta a  " +
						       "JOIN detallev d  " +
						         "ON a.ven_id = d.ven_id  " +
							   "LEFT JOIN detallevlote k " + 
						              "ON a.ven_id = k.ven_id  " +
						               "AND d.art_id = k.art_id " + 
						       "LEFT JOIN lote l " + 
						              "ON k.lot_id = l.lot_id " + 
						              "AND d.art_id = l.art_id " + 
							   "WHERE   l.numlote is not null and a.ven_id in (:vList) " +
							   "ORDER BY a.ven_id, d.art_id; ";
					
					query = session.createSQLQuery(sql);
					query.setResultTransformer(Transformers.aliasToBean(DetalleLote.class));
					query.setParameterList("vList", ids);
					
					Cliente cli = null;
					List<DetalleLote> lotList= query.list();
					List<Articulo> aList = null;
					List<ArticuloImpuesto> aiList = null;
					List<TipoPago> tpList = null;
					List<DetalleLote> dlList = null;					
					List<Articulo> aKitList = null;
					List<DetalleLote> aLotKitList = null;
					
					for(Sales s : rows) {
						
						s.setEmpresa(cmp.get(0));

						for(Cliente o : cliList) {
							if(o.getClienteId().intValue() == s.getIdCliente().intValue()) {							
								cli = o;
								break;
							}
						}
						s.setCliente(cli);						
						
						aList = new ArrayList<Articulo>();
						for(Articulo o :  artList) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								aList.add(o);
							}
						}
						s.setArticulo(aList);
						
						//JAvila: Obtiene los articulos y lotes del kit
						aKitList = new ArrayList<Articulo>();
						for(Articulo o :  artKitList) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								aKitList.add(o);
							}
						}
						if(aKitList.size() > 0) {
							s.setArticuloPaquete(aKitList);
						}
						
						aLotKitList = new ArrayList<DetalleLote>();
						for(DetalleLote o :  lotListKit) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								aLotKitList.add(o);
							}
						}
						s.setDetalleLotePaquete(aLotKitList);
						//JAvila: FIN
						
						
						aiList = new ArrayList<ArticuloImpuesto>();
						for(ArticuloImpuesto o :  artInvList) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								aiList.add(o);
							}
						}
						s.setArticuloImpuesto(aiList);
						
						tpList = new ArrayList<TipoPago>();
						if(pagoList != null && !pagoList.isEmpty()) {
							for(TipoPago o :  pagoList) {
								if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
									tpList.add(o);
								}
							}	
						}
//						if(pagoMonList != null && !pagoMonList.isEmpty()) {
//							for(TipoPago o :  pagoMonList) {
//								if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
//									tpList.add(o);
//								}
//							}	
//						}
						s.setTipoPago(tpList);	
						
						dlList = new ArrayList<DetalleLote>();
						for(DetalleLote o :  lotList) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								dlList.add(o);
							}
						}
						s.setDetalleLote(dlList);
						
					}
					salesDTO.setVentasCanceladas(rows);
				}
				
			}
			return salesDTO;
	}
	
	//RETREIVE CREDIT NOTES FROM OUT OF DATE: Notas de cr�dito extempor�neas
	@SuppressWarnings({ "unchecked" })
	public SalesDTO getOutOfDateCreditNotes(String sucursal, String requestType, String[] params){
		
		SalesDTO salesDTO = new SalesDTO();
		Session session = sessionFactory.getCurrentSession();
		List<Sales> rows = null;
		Empresa emp = null;
		String sqlInit = "";
		int idCorteCaja = 0;
		String fromDate = "";
		String toDate = "";
		
		if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			idCorteCaja = Integer.valueOf(params[0]);
			
		} else if(AppConstants.REQUEST_TYPE_TIME.equals(requestType)) {
			fromDate = params[0];
			toDate = params[1];
		}
		
		//Obtiene las ventas correspondientes al periodo
		if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			
			sqlInit = "SELECT distinct ven_id " + 
					"from venta a " + 
					"join resumencortecaja b " + 
					"on a.rcc_id = b.rcc_id " + 
					"join cortecaja c " + 
					"on b.cor_id = c.cor_id and a.caj_id = c.caj_id " + 
					"where b.rcc_id = " + idCorteCaja + " and a.total > 0 ";			
		} else {
			
			sqlInit = " select distinct a.ven_id " +
					"	from historial h " +
					"   join venta a on h.id = a.ven_id " +
					"	where h.tabla = 'Venta'  " +
					"	and h.movimiento = 0 " +
					"   and h.fecha between '" + fromDate + "' and '" + toDate + "' " +
					"	and a.total > 0; ";			
		}		
		
		SQLQuery query = session.createSQLQuery(sqlInit);
		List<BigInteger> rowInitListVentas = query.list();
		
		List<Integer> idsVen = new ArrayList<Integer>();
		if(rowInitListVentas != null && !rowInitListVentas.isEmpty()) {
			for(BigInteger s : rowInitListVentas) {
				idsVen.add(s.intValue());
			}
		}

		//Obtiene todas las Notas de Credito correspondientes al periodo	
		if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			sqlInit = "SELECT a.ncr_id " +
					"	FROM notacredito a " + 
					"	join resumencortecaja b   " + 
					"	on a.rcc_id = b.rcc_id " + 
					"	where b.rcc_id = " + idCorteCaja + 
					"   and a.total > 0 ";	
		} else {
			sqlInit = "SELECT a.ncr_id " +
					"	FROM notacredito a " +
					"	JOIN historial h on a.ncr_id = h.id " +
				 	"	WHERE h.tabla = 'NotaCredito' " +
					"	AND h.movimiento = 0 " +							
					" 	AND h.fecha BETWEEN '" + fromDate + "' AND '" + toDate + "' " +						 
					"   AND a.total > 0 ";
		}
		query = session.createSQLQuery(sqlInit);		
		List<BigInteger> rowInitListNC = query.list();
		
		if(rowInitListNC != null && !rowInitListNC.isEmpty()) {			
			List<Integer> ids = new ArrayList<Integer>();
			for(BigInteger s : rowInitListNC) {
				ids.add(s.intValue());
			}
			
			String filtroVentas = "";
			if(idsVen != null && !idsVen.isEmpty()) {
				filtroVentas = " and a.ven_id not in (:vList) ";
			}
				//NOTA DE CREDITO EXTEMPORANEA
				String sql = "select c.rcc_id as idResumenCaja, a.ven_id as idVenta,c.fcf_id as idFactura, a.tic_id as idTicket, a.not_id as idNotaVenta, a.caj_id as idCaja, " + 
							"       c.ncr_id as idNotaCredito, e.nombre as descripcionCaja, v.nombre as nombreVendedor, c.fecha as fechaNC, c.folio as folio, c.serieFolio as serieFolio, " + 
							"       c.nombreE as emisor, c.rfcE as rfcEmisor, c.nombreC as receptor, c.rfcC as rfcReceptor, " + 
							"       c.folioNC as folioNC, c.monAbr as moneda, c.monTipoCambio as tipoCambio, c.subtotal as subTotal, " + 
							"       c.descuento as descuento, c.total as total, c.formaPago as formaPago, c.metodoPago as metodoPago, " +
							"		c.monSubtotal AS monedaSubtotal, c.monDescuento AS monedaDescuento, c.monTotal AS monedaTotal, c.monCambio AS monedaCambio, " +
							"       c.uuid as uuid, c.usoCfdi as usoCfdi, c.devAplicada as devolucionAplicada, f.ven_id as idVentaAplicada, c.status as status, l.precio as precioCliente " + 
							"       FROM venta a " + 
							"       JOIN notacredito c on (a.tic_id = c.tic_id OR a.not_id = c.not_id OR a.rem_id = c.rem_id)" +
							"       LEFT JOIN caja e on c.caj_id = e.caj_id " +
							"		LEFT JOIN detallev f on c.ncr_id = f.ncr_id " +
							" 		LEFT JOIN vendedor v on c.vnd_id = v.vnd_id  " +
							" 		LEFT JOIN cliente l on c.cli_id = l.cli_id " +
							" WHERE c.ncr_id in (:ncList) " + filtroVentas + 
							" ORDER BY a.ven_id, c.ncr_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(NotaCredito.class));
				query.setParameterList("ncList", ids);
				if(idsVen != null && !idsVen.isEmpty()) {
					query.setParameterList("vList", idsVen);
				}
				List<NotaCredito> ncTicketList= query.list();				
				
				// NOTA DE CREDITO EXTEMPORANEA POR FACTURA
				sql = " select d.rcc_id as idResumenCaja, a.ven_id as idVenta, d.fcf_id as idFactura, a.tic_id as idTicket, a.not_id as idNotaVenta, a.caj_id as idCaja, " +
						"		d.ncr_id as idNotaCredito, e.nombre as descripcionCaja, v.nombre as nombreVendedor, d.fecha as fechaNC, c.fechaCert as fechaFactura, d.folio as folio, d.serieFolio as serieFolio, " +
						"		d.nombreE as emisor, d.rfcE as rfcEmisor, d.nombreC as receptor, d.rfcC as rfcReceptor, " +
						"		d.folioNC as folioNC, d.monAbr as moneda, d.monTipoCambio as tipoCambio, d.subtotal as subTotal, " +
						"		d.descuento as descuento, d.total as total, d.formaPago as formaPago, d.metodoPago as metodoPago, " +
						"		d.monSubtotal AS monedaSubtotal, d.monDescuento AS monedaDescuento, d.monTotal AS monedaTotal, d.monCambio AS monedaCambio, " +
						"		d.uuid as uuid, d.usoCfdi as usoCfdi, d.devAplicada as devolucionAplicada, f.ven_id as idVentaAplicada, d.status as status, l.precio as precioCliente " +
						"		FROM notacredito d " +
						"		JOIN facturacfdi c on d.fcf_id = c.fcf_id " +
						"		JOIN facturacfdiven b on d.fcf_id = b.fcf_id " +
						"		JOIN detallen n on d.ncr_id = n.ncr_id and b.ven_id = n.ven_id  " +
						"		JOIN venta a on n.ven_id = a.ven_id " +
						"		LEFT JOIN caja e on d.caj_id = e.caj_id " + 		
						"		LEFT JOIN detallev f on d.ncr_id = f.ncr_id " +  		
						"		LEFT JOIN vendedor v on d.vnd_id = v.vnd_id " +
						" 		LEFT JOIN cliente l on d.cli_id = l.cli_id " +
						" 		WHERE d.ncr_id in (:ncList) "  + filtroVentas +
						" 		ORDER BY a.ven_id, d.ncr_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(NotaCredito.class));
				query.setParameterList("ncList", ids);
				if(idsVen != null && !idsVen.isEmpty()) {
					query.setParameterList("vList", idsVen);
				}				
				List<NotaCredito> ncFactList= query.list();
				
				ids = new ArrayList<Integer>();
				if(ncTicketList != null && !ncTicketList.isEmpty()) {
					for(NotaCredito n : ncTicketList) {
						ids.add(n.getIdNotaCredito().intValue());
					}
				}
				if(ncFactList != null && !ncFactList.isEmpty()) {
					for(NotaCredito n : ncFactList) {
						ids.add(n.getIdNotaCredito().intValue());
					}	
				}				
				if(ids.isEmpty()) {
					return null;
				}
				
				sql = "SELECT a.emp_id AS empId, " + 
						   "a.nombre  AS nombre, " + 
					       "a.nombreFiscal  AS nombreFiscal, " + 
					       "a.cuenta  AS cuenta, " +
					       "a.rfc  AS rfc " +
					       "FROM empresa a " ;
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Empresa.class));
				List<Empresa> empresa = query.list();

				if(empresa != null) {			
					emp = empresa.get(0);
					
				sql = "select a.ncr_id as idNotaCredito, a.imp_id as idImpuesto, " + 
						"       a.total as totalImpuesto, a.subtotal as subTotal, a.tipoFactor as tipoFactor, " + 
						"       a.nombreImp as nombreImpuesto, a.valor as porcentajeImpuesto " + 
						"       FROM notacreditoimp a " + 
						" WHERE  a.ncr_id in (:ncList) " +
						" ORDER BY a.ncr_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(NotaCreditoImpuestos.class));
				query.setParameterList("ncList", ids);
				List<NotaCreditoImpuestos> ncImpList= query.list();				
				
				 sql = " SELECT c.cli_id AS idCliente, " +
				 " IFNULL(e.nub_id, 0) AS clienteNubeId, " +
				 " replace(c.nombre, ',', '_') AS nombreCliente, " +
				 " c.curp AS curp, " +
				 " c.diasCredito AS diasCredito, " +
				 " c.clave AS claveCliente, " +
				 " c.precio AS precioCliente, " +
				 " v.nombre AS nombreVendedor, " +
				 " b.tic_id AS idTicket, " +
				 " n.not_id AS idNotaVenta, " +
				 " a.ncr_id AS idVenta,  " +
				 " a.fecha AS ventaFecha,  " +
				 " d.nombre AS ventaCaja, " +
				 " a.subtotal AS ventaSubtotal,  " +
				 " a.descuento AS ventaDescuento,  " +
				 " a.total AS ventaTotal,  " +
				 " a.status AS ventaStatus,  " +
				 " a.monAbr AS ventaMoneda,  " +
				 " a.monTipoCambio AS ventaTipoCambio, " +
				 " a.monSubtotal AS monedaSubtotal,  " + 
				 " a.monDescuento AS monedaDescuento,  " + 
				 " a.monTotal AS monedaTotal,  " +
				 " a.monCambio AS monedaCambio, " +
				 " a.rcc_id AS idResumenCaja, " +
				 " a.can_caj_id AS idCancelaCaja, " +
				 " a.can_rcc_id AS idCancelaResumenCaja " +
				 " FROM notacredito a  " +
				 "  LEFT JOIN facturacfdiven f " +
				 "  	ON f.fcf_id is not null IS NOT NULL " +
				 "  	AND a.fcf_id = f.fcf_id " +
				 "		AND f.ven_id IN (SELECT DISTINCT(g.ven_id) FROM detallen g where g.ncr_id = a.ncr_id) " +
				 "  LEFT JOIN venta s " +
				 " 	ON f.ven_id = s.ven_id " +
				 "  LEFT JOIN ticket b " +
				 "  	ON b.tic_id IS NOT NULL " +
				 "  	AND (a.tic_id = b.tic_id OR s.tic_id = b.tic_id)" +
				 "  LEFT JOIN nota n " +
				 "  	ON n.not_id IS NOT NULL " +
				 "  	AND (a.not_id = n.not_id OR s.not_id = n.not_id)" +
				 "  LEFT JOIN remision r " +
				 "  	ON r.rem_id IS NOT NULL " +
				 "  	AND (a.rem_id = r.rem_id OR s.rem_id = r.rem_id) " +
				 "  LEFT JOIN cliente c " +
				 "  	ON (b.cli_id = c.cli_id OR n.cli_id = c.cli_id OR r.cli_id = c.cli_id) " +
				 "  LEFT JOIN nubeid e " +
				 "  	ON c.cli_id = e.id AND e.entidad = 'Cliente' " +
				 "  LEFT JOIN caja d " +
				 "  	ON a.caj_id = d.caj_id " +
				 "  LEFT JOIN vendedor v  " +
				 "  	ON a.vnd_id = v.vnd_id  " +
				 " WHERE a.ncr_id in (:ncList) " +
				 " ORDER BY a.ncr_id;";

				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Sales.class));
				query.setParameterList("ncList", ids);
				rows = query.list();
				
				sql = "SELECT " +
						" a.ncr_id AS idVenta, " +
						" c.cli_id AS clienteId, " +
						" replace(c.nombre, ',', '_') AS clienteNombre,  " +
						" c.rfc AS clienteRFC, " +
						" c.curp AS clienteCURP, " +
						" c.domicilio AS clienteDomicilio, " +
						" c.colonia AS clienteColonia, " +
						" c.noExt AS clienteNoExt, " +
						" c.noInt AS clienteNoInt, " +
						" c.localidad AS clienteLocalidad, " +
						" c.ciudad AS clienteCiudad, " +
						" c.estado AS clienteEstado, " +
						" c.pais AS clientePais, " +
						" c.codigoPostal AS clienteCodigoPostal, " +
						" c.limite AS clienteLimite, " +
						" c.diasCredito AS clienteDiasCredito, " +
						" c.clave AS clienteClave " +
						" FROM notacredito a   " +
						" JOIN cliente c ON a.cli_id = c.cli_id " +
						" WHERE a.ncr_id in (:ncList)  " +
						"  ORDER BY a.ncr_id; ";

				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Cliente.class));
				query.setParameterList("ncList", ids);
				List<Cliente> cliList= query.list(); 
				
				sql = " SELECT  a.ncr_id AS idVenta,  " +
						" c.cli_id AS clienteId,  " +
						" replace(c.nombre, ',', '_') AS clienteNombre,   " +
						" c.rfc AS clienteRFC,  c.curp AS clienteCURP,  " +
						" c.domicilio AS clienteDomicilio,  " +
						" c.colonia AS clienteColonia,  " +
						" c.noExt AS clienteNoExt,  " +
						" c.noInt AS clienteNoInt,  " +
						" c.localidad AS clienteLocalidad, " +
						" c.ciudad AS clienteCiudad,  " +
						" c.estado AS clienteEstado,  " +
						" c.pais AS clientePais,  " +
						" c.codigoPostal AS clienteCodigoPostal, " +
						" c.limite AS clienteLimite,  " +
						" c.diasCredito AS clienteDiasCredito,  " +
						" c.clave AS clienteClave " +
						" FROM notacredito a " +
						" JOIN cliente c   ON a.cli_id = c.cli_id " +
						" WHERE a.ncr_id in (:ncList)" +
						" ORDER BY a.ncr_id;";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Cliente.class));
				query.setParameterList("ncList", ids);
				List<Cliente> cliNCFList= query.list(); 
				
				sql = " SELECT a.ncr_id AS idVenta, " +
						"  d.art_id AS articuloId, " +
						"  d.clave AS articuloClave, " +
						"  replace(d.descripcion, ',', '_') AS articuloDescripcion, " +
						"  g.nombre AS categoria, " +
						"  h.nombre AS departamento, " +
						"  e.precio1 AS articuloPrecio1, " +
						"  e.precio2 AS articuloPrecio2, " +
						"  e.precio3 AS articuloPrecio3, " +
						"  d.cantidad AS ventaDetalleCantidad, " +
						"  d.unidad AS ventaDetalleUnidad, " +
						"  d.preciosin AS ventaDetallePrecioSinIva, " +
						"  d.preciocon AS ventaDetallePrecioConIva, " +
						"  d.importesin AS ventaDetalleImporteSinIva, " +
						"  d.importecon AS ventaDetalleImporteConIva, " +
						"  (d.importeCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) " + 
						"									FROM detallenimpuesto i " + 
						"                                   WHERE a.ncr_id = i.ncr_id " +
						"                                   AND d.ven_id = i.ven_id " +
						"                                   AND e.art_id = i.art_id), 0)/100)) AS ventaDetalleImporteSinImp, " +
						"	d.importeCon-((d.importeCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) " + 
						"												FROM detallenimpuesto i " + 
						"                                               WHERE a.ncr_id = i.ncr_id " +
						"                                               AND d.ven_id = i.ven_id " +
						"                                               AND e.art_id = i.art_id), 0)/100))) AS ventaDetalleImpuesto, " +												
						"  d.monpreciosin AS monedaDetallePrecioSinIva, " +
						"  d.monpreciocon AS monedaDetallePrecioConIva, " +
						"  d.monimportesin AS monedaDetalleImporteSinIva, " +
						"  d.monimportecon AS monedaDetalleImporteConIva, " +
						"  (d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) " + 
						"									FROM detallenimpuesto i " + 
						"                                   WHERE a.ncr_id = i.ncr_id " +
						"                                   AND d.ven_id = i.ven_id " +
						"                                   AND e.art_id = i.art_id), 0)/100)) AS monedaDetalleImporteSinImp, " +
						"	d.monImporteCon-((d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) " + 
						"													FROM detallenimpuesto i " + 
						"                                                   WHERE a.ncr_id = i.ncr_id " +
						"                                                   AND d.ven_id = i.ven_id " +
						"                                                   AND e.art_id = i.art_id), 0)/100))) AS monedaDetalleImpuesto, " +
						"  d.ncr_id AS notaCreditoId, " +
						"  a.folioNC AS folioNC, " +
						"  a.folio AS folio, " +
						"  d.descTotal AS descuentoTotal, " +
						"  d.precioCompra AS precioCompra, " +
						"  e.lote AS lote, " +
						"  d.importecompra AS importeCompra, " +
						"  e.tipo AS tipo " +
						" FROM notacredito a  " +
						"  JOIN detallen d " +
						"  ON a.ncr_id = d.ncr_id " +
						"  LEFT JOIN articulo e " +
						"  ON d.art_id = e.art_id" +
						"  LEFT JOIN categoria g " +
						"  ON e.cat_id = g.cat_id " +
						"  LEFT JOIN departamento h " +
						"  ON g.dep_id = h.dep_id " +
						" WHERE a.ncr_id in (:ncList) " +
						" AND (d.dv_ven_id is not null OR d.dv_art_id is not null) " +
						" ORDER BY a.ncr_id, d.art_id;"; 
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
				query.setParameterList("ncList", ids);
				List<Articulo> artList= query.list();

				//Articulos por descuento
				sql = " SELECT a.ncr_id AS idVenta,  " + 
						"   d.art_id AS articuloId,  " + 
						"   d.clave AS articuloClave,  " + 
						"	replace(d.descripcion, ',', '_') AS articuloDescripcion, " +
						"	g.nombre AS categoria, " +
						"	h.nombre AS departamento, " +
						"   (e.precio1 * a.total)/c.total AS articuloPrecio1,  " + 
						"   (e.precio2 * a.total)/c.total AS articuloPrecio2,  " + 
						"   (e.precio3 * a.total)/c.total AS articuloPrecio3,  " + 
						"   d.cantidad AS ventaDetalleCantidad,  " + 
						"   d.unidad AS ventaDetalleUnidad,  " + 
						"   (d.preciosin * a.total)/c.total AS ventaDetallePrecioSinIva,  " + 
						"   (d.preciocon * a.total)/c.total AS ventaDetallePrecioConIva,  " + 
						"   (d.importesin * a.total)/c.total AS ventaDetalleImporteSinIva,  " + 
						"   (d.importecon * a.total)/c.total AS ventaDetalleImporteConIva,  " +
						"  CASE WHEN TRUNCATE(c.total, 0) = TRUNCATE((SELECT SUM(x.importeCon)												" +
						"											FROM detallev x															" +
						"											WHERE x.ven_id = c.ven_id												" +
						"											GROUP BY x.ven_id), 0) 													" +
						"  THEN																												" +
						"  		ROUND(((d.importeCon)/ (1 + (IFNULL((SELECT MAX(i.impuesto)													" + 
						"									FROM detallevimpuesto i															" + 
						"                                   WHERE i.ven_id = d.ven_id														" + 
						"                                   AND i.art_id = d.art_id), 0)/100)) * a.total)/c.total, 3)						" +						
						"  ELSE																												" +
						"  		ROUND(((d.importeNorCon-d.descTotal)/ (1 + (IFNULL((SELECT MAX(i.impuesto)									" + 
						"													FROM detallevimpuesto i											" + 
						"                                   				WHERE i.ven_id = d.ven_id										" + 
						"                                   				AND i.art_id = d.art_id), 0)/100)) * a.total)/c.total, 3)		" +
						"  END AS ventaDetalleImporteSinImp, 																				" +
						"  CASE WHEN TRUNCATE(c.total, 0) = TRUNCATE((SELECT SUM(x.importeCon)												" +
						"											FROM detallev x															" +
						"											WHERE x.ven_id = c.ven_id												" +
						"											GROUP BY x.ven_id), 0) 													" +
						"  THEN																												" +
						"  		ROUND((((d.importeCon)-((d.importeCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto)									" + 
						"													FROM detallevimpuesto i											" + 
						"                                                   WHERE i.ven_id = d.ven_id										" + 
						"                                                   AND i.art_id = d.art_id), 0)/100)))) * a.total)/c.total, 3)		" +
						"  ELSE																												" +
						"  		ROUND((((d.importeNorCon-d.descTotal)-((d.importeNorCon-d.descTotal)/ (1 +(IFNULL((SELECT MAX(i.impuesto)	" + 
						"																FROM detallevimpuesto i								" + 
						"                                                                WHERE i.ven_id = d.ven_id							" + 
						"                                                                AND i.art_id = d.art_id), 0)/100)))) * a.total)/c.total, 3)	" +
						"  END AS ventaDetalleImpuesto,																						" +
						"   (d.monpreciosin * a.total)/c.total AS monedaDetallePrecioSinIva,  " +
						"   (d.monpreciocon * a.total)/c.total AS monedaDetallePrecioConIva,  " + 
						"   (d.monimportesin * a.total)/c.total AS monedaDetalleImporteSinIva,  " +
						"   (d.monimportecon * a.total)/c.total AS monedaDetalleImporteConIva,  " +
						"  CASE WHEN TRUNCATE(IFNULL(c.monTotal,0), 0) = TRUNCATE((SELECT SUM(IFNULL(d.monImporteCon,0))		" +
						"															FROM detallev d								" +
						"															WHERE d.ven_id = c.ven_id					" +
						"															GROUP BY d.ven_id), 0) 						" +
						"  THEN																									" +
						"  ROUND(((d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto)										" + 
						"									FROM detallevimpuesto i												" + 
						"                                   WHERE i.ven_id = d.ven_id 											" + 
						"                                   AND i.art_id = d.art_id), 0)/100))* a.total)/c.total, 3)			" +
						"  ELSE																									" +
						"  ROUND(((d.monImporteNorCon-(d.descTotal/a.monTipoCambio))/ (1 +(IFNULL((SELECT MAX(i.impuesto)		" + 
						"																	FROM detallevimpuesto i				" + 
						"                                   								WHERE i.ven_id = d.ven_id 			" + 
						"                                   								AND i.art_id = d.art_id), 0)/100))* a.total)/c.total, 3)	" +
						"  END AS monedaDetalleImporteSinImp, 																	" +
						"  CASE WHEN TRUNCATE(IFNULL(c.monTotal,0), 0) = TRUNCATE((SELECT SUM(IFNULL(d.monImporteCon,0))		" +
						"															FROM detallev d								" +
						"															WHERE d.ven_id = c.ven_id					" +
						"															GROUP BY d.ven_id), 0) 						" +
						"  THEN																									" +
						"  ROUND(((d.monImporteCon-((d.monImporteCon)/ (1 +(IFNULL((SELECT MAX(i.impuesto) 						" + 
						"													FROM detallevimpuesto i 							" + 
						"                                                   WHERE i.ven_id = d.ven_id 							" + 
						"                                                   AND i.art_id = d.art_id), 0)/100)))) * a.total)/c.total, 3)	" +
						"  ELSE																									" +
						"  ROUND(((d.monImporteNorCon-(d.descTotal/a.monTipoCambio)-((d.monImporteNorCon-(d.descTotal/a.monTipoCambio)) " +
						"													/ (1 +(IFNULL((SELECT MAX(i.impuesto) 				" + 
						"																 FROM detallevimpuesto i 				" + 
						"                                                                WHERE i.ven_id = d.ven_id 				" + 
						"                                                                AND i.art_id = d.art_id), 0)/100))))* a.total)/c.total, 3)	" +
						"  END AS monedaDetalleImpuesto,																		" +						
						"   a.ncr_id AS notaCreditoId,  " + 
						"   a.folioNC AS folioNC,  " +
						"   a.folio AS folio, " +
						"   (d.descTotal * a.total)/c.total AS descuentoTotal,  " + 
						"   (d.precioCompra * a.total)/c.total AS precioCompra,  " + 
						"   e.lote AS lote,  " + 
						"   (d.importecompra * a.total)/c.total AS importeCompra,  " + 
						"   e.tipo AS tipo " +
						"  FROM notacredito a  " + 
						"  JOIN detallen b  " + 
						" 	ON a.ncr_id = b.ncr_id  " + 
						"  JOIN venta c  " + 
						" 	ON b.ven_id = c.ven_id  " + 
						"  JOIN detallev d  " + 
						" 	ON c.ven_id = d.ven_id  " + 
						"  LEFT JOIN articulo e  " + 
						"   ON d.art_id = e.art_id  " +
						"  LEFT JOIN categoria g " +
						"  ON e.cat_id = g.cat_id " +
						"  LEFT JOIN departamento h " +
						"  ON g.dep_id = h.dep_id " +
						"  WHERE a.ncr_id in (:ncList) " + 
						"  AND dv_ven_id is null  " + 
						"  AND dv_art_id is null " + 
						"  ORDER BY idVenta, articuloId;";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
				query.setParameterList("ncList", ids);
				List<Articulo> adList = query.list();
				if(adList != null && adList.size() > 0) {
					if(artList == null) {
						artList = new ArrayList<Articulo>(); 
					}
					for(Articulo ad : adList) {
						ad.setArticuloDescuento(true);
						artList.add(ad);
					}					
				}
				
				sql = "SELECT a.ncr_id      AS idVenta, " + 
						   "a.ncr_id		AS notaCreditoId, " +
					       "d.art_id        AS articuloId, " +					       
					       "d.clave			AS articuloClave, " +  
					       "h.nombre        AS ventaDetalleImpuestoNombre, " + 
					       "h.impuesto      AS ventaDetalleImpuestoValor, " + 
					       "h.total			AS ventaDetalleImpuestoTotal " +
					       "FROM   notacredito a " + 
					       "JOIN detallen d " + 
					         "ON a.ncr_id = d.ncr_id " + 
					       "LEFT JOIN articulo e " + 
					              "ON d.art_id = e.art_id " + 
					       "LEFT JOIN detallenimpuesto h " + 
					              "ON a.ncr_id = h.ncr_id " + 
								  "AND d.art_id = h.art_id " + 
						   "WHERE  a.ncr_id in (:ncList) " +
						   " AND h.total >= 0 " +
						   "ORDER BY a.ncr_id, d.art_id;";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(ArticuloImpuesto.class));
				query.setParameterList("ncList", ids);
				List<ArticuloImpuesto> artImpList= query.list();
				
				//Impuesto Articulos por descuento
				sql = "  SELECT a.ncr_id  AS idVenta, " +	
						" a.ncr_id		  AS notaCreditoId, " +
						" d.art_id        AS articuloId, " +						
						" d.clave		  AS articuloClave, " + 
						" e.nombre        AS ventaDetalleImpuestoNombre, " + 
						" e.impuesto      AS ventaDetalleImpuestoValor, " + 
						" (e.total * a.total)/c.total AS ventaDetalleImpuestoTotal " + 
						" FROM notacredito a " + 
						" JOIN detallen b " + 
						"	 ON a.ncr_id = b.ncr_id " + 
						" JOIN venta c " + 
						"	 ON b.ven_id = c.ven_id " + 
						" JOIN detallev d " + 
						"    ON c.ven_id = d.ven_id " + 
						" JOIN detallevimpuesto e " + 
						"     ON d.ven_id = e.ven_id " + 
						"     AND d.art_id = e.art_id " + 
						" WHERE  a.ncr_id in (:ncList) " + 
						"   AND dv_ven_id is null  " + 
						"   AND dv_art_id is null  " +
						" 	AND e.total >= 0 " +
						" ORDER BY idVenta, articuloId;";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(ArticuloImpuesto.class));
				query.setParameterList("ncList", ids);
				List<ArticuloImpuesto> aidList = query.list();
				if(aidList != null && aidList.size() > 0) {
					if(artImpList == null) {
						artImpList = new ArrayList<ArticuloImpuesto>(); 
					}					
					artImpList.addAll(aidList);
				}
				
				sql = " SELECT a.ncr_id      AS idNotaCredito, " +
						" j.nombre        AS tipoPagoNombre, " +
						" j.abr           AS tipoPagoClave, " +
						" i.total         AS ventaTipoPagoTotal " +
						" FROM   notacredito a " +
						" LEFT JOIN notacreditotipopago i " +
						"       ON a.ncr_id = i.ncr_id " +
						" LEFT JOIN tipopago j " +
						"       ON i.tpa_id = j.tpa_id " +
						" WHERE  a.ncr_id in (:ncList) " +
						" ORDER BY a.ncr_id;";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
				query.setParameterList("ncList", ids);
				List<TipoPago> pagoList= query.list();
				
				sql = " SELECT c.ncr_id      AS idNotaCredito, " +
						" p.nombre        AS tipoPagoNombre, " +
						" p.abr           AS tipoPagoClave, " +
						" c.total         AS ventaTipoPagoTotal " +
						" FROM creditoclientenotcre c " +
						" LEFT JOIN tipopago p ON p.tpa_id = 3 " +
						" WHERE  c.ncr_id in (:ncList) " +
						" ORDER BY c.ncr_id;";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
				query.setParameterList("ncList", ids);
				List<TipoPago> pagoCreditoList= query.list();
				
//				sql = " SELECT a.tic_id			AS idVenta, " +
//						" '" + AppConstants.TIPO_PAGO_MONEDERO_NOMBRE + "'  AS tipoPagoNombre, " +
//						" '" + AppConstants.TIPO_PAGO_MONEDERO_CLAVE + "'   AS tipoPagoClave,  " +
//						" ((SELECT SUM(x.importeNorCon) FROM detallev x WHERE x.ven_id = a.ven_id GROUP BY x.ven_id)  - d.importeCon - d.descTotal)	AS ventaTipoPagoTotal " +
//						" FROM venta a " +
//						" JOIN detallev d " +
//						" 	ON a.ven_id = d.ven_id " +
//						" WHERE  a.ven_id in (:vList) " +
//						" AND (d.importeNorCon - d.importeCon - d.descTotal) > 0 " +
//						" GROUP BY a.ven_id " +
//						" ORDER BY a.ven_id; ";
//				
//				query = session.createSQLQuery(sql);
//				query.setResultTransformer(Transformers.aliasToBean(TipoPago.class));
//				query.setParameterList("vList", venIds);
//				List<TipoPago> pagoMonList= query.list();				
				
				sql = "SELECT a.ncr_id        AS idVenta, " +
					       "d.art_id        AS articuloId, " + 
					       "d.clave         AS articuloClave, " +  
					       "l.numlote       AS loteNumero, " +
					       "l.fechafab      AS loteFechaFabricacion, " + 
					       "l.fechacad      AS loteFechaCaducidad,  " +
					       "l.exisinicial   AS loteExistenciaInicial, " + 
					       "l.exisactual    AS loteExistenciaActual, " +
					       "l.status        AS loteStatus,	 " +
					       "k.cantidad      AS cantidad " +
						   "FROM   notacredito a  " +
					       "JOIN detallen d  " +
					       		  "ON a.ncr_id = d.ncr_id  " +
						   "LEFT JOIN detallenlote k " + 
					              "ON a.ncr_id = k.ncr_id  " +
					              "AND d.art_id = k.art_id " + 
					       "LEFT JOIN lote l " + 
					              "ON k.lot_id = l.lot_id " + 
					              "AND d.art_id = l.art_id " + 
						   "WHERE l.numlote is not null and a.ncr_id in (:ncList) " +
						   "ORDER BY a.ncr_id, d.art_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(DetalleLote.class));
				query.setParameterList("ncList", ids);
				List<DetalleLote> lotList= query.list();

				List<Articulo> ncArtKitList = null;
				List<DetalleLote> ncLotKitList = null;
				//JAvila: Obtiene los articulos y lotes del kit
				sql = " 	SELECT b.ncr_id AS idVenta, " + 
						"		  q.art_id AS articuloId,  " + 
						"		  q.clave AS articuloClave,  " + 
						"		  replace(q.descripcion, ',', '_') AS articuloDescripcion,  " + 
						"		  q.precio1 AS articuloPrecio1,  " + 
						"		  q.precio2 AS articuloPrecio2,  " + 
						"		  q.precio3 AS articuloPrecio3,  " + 
						"		  b.cantidad AS ventaDetalleCantidad, " + 
						"         q.lote AS lote, " +
						"		  b.unidad AS ventaDetalleUnidad, " + 
						"		  q.precioCompra AS precioCompra, " +
						"		  b.paquete AS paquete, " +
						"		  a.tipo AS tipo " +
						"	FROM detallev a" + 
						"	JOIN detallepaqn b on a.art_id = b.paquete and a.ven_id=b.ven_id " + 
						"	JOIN articulo q on b.articulo = q.art_id " + 
						"	WHERE b.ncr_id in (:ncList);";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
				query.setParameterList("ncList", ids);
				ncArtKitList = query.list();
				
				sql = "SELECT  d.ncr_id        AS idVenta,  " + 
						"	   d.articulo      AS articuloId,   " + 
						"	   d.clave         AS articuloClave,    " + 
						"	   l.numlote       AS loteNumero,  " + 
						"	   l.fechafab      AS loteFechaFabricacion,   " + 
						"	   l.fechacad      AS loteFechaCaducidad,   " + 
						"	   l.exisinicial   AS loteExistenciaInicial,   " + 
						"	   l.exisactual    AS loteExistenciaActual,  " + 
						"	   l.status        AS loteStatus,	  " + 
						"	   d.paquete AS paquete, " +
						"	   k.cantidad      AS cantidad  " + 
						"	   FROM   venta a   " + 
						"	   JOIN detallepaqn d  ON a.ven_id = d.ven_id " + 
						"	   LEFT JOIN detallepaqnlote k  ON a.ven_id = k.ven_id " + 
						"			   AND d.articulo = k.articulo " + 
						"	   LEFT JOIN lote l ON k.lot_id = l.lot_id " + 
						"			  AND d.articulo = l.art_id " + 
						"	   WHERE   l.numlote is not null and d.ncr_id in (:ncList) " + 
						"	   ORDER BY d.ncr_id, d.articulo; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(DetalleLote.class));
				query.setParameterList("ncList", ids);
				ncLotKitList= query.list();
				//JAvila: Fin
				
				//Obtiene los articulos del kit por descuento
				sql = "	SELECT n.ncr_id AS idVenta, " + 
						"		  n.ncr_id AS notaCreditoId, " +
						"		  q.art_id AS articuloId,  " + 
						"		  q.clave AS articuloClave,  " + 
						"		  replace(q.descripcion, ',', '_') AS articuloDescripcion,  " + 
						"		  (q.precio1 * n.total)/c.total AS articuloPrecio1,  " + 
						"		  (q.precio2 * n.total)/c.total AS articuloPrecio2,  " + 
						"		  (q.precio3 * n.total)/c.total AS articuloPrecio3,  " + 
						"		  b.cantidad AS ventaDetalleCantidad, " + 
						"         q.lote AS lote, " + 
						"		  b.unidad AS ventaDetalleUnidad, " + 
						"		  (q.precioCompra * n.total)/c.total AS precioCompra, " + 
						"		  b.paquete AS paquete, " + 
						"		  a.tipo AS tipo " + 
						"	FROM notacredito n " + 
						"   JOIN detallen d on n.ncr_id = d.ncr_id " + 
						"   JOIN detallev a on d.ven_id = a.ven_id " + 
						"   JOIN detallep b on a.art_id = b.paquete and a.ven_id=b.ven_id " + 
						"	JOIN articulo q on b.articulo = q.art_id " + 
						"   JOIN venta c on d.ven_id = c.ven_id " + 
						"	WHERE n.ncr_id in (:ncList) " + 
						"   AND d.dv_ven_id is null " + 
						"	AND d.dv_art_id is null";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
				query.setParameterList("ncList", ids);
				List<Articulo> adNcList = query.list();
				if(adNcList != null && adNcList.size() > 0) {
					if(ncArtKitList == null) {
						ncArtKitList = new ArrayList<Articulo>();
					}
					for(Articulo ad : adNcList) {
						ad.setArticuloDescuento(true);
						ncArtKitList.add(ad);
					}						
				}
				
				sql = "select a.ncr_id as idVenta,c.fcf_id as idFactura,a.tic_id as idTicket, a.not_id as idNotaVenta, a.caj_id as idCaja, " + 
						"       c.fechaCert as fechaFactura, c.folio as folio, c.serieFolio as serieFolio, " + 
						"       c.nombreE as emisor, c.rfcE as rfcEmisor, c.nombreC as receptor, c.rfcC as rfcReceptor, " + 
						"       c.monAbr as moneda, c.monTipoCambio as tipoCambio, c.subtotal as subTotal, " + 
						"       c.descuento as descuento, c.total as total, c.formaPago as formaPago, c.metodoPago as metodoPago, " + 
						"       c.uuid as uuid, c.usoCfdi as usoCfdi " + 
						"       FROM notacredito a " + 
						"       JOIN facturacfdi c on a.fcf_id = c.fcf_id " + 
						" WHERE  a.ncr_id in (:ncList) " +
						" ORDER BY a.ncr_id, c.fcf_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(Factura.class));
				query.setParameterList("ncList", ids);
				List<Factura> factList= query.list();
				
				sql = "select a.ncr_id as idVenta, c.fcf_id as idFactura, d.imp_id as idImpuesto, " + 
						"       d.total as totalImpuesto, d.subtotal as subtotal, d.tipoFactor as tipoFactor, " + 
						"       d.nombreImp as nombreImpuesto, d.valor as porcentajeImpuesto " + 
						"       FROM notacredito a " +  
						"       JOIN facturacfdi c on a.fcf_id = c.fcf_id " + 
						"       JOIN facturacfdiimp d on c.fcf_id = d.fcf_id " + 
						" WHERE  a.ncr_id in (:ncList) " +
						" ORDER BY a.ncr_id, c.fcf_id, d.imp_id; ";
				
				query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(FacturaImpuestos.class));
				query.setParameterList("ncList", ids);
				List<FacturaImpuestos> factImpList= query.list();
				
				Cliente cli = null;
				List<Articulo> aList = null;
				List<ArticuloImpuesto> aiList = null;
				List<TipoPago> tpList = null;
				List<DetalleLote> dlList = null;
				List<DetalleLote> aLotKitList = null;
				List<Articulo> aKitList = null;
				Factura fact = null;
				List<FacturaImpuestos> fiList = null;

				for(Sales s : rows) {
					
					s.setSucursal(sucursal);
					s.setEmpresa(emp);
					
					if(cliList != null && cliList.size() > 0 ) {
						for(Cliente o : cliList) {
							if(o.getClienteId().intValue() == s.getIdCliente().intValue()) {							
								cli = o;
								break;
							}
						}
					}
					if(cliNCFList != null && cliNCFList.size() > 0 ) {
						for(Cliente o : cliNCFList) {
							if(o.getClienteId().intValue() == s.getIdCliente().intValue()) {							
								cli = o;
								break;
							}
						}
					}					
					s.setCliente(cli);
					
					aList = new ArrayList<Articulo>();
					for(Articulo o :  artList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							aList.add(o);
						}
					}
					s.setArticulo(aList);
					
					//JAvila: Obtiene los articulos y lotes del kit
					aKitList = new ArrayList<Articulo>();
					for(Articulo o :  ncArtKitList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							aKitList.add(o);
						}
					}
					if(aKitList.size() > 0) {
						s.setArticuloPaquete(aKitList);
					}					
					
					aLotKitList = new ArrayList<DetalleLote>();
					for(DetalleLote o :  ncLotKitList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							aLotKitList.add(o);
						}
					}
					s.setDetalleLotePaquete(aLotKitList);
					//JAvila: FIN
					
					aiList = new ArrayList<ArticuloImpuesto>();
					for(ArticuloImpuesto o :  artImpList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							aiList.add(o);
						}
					}
					s.setArticuloImpuesto(aiList);
					
					tpList = new ArrayList<TipoPago>();
					if(pagoList != null && !pagoList.isEmpty()) {
						for(TipoPago o :  pagoList) {
							if(o.getIdNotaCredito() != null && s.getIdVenta() != null) {
								if(o.getIdNotaCredito().intValue() == s.getIdVenta().intValue()) {
									tpList.add(o);
								}	
							}
						}
					}
					if(pagoCreditoList != null && !pagoCreditoList.isEmpty()) {
						for(TipoPago o :  pagoCreditoList) {
							if(o.getIdNotaCredito() != null && s.getIdVenta() != null) {
								if(o.getIdNotaCredito().intValue() == s.getIdVenta().intValue()) {
									tpList.add(o);
								}	
							}
						}
					}
//					if(pagoMonList != null && !pagoMonList.isEmpty()) {
//						for(TipoPago o :  pagoMonList) {
//							if(o.getIdVenta() != null && s.getIdVenta() != null) {
//								if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
//									tpList.add(o);
//								}	
//							}
//						}
//					}
					s.setTipoPago(tpList);
					
					dlList = new ArrayList<DetalleLote>();
					for(DetalleLote o :  lotList) {
						if(o.getIdVenta() != null) {
							if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
								dlList.add(o);
							}							
						}
					}
					s.setDetalleLote(dlList);
					
					fiList = new ArrayList<FacturaImpuestos>();
					for(Factura o :  factList) {
						if(o.getIdVenta().intValue() == s.getIdVenta().intValue()) {
							for(FacturaImpuestos fi : factImpList) {
								if(o.getIdFactura().intValue() == fi.getIdFactura().intValue()) {
									fiList.add(fi);
								}
							}
							o.setSucursal(sucursal);
							o.setFacturaImpuestos(fiList);
							fact = o;
							break;
						}
					}
					s.setFactura(fact);
					
					// NOTAS CREDITO
					NotaCredito nc = null;
					List<Articulo> ancList = null;
					List<ArticuloImpuesto> aincList = null;
					List<DetalleLote> aLotList = null;
					List<NotaCreditoImpuestos> ncrList = null;
					
					if(!ncTicketList.isEmpty()) {
						for(NotaCredito o :  ncTicketList) {
							if(o.getIdNotaCredito().intValue() == s.getIdVenta().intValue()) {
								o.setSucursal(sucursal);
								if(artList != null) {
									ancList = new ArrayList<Articulo>();
									for(Articulo onc : artList) {
										if(onc.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											ancList.add(onc);
										}
									}
									o.setArticulo(ancList);
								}
								
								if(ncArtKitList != null) {
									ancList = new ArrayList<Articulo>();
									for(Articulo onc : ncArtKitList) {
										if(onc.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											ancList.add(onc);
										}
									}
									if(ancList.size() > 0) {
										o.setArticuloPaquete(ancList);
									}									
								}

								if(artImpList != null) {
									aincList = new ArrayList<ArticuloImpuesto>();
									for(ArticuloImpuesto onc : artImpList) {
										if(onc.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											aincList.add(onc);
										}
									}
									o.setArticuloImpuesto(aincList);
								}
								
								if(ncImpList != null && ncImpList.size() > 0) {
									ncrList = new ArrayList<NotaCreditoImpuestos>();
									for(NotaCreditoImpuestos nci : ncImpList) {
										if(nci.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
											ncrList.add(nci);
										}
									}
								}
								o.setNotaCreditoImpuestos(ncrList);
								
								if(lotList != null) {
									aLotList = new ArrayList<DetalleLote>();
									for(DetalleLote ncl :  lotList) {
										if(ncl.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											aLotList.add(ncl);
										}
									}
								}								
								o.setDetalleLote(aLotList);
								
								//J.Avila: Detalle del lote de los kits
								if(ncLotKitList != null) {
									aLotList = new ArrayList<DetalleLote>();
									for(DetalleLote ncl :  ncLotKitList) {									
										if(ncl.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											aLotList.add(ncl);
										}
									}
								}
								o.setDetalleLotePaquete(aLotList);
								//J.Avila:Fin
								
								List<TipoPago> tpNCList = new ArrayList<TipoPago>();
								if(pagoList != null && !pagoList.isEmpty()) {
									for(TipoPago tp :  pagoList) {
										if(tp.getIdNotaCredito() != null && o.getIdNotaCredito() != null) {
											if(tp.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
												tpNCList.add(tp);
											}	
										}
									}
								}
								if(pagoCreditoList != null && !pagoCreditoList.isEmpty()) {
									for(TipoPago tp :  pagoCreditoList) {
										if(tp.getIdNotaCredito() != null && o.getIdNotaCredito() != null) {
											if(tp.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
												tpNCList.add(tp);
											}	
										}
									}
								}
								o.setTipoPago(tpNCList);							
								nc = o;
								break;
							}
						}
					}
					
					if(!ncFactList.isEmpty()) {						
						for(NotaCredito o :  ncFactList) {
							if(o.getIdNotaCredito().intValue() == s.getIdVenta().intValue()) {
								o.setSucursal(sucursal);
								if(artList != null) {
									ancList = new ArrayList<Articulo>();
									for(Articulo onc : artList) {
										if(onc.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											ancList.add(onc);
										}
									}
									o.setArticulo(ancList);
								}
								
								//J.Avila: detalle de articulos de Kit
								if(ncArtKitList != null) {
									ancList = new ArrayList<Articulo>();
									for(Articulo onc : ncArtKitList) {
										if(onc.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											ancList.add(onc);
										}
									}
									if(ancList.size() > 0) {
										o.setArticuloPaquete(ancList);
									}									
								}
								//J.Avila: Fin

								if(artImpList != null) {
									aincList = new ArrayList<ArticuloImpuesto>();
									for(ArticuloImpuesto onc : artImpList) {
										if(onc.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											aincList.add(onc);
										}
									}
									o.setArticuloImpuesto(aincList);
								}
								
								if(ncImpList != null && ncImpList.size() > 0) {
									if(ncrList == null) {
										ncrList = new ArrayList<NotaCreditoImpuestos>();
									}									
									for(NotaCreditoImpuestos nci : ncImpList) {
										if(nci.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
											ncrList.add(nci);
										}
									}									
								}
								o.setNotaCreditoImpuestos(ncrList);
								
								if(lotList != null) {
									aLotList = new ArrayList<DetalleLote>();
									for(DetalleLote ncl :  lotList) {									
										if(ncl.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											aLotList.add(ncl);
										}
									}
								}
								o.setDetalleLote(aLotList);
								
								//J.Avila: Detalle del lote de los kits
								
								if(ncLotKitList != null) {
									aLotList = new ArrayList<DetalleLote>();
									for(DetalleLote ncl :  ncLotKitList) {									
										if(ncl.getIdVenta().intValue() == o.getIdNotaCredito().intValue()) {
											aLotList.add(ncl);
										}
									}
								}
								o.setDetalleLotePaquete(aLotList);
								//J.Avila:Fin

								List<TipoPago> tpNCList = new ArrayList<TipoPago>();
								if(pagoList != null && !pagoList.isEmpty()) {
									for(TipoPago tp :  pagoList) {
										if(tp.getIdNotaCredito() != null && o.getIdNotaCredito() != null) {
											if(tp.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
												tpNCList.add(tp);
											}	
										}
									}
								}
								if(pagoCreditoList != null && !pagoCreditoList.isEmpty()) {
									for(TipoPago tp :  pagoCreditoList) {
										if(tp.getIdNotaCredito() != null && o.getIdNotaCredito() != null) {
											if(tp.getIdNotaCredito().intValue() == o.getIdNotaCredito().intValue()) {
												tpNCList.add(tp);
											}	
										}
									}
								}
								o.setTipoPago(tpNCList);								
								nc = o;
								break;
							}
						}
					}				
					s.setNotaCredito(nc);
				 }
			}

			salesDTO.setSales(rows);
			salesDTO.setVentasCanceladas(null);
			
			return salesDTO;
		}else {
			return null;
		}
			
	}
	
	@SuppressWarnings("unchecked")
	public List<MovimientosCaja> getCashTransactions(String sucursal, String requestType, String[] params){
		
		String sqlInit = "";
		int idCorteCaja = 0;
		
		if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			idCorteCaja = Integer.valueOf(params[0]);
			sqlInit = "SELECT '" + sucursal + "' as sucursal, "+
					"		a.mov_id as idMov, " + 
					"       a.ven_id as idVenta, " + 
					"       a.tipo as tipo, " + 
					"       a.status as idStatus, " + 
					"       a.caj_id as idCaja, " + 
					"       b.rcc_id as idResumenCaja, " + 
					"       a.cor_id as idCorteCaja, " +
					"		e.nombre AS descripcionCaja, " +
					"       c.nombre as tipoPago, " + 
					"       a.total as total, " + 
					"       d.fecha as fechaCierre, " + 
					"       a.comentario as comentario " + 
					"FROM " + 
					"       movimiento a " + 
					"       JOIN resumencortecaja b on a.cor_id = b.cor_id " + 
					"       JOIN tipopago c on a.tpa_id = c.tpa_id " + 
					"       JOIN cortecaja d on a.cor_id = d.cor_id " +
			        "		LEFT JOIN caja e on a.caj_id = e.caj_id " +	
					"WHERE b.rcc_id = " + idCorteCaja +
					"		AND a.ven_id is null " +
					"       AND a.ncr_id is null " + 
					"       AND a.com_id is null " + 
					"       AND a.acl_id is null " + 
					"       AND a.apr_id is null " +
					"		AND a.comentario <> '" + CC_SALIDA_TEXTO + "' ";
		}
		
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlInit);
		query.setResultTransformer(Transformers.aliasToBean(MovimientosCaja.class));
		return query.list();
		
	}

	@SuppressWarnings("unchecked")
	public PaymentDTO getCustomerPayments(String sucursal, String requestType, String[] params) {
				
		Session session = sessionFactory.getCurrentSession();
		PaymentDTO paymentDTO = new PaymentDTO();
		String sql = "";
		int idCorteCaja = 0;
		
		if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			idCorteCaja = Integer.valueOf(params[0]);
			
		}
		
		if(AppConstants.REQUEST_TYPE_CC.equals(requestType)) {
			sql = "" +
					" SELECT  " +
					" '" + sucursal + "'		as sucursal, " + 
					" a.acl_id					as abonoId, " +
					" a.ccl_id					as creditoId, " +
					" c.cli_id 					as clienteId, " +
					" n.nub_id 					as clienteNubeId, " +
					" h.fecha					as fechaAbono, " +
					" p.abr						as tipoPagoClave, " +
					" p.nombre 					as tipoPagoNombre, " +
					" a.total					as abonoTotal, " +
					" a.comentario 				as comentario, " +
					" j.nombre 					as descripcionCaja, " +
					" SUBSTRING_INDEX(l.curp,'/',1)		as numeroCuenta, " +
					" SUBSTRING_INDEX(l.curp,'/',-1)	as numeroSitio, " +
					" v.tic_id					as idTicket, " +
					" v.not_id					as idNotaVenta, " +
					" v.ven_id					as idVenta, " +
					" r.rcc_id 					as idResumenCaja " +
					" FROM abonocliente a " +
					" JOIN tipopago p ON a.tpa_id = p.tpa_id " +
					" JOIN creditocliente c ON a.ccl_id = c.ccl_id " +
					" JOIN cliente l ON c.cli_id = l.cli_id " +
					" JOIN nubeid n ON c.cli_id = n.id AND n.entidad = 'Cliente'  " +
					" JOIN venta v ON c.ven_id = v.ven_id " +
					" JOIN movimiento m ON a.acl_id = m.acl_id " +
					" JOIN cortecaja cc ON m.cor_id = cc.cor_id " +
					" JOIN resumencortecaja r ON cc.cor_id = r.cor_id " +
					" JOIN historial h ON a.acl_id = h.id AND h.tabla = 'AbonoCliente' AND h.movimiento = 0 " +
					" LEFT JOIN caja j ON m.caj_id = j.caj_id" +
					" WHERE r.rcc_id = :idCorteCaja " +
					" ORDER BY a.acl_id";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Abono.class));
			query.setParameter("idCorteCaja", idCorteCaja);
			List<Abono> lstAbono = query.list();
			paymentDTO.setLstPayment(lstAbono);
		}
		
		return paymentDTO;
	}

	@SuppressWarnings("unchecked")
	public List<CustomerDTO> getCustomers(String value){
		
		String sqlInit = " SELECT " + 
				" concat(domicilio, ' ', noExt, ' ', colonia, ' ', localidad) as address1, " + 
				" pais as country, " + 
				" ciudad as city, " + 
				" codigoPostal as postalCode, " + 
				" estado as state, " + 
				" nombre as name, " + 
				" rfc as rfc, " +
				" curp as accountNumber, " +
				" cli_id as account " +
				" FROM cliente " +
				" WHERE cli_id in (select id " +
				" 					from historial " +
                " 					where tabla = 'Cliente' " +
                " 					and movimiento = 0 " +
                "					and fecha > '" + value + "') " +
                " AND status = 1";
		
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlInit);
		query.setResultTransformer(Transformers.aliasToBean(CustomerDTO.class));
		return query.list();	
	}
	
	@SuppressWarnings("unchecked")
	public boolean customerExists(ClientesJson customer){		
		boolean exists = true;
		
		String sqlInit = " select c.cli_id as clienteId, " +
						" ifnull(n.nub_id, 0) as clienteNubeId " +
						" from cliente c " +
						" left join nubeid n  " +
						" on c.cli_id = n.id  " +
						" and n.entidad = 'Cliente'  " +
						" where trim(upper(c.nombre)) = trim(upper(:nombre)); ";
		
		Session session = sessionFactory.getCurrentSession();				
		SQLQuery query = session.createSQLQuery(sqlInit);
		
		query.setResultTransformer(Transformers.aliasToBean(Cliente.class));
		query.setParameter("nombre", customer.getNombre());		
		List<Cliente> lst = (List<Cliente>) query.list();
			
		if(lst != null && lst.size() > 0) {
			System.out.println("El cliente " + customer.getNombre() + " ya fue registrado.");
			customer.setCliId(lst.get(0).getClienteId());
		} else {
			System.out.println("El cliente " + customer.getNombre() + " a�n no ha sido registrado.");
			exists = false;
		}
		
		return exists;
	}
	
	public void updateCustomers(CustomerDTO c){
		
		int idCliente = Integer.valueOf(c.getAccount()).intValue();
		String curp = "'" + c.getAccountNumber() + "/" + c.getSiteNumber() + "'";
		String sqlInit = "Update cliente set curp = " + curp + " where cli_id = " + idCliente;
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlInit);

		query.executeUpdate();	
	}	
	
	public void updateCustomerCredit(CreditDTO c){
		
		int idCliente = c.getCliente().getClienteId();
		BigDecimal creditoLimite = BigDecimal.valueOf(c.getCliente().getClienteLimite().doubleValue()); 
		String sqlInit = "Update cliente set limite = " + creditoLimite + " where cli_id = " + idCliente;
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlInit);

		query.executeUpdate();
	}	
	
	@SuppressWarnings("unchecked")
	public List<CreditDTO> getCustomerCredits(String sucursal, String value) {
		
		List<CreditDTO> lstCreditDTO = new ArrayList<CreditDTO>();		
		
		String sql = "SELECT " +
				" n.nub_id AS clienteNubeId," +
				" c.cli_id AS clienteId, " +
				" replace(c.nombre, ',', '_') AS clienteNombre,  " +
				" c.rfc AS clienteRFC, " +
				" c.curp AS clienteCURP, " +
				" c.domicilio AS clienteDomicilio, " +
				" c.colonia AS clienteColonia, " +
				" c.noExt AS clienteNoExt, " +
				" c.noInt AS clienteNoInt, " +
				" c.localidad AS clienteLocalidad, " +
				" c.ciudad AS clienteCiudad, " +
				" c.estado AS clienteEstado, " +
				" c.pais AS clientePais, " +
				" c.codigoPostal AS clienteCodigoPostal, " +
				" c.limite AS clienteLimite, " +
				" c.diasCredito AS clienteDiasCredito, " +
				" c.clave AS clienteClave, " +
				" c.precio AS clientePrecio " +
				" FROM cliente c " +
				" JOIN nubeid n ON c.cli_id = n.id AND n.entidad = 'Cliente' " + 
				" ORDER BY c.cli_id;";

		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sql);
		query.setResultTransformer(Transformers.aliasToBean(Cliente.class));
		List<Cliente> lstCliente= query.list();

		if(lstCliente != null && !lstCliente.isEmpty()) {
			List<Integer> ids = new ArrayList<Integer>();
			for(Cliente c : lstCliente) {
				ids.add(c.getClienteId().intValue());
			}
			
			sql = " SELECT '" + sucursal + "' as sucursal, " +
					" c.ccl_id as creditoId, " +
					" c.cli_id as clienteId, " +
					" n.nub_id AS clienteNubeId," +
					" c.ven_id as idVenta, " +
					" c.fechaLimite as fechaLimite, " +
					" c.total as total, " +
					" c.comentario as comentario, " +
					" c.status as status " +
					" FROM creditocliente c " +
					" LEFT JOIN nubeid n ON c.cli_id = n.id AND n.entidad = 'Cliente' " +
					" WHERE c.cli_id in (:Ids)";
			
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Credito.class));
			query.setParameterList("Ids", ids);
			List<Credito> lstCreditos = query.list();

			sql =	" SELECT '" + sucursal + "'		as sucursal,  " +
					" a.acl_id 						as abonoId,  " +
					" a.ccl_id 						as creditoId,  " +
					" c.cli_id 						as clienteId,  " +
					" n.nub_id 						as clienteNubeId,  " +
					" CASE WHEN h.fecha IS NOT NULL THEN h.fecha " +
					"	  ELSE DATE(DATE_FORMAT(a.fecha, '%Y-%m-%d 12:00:00')) " +
					"	  END						as fechaAbono, " +
					" p.abr 							as tipoPagoClave,  " +
					" p.nombre 						as tipoPagoNombre,  " +
					" a.total 						as abonoTotal,  " +
					" a.comentario 					as comentario,  " +
					" j.nombre 						as descripcionCaja,  " +
					" SUBSTRING_INDEX(l.curp,'/',1)	as numeroCuenta,  " +
					" SUBSTRING_INDEX(l.curp,'/',-1)as numeroSitio,  " +
					" v.tic_id						as idTicket,  " +
					" v.not_id						as idNotaVenta,  " +
					" v.ven_id						as idVenta,  " +
					" r.rcc_id						as idResumenCaja  " +
					" FROM abonocliente a      " +
					" JOIN tipopago p ON a.tpa_id = p.tpa_id  " +
					" JOIN creditocliente c ON a.ccl_id = c.ccl_id  " +
					" JOIN cliente l ON c.cli_id = l.cli_id  " +
					" JOIN nubeid n ON c.cli_id = n.id AND n.entidad = 'Cliente'  " +
					" JOIN venta v ON c.ven_id = v.ven_id  " +
					" LEFT JOIN historial h ON a.acl_id = h.id AND h.tabla = 'AbonoCliente' AND h.movimiento = 0  " +
					" LEFT JOIN movimiento m ON a.acl_id = m.acl_id " +
					" LEFT JOIN cortecaja cc ON m.cor_id = cc.cor_id " +
					" LEFT JOIN resumencortecaja r ON cc.cor_id = r.cor_id " +
					" LEFT JOIN caja j ON m.caj_id = j.caj_id " +
					" WHERE c.cli_id in (:Ids)  " +
					" ORDER BY a.acl_id;";

			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Abono.class));
			query.setParameterList("Ids", ids);
			List<Abono> lstAbono = query.list();
			
			sql =	" select '" + sucursal + "'			as sucursal,  " +
					"  a.ncr_id 						as idNotaCredito, " +
					"  n.folio							as folio, " +
					"  n.folioNC						as folioNC, " +
					"  n.serieFolio						as serieFolio, " +
					"  a.ccl_id 						as creditoId, " +
					"  c.cli_id 						as clienteId, " +
					"  nu.nub_id 						as clienteNubeId,  " +
					"  n.fecha							as fecha, " +
					"  a.total 							as total, " +
					"  a.status 						as status,  " +
					"  j.nombre 						as descripcionCaja,  " +
					"  SUBSTRING_INDEX(l.curp,'/',1)	as numeroCuenta,  " +
					"  SUBSTRING_INDEX(l.curp,'/',-1) 	as numeroSitio,  " +
					"  v.tic_id							as idTicket,  " +
					"  v.not_id							as idNotaVenta,  " +
					"  v.ven_id							as idVenta,  " +
					"  n.rcc_id							as idResumenCaja " +
					" FROM creditoclientenotcre a " +
					" JOIN notacredito n ON a.ncr_id = n.ncr_id " +
					" JOIN creditocliente c ON a.ccl_id = c.ccl_id " +
					" JOIN cliente l ON c.cli_id = l.cli_id " +
					" JOIN nubeid nu ON c.cli_id = nu.id AND nu.entidad = 'Cliente'  " +
					" JOIN venta v ON c.ven_id = v.ven_id " +
					" LEFT JOIN caja j ON n.caj_id = j.caj_id " +
					" WHERE c.cli_id IN (:Ids)  " +
					" ORDER BY n.ncr_id; ";

			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(NotaCreditoAbono.class));
			query.setParameterList("Ids", ids);
			List<NotaCreditoAbono> lstNCAbono = query.list();
			
			CreditDTO creditDTO = null;
			List<Credito> creditos = null;
			List<Abono> abonos = null;
			List<NotaCreditoAbono> ncAbonos = null;
			
			for(Cliente cliente : lstCliente) {
				creditDTO = new CreditDTO();
				creditDTO.setSucursal(sucursal);
				creditDTO.setCliente(cliente);
				
				if(lstCreditos != null && lstCreditos.size() > 0) {					
					creditos = new ArrayList<Credito>();
					for(Credito credito : lstCreditos) {
						if(cliente.getClienteId().intValue() == credito.getClienteId().intValue()) {
							abonos = new ArrayList<Abono>();
							if(lstAbono != null && !lstAbono.isEmpty()) {
								for(Abono abono : lstAbono) {
									if(abono.getCreditoId().intValue() == credito.getCreditoId().intValue()) {
										abonos.add(abono);
									}
								}
							}
							ncAbonos = new ArrayList<NotaCreditoAbono>();
							if(lstNCAbono != null && !lstNCAbono.isEmpty()) {
								for(NotaCreditoAbono ncAbono : lstNCAbono) {
									if(ncAbono.getCreditoId().intValue() == credito.getCreditoId().intValue()) {
										ncAbonos.add(ncAbono);
									}
								}
							}
							credito.setAbono(abonos);
							credito.setNotacredito(ncAbonos);							
							creditos.add(credito);
						}						
					}
					creditDTO.setCreditos(creditos);
				}				
				lstCreditDTO.add(creditDTO);
			}		
		}				
		return lstCreditDTO;
	}	

	public void updateTipoCambioIntoMoneda(double tc) {
		Session session = this.sessionFactory.getCurrentSession();
		String sql = "UPDATE MONEDA SET tipoCambio=:tc where abr='USD'";
		Query query = session.createSQLQuery(sql);
		query.setParameter("tc",tc);
		query.executeUpdate();
	}

	public Date getCCDate(int idCC) {
		 
		String sqlInit = "SELECT IFNULL(MAX(fecha), SYSDATE()) as fecha FROM venta WHERE rcc_id = :rcc_id ";
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlInit);
		query.setParameter("rcc_id", idCC);
		Date lastDate = (Date)query.uniqueResult();
		
		return lastDate;
	}
	
	public BigInteger getLastId() {
		 
		String sqlInit = "SELECT IFNULL(MAX(rcc_id), 0) as ultimo FROM resumencortecaja;";
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlInit);		
		BigInteger lastId = (BigInteger)query.uniqueResult();
		
		return lastId;
	}
	
	public BigDecimal getConteoArticulo() {
		String sqlInit = "select sum(existencia) from articulo where status = 1 and tipo = 0";
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlInit);
		BigDecimal conteo = (BigDecimal) query.uniqueResult();
		
		return conteo;
	}

	public BigDecimal getConteoLote() {
		String sqlInit = "select sum(exisactual) from lote";
		Session session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(sqlInit);
		BigDecimal conteo = (BigDecimal) query.uniqueResult();
		
		return conteo;
	}
	
	public void setDataSource(DataSource dataSource) {
		try {
			this.dataSource = dataSource;
			this.conn = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

package com.tdo.integration.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.dto.integration.dto.InventoryTransactionDTO;
import com.tdo.integration.client.localmodel.CreditNoteRequestEntity;
import com.tdo.integration.client.model.Articulo;

@Repository("customerLocalDao")
@Transactional 
public class CustomerLocalDao {
	
	DataSource dataSource;
	Connection conn = null;
    private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public void setDataSource(DataSource dataSource) {
		try {
			this.dataSource = dataSource;
			this.conn = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public String getUsdPricingItem(String itemCode) {
		String itemIdentifier = "";
		try {
			Connection dbConnection = dataSource.getConnection();
			String selectSQL = "SELECT itemIdentifier FROM sicarproductprices WHERE itemIdentifier = ?";
			PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
			preparedStatement.setString(1, itemCode);
			ResultSet rs = preparedStatement.executeQuery();
			if (!rs.next()) {
			    System.out.println("no data");
			} else {
				itemIdentifier = rs.getString("itemIdentifier");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return itemIdentifier;
	}
	
	
	@Transactional
	public void updateUsdPricing(Articulo art, InventoryTransactionDTO o, double usdAvgCost, double exchangeRate) {
		
		Session session = sessionFactory.getCurrentSession();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String transactionDate = dateFormat.format(date);
		try {
			String itemCode = art.getArticuloClave();
			double profit1 = art.getVentaDetallePrecioSinIva().doubleValue();
			double profit2 = art.getVentaDetallePrecioConIva().doubleValue();
			double profit3 = art.getVentaDetalleImporteSinIva().doubleValue();

			if(!"".equals(itemCode)) {
				double price1 = 0;
				double price2 = 0;
				double price3 = 0;
				
				price1 = usdAvgCost + (usdAvgCost*(profit1/100));
				price2 = usdAvgCost + (usdAvgCost*(profit2/100));
				price3 = usdAvgCost + (usdAvgCost*(profit3/100));
				
				price1 = Math. round(price1 * 1000.0) / 1000.0;
				price2 = Math. round(price2 * 1000.0) / 1000.0;
				price3 = Math. round(price3 * 1000.0) / 1000.0;
				usdAvgCost = Math. round(usdAvgCost * 1000.0) / 1000.0;
				
				String sql = "UPDATE sicarproductprices SET exchangeRate = :avRate, purchasePriceAvrg = :avPrice, productPrice1 = :price1,"
						+ "productPrice2 = :price2, productPrice3 = :price3, updatedDate = :updatedDate "
						+ " WHERE itemIdentifier = :itemIdentifier";
				Query qry = session.createSQLQuery(sql);
				qry.setParameter("avRate", exchangeRate);
				qry.setParameter("avPrice", usdAvgCost);
				qry.setParameter("price1", price1);
				qry.setParameter("price2", price2);
				qry.setParameter("price3", price3);
				qry.setParameter("updatedDate", transactionDate);
				qry.setParameter("itemIdentifier", itemCode);
				qry.executeUpdate();
			}

		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void insertItemPricing(Articulo art, InventoryTransactionDTO o, double usdAvgCost, double exchangeRate) {
		
		try {
			Connection dbConnection = dataSource.getConnection();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
			Date date = new Date();
			String transactionDate = dateFormat.format(date);
			
			String sql = "INSERT INTO sicarproductprices (currency, description, exchangeRate, itemIdentifier, "
					    + "productPrice1, productPrice2, productPrice3, profitPercentage1, profitPercentage2, "
					    + "profitPercentage3, purchasePriceAvrg, updatedBy, updatedDate) "
					    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";			
			
			usdAvgCost = Math. round(usdAvgCost * 100.0) / 100.0;
			
			double p1 = ((art.getVentaDetallePrecioSinIva().doubleValue()/100) * usdAvgCost)  + usdAvgCost;
			double p2 = ((art.getVentaDetallePrecioConIva().doubleValue()/100) * usdAvgCost)  + usdAvgCost;
			double p3 = ((art.getVentaDetalleImporteSinIva().doubleValue()/100) * usdAvgCost)  + usdAvgCost;
			
			p1 = Math. round(p1 * 1000.0) / 1000.0;
			p2 = Math. round(p2 * 1000.0) / 1000.0;
			p3 = Math. round(p3 * 1000.0) / 1000.0;
			
			PreparedStatement st = dbConnection.prepareStatement(sql);
			st.setString(1, "USD");
			st.setString(2, art.getArticuloDescripcion());
			st.setDouble(3, exchangeRate);
			st.setString(4, art.getArticuloClave());
			st.setDouble(5, p1);
			st.setDouble(6, p2);
			st.setDouble(7, p3);
			st.setDouble(8, art.getVentaDetallePrecioSinIva().doubleValue());
			st.setDouble(9, art.getVentaDetallePrecioConIva().doubleValue());
			st.setDouble(10, art.getVentaDetalleImporteSinIva().doubleValue());
			st.setDouble(11, usdAvgCost);
			st.setString(12, "EAI");
			st.setString(13, transactionDate);
			st.executeUpdate();
		    st.close();
		    
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public List<CreditNoteRequestEntity> getCreditNoteRequestListByStatus(String statusParam) {

		Session session = sessionFactory.getCurrentSession();
		String sql = "select id,folio, uuid, status from CreditNoteRequest where status='" + statusParam
				+ "'  order by date desc";

		try {
			Query query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(CreditNoteRequestEntity.class));
			return query.list();

		} catch (Exception e) {
			return null;
		}
	}
	
	/*
	 * updates creditNoteRequest table with the updated status.
	 */
	public void saveCreditNoteRequest(CreditNoteRequestEntity creditNoteRequest) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "UPDATE creditnoterequest SET code = :code, lastUpdateTime = :lastUpdateTime,status=:status,description=:description,uuid=:uuid "
				+ "where id=:id and folio=:folio";
		Query qry = session.createSQLQuery(sql);
		qry.setParameter("code", creditNoteRequest.getCode());
		qry.setParameter("lastUpdateTime", creditNoteRequest.getLastUpdateTime());
		qry.setParameter("status", creditNoteRequest.getStatus());
		qry.setParameter("description", creditNoteRequest.getDesc());
		qry.setParameter("id", creditNoteRequest.getId());
		qry.setParameter("folio", creditNoteRequest.getFolio());
		qry.setParameter("uuid", creditNoteRequest.getUuid());
		qry.executeUpdate();
		
	}
	
}


package com.tdo.integration.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.client.localmodel.Udc;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.RestClientDao;

@Service("udcService")
public class UDCService {
		
	@Autowired
	ClientLocalDao clientLocalDao;
	@Autowired
	RestClientDao restClientDao;
	
	
	public Udc getUdc(String udcKey) throws Exception{
		return clientLocalDao.getUdcByUdcKey(udcKey);
	}
	
	public void updateMonedaUdc(Udc udc) throws Exception{
		try {
			 clientLocalDao.saveOrUpdateUdc(udc);
			 double tc = Double.parseDouble(udc.getUdcValue());
			 restClientDao.updateTipoCambioIntoMoneda(tc);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveUdc(Udc udc){
		try {
			 clientLocalDao.saveOrUpdateUdc(udc);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public Udc searchBySystemAndKey(String udcSystem, String udcKey){
		return clientLocalDao.searchBySystemAndKey(udcSystem, udcKey);
	}
	
	

}

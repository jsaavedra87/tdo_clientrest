package com.tdo.integration.services;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.CreditDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.tdo.integration.dao.RestClientDao;

@Service("creditService")
public class CreditService {

	@Autowired
	RestClientDao restClientDao; 	

	public List<CreditDTO> getCustomerCreditSicar(String sucursal, String value) {		
		return restClientDao.getCustomerCredits(sucursal, value);
	}
	
	public void updateCustomerSicar(String res) {

				
		try {
			if(res != null && !res.isEmpty() && !res.contains("Error:")) {
				JSONArray jsonArr = new JSONArray(res);
				
				for (int i = 0; i < jsonArr.length(); i++) {
			        JSONObject jsonObj = jsonArr.getJSONObject(i);
			        ObjectMapper mapper = new ObjectMapper();
			        CreditDTO c = mapper.readValue(jsonObj.toString(), CreditDTO.class);		        
			        restClientDao.updateCustomerCredit(c);
			    }
			} else if(res != null && !res.isEmpty() && res.contains("Error:")){
				System.out.println("L�MITES DE CR�DITO: Error al consumir el ws: " + res);
			} else {
				System.out.println("L�MITES DE CR�DITO: No se obtuvieron registros del ws.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}

package com.tdo.integration.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.client.localmodel.FileTransferControl;
import com.tdo.integration.client.localmodel.Sucursal;
import com.tdo.integration.dao.ClientLocalDao;

@Service("clientLocalService")
public class ClientLocalService {
	
	@Autowired
	ClientLocalDao clientLocalDao;
	
	public void saveBranch(Sucursal s) {
		clientLocalDao.saveBranch(s);
	}
	
	public Sucursal getBranch() {
		return clientLocalDao.getBranch(1);
	}
	
	public FileTransferControl searchFileTransferControlByIdCC(String transactionType, int  idCC) {
		List<FileTransferControl> lstFTC = null;
		FileTransferControl o = null;
		
		try 
		{
			lstFTC = clientLocalDao.searchFileTransferControlByIdCC(transactionType, idCC);
			if(lstFTC != null && lstFTC.size() > 0) {
				o = lstFTC.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return o;
	}

	public FileTransferControl searchFileTransferControlByFromDate(String transactionType, Date fromDate) {
		List<FileTransferControl> lstFTC = null;
		FileTransferControl o = null;
		
		try 
		{
			lstFTC = clientLocalDao.searchFileTransferControlByFromDate(transactionType, fromDate);
			if(lstFTC != null && lstFTC.size() > 0) {
				o = lstFTC.get(0);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return o;
	}	
	
	public List<FileTransferControl> searchFileTransferControlByDateRange(String transactionType, Date fromDate, Date toDate) {
		return clientLocalDao.searchFileTransferControlByDateRange(transactionType, fromDate, toDate);
	}
	
	public List<FileTransferControl> searchFileTransferControlByStatus(String transactionType, String[] status) {		
		return clientLocalDao.searchFileTransferControlByStatus(transactionType, status);
	}
	
	public List<FileTransferControl> searchFileTransferControlByDateRangeAndStatus(String transactionType, Date fromDate, Date toDate, String[] status) {
		return clientLocalDao.searchFileTransferControlByDateRangeAndStatus(transactionType, fromDate, toDate, status);
	}
	
	public FileTransferControl createFileTransferControl(String transactionType, int idCC, String fromDate, String toDate, String status, String log) {
		FileTransferControl ftc = new FileTransferControl();
		
		try 
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Sucursal suc = clientLocalDao.getBranch();
			
			ftc.setBranchId(suc.getId());
			ftc.setBranch(suc.getSucursal());
			ftc.setIdCC(idCC);
			ftc.setFromDate(dateFormat.parse(fromDate));
			ftc.setToDate(dateFormat.parse(toDate));
			ftc.setLastUpdate(new Date());
			ftc.setTransactionType(transactionType);
			ftc.setStatus(status);
			ftc.setLog(log);
			
			clientLocalDao.saveOrUpdateFileTransferControl(ftc);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ftc;
	}

	public FileTransferControl updateFileTransferControlStatus(FileTransferControl ftc, String newStatus) {		
		try 
		{
			ftc.setStatus(newStatus);
			ftc.setLastUpdate(new Date());
			clientLocalDao.saveOrUpdateFileTransferControl(ftc);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ftc;
	}
}

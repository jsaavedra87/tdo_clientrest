package com.tdo.integration.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.dao.InventorySicarDao;

@Service("newItemService")
public class NewItemService {

	@Autowired
	InventorySicarDao inventorySicarDao;
	
	public void updateItemLot() {
		inventorySicarDao.updateSicarItemLot();
	}
		
}

package com.tdo.integration.services;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.InventoryMonitorDTO;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.dao.RestClientDao;

@Service("inventoryMonitorService")
public class InventoryMonitorService {
	
	@Autowired
	RestClientDao restClientDao; 

	@Autowired
	RESTService restService;

	
	public void startInventoryMonitor(int branchId, String branchName, String branchCode, String restInventoryMonitorService) {
		InventoryMonitorDTO im = new InventoryMonitorDTO();
		
		BigDecimal conteoArticulo = this.getConteoArticulo();
		BigDecimal conteoLote = this.getConteoLote();
		
		im.setBranchId(branchId);
		im.setBranchName(branchName);
		im.setBranchCode(branchCode);
		im.setItemCount(conteoArticulo);
		im.setLotCount(conteoLote);
		im.setLastUpdate(new Date());
		im.setStatus(AppConstants.INVENTORY_STATUS_OK);
		
		String response = restService.sendRequestObject(restInventoryMonitorService, im);
		System.out.print("Respuesta del env�o de monitoreo de inventario: " + response);
	}
	
	public BigDecimal getConteoArticulo() {
		return restClientDao.getConteoArticulo();
	}
	
	public BigDecimal getConteoLote() {
		return restClientDao.getConteoLote();
	}
}

package com.tdo.integration.services;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.ArLockBoxImportC;
import com.dto.integration.dto.PaymentDTO;
import com.tdo.integration.client.model.Abono;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.RestClientDao;

@Service("paymentService")
public class PaymentService {

	@Autowired
	RestClientDao restClientDao;
	
	@Autowired
	ClientLocalDao clientLocalDao;	
	
	public List<ArLockBoxImportC> createArLockBoxImportC(String sucursal, String requestType, String[] params) {
		
		List<ArLockBoxImportC> lstPayments = new ArrayList<ArLockBoxImportC>();		
		PaymentDTO paymentDTO = restClientDao.getCustomerPayments(sucursal, requestType, params);		
		Format formatter = new SimpleDateFormat("yyMMdd");
		List<Abono> lstPayment = null;
		
		if(paymentDTO != null && paymentDTO.getLstPayment() != null && !paymentDTO.getLstPayment().isEmpty()) {
			lstPayment = paymentDTO.getLstPayment();
			
			String strIdBranch = String.format("%02d", clientLocalDao.getBranch(0).getId());			
			int itemNumber = 1;
			String receiptNumber = "";
			String receiptDate = "";
			String transactionReference = "";
			ArLockBoxImportC obj = null;
			
			for(Abono payment : lstPayment) {
				
				receiptNumber = new StringBuffer("REC").append(strIdBranch).append("-").append(payment.getAbonoId()).toString();
				receiptDate = formatter.format(payment.getFechaAbono());
				
				if(payment.getIdTicket() != null) {
					transactionReference = new StringBuffer("FAC").append(strIdBranch).append("-T").append(payment.getIdTicket()).toString();
				} else if (payment.getIdNotaVenta() != null) {
					transactionReference = new StringBuffer("FAC").append(strIdBranch).append("-N").append(payment.getIdNotaVenta()).toString();
				} else {
					transactionReference = new StringBuffer("FAC").append(strIdBranch).append("-V").append(payment.getIdVenta()).toString();
				}

				obj = new ArLockBoxImportC();
				obj.setRecord_Type("6");
				obj.setItem_Number(String.valueOf(itemNumber));
				obj.setRemittance_Amount(String.valueOf(payment.getAbonoTotal()));
				obj.setReceipt_Number(receiptNumber);
				obj.setReceipt_Date(receiptDate);
				obj.setCurrency("MXN");
				obj.setCustomer_Account_Number(payment.getNumeroCuenta());
				obj.setCustomer_Site(payment.getNumeroSitio());
				obj.setReceipt_Method("BANAMEX");
				obj.setLockbox_Number("LOCKBOX_TDO_1_BANAMEX");
				obj.setTransaction_Reference_1(transactionReference);
				obj.setApplied_Amount_1(String.valueOf(payment.getAbonoTotal()));
				
				lstPayments.add(obj);				
				itemNumber++;
			}
		}
		
		return lstPayments;
	}

}

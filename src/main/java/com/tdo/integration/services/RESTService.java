package com.tdo.integration.services;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

import org.springframework.stereotype.Service;

import com.dto.integration.dto.ArticulosJson;
import com.dto.integration.dto.ClientesJson;
import com.dto.integration.dto.CreditNoteResponseDTO;
import com.dto.integration.dto.InventoryTransactionDTO;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.tdo.integration.client.localmodel.Udc;

@Service("restService")
public class RESTService {
	
	private HttpURLConnection conn;
	
	public String sendRequestList(String urlStr, List<?> data) {
		
        String resString = "";
		try {
			ObjectMapper mapper = new ObjectMapper();			
			StringBuilder sb = new StringBuilder( mapper.writeValueAsString(data));
			String jsonInString = sb.toString();
			
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);
			
			 OutputStream os = conn.getOutputStream();
	         OutputStreamWriter osw = new OutputStreamWriter(os);
	         byte[] outputBytes = jsonInString.getBytes("UTF-8");
	         os.write(outputBytes);
	         osw.flush();
	         if (conn.getResponseCode() == 200) {
	        	    InputStream is = conn.getInputStream();
		            InputStreamReader isr = new InputStreamReader(is);
		            BufferedReader br = new BufferedReader(isr);
		            String line = null;
		            while ( (line = br.readLine()) != null)
		            {
		                resString = resString + line;
		            }
		            
		            if(!"".equals(resString)) {
		            }else {
		    			return "Ha ocurrido un error inesperado. El resultado del ws-* es null";
		            }
	 		} else {
				return "Error: " + conn.getResponseCode();
	 		}
	        osw.close();
	        os.close();			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return resString;
	}
	
	public String sendRequestObject(String urlStr, Object data) {
        String resString = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			StringBuilder sb = new StringBuilder( mapper.writeValueAsString(data));
			String jsonInString = sb.toString();
		
			System.out.println(jsonInString);
			
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);
			
			 OutputStream os = conn.getOutputStream();
	         OutputStreamWriter osw = new OutputStreamWriter(os);
	         byte[] outputBytes = jsonInString.getBytes("UTF-8");
	         os.write(outputBytes);
	         osw.flush();
	         if (conn.getResponseCode() == 200) {
	        	 InputStream is = conn.getInputStream();
		            // any response?
		            InputStreamReader isr = new InputStreamReader(is);
		            BufferedReader br = new BufferedReader(isr);
		            String line = null;
		            while ( (line = br.readLine()) != null)
		            {
		                resString = resString + line;
		            }
		            
		            if(!"".equals(resString)) {
			            mapper = new ObjectMapper();
			            JsonNode jsonNode = mapper.readTree(resString);
			            System.out.println(jsonNode.get("content").asText());
		            }else {
		    			return "Ha ocurrido un error inesperado. El resultado del ws-* es null";
		            }

		            
	 		}else {
				return "Error: " + conn.getResponseCode();
	 		}
	        osw.close();
	        os.close();			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		
		return resString;
	}
	
	public List<ClientesJson> sendRequestCustomerOracle(String urlStr, String type) {
		try {
		
			urlStr = urlStr + "?request=" + type;
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("GET");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);

	  	    String sb = readFullyAsString(conn.getInputStream(), "UTF-8");
	        List<ClientesJson> clientes = null;
	        
	        ObjectMapper objectMapper = new ObjectMapper();
	        TypeFactory typeFactory = objectMapper.getTypeFactory();
	        clientes = objectMapper.readValue(sb.toString(), typeFactory.constructCollectionType(List.class, ClientesJson.class));
	        
			return clientes;
		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				conn.disconnect();
			}			
		}
		return null;
	}
	
	public List<ArticulosJson> sendRequestItemOracle(String urlStr, String type, StringBuilder message) {
		try {
			
			urlStr = urlStr + "?request=" + type;
			
			message.append("Se envia petici�n a " + urlStr);
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("GET");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);
	  		conn.setConnectTimeout(60000);

	  		String sb = readFullyAsString(conn.getInputStream(), "UTF-8");
	  		
	  		message.append("Respuesta del servicio web que obtiene los art�culos: " + sb);
	        List<ArticulosJson> articulos = null;	        
	        ObjectMapper objectMapper = new ObjectMapper();
	        TypeFactory typeFactory = objectMapper.getTypeFactory();
	        articulos = objectMapper.readValue(sb.toString(), typeFactory.constructCollectionType(List.class, ArticulosJson.class));
	        
			return articulos;			
		} catch (MalformedURLException e) {
			e.printStackTrace();
			message.append("Ocurri� una excepcion en la peci�n (MalformedURLException): " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			message.append("Ocurri� una excepcion en la peci�n (IOException): " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			message.append("Ocurri� una excepcion en la peci�n (Exception): " + e.getMessage());
		} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}
		
	public List<InventoryTransactionDTO> sendRequestInventoryTrasfersOracle(String urlStr, String type) {
		HttpURLConnection connInv = null;
		try {
		
			urlStr = urlStr + "?request=" + type;
			URL url = new URL(urlStr);
			connInv = (HttpURLConnection) url.openConnection();
	  		connInv.setDoInput(true);
	  		connInv.setDoOutput(true);
	  		connInv.setRequestMethod("GET");
	  		connInv.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		connInv.setRequestProperty("Accept", "application/json");
	  		connInv.setAllowUserInteraction(true);
	  		connInv.setConnectTimeout(60000);

	  		String sb = readFullyAsString(connInv.getInputStream(), "UTF-8");
	        List<InventoryTransactionDTO> items = null;
	        
	        if(!"".equals(sb)) {
	        	 ObjectMapper objectMapper = new ObjectMapper();
	 	        TypeFactory typeFactory = objectMapper.getTypeFactory();
	 	        
	 	        try {
	 	        	items = objectMapper.readValue(sb.toString(), typeFactory.constructCollectionType(List.class, InventoryTransactionDTO.class));	 	        	
	 			} catch (IOException e) {
	 				e.printStackTrace();
	 				objectMapper.clearProblemHandlers();
	 				System.out.println("ERROR TRANSFERENCIAS DE INVENTARIO (IOException): " + e.getMessage()); 			
				}	 	        
	        }
			return items;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("ERROR TRANSFERENCIAS DE INVENTARIO (MalformedURLException): " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR TRANSFERENCIAS DE INVENTARIO (Exception): " + e.getMessage());			
		}
		finally {
			if(connInv != null) {
				connInv.disconnect();
			}
		}
		return null;
	}
	
	public double sendRequestOracle(String urlStr, String request) {
		
		double result = 0.0;
		try {
		
			urlStr = urlStr + "?request=" + request;
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("GET");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);

	  		String sb = readFullyAsString(conn.getInputStream(), "UTF-8");

	        if(!"".equals(sb)) {
	        	result = Double.valueOf(sb);
	        }
	        conn.disconnect();
			return result;
		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	@SuppressWarnings("unused")
	public String insertSicar(String urlStr, Object data) {
		String resString = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			StringBuilder sb = new StringBuilder( mapper.writeValueAsString(data));
			String jsonInString = sb.toString();
			
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);
	  		String encoded = Base64.getEncoder().encodeToString(("INTEGUSER" + ":"+  "Smartech2019$").getBytes(StandardCharsets.UTF_8));  //Java 8
	  		conn.setRequestProperty("Authorization", "Basic "+ encoded);
			
			 OutputStream os = conn.getOutputStream();
	         OutputStreamWriter osw = new OutputStreamWriter(os);
	         byte[] outputBytes = jsonInString.getBytes("UTF-8");
	         os.write(outputBytes);
	         osw.flush();
	         if (conn.getResponseCode() == 200) {
	        	 
	        	resString = readFullyAsString(conn.getInputStream(), "UTF-8");
	            if(!"".equals(resString)) {
		            mapper = new ObjectMapper();
		            JsonNode jsonNode = mapper.readTree(resString);
	            }else {
	    			return "Ha ocurrido un error inesperado. El resultado del ws-* es null";
	            }

		            
	 		}else {
				return "Error: " + conn.getResponseCode() + "-" + conn.getResponseMessage();
	 		}
	        osw.close();
	        os.close();			
		} catch (Exception e) {
			e.printStackTrace();
			return "Error en Sicar, " + e.getMessage();
		} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return resString;
	}
	
	public String readFullyAsString(InputStream inputStream, String encoding){
		String result = "";
		try {
			result = readFully(inputStream).toString(encoding);
		}catch(Exception e) {
			e.printStackTrace();
			result = "";
		}
		return result;  
    }

    private ByteArrayOutputStream readFully(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos;
    }
    
    //UA
    public List<Udc> sendRequestUdcByUdcKey(String urlStr, String udckey) {
		try {
		
			urlStr = urlStr + "?request=" + udckey;
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("GET");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);

	  		String sb = readFullyAsString(conn.getInputStream(), "UTF-8");
	        List<Udc> udcList = null;
	        
	        if(!"".equals(sb)) {
	        	 ObjectMapper objectMapper = new ObjectMapper();
	 	        TypeFactory typeFactory = objectMapper.getTypeFactory();
	 	       udcList = objectMapper.readValue(sb.toString(), typeFactory.constructCollectionType(List.class, Udc.class));
	        }
	       conn.disconnect();
			return udcList;
		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

    
    public List<CreditNoteResponseDTO> getTxtResponses(String urlStr) {
		try {
		
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("GET");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);

	  		String sb = readFullyAsString(conn.getInputStream(), "UTF-8");
	        List<CreditNoteResponseDTO> responseList = null;
	        
	        if(!"".equals(sb)) {
	        	 ObjectMapper objectMapper = new ObjectMapper();
	 	        TypeFactory typeFactory = objectMapper.getTypeFactory();
	 	       responseList = objectMapper.readValue(sb.toString(), typeFactory.constructCollectionType(List.class, CreditNoteResponseDTO.class));
	        }
	       conn.disconnect();
			return responseList;
		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

    
}

package com.tdo.integration.services;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.ClientesJson;
import com.dto.integration.dto.CustomerDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdo.integration.client.localmodel.CustomerAccountSite;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.RestClientDao;

@Service("newCustomerService")
public class NewCustomerService {
	
	@Autowired
	RestClientDao restClientDao; 
	
	@Autowired
	ClientLocalDao clientLocalDao;
	
	public List<CustomerDTO> getNewCustomer(String value) {
		
		List<CustomerDTO> list = restClientDao.getCustomers(value);
		for(CustomerDTO c : list) {
			c.setSite("");
			c.setMsg("");
			c.setProfileClassName("");
			
			if(org.springframework.util.StringUtils.hasText(c.getRfc())) {
				if(c.getRfc().contains("null")) {
					c.setRfc("XXX");
				}
			} else {
				c.setRfc("XXX");
			} 
			
			if(!org.springframework.util.StringUtils.hasText(c.getAddress1())) {
				c.setAddress1("Desconocido");
			}
			
			if(!org.springframework.util.StringUtils.hasText(c.getCity())) {
				c.setCity("Desconocido");
			}
			
			if(!org.springframework.util.StringUtils.hasText(c.getPostalCode())) {
				c.setPostalCode("99999");
			}
			
			if(!org.springframework.util.StringUtils.hasText(c.getState())) {
				c.setState("Desconocido");
			}
			
			if(!org.springframework.util.StringUtils.hasText(c.getCountry())) {
				c.setCountry("MX");
			}
			
			switch(c.getCountry().trim().toUpperCase()) {
			   case "M�XICO" :
			   case "MEXICO" :
			   case "MEX" :
			   case "" :
				   c.setCountry("MX");
				      break;
			   default :
			      break;
			}
			
			if(c.getAccountNumber() != null) {
				if(!"".equals(c.getAccountNumber())) {
					if(c.getAccountNumber().contains("/")) {
						String[] curp = c.getAccountNumber().replace("null", "").split("/");
						if(curp.length == 2) {
							c.setAccountNumber(curp[0].trim());
							c.setSiteNumber(curp[1]);
						} else {
							c.setAccountNumber("");
							c.setSiteNumber("");
						}
					}
				}
			}
		}
		return list;
	}
	
	public void updateCustomer(String res) {
		
		try {
			if(res != null && !res.isEmpty() && !res.contains("Error:")) {
				JSONArray jsonArr = new JSONArray(res);
				
				for (int i = 0; i < jsonArr.length(); i++) {				
			        JSONObject jsonObj = jsonArr.getJSONObject(i);
			        ObjectMapper mapper = new ObjectMapper();
			        CustomerDTO c = mapper.readValue(jsonObj.toString(), CustomerDTO.class);
			        
			        if(!"ERROR".equals(c.getMsg()) && org.springframework.util.StringUtils.hasText(c.getAccountNumber())  && org.springframework.util.StringUtils.hasText(c.getSiteNumber())) {
			        	restClientDao.updateCustomers(c);
			        	this.saveOrUpdateCustomerSiteAccount(c.getAccount(), c.getSiteNumber(), c.getAccountNumber());		        	
			        	String curp = "'" + c.getAccountNumber() + "/" + c.getSiteNumber() + "'";		        	
			        	System.out.println("Se actualiza el cliente " + c.getName() + " en Sicar, con el valor " + curp + ".");
			        }		        
			    }
			} else if(res != null && !res.isEmpty() && res.contains("Error:")){
				System.out.println("NUEVO CLIENTE: Error al consumir el ws: " + res);
			} else {
				System.out.println("NUEVO CLIENTE: No se obtuvieron registros del ws.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean customerExist(ClientesJson c) {
		boolean exists = true;

		try {			
			if(c.getNombre() != null && c.getNombre() != "NULL" && !c.getNombre().contains("NULL ")) {
				exists = restClientDao.customerExists(c);
			} else {
				System.out.println("El cliente " + c.getNombre() + " no es v�lido.");			
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Ocurri� un error al registrar el nuevo cliente: " + e.getMessage());
		}
		
		return exists;
	}
	
	public CustomerAccountSite getCustomerSiteAccount(int customerId) {
		return clientLocalDao.getCustomerSiteAccounById(customerId);
	}
	
	public void saveOrUpdateCustomerSiteAccountClienteJSon(ClientesJson c) {
		
		try {
			if(c != null && c.getCliId() > 0 && c.getCurp() != null && c.getCurp().length() > 0) {
				String[] customerData = c.getCurp().split("/");
				String accountNumber = "";
				String siteNumber = "";
				
				if(customerData[0] != null) {
					accountNumber = customerData[0];
				}
				if(customerData[1] != null) {
					siteNumber = customerData[1];
				}				
				
				if((accountNumber.isEmpty() || (accountNumber.endsWith("C") && !accountNumber.endsWith("AC"))) && siteNumber != null && !siteNumber.isEmpty() && siteNumber.endsWith("S")){
					accountNumber = siteNumber.replace("S", "A");
				}
				
				System.out.println("Se registra el cliente: " + c.getNombre() + " en la tabla ClientSiteAccount (Account " + accountNumber + ", Site " + siteNumber + ").");
				this.saveOrUpdateCustomerSiteAccount(c.getCliId(), siteNumber, accountNumber);				
			} else {
				if(c == null) {
					System.out.println("ClienteJSon nulo.");
				} else if(c.getCliId() <= 0) {
					System.out.println("Id del cliente no v�lido [" + c.getCliId() + "].");
				} else if(c.getCurp() == null) {
					System.out.println("Curp del cliente nulo.");
				} else if(c.getCurp().length() == 0) {
					System.out.println("Curp del cliente vac�o.");
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Ocurri� un error al registrar en ClientSiteAccount del nuevo cliente: " + e.getMessage());
		}		
	}
	
	public void saveOrUpdateCustomerSiteAccount(int customerId, String siteNumber, String accountNumber) {
		
		try {
			CustomerAccountSite csa = new CustomerAccountSite();
			csa.setCustomerId(customerId);
			csa.setSiteNumber(siteNumber);
			csa.setAccountNumber(accountNumber);
			
			clientLocalDao.saveOrUpdateCustomerSiteAccoun(csa);			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Ocurri� un error al registrar en CustomerSiteAccount del nuevo cliente: " + e.getMessage());
		}		
	}
	
}

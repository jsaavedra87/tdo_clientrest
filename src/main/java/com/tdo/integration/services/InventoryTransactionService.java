package com.tdo.integration.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.InventoryTransactionDTO;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.client.localmodel.InventoryTransferControl;
import com.tdo.integration.client.localmodel.Udc;
import com.tdo.integration.client.model.Articulo;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.CustomerLocalDao;
import com.tdo.integration.dao.InventorySicarDao;

@Service("inventoryTransactionService")
@Transactional
public class InventoryTransactionService {
	
	private final static String INV_PURCHASE_RECEIPT = "Purchase Order Receipt";
	private final static String ITEM_TYPE_USD = "USD";

	@Autowired
	InventorySicarDao inventorySicarDao; 
	
	@Autowired
	ClientLocalDao clientLocalDao;
	
	@Autowired
	CustomerLocalDao customerLocalDao;
	
	@Autowired 
	UDCService udcService;
	
	@Autowired
	RESTService restService;

	public String newInventoryTransactionsRecord(List<InventoryTransactionDTO> invList, Properties props, String organizationCode, String fromDate) {
		boolean saveOrUpdateTransaction = false;
		boolean transactionExists = false;
		StringBuffer result = new StringBuffer();
		
		try {
			Date date = dateCalculate(fromDate.replace("_", " "), "yyyy-MM-dd HH:mm:ss", -1);
			List<InventoryTransferControl> listITC = clientLocalDao.getInventoryTransferControlByDate(date);
			inventorySicarDao.updateSicarItemLot();
			
			for(InventoryTransactionDTO o : invList) {
				try {
					saveOrUpdateTransaction = false;
					transactionExists = false;

					if(listITC != null && listITC.size() > 0) {
						for(InventoryTransferControl itc : listITC) {
							if(o.getTransactionId().equals(itc.getTransactionId())) {							
								transactionExists = true;
								break;
							} 
						}
					}

					if(!transactionExists) {
						result.append("\r| ").append(o.getTransactionId()).append(" | ");
						if("MXN".equals(o.getItemType()) || "USD".equals(o.getItemType())) {
							List<BigInteger> lstLotId = null;
							int lotId = 0;
							int itemId = 0;																	
							itemId = inventorySicarDao.getItemId(o.getItemName().trim());
							
							if(itemId > 0) {
								saveOrUpdateTransaction = true;
								result.append("Obtiene articulo de Sicar ").append(o.getItemName().trim()).append(" | ");							
								o.setItemId(itemId);
								
								if(o.getQuantity() > 0) {
									result.append("Ingreso de Inventario ").append(o.getQuantity()).append(" | ");
									lotId = inventorySicarDao.getLotId(o.getLotNumber().trim(),  itemId);
									
									if(lotId > 0) {
										result.append("Obtiene lote ").append(o.getLotNumber().trim()).append(" de Sicar | ");
						        		double lotQty = inventorySicarDao.getLotQty(lotId);
						        		double oldQty = lotQty;
						        		result.append("Obtiene la cantidad inicial en el lote de Sicar ").append(oldQty).append(" | ");
						        		
						        		if(lotQty > 0) {
						        			lotQty = lotQty + o.getQuantity();
						        		} else {
						        			lotQty = o.getQuantity();
						        		}
						        								        		
						        		result.append("Actualiza la cantidad en el lote de Sicar ").append(lotQty).append(" | ");
						        		inventorySicarDao.updateLotQuantity(itemId, lotId, lotQty, oldQty, o.getQuantity());
									} else {																				
										result.append("Inserta nuevo lote ").append(o.getLotNumber()).append(" en Sicar | ");
					        			result.append("Actualiza la cantidad ").append(o.getQuantity()).append(" en el lote de Sicar | ");
					        			inventorySicarDao.insertLot(o);
									}
									
								} else {									
									result.append("Descuento de Inventario ").append(o.getQuantity()).append(" | ");
									lstLotId = inventorySicarDao.getListLotId(o.getLotNumber().trim(),  itemId, 1);
									double quantity = Math.abs(o.getQuantity());
									
									if(lstLotId != null && !lstLotId.isEmpty()) {
										for(BigInteger id : lstLotId) {
											result.append("Obtiene lote ").append(o.getLotNumber().trim()).append(" de Sicar (lot_id ").append(id).append(") | ");
							        		double lotQty = inventorySicarDao.getLotQty(id.intValue());
							        		double oldQty = lotQty;
							        		double newQty = 0.0;
							        		double itemQty = 0.0;
							        		
							        		result.append("Obtiene la cantidad inicial en el lote ").append(oldQty).append(" | ");
							        		
							        		if(lotQty > 0 && quantity > 0) {							        			
							        			if(lotQty >= quantity) {							        				
							        				
							        				itemQty = quantity*(-1);
							        				newQty = lotQty - quantity;
							        				quantity = 0.0;
							        				
							        				result.append("Actualiza la cantidad ").append(newQty).append(" en el lote de Sicar | ");
							        				inventorySicarDao.updateLotQuantity(itemId, id.intValue(), newQty, oldQty, itemQty);							        				
							        			} else {							        				
							        				
							        				itemQty = lotQty*(-1);
							        				newQty = 0.0;
							        				quantity = quantity - lotQty;
							        				
							        				result.append("Actualiza la cantidad ").append(newQty).append(" en el lote de Sicar | ");
							        				inventorySicarDao.updateLotQuantity(itemId, id.intValue(), newQty, oldQty, itemQty);							        				
							        			}							        										        			
							        		}
										}
									}
								}
					        	
//					        	if(INV_PURCHASE_RECEIPT.equals(o.getTransactionType().trim()) && AppConstants.CEDI_CODE.equals(organizationCode) ) {
//					        		Udc udc = udcService.searchBySystemAndKey(AppConstants.UDC_TIPO_CAMBIO, AppConstants.UDC_TIPO_CAMBIO_KEY);
//					        		result.append("Actualiza Costo Promedio en Sicar ").append(String.valueOf(o.getAverageCost())).append(" MXN | ");;
//					        		inventorySicarDao.registerAverageCost(o);
//					        		
//					        		if(ITEM_TYPE_USD.equals(o.getItemType())) {					        			
//					        			result.append("Art�culo en USD | Obtiene Art�culo USD en Sicar ").append(o.getItemName().trim()).append(" | ");
//					        			Articulo art = inventorySicarDao.getArticulo(o.getItemName().trim());
//					        			
//					        			result.append("Obtiene el precio del art�culo en sicarproductprices ").append(art.getArticuloClave()).append(" | ");
//					        			String itemCode = customerLocalDao.getUsdPricingItem(art.getArticuloClave());				        			
//					        			String value = new StringBuffer().append(organizationCode).append("/").append(o.getItemName()).toString();
//					        								        			
//					        			double usdAvgCost = restService.sendRequestOracle(props.getProperty("restAvgRate"), value);
//					        			double usdExchangeRate = Double.valueOf(udc.getUdcValue());
//
//					        			result.append("Tipo de Cambio ").append(String.valueOf(usdExchangeRate)).append(" | ");
//					        			
//					        			if(usdAvgCost == 0) {					        				
//					        				if(usdExchangeRate > 0) {
//						        				if(o.getAverageCost() > 0) {
//						        					usdAvgCost = o.getAverageCost() / usdExchangeRate;
//						        				} else {
//						        					usdAvgCost = art.getPrecioCompra().doubleValue() / usdExchangeRate;
//						        				}
//					        				}					        				
//					        			}
//					        			
//					        			
//					        			if("".equals(itemCode)) {
//					        				result.append("Inserta el nuevo Costo Promedio USD ").append(String.valueOf(usdAvgCost)).append(" | ");
//					        				customerLocalDao.insertItemPricing(art,  o,  usdAvgCost, usdExchangeRate);
//					        			}else {
//					        				result.append("Actualiza el nuevo Costo Promedio USD ").append(String.valueOf(usdAvgCost)).append(" | ");
//					        				customerLocalDao.updateUsdPricing(art,  o,  usdAvgCost, usdExchangeRate);
//					        			}
//
//					        			double newAvgCost = (usdAvgCost * usdExchangeRate);					        			
//					        			newAvgCost = Math. round(newAvgCost * 1000.0) / 1000.0;
//					        			o.setAverageCost(newAvgCost);
//					        			
//					        			result.append("Actualiza nuevo costo Promedio en Sicar USD-MXN ").append(String.valueOf(newAvgCost)).append(" | ");					        			
//					        			inventorySicarDao.registerAverageCost(o);
//					        		}					        		
//					        	}
					        	inventorySicarDao.updateSicarNubeAct(itemId);	
							} else {
								result.append("No se encontr� el art�culo " + o.getItemName().trim() + " en Sicar | ");
							}
						} else {
							saveOrUpdateTransaction = true;
							result.append("El tipo de moneda es: " + o.getItemType() + " (Valores permitidos MXN o USD) | ");
						}
						
						if(saveOrUpdateTransaction) {
							saveOrUpdateInventoryTransactionsControl(o, AppConstants.INV_TRANS_COMPLETED);
						}
						
						result.append("Proceso Terminado.");						
					}
				} catch (Exception e) {
					result.append("Error: ").append(e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
			
		} catch(Exception e){
			result.append("Error: ").append(e.getMessage());
			e.printStackTrace();			
		}
		
		return result.toString();
	}
	
	public String updateInventoryTransactionsRecord(List<InventoryTransactionDTO> invList, Properties props, String organizationCode, String fromDate) {
		boolean updateTransaction = false;
		boolean transactionExists = false;
		double initCost = 0;
		StringBuffer result = new StringBuffer();
		
		try {
        	if(AppConstants.CEDI_CODE.equals(organizationCode)) {
    			Date date = dateCalculate(fromDate.replace("_", " "), "yyyy-MM-dd HH:mm:ss", -1);
    			List<InventoryTransferControl> listITC = clientLocalDao.getInventoryTransferControlByDate(date);
    			
    			for(InventoryTransactionDTO o : invList) {
    				try {
    					if(INV_PURCHASE_RECEIPT.equals(o.getTransactionType().trim())) {
        					updateTransaction = false;
        					transactionExists = false;
        					initCost = 0;
        					
        					if(listITC != null && listITC.size() > 0) {
        						for(InventoryTransferControl itc : listITC) {
        							if(o.getTransactionId().equals(itc.getTransactionId())) {							
        								transactionExists = true;
        								initCost = itc.getAverageCost().doubleValue();
        								break;
        							} 
        						}
        					}
        					
        					if(transactionExists) {
        						//SI LA TRANSACCI�N EXISTE, ACTUALIZAR EL COSTO SI HA CAMBIADO
        						result.append("\r| ").append(o.getTransactionId()).append(" | ");
        						if("MXN".equals(o.getItemType()) || "USD".equals(o.getItemType())) {        							
        							int itemId = 0;																	
        							itemId = inventorySicarDao.getItemId(o.getItemName().trim());
        							
        							if(itemId > 0) {
        								o.setItemId(itemId);
    				        			result.append("Art�culo | Obtiene Art�culo en Sicar ").append(o.getItemName().trim()).append(" | ");
    				        			Articulo art = inventorySicarDao.getArticulo(o.getItemName().trim());
    				        			
        				        		//SI ES ART�CULO EN DOLARES
        				        		if(ITEM_TYPE_USD.equals(o.getItemType())) {
        				        			Udc udc = udcService.searchBySystemAndKey(AppConstants.UDC_TIPO_CAMBIO, AppConstants.UDC_TIPO_CAMBIO_KEY);        				        			
        				        			result.append("Obtiene el precio del art�culo en sicarproductprices ").append(art.getArticuloClave()).append(" | ");
        				        			String itemCode = customerLocalDao.getUsdPricingItem(art.getArticuloClave());				        			
        				        			String value = new StringBuffer().append(organizationCode).append("/").append(o.getItemName()).toString();
        				        								        			
        				        			double usdAvgCost = restService.sendRequestOracle(props.getProperty("restAvgRate"), value);
        				        			double usdExchangeRate = Double.valueOf(udc.getUdcValue());

        				        			result.append("Tipo de Cambio ").append(String.valueOf(usdExchangeRate)).append(" | ");
        				        			
        				        			if(usdAvgCost == 0) {					        				
        				        				if(usdExchangeRate > 0) {
        					        				if(o.getAverageCost() > 0) {
        					        					usdAvgCost = o.getAverageCost() / usdExchangeRate;
        					        				} else {
        					        					usdAvgCost = art.getPrecioCompra().doubleValue() / usdExchangeRate;
        					        				}
        				        				}					        				
        				        			}
        				        			        				        		

        				        			double newAvgCost = (usdAvgCost * usdExchangeRate);					        			
        				        			newAvgCost = Math. round(newAvgCost * 1000.0) / 1000.0;
        				        			o.setAverageCost(newAvgCost);
        				        			
        				        			if((int)art.getPrecioCompra().doubleValue() != (int)o.getAverageCost()) {
            				        			if("".equals(itemCode)) {
            				        				result.append("Inserta el nuevo Costo Promedio USD en SicarProductPrices ").append(String.valueOf(usdAvgCost)).append(" | ");
            				        				customerLocalDao.insertItemPricing(art,  o,  usdAvgCost, usdExchangeRate);
            				        			}else {
            				        				result.append("Actualiza el nuevo Costo Promedio USD en SicarProductPrices ").append(String.valueOf(usdAvgCost)).append(" | ");
            				        				customerLocalDao.updateUsdPricing(art,  o,  usdAvgCost, usdExchangeRate);
            				        			}	
        				        			}
        				        		}
        				        		
        			        			if((int) art.getPrecioCompra().doubleValue() != (int)o.getAverageCost()) {
        			        				result.append("Actualiza Costo Promedio en Sicar ").append(String.valueOf(art.getPrecioCompra().toString())).append(" - ").append(String.valueOf(o.getAverageCost())).append(" MXN | ");;
        				        			inventorySicarDao.registerAverageCost(o);
        				        			inventorySicarDao.updateSicarNubeAct(itemId);
        				        			updateTransaction = true;
        			        			}
        							} else {
        								result.append("No se encontr� el art�culo " + o.getItemName().trim() + " en Sicar | ");
        							}
        						} else {
        							updateTransaction = true;
        							result.append("El tipo de moneda es: " + o.getItemType() + " (Valores permitidos MXN o USD) | ");
        						}
        						
        						if(updateTransaction) {
        							updateInventoryTransactionsControl(o, AppConstants.INV_TRANS_COMPLETED);
        						}
        						
        						result.append("Proceso Terminado.");
        					}
    					}    					
    				} catch (Exception e) {
    					result.append("Error: ").append(e.getMessage());
    					e.printStackTrace();
    					continue;
    				}
    			}
        	}			
		} catch(Exception e){
			result.append("Error: ").append(e.getMessage());
			e.printStackTrace();			
		}
		
		return result.toString();
	}
	
	public void updateSicarPurchaseCostItem() {
		inventorySicarDao.updateSicarPurchaseCostItem();
	}
	
	public void updateSicarDesglosaIEPS() {
		inventorySicarDao.updateSicarDesglosaIEPS();
	}
	
	public void saveOrUpdateInventoryTransactionsControl(InventoryTransactionDTO transaction, String status) {		
		
		InventoryTransferControl itc = new InventoryTransferControl();

		if(transaction.getCreationDate() != null) {
			itc.setCreationDate(transaction.getCreationDate().toGregorianCalendar().getTime());
		}
		
		if(transaction.getExpirationDate() != null) {
			itc.setExpirationDate(transaction.getExpirationDate().toGregorianCalendar().getTime());
		}
		
		if(transaction.getTransactionDate() != null) {
			itc.setTransactionDate(transaction.getTransactionDate().toGregorianCalendar().getTime());
		}
		
		if(transaction.getTransactionRealDate() != null) {
			itc.setTransactionRealDate(transaction.getTransactionRealDate().toGregorianCalendar().getTime());
		}		
		
		itc.setAverageCost(new BigDecimal(transaction.getAverageCost()));		
		itc.setInventoryLocation(transaction.getInventoryLocation());
		itc.setInventoryOrganizationCode(transaction.getInventoryOrganizationCode());
		itc.setInventoryOrganizationName(transaction.getInventoryOrganizationName());
		itc.setItemDescription(transaction.getItemDescription());
		itc.setItemId(transaction.getItemId());
		itc.setItemName(transaction.getItemName());
		itc.setItemType(transaction.getItemType());
		itc.setLotNumber(transaction.getLotNumber());
		itc.setQuantity(new BigDecimal(transaction.getQuantity()));		
		itc.setSubInventory(transaction.getSubInventory());		
		itc.setTransactionId(transaction.getTransactionId());
		itc.setTransactionType(transaction.getTransactionType());
		itc.setUom(transaction.getUom());
		itc.setLastUpdate(new Date());		
		itc.setStatus(status);

		clientLocalDao.saveOrUpdateInventoryTransferControl(itc);			
	}

	public void updateInventoryTransactionsControl(InventoryTransactionDTO transaction, String status) {		
		
		InventoryTransferControl itc = new InventoryTransferControl();

		if(transaction.getCreationDate() != null) {
			itc.setCreationDate(transaction.getCreationDate().toGregorianCalendar().getTime());
		}
		
		if(transaction.getExpirationDate() != null) {
			itc.setExpirationDate(transaction.getExpirationDate().toGregorianCalendar().getTime());
		}
		
		if(transaction.getTransactionDate() != null) {
			itc.setTransactionDate(transaction.getTransactionDate().toGregorianCalendar().getTime());
		}
		
		if(transaction.getTransactionRealDate() != null) {
			itc.setTransactionRealDate(transaction.getTransactionRealDate().toGregorianCalendar().getTime());
		}		
		
		itc.setAverageCost(new BigDecimal(transaction.getAverageCost()));		
		itc.setInventoryLocation(transaction.getInventoryLocation());
		itc.setInventoryOrganizationCode(transaction.getInventoryOrganizationCode());
		itc.setInventoryOrganizationName(transaction.getInventoryOrganizationName());
		itc.setItemDescription(transaction.getItemDescription());
		itc.setItemId(transaction.getItemId());
		itc.setItemName(transaction.getItemName());
		itc.setItemType(transaction.getItemType());
		itc.setLotNumber(transaction.getLotNumber());
		itc.setQuantity(new BigDecimal(transaction.getQuantity()));		
		itc.setSubInventory(transaction.getSubInventory());		
		itc.setTransactionId(transaction.getTransactionId());
		itc.setTransactionType(transaction.getTransactionType());
		itc.setUom(transaction.getUom());
		itc.setLastUpdate(new Date());		
		itc.setStatus(status);

		clientLocalDao.updateInventoryTransferControl(itc);			
	}
	
	public static Date dateCalculate(String dateString, String dateFormat, int days) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.DATE, days);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
}


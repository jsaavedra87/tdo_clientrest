package com.tdo.integration.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.BaseResponse;
import com.dto.integration.dto.RaInterface;
import com.dto.integration.dto.RaInterfaceDistributionsAll;
import com.dto.integration.dto.RaInterfaceLinesAll;
import com.dto.integration.dto.SalesDTO;
import com.dto.integration.util.AppConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdo.integration.client.localmodel.BusinessUnitMapping;
import com.tdo.integration.client.localmodel.CustomerAccountSite;
import com.tdo.integration.client.localmodel.FileTransferControl;
import com.tdo.integration.client.localmodel.GeneralLedgerMapping;
import com.tdo.integration.client.localmodel.GeneralLedgerTaxMapping;
import com.tdo.integration.client.localmodel.Udc;
import com.tdo.integration.client.model.Articulo;
import com.tdo.integration.client.model.ArticuloImpuesto;
import com.tdo.integration.client.model.NotaCredito;
import com.tdo.integration.client.model.Sales;
import com.tdo.integration.client.model.TipoPago;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.RestClientDao;

@Service("cxCService")
public class CxCService {
	
	@Autowired
	RestClientDao restClientDao; 
	
	@Autowired
	ClientLocalDao clientLocalDao;
	
	@Autowired
	ClientLocalService clientLocalService;

	@Autowired
	RESTService restService;
	
	@Autowired 
	UDCService udcService;
	
	List<Sales> s;
	
	private final String TIPO_PAGO_CREDITO = "CR";
	private final String TAX_TYPE_16 = "SUBTOTAL 16";
	private final String TAX_TYPE_0 = "SUBTOTAL 0";
	private final String IVA_TEXTO = "I.V.A.";
	private final String IEPS_TEXTO = "IEPS";
	private final String EXENTO_TEXTO = "EXENTO";
	
	Calendar now = Calendar.getInstance();
	String year = String.valueOf(now.get(Calendar.YEAR));
	String month = String.valueOf(now.get(Calendar.MONTH) + 1); // Note: zero based!
	String day = String.valueOf(now.get(Calendar.DAY_OF_MONTH));
	String hour = String.valueOf(now.get(Calendar.HOUR_OF_DAY));
	String minute = String.valueOf(now.get(Calendar.MINUTE));
	String second = String.valueOf(now.get(Calendar.SECOND));
	
	List<GeneralLedgerMapping> glm = null;
	Map<String, GeneralLedgerMapping> glmMap = new HashMap<String, GeneralLedgerMapping>();
	
	List<GeneralLedgerMapping> glmnc = null;
	Map<String, GeneralLedgerMapping> glmNcMap = new HashMap<String, GeneralLedgerMapping>();
	
	List<BusinessUnitMapping> bu = null;
	Map<String, BusinessUnitMapping> buMap = new HashMap<String, BusinessUnitMapping>();
	
	List<GeneralLedgerTaxMapping> gltm = null;
	Map<String, GeneralLedgerTaxMapping> gltmMap = new HashMap<String, GeneralLedgerTaxMapping>();
	
	@SuppressWarnings("unused")
	public void startCXCProcess(String sucursal, int idCC, String restARService, String restARFindService) {
		String transactionType = AppConstants.FILE_TRANSACTION_TYPE_AR;
		List<FileTransferControl> lstFTC = null;
		String dateFormat = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		BigInteger ultimoCC;
		String initFromDate = sdf.format(new Date());
		String initToDate = sdf.format(new Date());		
		String fromDate = "";
		String toDate = "";	
				
		try {			
			//Verifica si ya existe el registro
			FileTransferControl obj = clientLocalService.searchFileTransferControlByIdCC(transactionType, idCC);
			
			//Si no existe crea nuevo registro
			if(obj == null) {
				clientLocalService.createFileTransferControl(transactionType, idCC, initFromDate, initToDate, AppConstants.FILE_REQUEST_NEW, null);
			}
			
			//Obtiene registros pendientes desde siete d�as antes para procesarlos
			Date queryFromDate = dateCalculateDays(initFromDate.substring(0, 10) + " 00:00:00", dateFormat, -7);
			Date queryToDate = sdf.parse(initToDate);
			String[] pendingStatus = new String[]{AppConstants.FILE_REQUEST_NEW, AppConstants.FILE_REQUEST_SENT, AppConstants.FILE_REQUEST_ERROR};
			
			lstFTC = clientLocalService.searchFileTransferControlByDateRangeAndStatus(transactionType, queryFromDate, queryToDate, pendingStatus);

			//Procesa los registros pendientes			
			if(lstFTC != null && lstFTC.size() > 0) {
				for(FileTransferControl ftc : lstFTC) {					
					try {
						fromDate = sdf.format(ftc.getFromDate());
						toDate = sdf.format(ftc.getToDate());
						ftc.setLog("");
						
						if(!AppConstants.FILE_REQUEST_NEW.equals(ftc.getStatus())) {
							clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NEW);
						}						    
						
						//SE CREAN LISTAS DE AR
						RaInterface raInterface = this.createRaInterfaceLinesAll(sucursal, AppConstants.REQUEST_TYPE_CC, new String[] { String.valueOf(ftc.getIdCC()) }, ftc, restARFindService);
						
						if(raInterface != null && ((raInterface.getRaInterfaceDistributionsAll() != null && !raInterface.getRaInterfaceDistributionsAll().isEmpty())
								|| (raInterface.getRaInterfaceLinesAll() != null && !raInterface.getRaInterfaceLinesAll().isEmpty()))) {
							
							if(AppConstants.FILE_REQUEST_ERROR.equals(ftc.getStatus())) {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
							} else {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SENT);
							}
							raInterface.setFileTransferControl(ftc);
							
							String response = restService.sendRequestObject(restARService, raInterface);
							System.out.print("Respuesta de env�o de AR para el CC" + String.valueOf(ftc.getIdCC()) + ": \r" + response);
							
							if(response != null && response.replace("\"", "").contains(AppConstants.FILE_RESPONSE_SUCCESS)) {
								ftc.setLog(response.replace("\"", ""));
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SUCCESS);
							} else {
								if(!AppConstants.FILE_REQUEST_ERROR.equals(ftc.getStatus())) {
									ftc.setLog("Error en la respuesta del ws: " + response);
									clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
								}
							}							
						} else {							
							ultimoCC = this.getLastId();
							if(ftc.getIdCC() <= ultimoCC.intValue()) {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NO_SALE);
							} else {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NEW);
							}							
						}
						
					} catch (Exception e) {
						ftc.setLog("Error en clientREST: " + e.getMessage());
						clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			clientLocalService.createFileTransferControl(transactionType, idCC, initFromDate, initToDate, AppConstants.FILE_REQUEST_ERROR, "Ocurri� un error al ejecutar la integraci�n de env�o de AR: " + e.getMessage());
		}
	}
	
	public RaInterface createRaInterfaceLinesAll(String sucursal, String requestType, String[] params, FileTransferControl ftc, String restARFindService) throws Exception {
		Udc udc = null;		
		RaInterface raInterface = new RaInterface();
	
		udc = udcService.searchBySystemAndKey(AppConstants.BUSINESS_UNIT_ID_SYSTEM, AppConstants.BUSINESS_UNIT_ID);		
		raInterface.setBusinessUnitId(udc.getUdcValue());
		
		List<RaInterfaceLinesAll> list = new ArrayList<RaInterfaceLinesAll>();
		List<RaInterfaceDistributionsAll> listDist = new ArrayList<RaInterfaceDistributionsAll>();		
		List<Sales> sales = new ArrayList<Sales>();
		List<Sales> cancelledSales = new ArrayList<Sales>();
		SalesDTO salesDTO = restClientDao.getSales(sucursal, requestType, params);
		
		if(salesDTO != null) {
			sales = salesDTO.getSales();
			cancelledSales = salesDTO.getVentasCanceladas();
		}
		
		SalesDTO ncDTO = restClientDao.getOutOfDateCreditNotes(sucursal, requestType, params);
		List<Sales> creditNote =  new ArrayList<Sales>();
		if(ncDTO != null) {
			creditNote = ncDTO.getSales();
		}
				
		glm = clientLocalDao.getTaxSalesAccounts(sucursal, "AR");
		if(glm != null) {
			for(GeneralLedgerMapping o : glm) {
				glmMap.put(o.getTaxCodeReference(), o);
			}
		}
		
		glmnc = clientLocalDao.getTaxSalesAccounts(sucursal, "ARNC");
		if(glmnc != null) {
			for(GeneralLedgerMapping o : glmnc) {
				glmNcMap.put(o.getTaxCodeReference(), o);
			}
		}
		
		gltm = clientLocalDao.getTaxMapping();
		if(gltm != null) {
			for(GeneralLedgerTaxMapping o : gltm) {
				gltmMap.put(o.getTaxCodeSICAR(), o);
			}
		}

		bu = clientLocalDao.getBusinessUnitMapping();
		if(bu != null) {
			for(BusinessUnitMapping o : bu) {
				buMap.put(o.getCuentaEntidadSICAR(), o);
			}
		}
		
		boolean isCredit = false;
		boolean hasTax = false;
		String taxType = "";
		isCredit = false;
		
		AtomicInteger lineTrans = new AtomicInteger(1);
        AtomicInteger lineNumber = new AtomicInteger(1);        
        List<NotaCredito> notaCreditoList = new ArrayList<NotaCredito>();        
        
		if(sales == null) {
			sales = new ArrayList<Sales>();
		}
		
		for(Sales s : sales) {			
			BigDecimal creditSalePercent = new BigDecimal(0);
			BigDecimal initSaleImport = new BigDecimal(0);
			BigDecimal initCurrencySaleImport = new BigDecimal(0);
			List<TipoPago> l = s.getTipoPago();
			isCredit = false;
			if(!l.isEmpty()) {
				for(TipoPago t : l) {
					if(TIPO_PAGO_CREDITO.equals(t.getTipoPagoClave())) {
						if(t.getVentaTipoPagoTotal() != null &&  t.getVentaTipoPagoTotal().compareTo(new BigDecimal(0)) > 0) {
							creditSalePercent = t.getVentaTipoPagoTotal().divide(s.getVentaTotal(), 10, RoundingMode.HALF_UP);
						}
						isCredit = true;
						break;
					}
				}
				if(isCredit) {
					System.out.println("ID VENTA:" + s.getIdVenta().intValue());
					List<Articulo> arts = s.getArticulo();
					List<ArticuloImpuesto> impList = s.getArticuloImpuesto();
					if(!arts.isEmpty()) {
						lineNumber.set(1);
						for(Articulo a : arts){
							if(a.getVentaDetalleImporteConIva() != null) {
								initSaleImport = initSaleImport.add(a.getVentaDetalleImporteConIva());
							}
							if(a.getMonedaDetalleImporteConIva() != null) {
								initCurrencySaleImport = initCurrencySaleImport.add(a.getMonedaDetalleImporteConIva());
							}
						}
						for(Articulo a : arts){
							taxType = TAX_TYPE_0;  //Default
							hasTax = false;
							List<ArticuloImpuesto> lai = new ArrayList<ArticuloImpuesto>();
							if(!impList.isEmpty()) {
								for(ArticuloImpuesto i : impList) {
									if(s.getIdVenta().intValue() == i.getIdVenta().intValue() && a.getArticuloId().intValue() == i.getArticuloId()) {
										if(i.getVentaDetalleImpuestoNombre() != null) {
											lai.add(i);
											if(i.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO)) {												
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {
													taxType = TAX_TYPE_16;
													hasTax = true;
												}												
											}
											if(i.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO) || i.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
												hasTax = true;
											}											
										}
									}
								}								
							}
							registraArticuloFactura(a, s,  lai, lineTrans, lineNumber, list, listDist, initSaleImport, initCurrencySaleImport, taxType, hasTax, creditSalePercent, ftc);							
						}
					}
				
					//PROCESA NOTAS DE CREDITO
					NotaCredito nc = s.getNotaCredito();
					if(nc != null) {						
						Integer customerId = s.getCliente().getClienteId().intValue();
						String customerName = s.getCliente().getClienteNombre();
						CustomerAccountSite cas = clientLocalDao.getCustomerSiteAccounById(customerId);
						
						if(cas != null) {
							if(cas.getAccountNumber() != null && !cas.getAccountNumber().trim().isEmpty()) {
								nc.setAccNbr(cas.getAccountNumber());
							} else {
								ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
								ftc.setLog("No fue posible recuperar el dato Account Number del cliente: " + customerName + " (Id " + customerId + ").");
							}
							if(cas.getSiteNumber() != null && !cas.getSiteNumber().trim().isEmpty()) {
								nc.setSiteNbr(cas.getSiteNumber());
							} else {
								ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
								ftc.setLog("No fue posible recuperar el dato Site Number del cliente: " + customerName + " (Id " + customerId + ").");
							}
						} else {
							ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
							ftc.setLog("No fue posible recuperar los datos Account Number y Site Number del cliente: " + customerName + " (Id " + customerId + ").");
						}
						
						nc.setEmpresa(s.getEmpresa());						
						notaCreditoList.add(nc);
					}
				}
			}		
		}
		lineTrans.incrementAndGet();
		
		// Procesa notas de cr�dito en la misma transacci�n
		if(!notaCreditoList.isEmpty()) {
			for(NotaCredito nc : notaCreditoList) {
				BigDecimal creditSalePercent = new BigDecimal(0);
				List<TipoPago> l = nc.getTipoPago();
				isCredit = false;
				if(nc.getStatus() == 1 && !l.isEmpty()) {
					for(TipoPago t : l) {
						if(TIPO_PAGO_CREDITO.equals(t.getTipoPagoClave())) {
							if(t.getVentaTipoPagoTotal() != null &&  t.getVentaTipoPagoTotal().compareTo(new BigDecimal(0)) > 0) {
								creditSalePercent = t.getVentaTipoPagoTotal().divide(nc.getTotal(), 10, RoundingMode.HALF_UP);
							}
							isCredit = true;
							break;
						}
					}
					if(isCredit) {
						System.out.println("ID NOTA CREDITO:" + nc.getIdNotaCredito().intValue());				
						List<Articulo> artList = nc.getArticulo();
						List<ArticuloImpuesto> impList = nc.getArticuloImpuesto();
						if(artList != null) {
							lineNumber.set(1);
							for(Articulo a : artList) {
								taxType = TAX_TYPE_0;
								hasTax = false;
								List<ArticuloImpuesto> lai = new ArrayList<ArticuloImpuesto>();
								if(!impList.isEmpty()) {
									for(ArticuloImpuesto i : impList) {
										if(a.getArticuloId().intValue() == i.getArticuloId()) {
											if(i.getVentaDetalleImpuestoNombre() != null) {
												lai.add(i);
												if(i.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO)) {												
													if(i.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {
														taxType = TAX_TYPE_16;
														hasTax = true;
													}												
												}
												if(i.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO) || i.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
													hasTax = true;
												}
											}
										}
									}
								}
								registraArticuloNC(a, nc,  lai, lineTrans, lineNumber, list, listDist, taxType, hasTax, creditSalePercent, restARFindService, ftc);								
							}
						}
					}
				}
			}
			lineTrans.incrementAndGet();
		}
		
		
		// PROCESA LAS CANCELACIONES
		if(cancelledSales != null) {
			for(Sales s : cancelledSales) {
				BigDecimal creditSalePercent = new BigDecimal(0);
				List<TipoPago> l = s.getTipoPago();				
				isCredit = false;
				if(!l.isEmpty()) {
					for(TipoPago t : l) {
						if(TIPO_PAGO_CREDITO.equals(t.getTipoPagoClave())) {
							if(t.getVentaTipoPagoTotal() != null &&  t.getVentaTipoPagoTotal().compareTo(new BigDecimal(0)) > 0) {
								creditSalePercent = t.getVentaTipoPagoTotal().divide(s.getVentaTotal(), 10, RoundingMode.HALF_UP);
							}
							isCredit = true;
							break;
						}
					}
					if(isCredit) {
						List<Articulo> artList = s.getArticulo();
						List<ArticuloImpuesto> impList = s.getArticuloImpuesto();
						if(artList != null) {
							lineNumber.set(1);
							for(Articulo a : artList) {
								taxType = TAX_TYPE_0;
								hasTax = false;
								List<ArticuloImpuesto> lai = new ArrayList<ArticuloImpuesto>();
								if(!impList.isEmpty()) {
									for(ArticuloImpuesto i : impList) {
										if(a.getArticuloId().intValue() == i.getArticuloId()) {
											if(i.getVentaDetalleImpuestoNombre() != null) {
												lai.add(i);
												if(i.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO)) {												
													if(i.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {
														taxType = TAX_TYPE_16;
														hasTax = true;
													}												
												}
												if(i.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO) || i.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
													hasTax = true;
												}
											}
										}
									}
								}
								registraArticuloNCExtemporaneo(a, s,  lai, lineTrans, lineNumber, list, listDist, taxType, hasTax, creditSalePercent, restARFindService, ftc);								
							}
						}
					}
				}
			}
			lineTrans.incrementAndGet();
		}

				
		// Procesa notas de cr�dito extempor�neas
		isCredit = false;
		for(Sales s : creditNote) {
			BigDecimal creditSalePercent = new BigDecimal(0);			
			List<TipoPago> l = s.getTipoPago();
			isCredit = false;
			if(s.getVentaStatus() == 1 && !l.isEmpty()) {
				for(TipoPago t : l) {
					if(TIPO_PAGO_CREDITO.equals(t.getTipoPagoClave())) {
						if(t.getVentaTipoPagoTotal() != null &&  t.getVentaTipoPagoTotal().compareTo(new BigDecimal(0)) > 0) {
							creditSalePercent = t.getVentaTipoPagoTotal().divide(s.getVentaTotal(), 10, RoundingMode.HALF_UP);
						}						
						isCredit = true;
						break;
					}
				}
				if(isCredit) {
					System.out.println("ID NOTA CREDITO:" + s.getIdVenta().intValue());
					List<Articulo> arts = s.getArticulo();
					List<ArticuloImpuesto> impList = s.getArticuloImpuesto();
					if(!arts.isEmpty()) {
						lineNumber.set(1);
						for(Articulo a : arts){
							taxType = TAX_TYPE_0;
							hasTax = false;
							List<ArticuloImpuesto> lai = new ArrayList<ArticuloImpuesto>();
							if(!impList.isEmpty()) {
								for(ArticuloImpuesto i : impList) {
									if(s.getIdVenta().intValue() == i.getIdVenta().intValue() && a.getArticuloId().intValue() == i.getArticuloId()) {
										if(i.getVentaDetalleImpuestoNombre() != null) {
											lai.add(i);
											if(i.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO)) {												
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {
													taxType = TAX_TYPE_16;
													hasTax = true;
												}												
											}
											if(i.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO) || i.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
												hasTax = true;
											}
										}
									}
								}
							}
							registraArticuloNCExtemporaneo(a, s,  lai, lineTrans, lineNumber, list, listDist, taxType, hasTax, creditSalePercent, restARFindService, ftc);							
						}				
					}
				}
			}		
		}
		
		raInterface.setRaInterfaceLinesAll(list);
		raInterface.setRaInterfaceDistributionsAll(listDist);
		return raInterface;
  }
	
	@SuppressWarnings("unused")
	public void registraArticuloFactura(Articulo a, Sales s, List<ArticuloImpuesto> lai, AtomicInteger lineTrans,
			                     AtomicInteger lineNumber, List<RaInterfaceLinesAll> list, List<RaInterfaceDistributionsAll> listDist,
			                     BigDecimal initSaleImport, BigDecimal initCurrencySaleImport, 
			                     String taxType, boolean hasTax, BigDecimal creditSalePercent,
			                     FileTransferControl ftc) throws Exception {
		String strTN = null;
		
		try {
			SimpleDateFormat sdfr = new SimpleDateFormat("yyyy/MM/dd");
			Format formatter = new SimpleDateFormat("yyMMdd");		
			this.validatePackageItem(a, s.getArticuloPaquete());
			
			//RAinterfaceALL
			RaInterfaceLinesAll ar = new RaInterfaceLinesAll();
			ar.setTransaction_Batch_Source_Name("SICAR");
			ar.setTransaction_Type_Name("FACTURA");
			
			if(s.getDiasCredito() != null) {
				ar.setPayment_Terms(String.valueOf(s.getDiasCredito()));
			} else {
				ar.setPayment_Terms("0");
			}
			
			ar.setTransaction_Date(sdfr.format(s.getVentaFecha()));
			ar.setAccounting_Date(sdfr.format(s.getVentaFecha()));
	 		
			String strIdBranch = String.format("%02d", clientLocalDao.getBranch(0).getId());
			String strDate = formatter.format(s.getVentaFecha());				
	
			if(s.getIdTicket() != null) {
				strTN = new StringBuffer("FAC").append(strIdBranch).append("-T").append(s.getIdTicket().intValue()).toString();
				
			} else if(s.getIdNotaVenta() != null) {
				strTN = new StringBuffer("FAC").append(strIdBranch).append("-N").append(s.getIdNotaVenta().intValue()).toString();
				
			} else {
				strTN = new StringBuffer("FAC").append(strIdBranch).append("-V").append(s.getIdVenta().intValue()).toString();
			}
					
			ar.setTransaction_Number(strTN);
			
			String accNbr = "";
			String siteNbr = "";
	
			Integer customerId = s.getCliente().getClienteId().intValue();
			String customerName = s.getCliente().getClienteNombre();
			CustomerAccountSite cas = clientLocalDao.getCustomerSiteAccounById(customerId);
			
			if(cas != null) {
				if(cas.getAccountNumber() != null && !cas.getAccountNumber().trim().isEmpty()) {
					accNbr = cas.getAccountNumber();
				} else {
					ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
					ftc.setLog("No fue posible recuperar el dato Account Number del cliente: " + customerName + " (Id " + customerId + ").");
				}
				if(cas.getSiteNumber() != null && !cas.getSiteNumber().trim().isEmpty()) {
					siteNbr = cas.getSiteNumber();
				} else {
					ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
					ftc.setLog("No fue posible recuperar el dato Site Number del cliente: " + customerName + " (Id " + customerId + ").");
				}
			} else {
				ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
				ftc.setLog("No fue posible recuperar los datos Account Number y Site Number del cliente: " + customerName + " (Id " + customerId + ").");
			}
			
			ar.setBill_to_Customer_Account_Number(accNbr);
			ar.setBill_to_Customer_Site_Number(siteNbr);
			ar.setTransaction_Line_Type("LINE");
			ar.setTransaction_Line_Description(a.getArticuloDescripcion());
			ar.setCurrency_Code(s.getVentaMoneda());
			ar.setCurrency_Conversion_Type("User");
			ar.setCurrency_Conversion_Date(sdfr.format(s.getVentaFecha()));
			ar.setCurrency_Conversion_Rate(String.valueOf(s.getVentaTipoCambio()));
			
			String quantity = String.valueOf(a.getVentaDetalleCantidad());
			String price = "";
			String amount = "";
			BigDecimal noTaxImport;
			
			if(AppConstants.CURRENCY_CODE_MXN.equals(s.getVentaMoneda())) {
				noTaxImport = a.getVentaDetalleImporteSinImp();
				
				//Se obtiene la parte proporcional pagada a cr�dito
				noTaxImport = creditSalePercent.multiply(noTaxImport);
				
				price = getValidatePrice(noTaxImport, a.getVentaDetalleCantidad()).toString();
				amount = getValidateAmount(a.getVentaDetalleCantidad(), new BigDecimal(price), noTaxImport, ftc).toString();
				
			} else {
				noTaxImport = a.getMonedaDetalleImporteSinImp();
				
				//Se obtiene la parte proporcional pagada a cr�dito
				noTaxImport = creditSalePercent.multiply(noTaxImport);
				
				price = getValidatePrice(noTaxImport, a.getVentaDetalleCantidad()).toString();
				amount = getValidateAmount(a.getVentaDetalleCantidad(), new BigDecimal(price), noTaxImport, ftc).toString();
			}
			
			ar.setTransaction_Line_Amount(amount);
			ar.setTransaction_Line_Quantity(quantity);
			ar.setUnit_Selling_Price(price);
			ar.setLine_Transactions_Flexfield_Context("SICAR");
			ar.setLine_Transactions_Flexfield_Segment_1(ar.getTransaction_Number());
			ar.setLine_Transactions_Flexfield_Segment_2("Line" + lineNumber);
			ar.setLine_Transactions_Flexfield_Segment_3(ar.getTransaction_Type_Name());
			ar.setTax_Classification_Code("");
			ar.setUnit_of_Measure_Code(a.getVentaDetalleUnidad());
			ar.setTax_Regime_Code("");
			ar.setTax("");
			ar.setTax_Status_Code("");
			ar.setTax_Rate_Code("");
			ar.setTax_Rate("");
			ar.setLink_to_Transactions_Flexfield_Context("");
			ar.setLink_to_Transactions_Flexfield_Segment_1("");
			ar.setLink_to_Transactions_Flexfield_Segment_2("");
			ar.setLink_to_Transactions_Flexfield_Segment_3("");		
			ar.setInventory_Item_Number(a.getArticuloClave());
			ar.setOverride_AutoAccounting_Flag("Y");
			ar.setBusiness_Unit_Name(((BusinessUnitMapping)buMap.get(s.getEmpresa().getCuenta())).getBusinessUnit());
			ar.setEnd("END");
			
			//RAinterfaceAccountsDistribution		
			RaInterfaceDistributionsAll ad = new RaInterfaceDistributionsAll();
			ad.setAccount_Class("REV");
			ad.setAmount(ar.getTransaction_Line_Amount());
			ad.setPercent("100");
			ad.setLine_Transactions_Flexfield_Context(ar.getLine_Transactions_Flexfield_Context());
			ad.setLine_Transactions_Flexfield_Segment_1(ar.getLine_Transactions_Flexfield_Segment_1());
			ad.setLine_Transactions_Flexfield_Segment_2(ar.getLine_Transactions_Flexfield_Segment_2());
			ad.setLine_Transactions_Flexfield_Segment_3(ar.getTransaction_Type_Name());
			GeneralLedgerMapping glm = (GeneralLedgerMapping)glmMap.get(taxType.trim());
			ad.setAccounting_Flexfield_Segment_1(glm.getCompany());
			ad.setAccounting_Flexfield_Segment_2(glm.getCostCenter());
			ad.setAccounting_Flexfield_Segment_3(glm.getAccount());
			ad.setBusiness_Unit_Name(((BusinessUnitMapping)buMap.get(s.getEmpresa().getCuenta())).getBusinessUnit());
			ad.setEnd("END");
			
			list.add(ar);
			listDist.add(ad);
			lineNumber.incrementAndGet();
	
		} catch (Exception e) {
			e.printStackTrace();
			ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
			ftc.setLog("Error al  insertar l�nea en AR " + strTN + ":" + e.getMessage());
		}
	}	
	
	@SuppressWarnings("unused")
	public void registraArticuloNC(Articulo a, NotaCredito s, List<ArticuloImpuesto> lai, AtomicInteger lineTrans, AtomicInteger lineNumber,
			List<RaInterfaceLinesAll> list, List<RaInterfaceDistributionsAll> listDist, String taxType, boolean hasTax, BigDecimal creditSalePercent, String restARFindService,
			FileTransferControl ftc) throws Exception {

		String strTN = null;
		String strTR = null;
		
		try {			
			SimpleDateFormat sdfr = new SimpleDateFormat("yyyy/MM/dd");
			Format formatter = new SimpleDateFormat("yyMMdd");
			this.validatePackageItem(a, s.getArticuloPaquete());

			//RAinterfaceALL
			RaInterfaceLinesAll ar = new RaInterfaceLinesAll();
			ar.setTransaction_Batch_Source_Name("SICAR");
			ar.setTransaction_Type_Name("Nota de Credito");
			ar.setPayment_Terms("");
			ar.setTransaction_Date(sdfr.format(s.getFechaNC()));
			ar.setAccounting_Date(sdfr.format(s.getFechaNC()));
	
			String strIdBranch = String.format("%02d", clientLocalDao.getBranch(0).getId());
			String strDate = formatter.format(s.getFechaNC());
			
			if(s.getIdTicket() != null) {			
				strTN = new StringBuffer("NC").append(strIdBranch).append("-T").append(s.getIdTicket().intValue()).toString();
				strTR = new StringBuffer("FAC").append(strIdBranch).append("-T").append(s.getIdTicket().intValue()).toString();
			} else if(s.getIdNotaVenta() != null) {
				strTN = new StringBuffer("NC").append(strIdBranch).append("-N").append(s.getIdNotaVenta().intValue()).toString();
				strTR = new StringBuffer("FAC").append(strIdBranch).append("-N").append(s.getIdNotaVenta().intValue()).toString();
			} else {
				strTN = new StringBuffer("NC").append(strIdBranch).append("-V").append(s.getIdVenta().intValue()).toString();
				strTR = new StringBuffer("FAC").append(strIdBranch).append("-V").append(s.getIdVenta().intValue()).toString();
			}
			
			if(a.getFolioNC() != null) {
				strTN = new StringBuffer(strTN).append("-").append(a.getFolioNC().intValue()).toString();
			} else if(a.getFolio() != null) {
				strTN = new StringBuffer(strTN).append("-F").append(a.getFolio().intValue()).toString();
			} else {
				strTN = new StringBuffer(strTN).append("-C").toString();
			}
	
			ar.setTransaction_Number(strTN);
			ar.setBill_to_Customer_Account_Number(s.getAccNbr());
			ar.setBill_to_Customer_Site_Number(s.getSiteNbr());		
			ar.setTransaction_Line_Type("LINE");
			ar.setTransaction_Line_Description(a.getArticuloDescripcion());
			ar.setCurrency_Code(s.getMoneda());
			ar.setCurrency_Conversion_Type("User");
			ar.setCurrency_Conversion_Date(sdfr.format(s.getFechaNC()));
			ar.setCurrency_Conversion_Rate(String.valueOf(s.getTipoCambio()));
			
			String quantity = String.valueOf(a.getVentaDetalleCantidad());
			String price = "";
			String amount = "";
			BigDecimal noTaxImport;
			
			if(AppConstants.CURRENCY_CODE_MXN.equals(s.getMoneda())) {
				//Se obtiene la parte proporcional pagada a cr�dito
				noTaxImport = creditSalePercent.multiply(a.getVentaDetalleImporteSinImp());
				
				price = getValidatePrice(noTaxImport, a.getVentaDetalleCantidad())
						.multiply(new BigDecimal(-1))
						.toString();
				
				amount = getValidateAmount(a.getVentaDetalleCantidad(), new BigDecimal(price).abs(), noTaxImport, ftc)
						.multiply(new BigDecimal(-1))
						.toString();	
			} else {
				//Se obtiene la parte proporcional pagada a cr�dito
				noTaxImport = creditSalePercent.multiply(a.getMonedaDetalleImporteSinImp());
				
				price = getValidatePrice(noTaxImport, a.getVentaDetalleCantidad())
						.toString();
				
				amount = getValidateAmount(a.getVentaDetalleCantidad(), new BigDecimal(price).abs(), noTaxImport, ftc)
						.multiply(new BigDecimal(-1))
						.toString();			
			}
			
			ar.setTransaction_Line_Amount(amount);
			ar.setTransaction_Line_Quantity(quantity);
			ar.setUnit_Selling_Price(price);
			ar.setLine_Transactions_Flexfield_Context("SICAR");
			ar.setLine_Transactions_Flexfield_Segment_1(ar.getTransaction_Number());
			ar.setLine_Transactions_Flexfield_Segment_2("Line" + lineNumber);
			ar.setLine_Transactions_Flexfield_Segment_3(ar.getTransaction_Type_Name());
			ar.setTax_Classification_Code("");
			ar.setUnit_of_Measure_Code(a.getVentaDetalleUnidad());
			ar.setTax_Regime_Code("");
			ar.setTax("");
			ar.setTax_Status_Code("");
			ar.setTax_Rate_Code("");
			ar.setTax_Rate("");
			ar.setLink_to_Transactions_Flexfield_Context("");
			ar.setLink_to_Transactions_Flexfield_Segment_1("");
			ar.setLink_to_Transactions_Flexfield_Segment_2("");
			ar.setLink_to_Transactions_Flexfield_Segment_3("");
			
			String lineReference = getTransactionLineNumber(list, strTR, a.getArticuloClave(), a.getArticuloDescripcion(), restARFindService, ftc);		
			ar.setReference_Transactions_Flexfield_Context("SICAR");
			ar.setReference_Transactions_Flexfield_Segment_1(strTR);
			ar.setReference_Transactions_Flexfield_Segment_2(lineReference);
			ar.setReference_Transactions_Flexfield_Segment_3("FACTURA");
			ar.setInventory_Item_Number(a.getArticuloClave());
			ar.setOverride_AutoAccounting_Flag("Y");
			ar.setBusiness_Unit_Name(((BusinessUnitMapping) buMap.get(s.getEmpresa().getCuenta())).getBusinessUnit());
			ar.setEnd("END");
	
			//RAinterfaceAccountsDistribution		
			RaInterfaceDistributionsAll ad = new RaInterfaceDistributionsAll();
			ad.setAccount_Class("REV");
			ad.setAmount(ar.getTransaction_Line_Amount());
			ad.setPercent("100");
			ad.setLine_Transactions_Flexfield_Context(ar.getLine_Transactions_Flexfield_Context());
			ad.setLine_Transactions_Flexfield_Segment_1(ar.getLine_Transactions_Flexfield_Segment_1());
			ad.setLine_Transactions_Flexfield_Segment_2(ar.getLine_Transactions_Flexfield_Segment_2());
			ad.setLine_Transactions_Flexfield_Segment_3(ar.getTransaction_Type_Name());
			
			GeneralLedgerMapping glmnc = (GeneralLedgerMapping) glmNcMap.get(taxType.trim());		
			ad.setAccounting_Flexfield_Segment_1(glmnc.getCompany());
			ad.setAccounting_Flexfield_Segment_2(glmnc.getCostCenter());
			ad.setAccounting_Flexfield_Segment_3(glmnc.getAccount());
			ad.setBusiness_Unit_Name(((BusinessUnitMapping) buMap.get(s.getEmpresa().getCuenta())).getBusinessUnit());
			ad.setEnd("END");
	
			list.add(ar);
			listDist.add(ad);
			lineNumber.incrementAndGet();
			
		} catch (Exception e) {
			e.printStackTrace();
			ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
			ftc.setLog("Error al  insertar l�nea en AR " + strTN + ":" + e.getMessage());
		}
	}
	
	@SuppressWarnings("unused")
	public void registraArticuloNCExtemporaneo(Articulo a, Sales s, List<ArticuloImpuesto> lai, AtomicInteger lineTrans, AtomicInteger lineNumber,
			List<RaInterfaceLinesAll> list, List<RaInterfaceDistributionsAll> listDist, String taxType, boolean hasTax, BigDecimal creditSalePercent, String restARFindService,
			FileTransferControl ftc) throws Exception {

		String strTN = null;
		String strTR = null;
		
		try {
			SimpleDateFormat sdfr = new SimpleDateFormat("yyyy/MM/dd");
			Format formatter = new SimpleDateFormat("yyMMdd");
			this.validatePackageItem(a, s.getArticuloPaquete());

			//RAinterfaceALL
			RaInterfaceLinesAll ar = new RaInterfaceLinesAll();
			ar.setTransaction_Batch_Source_Name("SICAR");
			ar.setTransaction_Type_Name("Nota de Credito");
			ar.setPayment_Terms("");
			ar.setTransaction_Date(sdfr.format(s.getVentaFecha()));
			ar.setAccounting_Date(sdfr.format(s.getVentaFecha()));

			String strIdBranch = String.format("%02d", clientLocalDao.getBranch(0).getId());
			String strDate = formatter.format(s.getVentaFecha());
					
			if(s.getIdTicket() != null) {			
				strTN = new StringBuffer("NC").append(strIdBranch).append("-T").append(s.getIdTicket().intValue()).toString();
				strTR = new StringBuffer("FAC").append(strIdBranch).append("-T").append(s.getIdTicket().intValue()).toString();
				
			} else if(s.getIdNotaVenta() != null) {
				strTN = new StringBuffer("NC").append(strIdBranch).append("-N").append(s.getIdNotaVenta().intValue()).toString();
				strTR = new StringBuffer("FAC").append(strIdBranch).append("-N").append(s.getIdNotaVenta().intValue()).toString();
				
			} else {
				strTN = new StringBuffer("NC").append(strIdBranch).append("-V").append(s.getIdVenta().intValue()).toString();
				strTR = new StringBuffer("FAC").append(strIdBranch).append("-V").append(s.getIdVenta().intValue()).toString();
			}
			
			if(a.getFolioNC() != null) {
				strTN = new StringBuffer(strTN).append("-").append(a.getFolioNC().intValue()).toString();
			} else if(a.getFolio() != null) {
				strTN = new StringBuffer(strTN).append("-F").append(a.getFolio().intValue()).toString();
			} else {
				strTN = new StringBuffer(strTN).append("-C").toString();
			}			
			ar.setTransaction_Number(strTN);		
			
			String accNbr = "";
			String siteNbr = "";
			
			Integer customerId = s.getCliente().getClienteId().intValue();
			String customerName = s.getCliente().getClienteNombre();
			CustomerAccountSite cas = clientLocalDao.getCustomerSiteAccounById(customerId);
			
			if(cas != null) {
				if(cas.getAccountNumber() != null && !cas.getAccountNumber().trim().isEmpty()) {
					accNbr = cas.getAccountNumber();
				} else {
					ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
					ftc.setLog("No fue posible recuperar el dato Account Number del cliente: " + customerName + " (Id " + customerId + ").");
				}
				if(cas.getSiteNumber() != null && !cas.getSiteNumber().trim().isEmpty()) {
					siteNbr = cas.getSiteNumber();
				} else {
					ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
					ftc.setLog("No fue posible recuperar el dato Site Number del cliente: " + customerName + " (Id " + customerId + ").");
				}
			} else {
				ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
				ftc.setLog("No fue posible recuperar los datos Account Number y Site Number del cliente: " + customerName + " (Id " + customerId + ").");
			}
			
			ar.setBill_to_Customer_Account_Number(accNbr);
			ar.setBill_to_Customer_Site_Number(siteNbr);
			
			ar.setTransaction_Line_Type("LINE");
			ar.setTransaction_Line_Description(a.getArticuloDescripcion());
			ar.setCurrency_Code(s.getVentaMoneda());
			ar.setCurrency_Conversion_Type("User");
			ar.setCurrency_Conversion_Date(sdfr.format(s.getVentaFecha()));
			ar.setCurrency_Conversion_Rate(String.valueOf(s.getVentaTipoCambio()));
			
			String quantity = String.valueOf(a.getVentaDetalleCantidad());
			String price = "";
			String amount = "";
			BigDecimal noTaxImport;
			
			if(AppConstants.CURRENCY_CODE_MXN.equals(s.getVentaMoneda())) {
				//Se obtiene la parte proporcional pagada a cr�dito
				noTaxImport = creditSalePercent.multiply(a.getVentaDetalleImporteSinImp());
				
				price = getValidatePrice(noTaxImport, a.getVentaDetalleCantidad())
						.multiply(new BigDecimal(-1))
						.toString();
				
				amount = getValidateAmount(a.getVentaDetalleCantidad(), new BigDecimal(price).abs(), noTaxImport, ftc)
						.multiply(new BigDecimal(-1))
						.toString();	
			} else {
				//Se obtiene la parte proporcional pagada a cr�dito
				noTaxImport = creditSalePercent.multiply(a.getMonedaDetalleImporteSinImp());
						
				price = getValidatePrice(noTaxImport, a.getVentaDetalleCantidad())
						.toString();
				
				amount = getValidateAmount(a.getVentaDetalleCantidad(), new BigDecimal(price).abs(), noTaxImport, ftc)
						.multiply(new BigDecimal(-1))
						.toString();			
			}	
			
			ar.setTransaction_Line_Amount(amount);
			ar.setTransaction_Line_Quantity(quantity);
			ar.setUnit_Selling_Price(price);
			ar.setLine_Transactions_Flexfield_Context("SICAR");
			ar.setLine_Transactions_Flexfield_Segment_1(ar.getTransaction_Number());
			ar.setLine_Transactions_Flexfield_Segment_2("Line" + lineNumber);
			ar.setLine_Transactions_Flexfield_Segment_3(ar.getTransaction_Type_Name());
			ar.setTax_Classification_Code("");
			ar.setUnit_of_Measure_Code(a.getVentaDetalleUnidad());
			ar.setTax_Regime_Code("");
			ar.setTax("");
			ar.setTax_Status_Code("");
			ar.setTax_Rate_Code("");
			ar.setTax_Rate("");
			ar.setLink_to_Transactions_Flexfield_Context("");
			ar.setLink_to_Transactions_Flexfield_Segment_1("");
			ar.setLink_to_Transactions_Flexfield_Segment_2("");
			ar.setLink_to_Transactions_Flexfield_Segment_3("");
			
			String lineReference = getTransactionLineNumber(list, strTR, a.getArticuloClave(), a.getArticuloDescripcion(), restARFindService, ftc);
			ar.setReference_Transactions_Flexfield_Context("SICAR");		
			ar.setReference_Transactions_Flexfield_Segment_1(strTR);
			ar.setReference_Transactions_Flexfield_Segment_2(lineReference);
			ar.setReference_Transactions_Flexfield_Segment_3("FACTURA");		
			ar.setInventory_Item_Number(a.getArticuloClave());
			ar.setOverride_AutoAccounting_Flag("Y");
			ar.setBusiness_Unit_Name(((BusinessUnitMapping) buMap.get(s.getEmpresa().getCuenta())).getBusinessUnit());
			ar.setEnd("END");

			//RAinterfaceAccountsDistribution		
			RaInterfaceDistributionsAll ad = new RaInterfaceDistributionsAll();
			ad.setAccount_Class("REV");
			ad.setAmount(ar.getTransaction_Line_Amount());
			ad.setPercent("100");
			ad.setLine_Transactions_Flexfield_Context(ar.getLine_Transactions_Flexfield_Context());
			ad.setLine_Transactions_Flexfield_Segment_1(ar.getLine_Transactions_Flexfield_Segment_1());
			ad.setLine_Transactions_Flexfield_Segment_2(ar.getLine_Transactions_Flexfield_Segment_2());
			ad.setLine_Transactions_Flexfield_Segment_3(ar.getTransaction_Type_Name());
			
			GeneralLedgerMapping glmnc = (GeneralLedgerMapping) glmNcMap.get(taxType.trim());
			
			ad.setAccounting_Flexfield_Segment_1(glmnc.getCompany());
			ad.setAccounting_Flexfield_Segment_2(glmnc.getCostCenter());
			ad.setAccounting_Flexfield_Segment_3(glmnc.getAccount());
			ad.setBusiness_Unit_Name(((BusinessUnitMapping) buMap.get(s.getEmpresa().getCuenta())).getBusinessUnit());
			ad.setEnd("END");

			list.add(ar);
			listDist.add(ad);
			lineNumber.incrementAndGet();

		} catch (Exception e) {
			e.printStackTrace();
			ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
			ftc.setLog("Error al  insertar l�nea en AR " + strTN + ":" + e.getMessage() + " " + e.getStackTrace()[0].toString());
		}
	}
	
	public void validatePackageItem(Articulo a, List<Articulo> lstPackage) {		
		if(a.getTipo() != null && a.getTipo() == 2 && lstPackage != null && !lstPackage.isEmpty()) {
			for(Articulo ap : lstPackage) {
				if((ap.getPaquete().intValue() == a.getArticuloId().intValue()) && (a.getIdVenta().intValue() == ap.getIdVenta().intValue())) {
					a.setArticuloClave(ap.getArticuloClave());
					a.setVentaDetalleUnidad(ap.getVentaDetalleUnidad());
					a.setVentaDetalleCantidad(ap.getVentaDetalleCantidad());
					a.setVentaDetallePrecioSinIva(a.getVentaDetalleImporteSinIva().divide(ap.getVentaDetalleCantidad(), 10, RoundingMode.HALF_UP));
					a.setVentaDetallePrecioConIva(a.getVentaDetalleImporteConIva().divide(ap.getVentaDetalleCantidad(), 10, RoundingMode.HALF_UP));
					
					if(ap.getMonedaDetalleImporteSinIva() != null) {
						a.setMonedaDetallePrecioSinIva(ap.getMonedaDetalleImporteSinIva().divide(ap.getVentaDetalleCantidad(), 10, RoundingMode.HALF_UP));
					}
					
					if(ap.getMonedaDetalleImporteConIva() != null) {
						a.setMonedaDetallePrecioConIva(ap.getMonedaDetalleImporteConIva().divide(ap.getVentaDetalleCantidad(), 10, RoundingMode.HALF_UP));
					}					
				}
			}
		}		
	}
	
	public String getTransactionLineNumber(List<RaInterfaceLinesAll> lstAR, String transactionNumber, String itemCode, String itemDescription, String restARFindService, FileTransferControl ftc) throws Exception {
		String lineNumber = "";
		String response = "";
		String value = "";
		
		if(lstAR != null && !lstAR.isEmpty()) {
			for(RaInterfaceLinesAll ar : lstAR) {
				if(ar.getTransaction_Number().equals(transactionNumber) && ar.getTransaction_Line_Description().equals(itemDescription) && "LINE".equals(ar.getTransaction_Line_Type())) {
					lineNumber = ar.getLine_Transactions_Flexfield_Segment_2();
					break;
				}
			}
		}
		
		if(lineNumber.isEmpty()) {
			value = new StringBuffer().append(transactionNumber).append("/").append(itemCode).toString();
			response = restService.sendRequestObject(restARFindService, value);
			
			if(response != null && response.replace("\"", "").contains(AppConstants.FILE_RESPONSE_SUCCESS)) {  
		        ObjectMapper mapper = new ObjectMapper();
		        BaseResponse baseResponse = mapper.readValue(response, BaseResponse.class);
				lineNumber = "Line" + baseResponse.getContent();
			} else {
				ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
				ftc.setLog("No fue posible obtener el dato Line Number para: " +  value);
			}
		}		
		
		return lineNumber;
	}
	
	public BigDecimal getValidateAmount(BigDecimal quantity, BigDecimal price, BigDecimal amount, FileTransferControl ftc) throws Exception {
		String floatFormat = AppConstants.FLOAT_FORMAT_2;
		BigDecimal newAmount = amount;
		BigDecimal result;
		BigDecimal diff;		
		
		result = new BigDecimal(String.format(floatFormat, quantity.multiply(price)));
		
		if(new BigDecimal(String.format(floatFormat, result)).compareTo(new BigDecimal(String.format(floatFormat, amount))) != 0 ) {			
			diff = new BigDecimal(String.format(floatFormat, result)).subtract(new BigDecimal(String.format(floatFormat, amount)));
			
			if(diff.abs().compareTo(new BigDecimal(3)) < 0) {
				newAmount = amount.add(diff);				
			} else {
				ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
				ftc.setLog("Las cantidades no coinciden. Diferencia de " + diff.toString());
			}			
		}
		
		return new BigDecimal(String.format(floatFormat, newAmount));
	}
	
	public BigDecimal getValidatePrice(BigDecimal amount, BigDecimal quantity) {
		String floatFormat = AppConstants.FLOAT_FORMAT_2;
		return new BigDecimal(String.format(floatFormat, new BigDecimal(String.format(floatFormat, amount)).divide(quantity, 10, RoundingMode.HALF_UP)));
	}
	
	public BigInteger getLastId() {		
		BigInteger lastId = restClientDao.getLastId();		
		return lastId;
	}
	
	public static Date dateCalculateDays(String dateString, String dateFormat, int days) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.DATE, days);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
	
	public static Date dateCalculateSeconds(String dateString, String dateFormat, int seconds) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.SECOND, seconds);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
}

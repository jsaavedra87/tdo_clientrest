package com.tdo.integration.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.GLInterface;
import com.dto.integration.dto.GLInterfaceV3;
import com.dto.integration.dto.SalesDTO;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.client.localmodel.FileTransferControl;
import com.tdo.integration.client.localmodel.GeneralLedgerMapping;
import com.tdo.integration.client.localmodel.Udc;
import com.tdo.integration.client.model.Articulo;
import com.tdo.integration.client.model.ArticuloImpuesto;
import com.tdo.integration.client.model.Sales;
import com.tdo.integration.client.model.TipoPago;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.RestClientDao;

@Service("glService")
public class GLService {
	
	@Autowired
	RestClientDao restClientDao;	
	
	@Autowired
	ClientLocalDao clientLocalDao;
	
	@Autowired 
	UDCService udcService;
	
	@Autowired
	ClientLocalService clientLocalService;
	
	@Autowired
	RESTService restService;	
	
	List<Sales> s;
	
	private final String TIPO_PAGO_CREDITO = "CR";
	private final String TIPO_PAGO_VALE = "VA";
	private final String IVA_TEXTO = "I.V.A.";
	private final String IEPS_TEXTO = "IEPS";
	private final String EXENTO_TEXTO = "IVA_";
	
	@SuppressWarnings("unused")
	public void startGLProcess(String sucursal, int idCC, String restGLService) {
		String transactionType = AppConstants.FILE_TRANSACTION_TYPE_GL;
		List<FileTransferControl> lstFTC = null;
		String dateFormat = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		BigInteger ultimoCC;
		String initFromDate = sdf.format(new Date());
		String initToDate = sdf.format(new Date());		
		String fromDate = "";
		String toDate = "";	

		try {			
			//Verifica si ya existe el registro
			FileTransferControl obj = clientLocalService.searchFileTransferControlByIdCC(transactionType, idCC);
			
			//Si no existe crea nuevo registro
			if(obj == null) {
				clientLocalService.createFileTransferControl(transactionType, idCC, initFromDate, initToDate, AppConstants.FILE_REQUEST_NEW, null);
			}
			
			//Obtiene registros pendientes desde siete d�as antes para procesarlos
			Date queryFromDate = dateCalculateDays(initFromDate.substring(0, 10) + " 00:00:00", dateFormat, -7);
			Date queryToDate = sdf.parse(initToDate);
			String[] pendingStatus = new String[]{AppConstants.FILE_REQUEST_NEW, AppConstants.FILE_REQUEST_SENT, AppConstants.FILE_REQUEST_ERROR};
			
			lstFTC = clientLocalService.searchFileTransferControlByDateRangeAndStatus(transactionType, queryFromDate, queryToDate, pendingStatus);

			//Procesa los registros pendientes			
			if(lstFTC != null && lstFTC.size() > 0) {
				for(FileTransferControl ftc : lstFTC) {					
					try {
						fromDate = sdf.format(ftc.getFromDate());
						toDate = sdf.format(ftc.getToDate());						
						
						if(!AppConstants.FILE_REQUEST_NEW.equals(ftc.getStatus())) {
							clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NEW);
						}
						
						//Se crea el archivo de GL
						GLInterface glInterface = this.createGLInterface(sucursal, AppConstants.REQUEST_TYPE_CC, new String[] { String.valueOf(ftc.getIdCC()) }, ftc);
						
						if(glInterface.getListGLInterface() != null && !glInterface.getListGLInterface().isEmpty()) {
							
							if(AppConstants.FILE_REQUEST_ERROR.equals(ftc.getStatus())) {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
							} else {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SENT);
							}
							
							glInterface.setFileTransferControl(ftc);
							String response = restService.sendRequestObject(restGLService, glInterface);
							System.out.print("Respuesta de env�o de GL para el CC" + String.valueOf(ftc.getIdCC()) + ": \r" + response);
							
							if(response != null && response.replace("\"", "").contains(AppConstants.FILE_RESPONSE_SUCCESS)) {
								ftc.setLog(response.replace("\"", ""));
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SUCCESS);
							} else {
								if(!AppConstants.FILE_REQUEST_ERROR.equals(ftc.getStatus())) {
									ftc.setLog("Error en la respuesta del ws: " + response);
									clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
								}
							}
							
						} else {							
							ultimoCC = this.getLastId();
							if(ftc.getIdCC() <= ultimoCC.intValue()) {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NO_SALE);
							} else {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NEW);
							}							
						}
						
					} catch (Exception e) {
						ftc.setLog("Error en clientREST: " + e.getMessage());
						clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			clientLocalService.createFileTransferControl(transactionType, idCC, initFromDate, initToDate, AppConstants.FILE_REQUEST_ERROR, "Ocurri� un error al ejecutar la integraci�n de env�o de GL: " + e.getMessage());
		}
	}
	
	public GLInterface createGLInterface(String sucursal, String requestType, String[] params, FileTransferControl ftc) throws Exception {
		
		List<Sales> sales = new ArrayList<Sales>();
		List<Sales> cancelledSales = new ArrayList<Sales>();
		SimpleDateFormat sdfr = new SimpleDateFormat("yyyy/MM/dd");
		List<GLInterfaceV3> list = new ArrayList<GLInterfaceV3>();
		GLInterface glinterface = new GLInterface();
		String dateString = null;
		Date dateCC = null;
		Udc udc = new Udc();
		int idCorteCaja = 0;
		
		idCorteCaja = Integer.valueOf(params[0]);
		
		udc = udcService.searchBySystemAndKey(AppConstants.DATA_ACCESS_SET_SYSTEM, AppConstants.DATA_ACCESS_SET);		
		glinterface.setDataAccesSet(udc.getUdcValue());			
		
		udc = udcService.searchBySystemAndKey(AppConstants.LEDGER_ID_SYSTEM, AppConstants.LEDGER_ID);		
		glinterface.setLedgerId(udc.getUdcValue());
		
		dateCC = restClientDao.getCCDate(idCorteCaja);
		dateString = sdfr.format(dateCC);		
		
		SalesDTO salesDTO = restClientDao.getSales(sucursal, requestType, params);
		if(salesDTO != null) {
			sales = salesDTO.getSales();
			cancelledSales = salesDTO.getVentasCanceladas();
		}
		
		//Nota de credito extempor�nea
		SalesDTO ncDTO = restClientDao.getOutOfDateCreditNotes(sucursal, requestType, params);
		List<Sales> creditNote =  new ArrayList<Sales>();
		if(ncDTO != null) {
			creditNote = ncDTO.getSales();
		}
		
		boolean isGL = false;
		double sumaDebito = 0.0;
		double sumaCredito = 0.0;
		double diferencia =  0.0;
		
		BigDecimal debitoEfectivoPorAplicar = new BigDecimal(0);
		BigDecimal debitoIVA16 = new BigDecimal(0);
		BigDecimal debitoIVA0 = new BigDecimal(0);
		BigDecimal debitoIEPS6 = new BigDecimal(0);
		BigDecimal debitoIEPS7 = new BigDecimal(0);
		BigDecimal debitoIEPS9 = new BigDecimal(0);
		BigDecimal debitoDevoluciones16 = new BigDecimal(0);
		BigDecimal debitoDevoluciones0 = new BigDecimal(0);
		BigDecimal debitoDevolucionesExcIVA = new BigDecimal(0);
		
		BigDecimal creditoEfectivoPorAplicar = new BigDecimal(0);
		BigDecimal creditoVentas16 = new BigDecimal(0);
		BigDecimal creditoVentas0 = new BigDecimal(0);		
		BigDecimal creditoIVA16 = new BigDecimal(0);
		BigDecimal creditoIVA0 = new BigDecimal(0);
		BigDecimal creditoIEPS6 = new BigDecimal(0);
		BigDecimal creditoIEPS7 = new BigDecimal(0);
		BigDecimal creditoIEPS9 = new BigDecimal(0);
		BigDecimal creditoDevoluciones16 = new BigDecimal(0);
		BigDecimal creditoDevoluciones0 = new BigDecimal(0);
		BigDecimal creditoVentasExcIVA = new BigDecimal(0);		
		
		Calendar now = Calendar.getInstance();
		now.setTime(dateCC);
		String year = String.valueOf(now.get(Calendar.YEAR));
		String month = String.valueOf(now.get(Calendar.MONTH) + 1); // Note: zero based!
		String day = String.valueOf(now.get(Calendar.DAY_OF_MONTH));
		String hour = String.valueOf(now.get(Calendar.HOUR_OF_DAY));
		String minute = String.valueOf(now.get(Calendar.MINUTE));
		String second = String.valueOf(now.get(Calendar.SECOND));
		String uniqueKey = year + month + day + hour + minute + second;		
		
		// PROCESA LAS VENTAS
		if(sales == null) {
			sales = new ArrayList<Sales>();
		}
		
		for(Sales s : sales) {
			BigDecimal glPagoSinCredito = new BigDecimal(0);
			BigDecimal glPagoEfectivo = new BigDecimal(0);
			BigDecimal glPagoVales = new BigDecimal(0);
			BigDecimal glPorcentajeSinCredito = new BigDecimal(0);
			BigDecimal glPorcentajeEfectivo = new BigDecimal(0);
			BigDecimal glPorcentajeVales = new BigDecimal(0);
			BigDecimal ventaDetalleImpuesto = new BigDecimal(0);
			BigDecimal ventaDetalleImporteConIva = new BigDecimal(0);					
			

			
			List<TipoPago> l = s.getTipoPago();
			isGL = false;
			if(l != null && !l.isEmpty()) {
				for(TipoPago t : l) {
					if(t.getTipoPagoClave() != null && t.getVentaTipoPagoTotal().compareTo(new BigDecimal(0)) > 0) {
						if(!TIPO_PAGO_CREDITO.equals(t.getTipoPagoClave())) {							
							if(TIPO_PAGO_VALE.equals(t.getTipoPagoClave())) {
								glPagoVales = glPagoVales.add(t.getVentaTipoPagoTotal());
								glPagoSinCredito = glPagoSinCredito.add(t.getVentaTipoPagoTotal());
							} else {
								glPagoEfectivo = glPagoEfectivo.add(t.getVentaTipoPagoTotal().subtract(s.getCambio()));
								glPagoSinCredito = glPagoSinCredito.add(t.getVentaTipoPagoTotal().subtract(s.getCambio()));
							}							
							isGL = true;
						}
					}
				}				
				if(isGL) {
					System.out.println("ID VENTA:" + s.getIdVenta().intValue());
					glPorcentajeVales = glPagoVales.divide(s.getVentaTotal(), 10, RoundingMode.HALF_UP);
					glPorcentajeEfectivo = glPagoEfectivo.divide(s.getVentaTotal(), 10, RoundingMode.HALF_UP);
					glPorcentajeSinCredito = glPagoSinCredito.divide(s.getVentaTotal(),10, RoundingMode.HALF_UP);
					
					System.out.println("Pago Vales: " + glPagoVales.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Pago Efectivo: " + glPagoEfectivo.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Pago Sin Credito: " + glPagoSinCredito.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Porcentaje Vales: " + glPorcentajeVales.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Porcentaje Efectivo: " + glPorcentajeEfectivo.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Porcentaje Sin Credito: " + glPorcentajeSinCredito.setScale( 2, RoundingMode.HALF_EVEN));
					
					List<Articulo> arts = s.getArticulo();
					List<ArticuloImpuesto> impList = s.getArticuloImpuesto();
					List<ArticuloImpuesto> chkTax = s.getArticuloImpuesto();					
					
					//VentaDetalleImporteConIva
					if(!arts.isEmpty()) {
						for(Articulo a : arts){
							boolean hasTax = false;
							boolean hasTax16 = false;
							boolean hasAmount0 = false;
							boolean hasAmount16 = false;
							ventaDetalleImporteConIva = new BigDecimal(0);
							ventaDetalleImpuesto = new BigDecimal(0);
							
							if(!chkTax.isEmpty()) {
								for(ArticuloImpuesto ai : chkTax) {
									if(s.getIdVenta().intValue() == ai.getIdVenta().intValue() && a.getArticuloId().intValue() == ai.getArticuloId()) {
										if(ai.getVentaDetalleImpuestoNombre() != null) {
											if(ai.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO) || ai.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO) || ai.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
												if(ai.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {
													hasTax16= true;
												}
												hasTax = true;
												ventaDetalleImpuesto = ventaDetalleImpuesto.add(ai.getVentaDetalleImpuestoTotal());
											}
										}
									}
								}
								ventaDetalleImporteConIva = new BigDecimal(String.format("%.2f", a.getVentaDetalleImporteSinIva().add(ventaDetalleImpuesto)));
							}
							
							if(!impList.isEmpty() && hasTax) {
								for(ArticuloImpuesto i : impList) {									
									if(s.getIdVenta().intValue() == i.getIdVenta().intValue() && a.getArticuloId().intValue() == i.getArticuloId()) {
										if(i.getVentaDetalleImpuestoNombre() != null) {
											
											//IVA
											if(i.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO)) {
												
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {													
													if(!hasAmount16) {
														creditoIVA16 = creditoIVA16.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
														creditoVentas16 = creditoVentas16.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														debitoDevoluciones16 = debitoDevoluciones16.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														System.out.println("Ventas16: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IVA16: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales16: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
														hasAmount16 = true;	
													}
												}
												
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 0.0 && !hasTax16) {
													creditoIVA0 = creditoIVA0.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
													if(!hasAmount0) {
														creditoVentas0 = creditoVentas0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
													}
													System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IVA0: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}
											}
											
											//IEPS
											if(i.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO)) {
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 6.0) {
													creditoIEPS6 = creditoIEPS6.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
													if(!hasAmount0 && !hasTax16) {
														creditoVentas0 = creditoVentas0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
													}
													System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS6: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}
												
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 7.0) {
													creditoIEPS7 = creditoIEPS7.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
													if(!hasAmount0 && !hasTax16) {
														creditoVentas0 = creditoVentas0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
													}
													System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS7: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}
												
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 9.0) {													
													creditoIEPS9 = creditoIEPS9.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
													if(!hasAmount0 && !hasTax16) {
														creditoVentas0 = creditoVentas0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
													}
													System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS9: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}
											}
											
											//EXENTO
											if(i.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {												
												if(!hasAmount0 && !hasTax16) {
													creditoVentasExcIVA = creditoVentasExcIVA.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
													debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
													hasAmount0 = true;
												}
												System.out.println("ExcentoIVA: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
											}
											
										}
									}
								}
							}
							//SIN IMPUESTO
							if(!hasTax && !hasAmount16 && !hasAmount0) {
								creditoVentas0 = creditoVentas0.add(glPorcentajeSinCredito.multiply(ventaDetalleImporteConIva));
								debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
								System.out.println("Sin Impuesto Ventas0: " + glPorcentajeSinCredito.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
							}
														
							debitoEfectivoPorAplicar = debitoEfectivoPorAplicar.add(glPorcentajeEfectivo.multiply(ventaDetalleImporteConIva));
							System.out.println("Efectivo Por Aplicar: " + glPorcentajeEfectivo.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
						}	
					}

					 if(s.getNotaCredito() != null) {												
						System.out.println("ID NOTA CREDITO: " + s.getNotaCredito().getIdNotaCredito().intValue());
						
						BigDecimal glNCPagoSinCredito = new BigDecimal(0);
						BigDecimal glNCPagoEfectivo = new BigDecimal(0);
						BigDecimal glNCPagoVales = new BigDecimal(0);
						BigDecimal glNCPorcentajeSinCredito = new BigDecimal(0);
						BigDecimal glNCPorcentajeEfectivo = new BigDecimal(0);
						BigDecimal glNCPorcentajeVales = new BigDecimal(0);
						
						List<Articulo> artsNC = s.getNotaCredito().getArticulo();
						List<ArticuloImpuesto> impListNC = s.getNotaCredito().getArticuloImpuesto();
						List<ArticuloImpuesto> chkTaxNC = s.getNotaCredito().getArticuloImpuesto();
	
						List<TipoPago> lstTipoPago = s.getNotaCredito().getTipoPago();
						boolean isNCGL = false;
						
						if(lstTipoPago != null && !lstTipoPago.isEmpty()) {
							for(TipoPago t : lstTipoPago) {
								if(t.getTipoPagoClave() != null && t.getVentaTipoPagoTotal().compareTo(new BigDecimal(0)) > 0) {
									if(!TIPO_PAGO_CREDITO.equals(t.getTipoPagoClave())) {							
										if(TIPO_PAGO_VALE.equals(t.getTipoPagoClave())) {
											glNCPagoVales = glNCPagoVales.add(t.getVentaTipoPagoTotal());
										} else {
											glNCPagoEfectivo = glNCPagoEfectivo.add(t.getVentaTipoPagoTotal());								
										}
										glNCPagoSinCredito = glNCPagoSinCredito.add(t.getVentaTipoPagoTotal());
										isNCGL = true;
									}
								}
							}
						}
						
						if(isNCGL) {
							glNCPorcentajeVales = glNCPagoVales.divide(s.getNotaCredito().getTotal(),10, RoundingMode.HALF_UP);
							glNCPorcentajeEfectivo = glNCPagoEfectivo.divide(s.getNotaCredito().getTotal(),10, RoundingMode.HALF_UP);
							glNCPorcentajeSinCredito = glNCPagoSinCredito.divide(s.getNotaCredito().getTotal(),10, RoundingMode.HALF_UP);
							
							System.out.println("Pago Vales: " + glNCPagoVales.setScale( 2, RoundingMode.HALF_EVEN));
							System.out.println("Pago Efectivo: " + glNCPagoEfectivo.setScale( 2, RoundingMode.HALF_EVEN));
							System.out.println("Pago Sin Credito: " + glNCPagoSinCredito.setScale( 2, RoundingMode.HALF_EVEN));
							System.out.println("Porcentaje Vales: " + glNCPorcentajeVales.setScale( 2, RoundingMode.HALF_EVEN));
							System.out.println("Porcentaje Efectivo: " + glNCPorcentajeEfectivo.setScale( 2, RoundingMode.HALF_EVEN));
							System.out.println("Porcentaje Sin Credito: " + glNCPorcentajeSinCredito.setScale( 2, RoundingMode.HALF_EVEN));

							if (!artsNC.isEmpty()) {
								for (Articulo a : artsNC) {
									boolean hasTax = false;
									boolean hasTax16= false;
									boolean hasAmount0 = false;
									boolean hasAmount16 = false;
									
									ventaDetalleImporteConIva = new BigDecimal(0);
									ventaDetalleImpuesto = new BigDecimal(0);
									
									if(!chkTaxNC.isEmpty()) {
										for(ArticuloImpuesto ai : chkTaxNC) {
											if(s.getIdVenta().intValue() == ai.getIdVenta().intValue() 
													&& a.getArticuloId().intValue() == ai.getArticuloId()
													&& a.getNotaCreditoId().intValue() == ai.getNotaCreditoId().intValue()) {
												if(ai.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO) || ai.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO) || ai.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
													if(ai.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {
														hasTax16= true;
													}
													hasTax = true;
													ventaDetalleImpuesto = ventaDetalleImpuesto.add(ai.getVentaDetalleImpuestoTotal());
												}
											}
										}
										
										ventaDetalleImporteConIva = new BigDecimal(String.format("%.2f", a.getVentaDetalleImporteSinIva().add(ventaDetalleImpuesto)));
									}
									
									if (!impListNC.isEmpty() && hasTax) {
										for (ArticuloImpuesto i : impListNC) {
											if (s.getIdVenta().intValue() == i.getIdVenta().intValue()
													&& a.getArticuloId().intValue() == i.getArticuloId()
													&& a.getNotaCreditoId().intValue() == i.getNotaCreditoId().intValue()) {
 
												if (i.getVentaDetalleImpuestoNombre() != null) {
													if (i.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO)) {
																												
														if (i.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {																														
															if(!hasAmount16) {
																debitoIVA16 = debitoIVA16.add(glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
																debitoDevoluciones16 = debitoDevoluciones16.add(glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));																														
																creditoDevoluciones16 = creditoDevoluciones16.add(glNCPorcentajeVales.multiply(ventaDetalleImporteConIva));
																hasAmount16 = true;
																System.out.println("Ventas16: " + glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IVA16: " + glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales16: " + glNCPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
															}
														}

														if (i.getVentaDetalleImpuestoValor().doubleValue() == 0.0 && !hasTax16) {
															debitoIVA0 = debitoIVA0.add(glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
															if(!hasAmount0) {
																debitoDevoluciones0 = debitoDevoluciones0.add(glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
																creditoDevoluciones0 = creditoDevoluciones0.add(glNCPorcentajeVales.multiply(ventaDetalleImporteConIva));
																hasAmount0 = true;
															}
															System.out.println("Ventas0: " + glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IVA0: " + glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glNCPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
														}
														
													} else if (i.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO)){
														
														if(i.getVentaDetalleImpuestoValor().doubleValue() == 9.0) {
															debitoIEPS9 = debitoIEPS9.add(glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
															if(!hasAmount0 && !hasTax16) {
																debitoDevoluciones0 = debitoDevoluciones0.add(glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
																creditoDevoluciones0 = creditoDevoluciones0.add(glNCPorcentajeVales.multiply(ventaDetalleImporteConIva));
																hasAmount0 = true;
															}
															System.out.println("Ventas0: " + glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS9: " + glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glNCPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
														}
														
														if(i.getVentaDetalleImpuestoValor().doubleValue() == 7.0) {
															debitoIEPS7 = debitoIEPS7.add(glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
															if(!hasAmount0 && !hasTax16) {
																debitoDevoluciones0 = debitoDevoluciones0.add(glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
																creditoDevoluciones0 = creditoDevoluciones0.add(glNCPorcentajeVales.multiply(ventaDetalleImporteConIva));
																hasAmount0 = true;
															}
															System.out.println("Ventas0: " + glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS7: " + glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glNCPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
														}													
														if(i.getVentaDetalleImpuestoValor().doubleValue() == 6.0) {
															debitoIEPS6 = debitoIEPS6.add(glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
															if(!hasAmount0 && !hasTax16) {
																debitoDevoluciones0 = debitoDevoluciones0.add(glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
																creditoDevoluciones0 = creditoDevoluciones0.add(glNCPorcentajeVales.multiply(ventaDetalleImporteConIva));
																hasAmount0 = true;
															}
															System.out.println("Ventas0: " + glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS6: " + glNCPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glNCPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
														}										
														
													} else if (i.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
														if(!hasAmount0 && !hasTax16) {
															debitoDevolucionesExcIVA = debitoDevolucionesExcIVA.add(glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
															creditoDevoluciones0 = creditoDevoluciones0.add(glNCPorcentajeVales.multiply(ventaDetalleImporteConIva));
															hasAmount0 = true;
															System.out.println("ExcentoIVA: " + glNCPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glNCPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));															
														}															
													}
												}												
											}
										}
									}
									
									if(!hasTax && !hasAmount0 && !hasAmount16) {
										debitoDevoluciones0 = debitoDevoluciones0.add(glNCPorcentajeSinCredito.multiply(ventaDetalleImporteConIva));
										creditoDevoluciones0 = creditoDevoluciones0.add(glNCPorcentajeVales.multiply(ventaDetalleImporteConIva));
										System.out.println("Sin Impuesto Ventas0: " + glNCPorcentajeSinCredito.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glNCPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
										
									}
									
									creditoEfectivoPorAplicar = creditoEfectivoPorAplicar.add(glNCPorcentajeEfectivo.multiply(ventaDetalleImporteConIva));
									System.out.println("Efectivo Por Aplicar: " + glNCPorcentajeEfectivo.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
								}
							}							
						}
					 }
				}
			}
		}
		
		// PROCESA LAS CANCELACIONES		
		if(cancelledSales != null) {			
			for(Sales s : cancelledSales) {
				BigDecimal glPagoSinCredito = new BigDecimal(0);
				BigDecimal glPagoEfectivo = new BigDecimal(0);
				BigDecimal glPagoVales = new BigDecimal(0);
				BigDecimal glPorcentajeSinCredito = new BigDecimal(0);
				BigDecimal glPorcentajeEfectivo = new BigDecimal(0);
				BigDecimal glPorcentajeVales = new BigDecimal(0);
				BigDecimal ventaDetalleImporteConIva = new BigDecimal(0);
				BigDecimal ventaDetalleImpuesto = new BigDecimal(0);
				
				List<TipoPago> l = s.getTipoPago();
				isGL = false;
				
				if(l != null && !l.isEmpty()) {
					for(TipoPago t : l) {
						if(t.getTipoPagoClave() != null && t.getVentaTipoPagoTotal().compareTo(new BigDecimal(0)) > 0) {
							if(!TIPO_PAGO_CREDITO.equals(t.getTipoPagoClave())) {							
								if(TIPO_PAGO_VALE.equals(t.getTipoPagoClave())) {
									glPagoVales = glPagoVales.add(t.getVentaTipoPagoTotal());
									glPagoSinCredito = glPagoSinCredito.add(t.getVentaTipoPagoTotal());
								} else {
									glPagoEfectivo = glPagoEfectivo.add(t.getVentaTipoPagoTotal().subtract(s.getCambio()));
									glPagoSinCredito = glPagoSinCredito.add(t.getVentaTipoPagoTotal().subtract(s.getCambio()));
								}								
								isGL = true;
							}
						}
					}
					if(isGL) {
						System.out.println("ID VENTA CANCELADA:" + s.getIdVenta().intValue());						
						glPorcentajeVales = glPagoVales.divide(s.getVentaTotal(),10, RoundingMode.HALF_UP);
						glPorcentajeEfectivo = glPagoEfectivo.divide(s.getVentaTotal(),10, RoundingMode.HALF_UP);
						glPorcentajeSinCredito = glPagoSinCredito.divide(s.getVentaTotal(),10, RoundingMode.HALF_UP);

						System.out.println("Pago Vales: " + glPagoVales.setScale( 2, RoundingMode.HALF_EVEN));
						System.out.println("Pago Efectivo: " + glPagoEfectivo.setScale( 2, RoundingMode.HALF_EVEN));
						System.out.println("Pago Sin Credito: " + glPagoSinCredito.setScale( 2, RoundingMode.HALF_EVEN));
						System.out.println("Porcentaje Vales: " + glPorcentajeVales.setScale( 2, RoundingMode.HALF_EVEN));
						System.out.println("Porcentaje Efectivo: " + glPorcentajeEfectivo.setScale( 2, RoundingMode.HALF_EVEN));
						System.out.println("Porcentaje Sin Credito: " + glPorcentajeSinCredito.setScale( 2, RoundingMode.HALF_EVEN));
						
						List<Articulo> arts = s.getArticulo();
						List<ArticuloImpuesto> impList = s.getArticuloImpuesto();
						List<ArticuloImpuesto> chkTaxNC = s.getArticuloImpuesto();
						
						if(!arts.isEmpty()) {
							for(Articulo a : arts){
								boolean hasTax = false;
								boolean hasTax16 = false;
								boolean hasAmount0 = false;
								boolean hasAmount16 = false;
								
								ventaDetalleImporteConIva = new BigDecimal(0);
								ventaDetalleImpuesto = new BigDecimal(0);

								if(!chkTaxNC.isEmpty()) {								
									for(ArticuloImpuesto ai : chkTaxNC) {
										if(s.getIdVenta().intValue() == ai.getIdVenta().intValue() && a.getArticuloId().intValue() == ai.getArticuloId()) {
											if(ai.getVentaDetalleImpuestoNombre() != null) {
												if(ai.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO) || ai.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO) || ai.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
													if(ai.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {
														hasTax16= true;
													}
													hasTax = true;
													ventaDetalleImpuesto = ventaDetalleImpuesto.add(ai.getVentaDetalleImpuestoTotal());
												}
											}
										}
									}									
									ventaDetalleImporteConIva = new BigDecimal(String.format("%.2f", a.getVentaDetalleImporteSinIva().add(ventaDetalleImpuesto)));
								}
								
								if(!impList.isEmpty() && hasTax) {
									for(ArticuloImpuesto i : impList) {
										if(s.getIdVenta().intValue() == i.getIdVenta().intValue() && a.getArticuloId().intValue() == i.getArticuloId()) {
											if(i.getVentaDetalleImpuestoNombre() != null) {												
												
												//CANC IVA
												if (i.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO)) {
													
													if (i.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {														
														if(!hasAmount16) {
															debitoIVA16 = debitoIVA16.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
															debitoDevoluciones16 = debitoDevoluciones16.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
															creditoDevoluciones16 = creditoDevoluciones16.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
															hasAmount16 = true;	
															System.out.println("Ventas16: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IVA16: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales16: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
														}
													}

													if (i.getVentaDetalleImpuestoValor().doubleValue() == 0.0 && !hasTax16) {
														debitoIVA0 = debitoIVA0.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
														if(!hasAmount0) {
															debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
															creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
															hasAmount0 = true;
														}
														System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IVA0: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
													}
												
												//CANC IEPS	
												} else if (i.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO)){
													
													if(i.getVentaDetalleImpuestoValor().doubleValue() == 9.0) {
														debitoIEPS9 = debitoIEPS9.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
														if(!hasAmount0 && !hasTax16) {
															debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
															creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
															hasAmount0 = true;
														}
														System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS9: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
													}													
													if(i.getVentaDetalleImpuestoValor().doubleValue() == 7.0) {
														debitoIEPS7 = debitoIEPS7.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
														if(!hasAmount0 && !hasTax16) {
															debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
															creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
															hasAmount0 = true;
														}
														System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS7: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
													}													
													if(i.getVentaDetalleImpuestoValor().doubleValue() == 6.0) {
														debitoIEPS6 = debitoIEPS6.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
														if(!hasAmount0 && !hasTax16) {
															debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
															creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
															hasAmount0 = true;
														}
														System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS6: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
													}										
													//CANC EXCENTO	
												} else if (i.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
													if(!hasAmount0 && !hasTax16) {
														debitoDevolucionesExcIVA = debitoDevolucionesExcIVA.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
														System.out.println("ExcentoIVA: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
													}													
												}					
											}
										}
									}
								}
								//CANC SIN
								if(!hasTax && !hasAmount0 && !hasAmount16) {
									debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(ventaDetalleImporteConIva));
									creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
									System.out.println("Sin Impuesto Ventas0: " + glPorcentajeSinCredito.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
								}
								
								creditoEfectivoPorAplicar = creditoEfectivoPorAplicar.add(glPorcentajeEfectivo.multiply(ventaDetalleImporteConIva));
								System.out.println("Efectivo Por Aplicar: " + glPorcentajeEfectivo.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
							}
						}
					}
				}
			}
		}
				
		// PROCESA LAS NOTAS DE CR�DITO EXTEMPOR�NEAS:
		for (Sales s : creditNote) {
			BigDecimal glPagoSinCredito = new BigDecimal(0);
			BigDecimal glPagoEfectivo = new BigDecimal(0);
			BigDecimal glPagoVales = new BigDecimal(0);
			BigDecimal glPorcentajeSinCredito = new BigDecimal(0);
			BigDecimal glPorcentajeEfectivo = new BigDecimal(0);
			BigDecimal glPorcentajeVales = new BigDecimal(0);
			BigDecimal ventaDetalleImporteConIva = new BigDecimal(0);
			BigDecimal ventaDetalleImpuesto = new BigDecimal(0);
			List<TipoPago> l = s.getTipoPago();			
			isGL = false;
			
			if (l != null && !l.isEmpty()) {
				for (TipoPago t : l) {
					if(t.getTipoPagoClave() != null && t.getVentaTipoPagoTotal().compareTo(new BigDecimal(0)) > 0) {
						if(!TIPO_PAGO_CREDITO.equals(t.getTipoPagoClave())) {							
							if(TIPO_PAGO_VALE.equals(t.getTipoPagoClave())) {
								glPagoVales = glPagoVales.add(t.getVentaTipoPagoTotal());
							} else {
								glPagoEfectivo = glPagoEfectivo.add(t.getVentaTipoPagoTotal());								
							}
							glPagoSinCredito = glPagoSinCredito.add(t.getVentaTipoPagoTotal());
							isGL = true;
						}
					}
				}
				if (isGL) {
					System.out.println("ID NC EXTEMPORANEA:" + s.getIdVenta().intValue());
										
					glPorcentajeVales = glPagoVales.divide(s.getVentaTotal(),10, RoundingMode.HALF_UP);
					glPorcentajeEfectivo = glPagoEfectivo.divide(s.getVentaTotal(),10, RoundingMode.HALF_UP);
					glPorcentajeSinCredito = glPagoSinCredito.divide(s.getVentaTotal(),10, RoundingMode.HALF_UP);					

					System.out.println("Pago Vales: " + glPagoVales.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Pago Efectivo: " + glPagoEfectivo.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Pago Sin Credito: " + glPagoSinCredito.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Porcentaje Vales: " + glPorcentajeVales.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Porcentaje Efectivo: " + glPorcentajeEfectivo.setScale( 2, RoundingMode.HALF_EVEN));
					System.out.println("Porcentaje Sin Credito: " + glPorcentajeSinCredito.setScale( 2, RoundingMode.HALF_EVEN));
					
					List<Articulo> arts = s.getArticulo();
					List<ArticuloImpuesto> impList = s.getArticuloImpuesto();
					List<ArticuloImpuesto> chkTaxNC = s.getArticuloImpuesto();
					
					if (!arts.isEmpty()) {
						for (Articulo a : arts) {
							boolean hasTax = false;
							boolean hasTax16 = false;
							boolean hasAmount0 = false;
							boolean hasAmount16 = false;
							
							ventaDetalleImporteConIva = new BigDecimal(0);
							ventaDetalleImpuesto = new BigDecimal(0);
							
							if(!chkTaxNC.isEmpty()) {
								for(ArticuloImpuesto ai : chkTaxNC) {
									if(s.getIdVenta().intValue() == ai.getIdVenta().intValue() && a.getArticuloId().intValue() == ai.getArticuloId()) {
										if(ai.getVentaDetalleImpuestoNombre() != null) {
											if(ai.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO) || ai.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO) || ai.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
												if(ai.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {
													hasTax16= true;
												}
												hasTax = true;
												ventaDetalleImpuesto = ventaDetalleImpuesto.add(ai.getVentaDetalleImpuestoTotal());
											}
										}
									}
								}								
								ventaDetalleImporteConIva = new BigDecimal(String.format("%.2f", a.getVentaDetalleImporteSinIva().add(ventaDetalleImpuesto)));
							}
							
							if (!impList.isEmpty() && hasTax) {
								for (ArticuloImpuesto i : impList) {									
									if (s.getIdVenta().intValue() == i.getIdVenta().intValue() && a.getArticuloId().intValue() == i.getArticuloId()) {
										if (i.getVentaDetalleImpuestoNombre() != null) {
											
											if (i.getVentaDetalleImpuestoNombre().contains(IVA_TEXTO)) {												
												if (i.getVentaDetalleImpuestoValor().doubleValue() == 16.0) {													
													if(!hasAmount16) {
														debitoIVA16 = debitoIVA16.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
														debitoDevoluciones16 = debitoDevoluciones16.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));																										
														creditoDevoluciones16 = creditoDevoluciones16.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount16 = true;
														System.out.println("Ventas16: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IVA16: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales16: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
													}
												}

												if (i.getVentaDetalleImpuestoValor().doubleValue() == 0.0 && !hasTax16) {																									
													debitoIVA0 = debitoIVA0.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
													if(!hasAmount0) {
														debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
													}
													System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IVA0: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}												
											} else if (i.getVentaDetalleImpuestoNombre().contains(IEPS_TEXTO)){												
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 9.0) {
													debitoIEPS9 = debitoIEPS9.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
													if(!hasAmount0 && !hasTax16) {
														debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
													}
													System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS9: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}													
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 7.0) {
													debitoIEPS7 = debitoIEPS7.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
													if(!hasAmount0 && !hasTax16) {
														debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
													}
													System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS7: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}													
												if(i.getVentaDetalleImpuestoValor().doubleValue() == 6.0) {
													debitoIEPS6 = debitoIEPS6.add(glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()));
													if(!hasAmount0 && !hasTax16) {
														debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
														creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
														hasAmount0 = true;
													}
													System.out.println("Ventas0: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " IEPS6: " + glPorcentajeSinCredito.multiply(i.getVentaDetalleImpuestoTotal()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}										
												
											} else if (i.getVentaDetalleImpuestoNombre().contains(EXENTO_TEXTO)) {
												if(!hasAmount0 && !hasTax16) {
													debitoDevolucionesExcIVA = debitoDevolucionesExcIVA.add(glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()));
													creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
													hasAmount0 = true;
													System.out.println("ExcentoIVA: " + glPorcentajeSinCredito.multiply(a.getVentaDetalleImporteSinIva()).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
												}
											}											
										}
									}
								}
							}
							
							if(!hasTax && !hasAmount0 && !hasAmount16) {
								debitoDevoluciones0 = debitoDevoluciones0.add(glPorcentajeSinCredito.multiply(ventaDetalleImporteConIva));
								creditoDevoluciones0 = creditoDevoluciones0.add(glPorcentajeVales.multiply(ventaDetalleImporteConIva));
								System.out.println("Sin Impuesto Ventas0: " + glPorcentajeSinCredito.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN) + " Vales0: " + glPorcentajeVales.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
							}
							
							creditoEfectivoPorAplicar = creditoEfectivoPorAplicar.add(glPorcentajeEfectivo.multiply(ventaDetalleImporteConIva));
							System.out.println("Efectivo Por Aplicar: " + glPorcentajeEfectivo.multiply(ventaDetalleImporteConIva).setScale( 2, RoundingMode.HALF_EVEN));
						}
					}
				}
			}
		}

		
		sumaDebito = new BigDecimal(String.format("%.2f", debitoEfectivoPorAplicar))
				.add(new BigDecimal(String.format("%.2f", debitoIVA16)))
				.add(new BigDecimal(String.format("%.2f", debitoIVA0)))
				.add(new BigDecimal(String.format("%.2f", debitoIEPS6)))
				.add(new BigDecimal(String.format("%.2f", debitoIEPS7)))
				.add(new BigDecimal(String.format("%.2f", debitoIEPS9)))
				.add(new BigDecimal(String.format("%.2f", debitoDevoluciones0)))
				.add(new BigDecimal(String.format("%.2f", debitoDevoluciones16)))
				.add(new BigDecimal(String.format("%.2f", debitoDevolucionesExcIVA)))
				.doubleValue();
		
		sumaCredito = new BigDecimal(String.format("%.2f", creditoEfectivoPorAplicar))
				.add(new BigDecimal(String.format("%.2f", creditoVentas16)))
				.add(new BigDecimal(String.format("%.2f", creditoVentas0)))				
				.add(new BigDecimal(String.format("%.2f", creditoIVA16)))
				.add(new BigDecimal(String.format("%.2f", creditoIVA0)))
				.add(new BigDecimal(String.format("%.2f", creditoIEPS6)))
				.add(new BigDecimal(String.format("%.2f", creditoIEPS7)))
				.add(new BigDecimal(String.format("%.2f", creditoIEPS9)))				
				.add(new BigDecimal(String.format("%.2f", creditoDevoluciones0)))
				.add(new BigDecimal(String.format("%.2f", creditoDevoluciones16)))
				.add(new BigDecimal(String.format("%.2f", creditoVentasExcIVA)))
				.doubleValue();
		
		if(Double.compare(sumaDebito, sumaCredito) != 0 ) {
			diferencia = (Math.round(sumaDebito*100.0)/100.0) - (Math.round(sumaCredito*100.0)/100.0);
			System.out.println("Diferencia (D�bito-Cr�dito): " + diferencia);
			
			if(Math.abs(diferencia) < 1) {
				if(creditoVentas16.doubleValue() > 0) {
					creditoVentas16 = creditoVentas16.add(new BigDecimal(String.format("%.2f", diferencia)));
				} else if (creditoVentas0.doubleValue() > 0) {
					creditoVentas0 = creditoVentas0.add(new BigDecimal(String.format("%.2f", diferencia)));
				}	
			} else {
				ftc.setStatus(AppConstants.FILE_REQUEST_ERROR);
				ftc.setLog("Las cantidades de Cr�dito y D�bito no coinciden. Diferencia de " + String.valueOf(diferencia));
			}
		} 
		
		List<GeneralLedgerMapping> accList = clientLocalDao.getGlSalesAccounts(sucursal, "GL");
		if(!accList.isEmpty()) {
			for(GeneralLedgerMapping o : accList) {				
				GLInterfaceV3 obj = new GLInterfaceV3();
			    obj.setSegment1(o.getCompany());
				obj.setSegment2(o.getCostCenter());
				obj.setSegment3(o.getAccount());
				boolean hasAmount = false;
				switch(o.getSequence()) {
				   case 1 :
						obj.setEntered_Debit_Amount(Math.round(debitoEfectivoPorAplicar.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditoEfectivoPorAplicar.doubleValue()*100.0)/100.0);
					   	break;
				   case 2 :
						obj.setEntered_Debit_Amount(0);
						obj.setEntered_Credit_Amount(Math.round(creditoVentas16.doubleValue()*100.0)/100.0);
				      break;
				   case 3 :
						obj.setEntered_Debit_Amount(0);
						obj.setEntered_Credit_Amount(Math.round(creditoVentas0.doubleValue()*100.0)/100.0); 
					      break;
				   case 4 :
						obj.setEntered_Debit_Amount(0);
						obj.setEntered_Credit_Amount(Math.round(creditoVentasExcIVA.doubleValue()*100.0)/100.0);
					      break;
				   case 5 :
						obj.setEntered_Debit_Amount(Math.round(debitoIVA16.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditoIVA16.doubleValue()*100.0)/100.0); 
					      break; 
				   case 6 :
						obj.setEntered_Debit_Amount(Math.round(debitoIVA0.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditoIVA0.doubleValue()*100.0)/100.0);  
					      break;
				   case 7 :
						obj.setEntered_Debit_Amount(Math.round(debitoIEPS6.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditoIEPS6.doubleValue()*100.0)/100.0);
					      break;
				   case 8:
						obj.setEntered_Debit_Amount(Math.round(debitoIEPS7.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditoIEPS7.doubleValue()*100.0)/100.0);      
						  break;
				   case 9 :
						obj.setEntered_Debit_Amount(Math.round(debitoIEPS9.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditoIEPS9.doubleValue()*100.0)/100.0);   
					   	  break;
				   case 10 : 
						obj.setEntered_Debit_Amount(Math.round(debitoDevoluciones0.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditoDevoluciones0.doubleValue()*100.0)/100.0); 
				      break;
				   case 11 :
						obj.setEntered_Debit_Amount(Math.round(debitoDevoluciones16.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditoDevoluciones16.doubleValue()*100.0)/100.0);   
					      break;
				   case 12 :
					    obj.setEntered_Debit_Amount(Math.round(debitoDevolucionesExcIVA.doubleValue()*100.0)/100.0);
						obj.setEntered_Credit_Amount(0);
					      break;
				   default :
				      break;
				}
				
				if(obj.getEntered_Debit_Amount() != 0 || obj.getEntered_Credit_Amount() != 0) {
					hasAmount = true;
				}
					
				if(hasAmount) {
					obj.setStatus_Code("NEW");
					obj.setLedger_ID(o.getLedgerId());
					obj.setEffective_Date_of_Transaction(dateString);
					obj.setJournal_Source("Balance Transfer");
					obj.setJournal_Category("Third Party Merge");
					obj.setCurrency_Code("MXN");
					obj.setJournal_Entry_Creation_Date(dateString);
					obj.setActual_Flag("A");
					obj.setREFERENCE4("JE-" + uniqueKey + "-" + year);
					obj.setREFERENCE5("CIERRE CAJA " + sucursal + " " + day + "-" + month + "-" + year);
					obj.setInterface_Group_Identifier("1");					
					obj.setSegment4("");
					obj.setSegment5("");
					obj.setSegment6("");
					obj.setSegment7("");
					obj.setSegment8("");
					obj.setSegment9("");
					obj.setSegment10("");
					obj.setSegment11("");
					obj.setSegment12("");
					obj.setSegment13("");
					obj.setSegment14("");
					obj.setSegment15("");
					obj.setSegment16("");
					obj.setSegment17("");
					obj.setSegment18("");
					obj.setSegment19("");
					obj.setSegment20("");
					obj.setSegment21("");
					obj.setSegment22("");
					obj.setSegment23("");
					obj.setSegment24("");
					obj.setSegment25("");
					obj.setSegment26("");
					obj.setSegment27("");
					obj.setSegment28("");
					obj.setSegment29("");
					obj.setSegment30("");
					obj.setConverted_Debit_Amount("");
					obj.setConverted_Credit_Amount("");
					obj.setREFERENCE1("");
					obj.setREFERENCE2("");
					obj.setREFERENCE3("");
					obj.setREFERENCE6("");
					obj.setREFERENCE7("");
					obj.setREFERENCE8("");
					obj.setREFERENCE9("");
					obj.setREFERENCE10("");
					obj.setReference_column_1("");
					obj.setReference_column_2("");
					obj.setReference_column_3("");
					obj.setReference_column_4("");
					obj.setReference_column_5("");
					obj.setReference_column_6("");
					obj.setReference_column_7("");
					obj.setReference_column_8("");
					obj.setReference_column_9("");
					obj.setReference_column_10("");
					obj.setStatistical_Amount("");
					obj.setCurrency_Conversion_Type("");
					obj.setCurrency_Conversion_Date("");
					obj.setCurrency_Conversion_Rate("");
					obj.setContext_field_for_Journal_Entry_Line_DFF("");
					obj.setATTRIBUTE1("");
					obj.setATTRIBUTE2("");
					obj.setATTRIBUTE3("");
					obj.setATTRIBUTE4("");
					obj.setATTRIBUTE5("");
					obj.setATTRIBUTE6("");
					obj.setATTRIBUTE7("");
					obj.setATTRIBUTE8("");
					obj.setATTRIBUTE9("");
					obj.setATTRIBUTE10("");
					obj.setATTRIBUTE11("");
					obj.setATTRIBUTE12("");
					obj.setATTRIBUTE13("");
					obj.setATTRIBUTE14("");
					obj.setATTRIBUTE15("");
					obj.setATTRIBUTE16("");
					obj.setATTRIBUTE17("");
					obj.setATTRIBUTE18("");
					obj.setATTRIBUTE19("");
					obj.setATTRIBUTE20("");
					obj.setContext_field_for_Captured_Information_DFF("");
					obj.setAverage_Journal_Flag("");
					obj.setClearing_Company("");
					obj.setLedger_Name("");
					obj.setEncumbrance_Type_ID("");
					obj.setReconciliation_Reference("");
					obj.setEnd("END");
					list.add(obj);
				}

			}
		}	
		glinterface.setListGLInterface(list);
		return glinterface;
	}
	
	public static BigDecimal getBigDecimal(Double value) {
		if(value == null) {
			value = 0.0;
		}
		return new BigDecimal(String.format("%.2f", value.doubleValue()));
	}

	public BigInteger getLastId() {		
		BigInteger lastId = restClientDao.getLastId();		
		return lastId;
	}
	
	public static Date dateCalculateDays(String dateString, String dateFormat, int days) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.DATE, days);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
	
	public static Date dateCalculateSeconds(String dateString, String dateFormat, int seconds) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.SECOND, seconds);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
}

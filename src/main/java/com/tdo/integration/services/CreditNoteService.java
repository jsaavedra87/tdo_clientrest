package com.tdo.integration.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.CreditNoteResponseDTO;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.client.localmodel.CreditNoteRequestEntity;
import com.tdo.integration.client.localmodel.Sucursal;
import com.tdo.integration.client.localmodel.Udc;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.CustomerLocalDao;
import com.tdo.integration.dao.InventorySicarDao;

@Service("creditNoteService")
@Transactional
public class CreditNoteService {
	
	@Autowired
	InventorySicarDao inventorySicarDao; 
	
	@Autowired
	ClientLocalDao clientLocalDao;
	
	@Autowired
	CustomerLocalDao customerLocalDao;
	
	@Autowired
	RESTService restService;
	Properties props;
	
	public List<CreditNoteRequestEntity> getProcessedCreditNotes() throws Exception {
		
		Udc udc =clientLocalDao.getUdcByUdcKey(AppConstants.STATUS_NC_PROCESSED);
		return customerLocalDao.getCreditNoteRequestListByStatus(udc.getUdckey());
		
	}
	
	public void updateCreditNoteTxtResponse(List<CreditNoteResponseDTO> respList) throws Exception {
		
		List<CreditNoteRequestEntity> inprocessCreditNotes = customerLocalDao.getCreditNoteRequestListByStatus(AppConstants.STATUS_NC_INPROCESS);
		Sucursal sucursal = clientLocalDao.getBranch();
		Map <String, String> responses = new HashMap<String,String>();

	for (CreditNoteResponseDTO cndto : respList) {
		if(cndto.getSucursal().equals( String.format("%02d",sucursal.getId()))) {
			responses.put(cndto.getFolio(), cndto.getResponseMap().get(cndto.getFolio()).toString());
		}
	}
		if(inprocessCreditNotes!=null) {
			for (CreditNoteRequestEntity creditNoteRequest : inprocessCreditNotes) {
				String folioNC =creditNoteRequest.getFolio();
				String value = responses.get(folioNC);
				if(value!=null) {
					String[] arrayResponses = value.split("\\|");
					String val1 = arrayResponses[0];
					String val2 =arrayResponses[1];
					String val3 = arrayResponses[2];
					
					if(AppConstants.SUCCESS_RESPONSE_CODE.equals(arrayResponses[0])){
						//save as FACTURADA, save uuid in sicar. 
						inventorySicarDao.updateUUIDIntoSicarNotaCredito(val2,folioNC );
						creditNoteRequest.setUuid(val2);
						creditNoteRequest.setStatus(AppConstants.STATUS_NC_PROCESSED);
					}else {
						creditNoteRequest.setLastUpdateTime(new Date());
						creditNoteRequest.setStatus(AppConstants.STATUS_NC_ERROR);
					}
					creditNoteRequest.setLastUpdateTime(new Date());
					creditNoteRequest.setCode(val1);
					creditNoteRequest.setDesc(val3);
					customerLocalDao.saveCreditNoteRequest(creditNoteRequest);
				}else {
					System.out.println("el Folio "+creditNoteRequest.getFolio()+ " A�n no tiene respuesta");
				}
			}
		}
	}
	
	
}


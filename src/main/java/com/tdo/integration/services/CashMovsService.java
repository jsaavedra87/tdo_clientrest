package com.tdo.integration.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.GLInterface;
import com.dto.integration.dto.GLInterfaceV3;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.client.localmodel.FileTransferControl;
import com.tdo.integration.client.localmodel.GeneralLedgerMapping;
import com.tdo.integration.client.localmodel.Udc;
import com.tdo.integration.client.model.MovimientosCaja;
import com.tdo.integration.client.model.Sales;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.RestClientDao;

@Service("cashMovsService")
public class CashMovsService {
	
	@Autowired
	RestClientDao restClientDao; 
	
	@Autowired
	ClientLocalDao clientLocalDao;
	
	@Autowired 
	UDCService udcService;
	
	@Autowired
	ClientLocalService clientLocalService;

	@Autowired
	RESTService restService;
	
	List<Sales> s;

	@SuppressWarnings("unused")
	public void startCashMovsProcess(String sucursal, int idCC, String restGLService) {
		String transactionType = AppConstants.FILE_TRANSACTION_TYPE_MOVEMENTS;
		List<FileTransferControl> lstFTC = null;
		String dateFormat = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		String initFromDate = sdf.format(new Date());
		String initToDate = sdf.format(new Date());		
		String fromDate = "";
		String toDate = "";	
				
		try {			
			//Verifica si ya existe el registro
			FileTransferControl obj = clientLocalService.searchFileTransferControlByIdCC(transactionType, idCC);
			
			if(obj != null) {
				if(!AppConstants.FILE_REQUEST_SUCCESS.equals(obj.getStatus())) {
					clientLocalService.updateFileTransferControlStatus(obj, AppConstants.FILE_REQUEST_NEW);
				}				
			} else {
				clientLocalService.createFileTransferControl(transactionType, idCC, initFromDate, initToDate, AppConstants.FILE_REQUEST_NEW, null);
			}
			
			//Obtiene registros pendientes desde siete d�as antes para procesarlos
			Date queryFromDate = dateCalculateDays(initFromDate.substring(0, 10) + " 00:00:00", dateFormat, -7);
			Date queryToDate = sdf.parse(initToDate);
			String[] pendingStatus = new String[]{AppConstants.FILE_REQUEST_NEW, AppConstants.FILE_REQUEST_SENT, AppConstants.FILE_REQUEST_ERROR};
			
			lstFTC = clientLocalService.searchFileTransferControlByDateRangeAndStatus(transactionType, queryFromDate, queryToDate, pendingStatus);

			//Procesa los registros pendientes			
			if(lstFTC != null && lstFTC.size() > 0) {
				for(FileTransferControl ftc : lstFTC) {					
					try {
						fromDate = sdf.format(ftc.getFromDate());
						toDate = sdf.format(ftc.getToDate());
						
						GLInterface glInterface = this.createCashTransactionsInterface(sucursal, AppConstants.REQUEST_TYPE_CC, new String[] { String.valueOf(idCC) });
						
						if(glInterface.getListGLInterface() != null && !glInterface.getListGLInterface().isEmpty()) {
							
							clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SENT);
							glInterface.setFileTransferControl(ftc);
							String response = restService.sendRequestObject(restGLService, glInterface);
							System.out.print("Respuesta del env�o de Movimietos de Caja para el CC" + String.valueOf(idCC) + ": \r" + response);
							
							if(response != null && response.replace("\"", "").contains(AppConstants.FILE_RESPONSE_SUCCESS)) {
								ftc.setLog(response.replace("\"", ""));
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SUCCESS);
							} else {
								ftc.setLog("Error en la respuesta del ws: " + response);
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
							}
							
						}  else {
							clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NO_SALE);
						}
						
					} catch (Exception e) {
						ftc.setLog("Error en clientREST: " + e.getMessage());
						clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			clientLocalService.createFileTransferControl(transactionType, idCC, initFromDate, initToDate, AppConstants.FILE_REQUEST_ERROR, "Ocurri� un error al ejecutar la integraci�n de env�o de Inventarios: " + e.getMessage());
		}
	}
	
	public GLInterface createCashTransactionsInterface(String sucursal, String requestType, String[] params) throws Exception {		

		Udc udc = new Udc();		
		GLInterface glinterface = new GLInterface();
		String dateString = null;
		Date dateCC = null;
		int idCorteCaja = 0;
		
		SimpleDateFormat sdfr = new SimpleDateFormat("yyyy/MM/dd");
		List<GLInterfaceV3> listRecords = new ArrayList<GLInterfaceV3>();
		double debitAmtVD = 0.0d;
		double creditAmtVD = 0.0d;
		double debitAmtCTA = 0.0d;		
		double creditAmtCTA = 0.0d;			
		double debitTotal = 0.0;
		double creditTotal = 0.0;
		double diff =  0.0;
		
		udc = udcService.searchBySystemAndKey(AppConstants.DATA_ACCESS_SET_SYSTEM, AppConstants.DATA_ACCESS_SET);		
		glinterface.setDataAccesSet(udc.getUdcValue());
			
		udc = udcService.searchBySystemAndKey(AppConstants.LEDGER_ID_SYSTEM, AppConstants.LEDGER_ID);		
		glinterface.setLedgerId(udc.getUdcValue());
		
		idCorteCaja = Integer.valueOf(params[0]);
		dateCC = restClientDao.getCCDate(idCorteCaja);
		dateString = sdfr.format(dateCC);
		
		Calendar now = Calendar.getInstance();
		now.setTime(dateCC);
		String year = String.valueOf(now.get(Calendar.YEAR));
		String month = String.valueOf(now.get(Calendar.MONTH) + 1); // Note: zero based!
		String day = String.valueOf(now.get(Calendar.DAY_OF_MONTH));
		String hour = String.valueOf(now.get(Calendar.HOUR_OF_DAY));
		String minute = String.valueOf(now.get(Calendar.MINUTE));
		String second = String.valueOf(now.get(Calendar.SECOND));
		String uniqueKey = year + month + day + hour + minute + second;
		
		List<MovimientosCaja> list = restClientDao.getCashTransactions(sucursal, requestType, params);
		if(list != null) {
			if(!list.isEmpty()) {
				for(MovimientosCaja o : list) {
					if(o.getTipo().intValue() == 1) {
						debitAmtCTA = debitAmtCTA + o.getTotal().doubleValue();
						creditAmtVD = creditAmtVD + o.getTotal().doubleValue();
					}else if(o.getTipo().intValue() == 2) {
						debitAmtVD = debitAmtVD + o.getTotal().doubleValue();
						creditAmtCTA = creditAmtCTA + o.getTotal().doubleValue();						
					}
				}
			}
		}
		
		debitTotal = debitAmtCTA + debitAmtVD;
		creditTotal = creditAmtCTA + creditAmtVD;
		
		if(Double.compare(debitTotal, creditTotal) != 0 ) {
			diff = (Math.round(debitTotal*100.0)/100.0) - (Math.round(creditTotal*100.0)/100.0);
			System.out.println("Diferencia (D�bito-Cr�dito): " + diff);
			
			if(Math.abs(diff) < 1) {
				if(creditAmtVD > 0) {
					creditAmtVD = creditAmtVD + diff;
				} else if (creditAmtCTA > 0) {
					creditAmtCTA = creditAmtCTA + diff;
				}	
			} else {
				throw new Exception("Las cantidades de Cr�dito y D�bito no coinciden. Diferencia de " + String.valueOf(diff));
			}			
		} 
		
		List<GeneralLedgerMapping> accList = clientLocalDao.getGlSalesAccounts(sucursal, "MC");
		if(!accList.isEmpty()) {
			for(GeneralLedgerMapping o : accList) {				
				GLInterfaceV3 obj = new GLInterfaceV3();
			    obj.setSegment1(o.getCompany());
				obj.setSegment2(o.getCostCenter());
				obj.setSegment3(o.getAccount());
				boolean hasAmount = false;
				switch(o.getSequence()) {
				   case 1 :
						obj.setEntered_Debit_Amount(Math.round(debitAmtVD*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditAmtVD*100.0)/100.0);
					   	break;
				   case 2 :
						obj.setEntered_Debit_Amount(Math.round(debitAmtCTA*100.0)/100.0);
						obj.setEntered_Credit_Amount(Math.round(creditAmtCTA*100.0)/100.0);
						break;
				   default :
				      break;
				}
				
				if(obj.getEntered_Debit_Amount() != 0 || obj.getEntered_Credit_Amount() != 0) {
					hasAmount = true;
				}
					
				if(hasAmount) {
					obj.setStatus_Code("NEW");
					obj.setLedger_ID(o.getLedgerId());
					obj.setEffective_Date_of_Transaction(dateString);
					obj.setJournal_Source("Balance Transfer");
					obj.setJournal_Category("Third Party Merge");
					obj.setCurrency_Code("MXN");
					obj.setJournal_Entry_Creation_Date(dateString);
					obj.setActual_Flag("A");
					obj.setREFERENCE4("JE-" + uniqueKey + "-" + year);
					obj.setREFERENCE5("MOVIMIENTOS CAJA " + sucursal + " " + day + "-" + month + "-" + year);
					obj.setInterface_Group_Identifier("1");
					
					obj.setSegment4("");
					obj.setSegment5("");
					obj.setSegment6("");
					obj.setSegment7("");
					obj.setSegment8("");
					obj.setSegment9("");
					obj.setSegment10("");
					obj.setSegment11("");
					obj.setSegment12("");
					obj.setSegment13("");
					obj.setSegment14("");
					obj.setSegment15("");
					obj.setSegment16("");
					obj.setSegment17("");
					obj.setSegment18("");
					obj.setSegment19("");
					obj.setSegment20("");
					obj.setSegment21("");
					obj.setSegment22("");
					obj.setSegment23("");
					obj.setSegment24("");
					obj.setSegment25("");
					obj.setSegment26("");
					obj.setSegment27("");
					obj.setSegment28("");
					obj.setSegment29("");
					obj.setSegment30("");
					obj.setConverted_Debit_Amount("");
					obj.setConverted_Credit_Amount("");
					obj.setREFERENCE1("");
					obj.setREFERENCE2("");
					obj.setREFERENCE3("");
					obj.setREFERENCE6("");
					obj.setREFERENCE7("");
					obj.setREFERENCE8("");
					obj.setREFERENCE9("");
					obj.setREFERENCE10("");
					obj.setReference_column_1("");
					obj.setReference_column_2("");
					obj.setReference_column_3("");
					obj.setReference_column_4("");
					obj.setReference_column_5("");
					obj.setReference_column_6("");
					obj.setReference_column_7("");
					obj.setReference_column_8("");
					obj.setReference_column_9("");
					obj.setReference_column_10("");
					obj.setStatistical_Amount("");
					obj.setCurrency_Conversion_Type("");
					obj.setCurrency_Conversion_Date("");
					obj.setCurrency_Conversion_Rate("");
					obj.setContext_field_for_Journal_Entry_Line_DFF("");
					obj.setATTRIBUTE1("");
					obj.setATTRIBUTE2("");
					obj.setATTRIBUTE3("");
					obj.setATTRIBUTE4("");
					obj.setATTRIBUTE5("");
					obj.setATTRIBUTE6("");
					obj.setATTRIBUTE7("");
					obj.setATTRIBUTE8("");
					obj.setATTRIBUTE9("");
					obj.setATTRIBUTE10("");
					obj.setATTRIBUTE11("");
					obj.setATTRIBUTE12("");
					obj.setATTRIBUTE13("");
					obj.setATTRIBUTE14("");
					obj.setATTRIBUTE15("");
					obj.setATTRIBUTE16("");
					obj.setATTRIBUTE17("");
					obj.setATTRIBUTE18("");
					obj.setATTRIBUTE19("");
					obj.setATTRIBUTE20("");
					obj.setContext_field_for_Captured_Information_DFF("");
					obj.setAverage_Journal_Flag("");
					obj.setClearing_Company("");
					obj.setLedger_Name("");
					obj.setEncumbrance_Type_ID("");
					obj.setReconciliation_Reference("");
					obj.setEnd("END");
					listRecords.add(obj);
				}

			}
		}
		glinterface.setListGLInterface(listRecords);
		return glinterface;
	}

	public static Date dateCalculateDays(String dateString, String dateFormat, int days) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.DATE, days);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
	
	public static Date dateCalculateSeconds(String dateString, String dateFormat, int seconds) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.SECOND, seconds);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
}

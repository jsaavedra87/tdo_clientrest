package com.tdo.integration.services;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.SalesDTO;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.client.localmodel.FileTransferControl;
import com.tdo.integration.client.model.MovimientosCaja;
import com.tdo.integration.client.model.NotaCredito;
import com.tdo.integration.client.model.Sales;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.RestClientDao;

@Service("salesService")
public class SalesService {

	@Autowired
	ClientLocalService clientLocalService;
	
	@Autowired
	RestClientDao restClientDao; 
	
	@Autowired
	ClientLocalDao clientLocalDao;
	
	@Autowired
	RESTService restService;
	
	List<Sales> s;
	
	@SuppressWarnings("unused")
	public void startCentralizedSalesProcess(String sucursal, int idCC, String restSalesService) {
		String transactionType = AppConstants.FILE_TRANSACTION_TYPE_CC_REPORT;
		List<FileTransferControl> lstFTC = null;
		String dateFormat = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		BigInteger ultimoCC;
		String initFromDate = sdf.format(new Date());
		String initToDate = sdf.format(new Date());		
		String fromDate = "";
		String toDate = "";	
				
		try {			
			//Verifica si ya existe el registro
			FileTransferControl obj = clientLocalService.searchFileTransferControlByIdCC(transactionType, idCC);
			
			if(obj != null) {
				if(!AppConstants.FILE_REQUEST_SUCCESS.equals(obj.getStatus())) {
					clientLocalService.updateFileTransferControlStatus(obj, AppConstants.FILE_REQUEST_NEW);
				}				
			} else {
				clientLocalService.createFileTransferControl(transactionType, idCC, initFromDate, initToDate, AppConstants.FILE_REQUEST_NEW, null);
			}
			
			//Obtiene registros pendientes desde siete d�as antes para procesarlos
			Date queryFromDate = dateCalculateDays(initFromDate.substring(0, 10) + " 00:00:00", dateFormat, -7);
			Date queryToDate = sdf.parse(initToDate);
			String[] pendingStatus = new String[]{AppConstants.FILE_REQUEST_NEW, AppConstants.FILE_REQUEST_SENT, AppConstants.FILE_REQUEST_ERROR};
			
			lstFTC = clientLocalService.searchFileTransferControlByDateRangeAndStatus(transactionType, queryFromDate, queryToDate, pendingStatus);

			//Procesa los registros pendientes			
			if(lstFTC != null && lstFTC.size() > 0) {
				for(FileTransferControl ftc : lstFTC) {					
					try {
						fromDate = sdf.format(ftc.getFromDate());
						toDate = sdf.format(ftc.getToDate());
						
						SalesDTO sales = this.getSalesDTO(sucursal, AppConstants.REQUEST_TYPE_CC, new String[] { String.valueOf(ftc.getIdCC()) });
						
						if(sales != null && ((sales.getSales() != null && !sales.getSales().isEmpty())
								|| (sales.getVentasCanceladas() != null && !sales.getVentasCanceladas().isEmpty())
								|| (sales.getCancelaciones() != null && !sales.getCancelaciones().isEmpty())
								|| (sales.getMovimientosCaja() != null && !sales.getMovimientosCaja().isEmpty())
								|| (sales.getNcExtemporaneas() != null && !sales.getNcExtemporaneas().isEmpty()))) {

							clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SENT);
							sales.setFileTransferControl(ftc);
							String response = restService.sendRequestObject(restSalesService, sales);
							System.out.print("Respuesta del env�o hacia la BD Centralizada para el CC" + ftc.getIdCC() + ": \r" + response);
							
							if(response != null && response.replace("\"", "").contains(AppConstants.FILE_RESPONSE_SUCCESS)) {
								ftc.setLog(response.replace("\"", ""));
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SUCCESS);
							} else {
								ftc.setLog("Error en la respuesta del ws: " + response);
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
							}							
						} else {							
							ultimoCC = this.getLastId();
							if(ftc.getIdCC() <= ultimoCC.intValue()) {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NO_SALE);
							} else {
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NEW);
							}							
						}
						
					} catch (Exception e) {
						ftc.setLog("Error en clientREST: " + e.getMessage());
						clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			clientLocalService.createFileTransferControl(transactionType, idCC, initFromDate, initToDate, AppConstants.FILE_REQUEST_ERROR, "Ocurri� un error al ejecutar la integraci�n de env�o de Ventas Centralizadas: " + e.getMessage());
		}
	}	
	
	public SalesDTO getSalesDTO(String sucursal, String requestType, String[] params) {
		SalesDTO salesDTO = restClientDao.getSales(sucursal, requestType, params);
		SalesDTO salesDTOOutOfDateCreditNotes = restClientDao.getOutOfDateCreditNotes(sucursal, requestType, params);
		List<MovimientosCaja> movimientos = restClientDao.getCashTransactions(sucursal, requestType, params);
		
		if(salesDTOOutOfDateCreditNotes != null) {
			if(salesDTOOutOfDateCreditNotes.getSales() != null && !salesDTOOutOfDateCreditNotes.getSales().isEmpty()){
				List<NotaCredito> cnList = new ArrayList<NotaCredito>();
				for(Sales s : salesDTOOutOfDateCreditNotes.getSales()) {
					if(s.getNotaCredito() != null) {
						cnList.add(s.getNotaCredito());
					}					
				}
				salesDTO.setNcExtemporaneas(cnList);				
			}
		}		
		salesDTO.setMovimientosCaja(movimientos);
		
		return salesDTO;
	 }
	
	public BigInteger getLastId() {		
		BigInteger lastId = restClientDao.getLastId();		
		return lastId;
	}

	public static Date dateCalculateDays(String dateString, String dateFormat, int days) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.DATE, days);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
	
	public static Date dateCalculateSeconds(String dateString, String dateFormat, int seconds) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.SECOND, seconds);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
}

package com.tdo.integration.services;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.InventoryTransaction;
import com.dto.integration.dto.InventoryTransactionCost;
import com.dto.integration.dto.InventoryTransactionHeader;
import com.dto.integration.dto.InventoryTransactionLots;
import com.dto.integration.dto.InventoryTransactionV1;
import com.dto.integration.dto.SalesDTO;
import com.dto.integration.util.AppConstants;
import com.tdo.integration.client.localmodel.FileTransferControl;
import com.tdo.integration.client.localmodel.GeneralLedgerMapping;
import com.tdo.integration.client.model.Articulo;
import com.tdo.integration.client.model.DetalleLote;
import com.tdo.integration.client.model.NotaCredito;
import com.tdo.integration.client.model.Sales;
import com.tdo.integration.dao.ClientLocalDao;
import com.tdo.integration.dao.RestClientDao;

@Service("inventoryService")
public class InventoryService {
	
	@Autowired
	RestClientDao restClientDao; 
	
	@Autowired
	ClientLocalDao clientLocalDao;	
	
	List<Sales> s;
	private static final String TYPE_SALES = "SALES";
	private static final String TYPE_NC = "NC";
	private static final String NO_LOT = "SL";
	
	String dateString = null;
	SimpleDateFormat sdfr = new SimpleDateFormat("yyyy/MM/dd");
	private String sucursal;
	
	@Autowired
	RESTService restService;
	
	@Autowired
	ClientLocalService clientLocalService;
	
	public void startInventoryProcess(String sucursal, String initFromDate, String initToDate, String restInventoryService) {
		String transactionType = AppConstants.FILE_TRANSACTION_TYPE_INV;
		InventoryTransaction it = new InventoryTransaction();
		List<FileTransferControl> lstFTC = null;
		String dateFormat = "yyyy-MM-dd HH:mm:ss";		
		String fromDate = "";
		String toDate = "";		
				
		try {
			 
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);			
			Date dateFromDate = sdf.parse(initFromDate);
			Date dateToDate = sdf.parse(initToDate);
			
			//Verifica si ya existe el registro
			FileTransferControl obj = clientLocalService.searchFileTransferControlByFromDate(transactionType, dateFromDate);
			
			if(obj != null) {
				if(!obj.getToDate().equals(dateToDate)) {
					//Si ya existe el registro, la fecha de inicio del nuevo registro ser� la fecha final del registro anterior (+1 segundo)
					initFromDate = sdf.format(dateCalculateSeconds(sdf.format(obj.getToDate()), dateFormat, 1));					
				}
			}
			
			
			//Inserta nuevo registro de solicitud
			clientLocalService.createFileTransferControl(transactionType, 0, initFromDate, initToDate, AppConstants.FILE_REQUEST_NEW, null);
			
			//Obtiene registros pendientes desde siete d�as antes para procesarlos
			Date queryFromDate = dateCalculateDays(initFromDate.substring(0, 10) + " 00:00:00", dateFormat, -7);
			Date queryToDate = sdf.parse(initToDate);
			String[] pendingStatus = new String[]{AppConstants.FILE_REQUEST_NEW, AppConstants.FILE_REQUEST_SENT, AppConstants.FILE_REQUEST_ERROR};
			
			lstFTC = clientLocalService.searchFileTransferControlByDateRangeAndStatus(transactionType, queryFromDate, queryToDate, pendingStatus);

			//Procesa los registros pendientes			
			if(lstFTC != null && lstFTC.size() > 0) {
				for(FileTransferControl ftc : lstFTC) {					
					try {
						fromDate = sdf.format(ftc.getFromDate());
						toDate = sdf.format(ftc.getToDate());
						
						InventoryTransactionV1 inv = this.createInventoryInterface(sucursal, AppConstants.REQUEST_TYPE_TIME, new String[] { fromDate, toDate });			
						it.setInventoryTransactionV1(inv);
						
						if(inv != null && ((inv.getInventoryTransactionCost() != null && !inv.getInventoryTransactionCost().isEmpty())
										|| (inv.getInventoryTransactionHeader() != null && !inv.getInventoryTransactionHeader().isEmpty())
										|| (inv.getInventoryTransactionLots() != null && !inv.getInventoryTransactionLots().isEmpty()))) {
							
							clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SENT);
							it.setFileTransferControl(ftc);
							
							String response = restService.sendRequestObject(restInventoryService, it);
							System.out.print("Respuesta de env�o de Inventario [" + fromDate + " - " + toDate + "]: \r" + response);
							
							if(response != null && response.replace("\"", "").contains(AppConstants.FILE_RESPONSE_SUCCESS)) {
								ftc.setLog(response.replace("\"", ""));
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_SUCCESS);
							} else {
								ftc.setLog("Error en la respuesta del ws: " + response);
								clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
							}				
						} else {
							clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_NO_SALE);
						}
					} catch (Exception e) {
						ftc.setLog("Error en clientREST: " + e.getMessage());
						clientLocalService.updateFileTransferControlStatus(ftc, AppConstants.FILE_REQUEST_ERROR);
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			clientLocalService.createFileTransferControl(transactionType, 0, initFromDate, initToDate, AppConstants.FILE_REQUEST_ERROR, "Ocurri� un error al ejecutar la integraci�n de env�o de Inventarios: " + e.getMessage());
		}
	}
	
	public InventoryTransactionV1 createInventoryInterface(String sucursal, String requestType, String[] params) {		
		this.sucursal = sucursal;
		
		List<Sales> sales = new ArrayList<Sales>();
		SalesDTO salesDTO = restClientDao.getSales(sucursal, requestType, params);
		List<Sales> cancelledSales = new ArrayList<Sales>();
		if(salesDTO != null) {
			sales = salesDTO.getSales();
			cancelledSales = salesDTO.getVentasCanceladas();
		}
		
		SalesDTO ncDTO = restClientDao.getOutOfDateCreditNotes(sucursal, requestType, params);
		List<Sales> creditNote =  new ArrayList<Sales>();
		if(ncDTO != null) {
			creditNote = ncDTO.getSales();
		}
		
		InventoryTransactionV1 it = new InventoryTransactionV1();
		List<InventoryTransactionHeader> ithList = new ArrayList<InventoryTransactionHeader>();
		List<InventoryTransactionLots> itlList = new ArrayList<InventoryTransactionLots>();
		List<InventoryTransactionCost> itcList = new ArrayList<InventoryTransactionCost>();
		List<NotaCredito> notaCreditoList = new ArrayList<NotaCredito>();

		AtomicInteger srcHdrId = new AtomicInteger(1);
		AtomicInteger intNum = new AtomicInteger(1);		
		AtomicInteger lotNum = new AtomicInteger(1);
		
		if(sales == null) {
			sales = new ArrayList<Sales>();
		}
		
		// Procesa notas de cr�dito
		List<GeneralLedgerMapping> accDevList = clientLocalDao.getInventoryAccountsDev(sucursal);
		if(!accDevList.isEmpty() && !notaCreditoList.isEmpty() ) {
			GeneralLedgerMapping accountDev = accDevList.get(0);
			for(NotaCredito s : notaCreditoList) {
				List<DetalleLote> dl = s.getDetalleLote();
				List<DetalleLote> dlKit = s.getDetalleLotePaquete();
				List<Articulo> arts = s.getArticulo();
				registerRecords(ithList,itlList,itcList,dl,dlKit,arts,s.getArticuloPaquete(),srcHdrId,intNum, lotNum, accountDev, s.getIdVenta(), s.getIdTicket(), s.getIdNotaVenta(), TYPE_NC);
			}
		}

		// Procesa notas de cr�dito extempor�neas
		if(!accDevList.isEmpty() && !creditNote.isEmpty()) {
			GeneralLedgerMapping accountDev = accDevList.get(0);
			for(Sales s : creditNote) {
				List<DetalleLote> dl = s.getDetalleLote();
				List<DetalleLote> dlKit = s.getDetalleLotePaquete();
				List<Articulo> arts = s.getArticulo();
				registerRecords(ithList,itlList,itcList,dl,dlKit,arts,s.getArticuloPaquete(),srcHdrId, intNum, lotNum, accountDev, s.getIdVenta(), s.getIdTicket(), s.getIdNotaVenta(), TYPE_NC);
			}
		}
		
		// Procesa cancelaciones
		if(cancelledSales != null) {
			if(!cancelledSales.isEmpty()) {
				GeneralLedgerMapping accountDev = accDevList.get(0);
				for(Sales s : cancelledSales) {
					List<DetalleLote> dl = s.getDetalleLote();
					List<DetalleLote> dlKit = s.getDetalleLotePaquete();
					List<Articulo> arts = s.getArticulo();
					registerRecords(ithList,itlList,itcList,dl,dlKit,arts,s.getArticuloPaquete(),srcHdrId,intNum, lotNum, accountDev, s.getIdVenta(), s.getIdTicket(), s.getIdNotaVenta(), TYPE_NC);
				}
			}
		}
					
		// Procesa ventas
		List<GeneralLedgerMapping> accList = clientLocalDao.getInventoryAccounts(sucursal);		
		if(!accList.isEmpty() && !sales.isEmpty()) {
			GeneralLedgerMapping account = accList.get(0);
			for(Sales s : sales) {
				NotaCredito nc = s.getNotaCredito();
				if(nc != null) {
					notaCreditoList.add(nc);
				}
				List<DetalleLote> dl = s.getDetalleLote();
				List<DetalleLote> dlKit = s.getDetalleLotePaquete();
				List<Articulo> arts = s.getArticulo();
				registerRecords(ithList,itlList,itcList,dl,dlKit,arts,s.getArticuloPaquete(),srcHdrId,intNum, lotNum, account, s.getIdVenta(), s.getIdTicket(), s.getIdNotaVenta(), TYPE_SALES);
			}
		}
		
		it.setInventoryTransactionCost(itcList);
		it.setInventoryTransactionHeader(ithList);
		it.setInventoryTransactionLots(itlList);
        return it;
	}
	
	private void registerRecords(List<InventoryTransactionHeader> ithList,
								 List<InventoryTransactionLots> itlList,
								 List<InventoryTransactionCost> itcList,
								 List<DetalleLote> dl,
								 List<DetalleLote> dlKit,
								 List<Articulo> arts,
								 List<Articulo> paquete,
								 AtomicInteger srcHdrId,
								 AtomicInteger intNum,								 
								 AtomicInteger lotNum, 
								 GeneralLedgerMapping account,
								 BigInteger BIidVenta,
								 BigInteger BIidTicket,
								 BigInteger BIidNotaVenta,
								 String recordType) {
		
		int mult = 0;
		int lineNum = 1;
		int idVenta = 0;
		int idTicket = 0;
		int idNotaVenta = 0;
		
		if(BIidVenta != null) {
			idVenta = BIidVenta.intValue();
		}
		if(BIidTicket != null) {
			idTicket = BIidTicket.intValue();
		}
		if(BIidNotaVenta != null) {
			idNotaVenta = BIidNotaVenta.intValue();
		}
		
		String transType = "";
		if(TYPE_SALES.equals(recordType)) {
			mult = -1;
			transType = "Venta Sicar";
		}else {
			mult = 1;
			transType = "Devolucion Sicar";
		}
		
		List<DetalleLote> lotToProcess = null;
		List<Articulo> artToProcess = null;		
		
		for(Articulo art : arts){	
			// JAvila: Rutina para insertr articulos de Kit en lugar del paquete
			// Tipo 2 = Kit: Procesa los articulos dentro del kit
			
			if(art.getTipo() != null) {
				if(art.getTipo() == 2) {
					if(paquete != null) {
						lotToProcess = new ArrayList<DetalleLote>();
						artToProcess = new ArrayList<Articulo>();
						for(Articulo ap : paquete) {
							if((ap.getPaquete().intValue() == art.getArticuloId().intValue()) && (art.getIdVenta().intValue() == ap.getIdVenta().intValue())) {
								artToProcess.add(ap);
							}
						}
						
						for(DetalleLote dlk : dlKit) {
							if((dlk.getPaquete().intValue() == art.getArticuloId().intValue()) && (art.getIdVenta().intValue() == dlk.getIdVenta().intValue())) {
								lotToProcess.add(dlk);
							}
						}
					}
				}else{
					lotToProcess  = dl;
					artToProcess = new ArrayList<Articulo>();
					artToProcess.add(art);
				}
			}
			
			if(lotToProcess != null && artToProcess != null) {
				for(Articulo a : artToProcess ) {
					if(!a.getArticuloDescuento()) {
						for(DetalleLote o : lotToProcess) {
							if(o.getArticuloId() == a.getArticuloId() && idVenta == o.getIdVenta().intValue()) {
	
								System.out.println(idVenta + ":" + o.getArticuloClave() + ":" + o.getLoteNumero());
								
								InventoryTransactionHeader ith = new InventoryTransactionHeader();
								InventoryTransactionLots itl = new InventoryTransactionLots();
								InventoryTransactionCost itc = new InventoryTransactionCost();
								boolean hasLot = false;
								
								try {
									String numLote = o.getLoteNumero();
									if(!"".equals(numLote)) {
										String[] lotStructure = numLote.split("_");
																				 
										if(NO_LOT.equals(lotStructure[0])) {
											if(lotStructure.length == 5) {  //NoLot_Sub_p1_p2_p3
												ith.setSUBINVENTORY_CODE(lotStructure[1]);
												ith.setLOC_SEGMENT1(lotStructure[2]); 
												ith.setLOC_SEGMENT2(lotStructure[3]); 
												ith.setLOC_SEGMENT3(lotStructure[4]);
												itl.setLOT_NUMBER("");
												hasLot = false;
											}
											
											if(lotStructure.length == 2) { //NoLot_Sub
												ith.setSUBINVENTORY_CODE(lotStructure[1]);
												ith.setLOC_SEGMENT1(""); 
												ith.setLOC_SEGMENT2(""); 
												ith.setLOC_SEGMENT3("");
												itl.setLOT_NUMBER("");
												hasLot = false;
											}
										}else {
											
											if(lotStructure[0].contains(",")) {
												lotStructure[0] = new StringBuffer().append("\"").append(lotStructure[0]).append("\"").toString();
											}
											
											if(lotStructure.length == 5) {  //Lote_sub_p1_p2_p3
												ith.setSUBINVENTORY_CODE(lotStructure[1]);
												ith.setLOC_SEGMENT1(lotStructure[2]); 
												ith.setLOC_SEGMENT2(lotStructure[3]); 
												ith.setLOC_SEGMENT3(lotStructure[4]);
												itl.setLOT_NUMBER(lotStructure[0]);
												hasLot = true;
											}
											
											if(lotStructure.length == 2) { //Lote_sub
												ith.setSUBINVENTORY_CODE(lotStructure[1]);
												ith.setLOC_SEGMENT1(""); 
												ith.setLOC_SEGMENT2(""); 
												ith.setLOC_SEGMENT3("");
												itl.setLOT_NUMBER(lotStructure[0]);
												hasLot = true;
											}
										}
																			
										//LOT DETAIL
										if(hasLot) {
											dateString = sdfr.format(o.getLoteFechaCaducidad());									
											itl.setLOT_EXPIRATION_DATE(dateString);
											itl.setPRIMARY_QUANTITY(String.valueOf((Math.round((o.getCantidad().doubleValue())*100.0)/100.0)*mult));
											itl.setTRANSACTION_QUANTITY(String.valueOf((Math.round((o.getCantidad().doubleValue())*100.0)/100.0)*mult));
											itl.setLOT_INTERFACE_NBR(String.valueOf(lotNum));						
											itlList.add(itl);
										}
										
										//LOT HEADER
										ith.setORGANIZATION_NAME(sucursal);
										ith.setPROCESS_FLAG("1");
										ith.setITEM_NUMBER(a.getArticuloClave());
										if(hasLot) {
											ith.setINV_LOTSERIAL_INTERFACE_NUM(String.valueOf(lotNum));
										}															
										ith.setTRANSACTION_QUANTITY(String.valueOf((Math.round((o.getCantidad().doubleValue())*100.0)/100.0)*mult));
										
										String uom = a.getVentaDetalleUnidad();
										String uomTransfer = "";
										switch(uom.trim()) {
										   case "PIEZA" :
										    	uomTransfer = "PIEZA";
											   	break;
										   case "PZ" :
											   uomTransfer = "PIEZA";
											   	break;
										   case "LITRO" :
											   uomTransfer = "LITRO";
											   	break;
										   case "LT" :
											   uomTransfer = "LITRO";
											   	break;
										   case "MT" :
											   uomTransfer = "METRO";
											   	break;
										   case "METRO" :
											   uomTransfer = "METRO";
											   	break;
										   case "M2" :
											   uomTransfer = "METRO2";
											   	break;
										   case "METRO2" :
											   uomTransfer = "METRO2";
											   	break;										   	
										   case "SER" :
											   uomTransfer = "SERVICIO";
											   	break;
										   case "KG" :
											   uomTransfer = "KILO";
											   	break;											   	
										   default :
											   uomTransfer = a.getVentaDetalleUnidad();
										        break;
								    	}
										SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
										sdf.setTimeZone(TimeZone.getTimeZone(AppConstants.TIME_ZONE_UTC));
										
										ith.setTRANSACTION_UNIT_OF_MEASURE(uomTransfer);
										ith.setTRANSACTION_DATE(sdf.format(new Date()));
										ith.setTRANSACTION_TYPE_NAME(transType);
										ith.setSOURCE_CODE("SR");
										ith.setSOURCE_HEADER_ID(String.valueOf(srcHdrId));
										ith.setSOURCE_LINE_ID(String.valueOf(lineNum));
										ith.setTRANSACTION_MODE("3");
										ith.setLOCK_FLAG("2");
										ith.setUSE_CURRENT_COST("N");
										if(idTicket != 0) {
											ith.setTRANSACTION_SOURCE_NAME(String.valueOf(idTicket));
										} else {
											ith.setTRANSACTION_SOURCE_NAME(String.valueOf(idNotaVenta));
										}
										
										ith.setTRANSACTION_REFERENCE(String.valueOf(lineNum));
										ith.setDST_SEGMENT1(account.getCompany());
										ith.setDST_SEGMENT2(account.getCostCenter());
										ith.setDST_SEGMENT3(account.getAccount());
										ith.setTRANSACTION_COST_IDENTIFIER(String.valueOf(intNum));
										ithList.add(ith);
										
										//LOT COST
										itc.setTRANSACTION_COST_IDENTIFIER(String.valueOf(intNum));
										itc.setCOST_COMPONENT_CODE("ITEM_PRICE");
										itc.setCOST(String.valueOf(Math.round((a.getPrecioCompra().doubleValue())*100.0)/100.0));
										
										itcList.add(itc);									
										intNum.incrementAndGet();
										lineNum++;
										if(hasLot) {
											lotNum.incrementAndGet();
										}
										
									}else {
										continue;
									}
								}catch(Exception e) {
									continue;
								}
							}
						}
					}
				}			
			}
			//JAvila: FIN
		}
		
	}
	
	public static Date dateCalculateDays(String dateString, String dateFormat, int days) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.DATE, days);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}

	public static Date dateCalculateMinutes(String dateString, String dateFormat, int minutes) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.MINUTE, minutes);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
	
	public static Date dateCalculateSeconds(String dateString, String dateFormat, int seconds) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat s = new SimpleDateFormat(dateFormat);
	    
	    try {	    	
	        cal.setTime(s.parse(dateString));	        
	        cal.add(Calendar.SECOND, seconds);	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    return cal.getTime();
	}
	
}

package com.dto.integration.util;

public final class AppConstants {
	
	 public static final String UDC_TIPO_CAMBIO = "TCAMBIO";
	 public static final String UDC_TIPO_CAMBIO_KEY = "USD";
	 
	 public static final String ITEM_MASTER_SYSTEM = "ITEMMASTER";
	 public static final String ITEM_MASTER_KEY = "LAST_EXECUTION";
	 
	 public static final String CUSTOMER_SYSTEM = "CUSTOMER";
	 public static final String CUSTOMER_KEY = "LAST_EXECUTION";
	 
	 public static final String CUSTOMER_SICAR_SYSTEM = "CUSTOMER_SICAR";
	 public static final String CUSTOMER_SICAR_KEY = "LAST_EXECUTION";	 
	 
	 public static final String INV_TRANSACTIONS_SYSTEM = "INVENTORY_TRANSACTIONS";
	 public static final String INV_TRANSACTIONS_KEY = "LAST_EXECUTION";
	 
	 public static final String BRANCH_SYSTEM = "BRANCH";
	 public static final String BRANCH_KEY = "CODE";
	 
	 public static final String VENTAS_SYSTEM_CIERRE = "VENTAS";
	 public static final String VENTAS_KEY_CIERRE = "CIERRE";
	 
	 public static final String VENTAS_SYSTEM_INVENTARIO = "VENTAS";
	 public static final String VENTAS_KEY_INVENTARIO = "INVENTARIO";

	 public static final String VENTAS_SYSTEM_GL = "VENTAS";
	 public static final String VENTAS_KEY_GL = "GL";
	 
	 public static final String VENTAS_SYSTEM_AR = "VENTAS";
	 public static final String VENTAS_KEY_AR = "AR";
	 
	 public static final String VENTAS_SYSTEM_INVENTORY = "VENTAS";
	 public static final String VENTAS_KEY_INVENTORY = "INVENTORY";
	 
	 public static final String VENTAS_SYSTEM_CASH_MOV = "VENTAS";
	 public static final String VENTAS_KEY_CASH_MOV = "CASH_MOV";
	 
	 public static final String VENTAS_SYSTEM_CENTRALIZED = "VENTAS";
	 public static final String VENTAS_KEY_CENTRALIZED = "CENTRALIZED";

	 public static final String VENTAS_SYSTEM_CREDIT = "VENTAS";
	 public static final String VENTAS_KEY_CREDIT = "CREDIT_LIMITS";
	 
	 public static final String VENTAS_SYSTEM_PAYMENT = "VENTAS";
	 public static final String VENTAS_KEY_PAYMENT = "PAYMENT";	 

	 public static final String REQUEST_TYPE_CC = "REQUEST_TYPE_CC";
	 public static final String REQUEST_TYPE_TIME = "REQUEST_TYPE_TIME";
	 
	 public static final String TIPO_PAGO_CLAVE_MONEDERO = "MN";
	 public static final String TIPO_PAGO_NOMBRE_MONEDERO = "Monedero";
	 
	 public static final String TIPO_PAGO_CLAVE_NOTA_CREDITO = "NC";
	 public static final String TIPO_PAGO_NOMBRE_NOTA_CREDITO = "Nota de Cr�dito";
	 
	 public static final String STATUS_ON = "1";
	 public static final String STATUS_OFF = "0";
	 
	 public static final String NC_DESCUENTO_RECORD_TYPE = "6";
	 public static final String CEDI_CODE = "CDI001";
	 public static final String CURRENCY_CODE_MXN = "MXN"; 
	 public static final String FLOAT_FORMAT_2 = "%.3f";
	 public static final String FLOAT_FORMAT_6 = "%.6f";
	 public static final String FLOAT_FORMAT_10 = "%.10f";
			 
	 public static final String RESPONSE_TXT_PATH="NCRESPONSEPATH";
	 public static final String UPDATING_CREDIT_NOTE="Actualizando UUID en sicar de la NC folio: ";
	 public static final String STATUS_NC_PROCESSED ="PROCESSED";
	 public static final String STATUS_NC_INPROCESS="INPROCESS";
	 public static final String SUCCESS_RESPONSE_CODE="00";
	 public static final String STATUS_NC_ERROR ="ERROR";
	 public static final String INVENTORY_STATUS_OK ="ESTABLE";
	 public static final String INVENTORY_STATUS_UNESPECTED ="REVISAR";	 
	 	 
	 public static final String FILE_TRANSACTION_TYPE_GL ="GL";
	 public static final String FILE_TRANSACTION_TYPE_AR ="AR";
	 public static final String FILE_TRANSACTION_TYPE_INV ="INVENTARIO";
	 public static final String FILE_TRANSACTION_TYPE_MOVEMENTS ="MOVIMIENTOS";
	 public static final String FILE_TRANSACTION_TYPE_PAYMENT ="ABONOS";
	 public static final String FILE_TRANSACTION_TYPE_CC_REPORT ="REPORTE CC";
	 
	 public static final String FILE_REQUEST_NEW = "NUEVO";
	 public static final String FILE_REQUEST_SENT = "ENVIADO";
	 public static final String FILE_REQUEST_ERROR = "ERROR";
	 public static final String FILE_REQUEST_SUCCESS = "PROCESADO";
	 public static final String FILE_REQUEST_NO_SALE = "SIN VENTAS";
	 
	 public static final String FILE_RESPONSE_SUCCESS = "status:SUCCESS";
	 public static final String FILE_RESPONSE_ERROR = "status:ERROR";
	 
	 public static final String BUSINESS_UNIT_ID_SYSTEM = "BRANCH";
	 public static final String BUSINESS_UNIT_ID = "BUSINESS_UNIT_ID";
	 
	 public static final String DATA_ACCESS_SET_SYSTEM = "BRANCH";
	 public static final String DATA_ACCESS_SET = "DATA_ACCESS_SET";
	
	 public static final String LEDGER_ID_SYSTEM = "BRANCH";
	 public static final String LEDGER_ID = "LEDGER_ID";
	 
	 public static final String TIME_ZONE_SYSTEM = "BRANCH";
	 public static final String TIME_ZONE = "TIME_ZONE";
	 public static final String TIME_ZONE_UTC = "UTC";
	 
	 public static final String INV_TRANS_IN_PROCESS = "IN PROCESS";
	 public static final String INV_TRANS_COMPLETED = "COMPLETED";

}

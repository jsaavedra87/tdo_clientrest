package com.dto.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class ClientesJson {

	@JsonProperty("cliId")
	private  int cliId;
	
	@JsonProperty("nombre")
	private  String nombre;
	
	@JsonProperty("representante")
	private  String representante;
	
	@JsonProperty("domicilio")
	private  String domicilio;
	
	@JsonProperty("noExt")
	private  String noExt;
	
	@JsonProperty("noInt")
	private  String noInt;
	
	@JsonProperty("localidad")
	private  String localidad;
	
	@JsonProperty("ciudad")
	private  String ciudad;
	
	@JsonProperty("estado")
	private  String estado;
	
	@JsonProperty("pais")
	private  String pais;
	
	@JsonProperty("codigoPostal")
	private  String codigoPostal;
	
	@JsonProperty("colonia")
	private  String colonia;
	
	@JsonProperty("rfc")
	private  String rfc;
	
	@JsonProperty("curp")
	private  String curp;
	
	@JsonProperty("telefono")
	private  String telefono;
	
	@JsonProperty("celular")
	private  String celular;
	
	@JsonProperty("mail")
	private  String mail;
	
	@JsonProperty("comentario")
	private  String comentario;
	
	@JsonProperty("status")
	private  int status;
	
	@JsonProperty("limite")
	private  float limite;
	
	@JsonProperty("precio")
	private  float precio;
	
	@JsonProperty("diasCredito")
	private  int diasCredito;
	
	@JsonProperty("retener")
	private  boolean retener;
	
	@JsonProperty("desglosarIEPS")
	private  boolean desglosarIEPS;
	
	@JsonProperty("clave")
	private  String clave;
	
	@JsonProperty("foto")
	private  String foto;
	
	@JsonProperty("huella")
	private  String huella;
	
	@JsonProperty("muestra")
	private  String muestra;
	
	@JsonProperty("usoCfdi")
	private  String usoCfdi;
	
	@JsonProperty("complementoList")
	private  String[] complementoList;

	public int getCliId() {
		return cliId;
	}

	public void setCliId(int cliId) {
		this.cliId = cliId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRepresentante() {
		return representante;
	}

	public void setRepresentante(String representante) {
		this.representante = representante;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getNoExt() {
		return noExt;
	}

	public void setNoExt(String noExt) {
		this.noExt = noExt;
	}

	public String getNoInt() {
		return noInt;
	}

	public void setNoInt(String noInt) {
		this.noInt = noInt;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public float getLimite() {
		return limite;
	}

	public void setLimite(float limite) {
		this.limite = limite;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getDiasCredito() {
		return diasCredito;
	}

	public void setDiasCredito(int diasCredito) {
		this.diasCredito = diasCredito;
	}

	public boolean getRetener() {
		return retener;
	}

	public void setRetener(boolean retener) {
		this.retener = retener;
	}

	public boolean isDesglosarIEPS() {
		return desglosarIEPS;
	}

	public void setDesglosarIEPS(boolean desglosarIEPS) {
		this.desglosarIEPS = desglosarIEPS;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getHuella() {
		return huella;
	}

	public void setHuella(String huella) {
		this.huella = huella;
	}

	public String getMuestra() {
		return muestra;
	}

	public void setMuestra(String muestra) {
		this.muestra = muestra;
	}

	public String getUsoCfdi() {
		return usoCfdi;
	}

	public void setUsoCfdi(String usoCfdi) {
		this.usoCfdi = usoCfdi;
	}

	public String[] getComplementoList() {
		return complementoList;
	}

	public void setComplementoList(String[] complementoList) {
		this.complementoList = complementoList;
	}

	
}

package com.dto.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class ArLockBoxImportC {

	private String Record_Type = "";
	private String Batch_Name = "";
	private String Item_Number = "";
	private String Remittance_Amount = "";
	private String Transit_Routing_Number = "";
	private String Customer_Bank_Account = "";
	private String Receipt_Number = "";
	private String Receipt_Date = "";
	private String Currency = "";
	private String Conversion_Rate_Type = "";
	private String Conversion_Rate = "";
	private String Customer_Account_Number = "";
	private String Customer_Site = "";
	private String Customer_Bank_Branch = "";
	private String Customer_Bank = "";
	private String Receipt_Method = "";
	private String Remittance_Bank_Branch = "";
	private String Remittance_Bank = "";
	private String Lockbox_Number = "";
	private String Deposit_Date = "";
	private String Deposit_Time = "";
	private String Anticipated_Clearing_Date = "";
	private String Transaction_Reference_1 = "";
	private String Transaction_Installment_1 = "";
	private String Transaction_Reference_Date_1 = "";
	private String Transaction_Currency_1 = "";
	private String Cross_Currency_Rate_1 = "";
	private String Applied_Amount_1 = "";
	private String Allocated_Receipt_Amount_1 = "";
	private String Customer_Reference_1 = "";
	private String Transaction_Reference_2 = "";
	private String Transaction_Installment_2 = "";
	private String Transaction_Reference_Date_2 = "";
	private String Transaction_Currency_2 = "";
	private String Cross_Currency_Rate_2 = "";
	private String Applied_Amount_2 = "";
	private String Allocated_Receipt_Amount_2 = "";
	private String Customer_Reference_2 = "";
	private String Transaction_Reference_3 = "";
	private String Transaction_Installment_3 = "";
	private String Transaction_Reference_Date_3 = "";
	private String Transaction_Currency_3 = "";
	private String Cross_Currency_Rate_3 = "";
	private String Applied_Amount_3 = "";
	private String Allocated_Receipt_Amount_3 = "";
	private String Customer_Reference_3 = "";
	private String Transaction_Reference_4 = "";
	private String Transaction_Installment_4 = "";
	private String Transaction_Reference_Date_4 = "";
	private String Transaction_Currency_4 = "";
	private String Cross_Currency_Rate_4 = "";
	private String Applied_Amount_4 = "";
	private String Allocated_Receipt_Amount_4 = "";
	private String Customer_Reference_4 = "";
	private String Transaction_Reference_5 = "";
	private String Transaction_Installment_5 = "";
	private String Transaction_Reference_Date_5 = "";
	private String Transaction_Currency_5 = "";
	private String Cross_Currency_Rate_5 = "";
	private String Applied_Amount_5 = "";
	private String Allocated_Receipt_Amount_5 = "";
	private String Customer_Reference_5 = "";
	private String Transaction_Reference_6 = "";
	private String Transaction_Installment_6 = "";
	private String Transaction_Reference_Date_6 = "";
	private String Transaction_Currency_6 = "";
	private String Cross_Currency_Rate_6 = "";
	private String Applied_Amount_6 = "";
	private String Allocated_Receipt_Amount_6 = "";
	private String Customer_Reference_6 = "";
	private String Transaction_Reference_7 = "";
	private String Transaction_Installment_7 = "";
	private String Transaction_Reference_Date_7 = "";
	private String Transaction_Currency_7 = "";
	private String Cross_Currency_Rate_7 = "";
	private String Applied_Amount_7 = "";
	private String Allocated_Receipt_Amount_7 = "";
	private String Customer_Reference_7 = "";
	private String Transaction_Reference_8 = "";
	private String Transaction_Installment_8 = "";
	private String Transaction_Reference_Date_8 = "";
	private String Transaction_Currency_8 = "";
	private String Cross_Currency_Rate_8 = "";
	private String Applied_Amount_8 = "";
	private String Allocated_Receipt_Amount_8 = "";
	private String Customer_Reference_8 = "";
	private String Comments = "";
	private String Payment_Interface_Flexfield_Segment_1 = "";
	private String Payment_Interface_Flexfield_Segment_2 = "";
	private String Payment_Interface_Flexfield_Segment_3 = "";
	private String Payment_Interface_Flexfield_Segment_4 = "";
	private String Payment_Interface_Flexfield_Segment_5 = "";
	private String Payment_Interface_Flexfield_Segment_6 = "";
	private String Payment_Interface_Flexfield_Segment_7 = "";
	private String Payment_Interface_Flexfield_Segment_8 = "";
	private String Payment_Interface_Flexfield_Segment_9 = "";
	private String Payment_Interface_Flexfield_Segment_10 = "";
	private String Payment_Interface_Flexfield_Segment_11 = "";
	private String Payment_Interface_Flexfield_Segment_12 = "";
	private String Payment_Interface_Flexfield_Segment_13 = "";
	private String Payment_Interface_Flexfield_Segment_14 = "";
	private String Payment_Interface_Flexfield_Segment_15 = "";
	
	public String getRecord_Type() {
		return Record_Type;
	}
	public void setRecord_Type(String record_Type) {
		Record_Type = record_Type;
	}
	public String getBatch_Name() {
		return Batch_Name;
	}
	public void setBatch_Name(String batch_Name) {
		Batch_Name = batch_Name;
	}
	public String getItem_Number() {
		return Item_Number;
	}
	public void setItem_Number(String item_Number) {
		Item_Number = item_Number;
	}
	public String getRemittance_Amount() {
		return Remittance_Amount;
	}
	public void setRemittance_Amount(String remittance_Amount) {
		Remittance_Amount = remittance_Amount;
	}
	public String getTransit_Routing_Number() {
		return Transit_Routing_Number;
	}
	public void setTransit_Routing_Number(String transit_Routing_Number) {
		Transit_Routing_Number = transit_Routing_Number;
	}
	public String getCustomer_Bank_Account() {
		return Customer_Bank_Account;
	}
	public void setCustomer_Bank_Account(String customer_Bank_Account) {
		Customer_Bank_Account = customer_Bank_Account;
	}
	public String getReceipt_Number() {
		return Receipt_Number;
	}
	public void setReceipt_Number(String receipt_Number) {
		Receipt_Number = receipt_Number;
	}
	public String getReceipt_Date() {
		return Receipt_Date;
	}
	public void setReceipt_Date(String receipt_Date) {
		Receipt_Date = receipt_Date;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getConversion_Rate_Type() {
		return Conversion_Rate_Type;
	}
	public void setConversion_Rate_Type(String conversion_Rate_Type) {
		Conversion_Rate_Type = conversion_Rate_Type;
	}
	public String getConversion_Rate() {
		return Conversion_Rate;
	}
	public void setConversion_Rate(String conversion_Rate) {
		Conversion_Rate = conversion_Rate;
	}
	public String getCustomer_Account_Number() {
		return Customer_Account_Number;
	}
	public void setCustomer_Account_Number(String customer_Account_Number) {
		Customer_Account_Number = customer_Account_Number;
	}
	public String getCustomer_Site() {
		return Customer_Site;
	}
	public void setCustomer_Site(String customer_Site) {
		Customer_Site = customer_Site;
	}
	public String getCustomer_Bank_Branch() {
		return Customer_Bank_Branch;
	}
	public void setCustomer_Bank_Branch(String customer_Bank_Branch) {
		Customer_Bank_Branch = customer_Bank_Branch;
	}
	public String getCustomer_Bank() {
		return Customer_Bank;
	}
	public void setCustomer_Bank(String customer_Bank) {
		Customer_Bank = customer_Bank;
	}
	public String getReceipt_Method() {
		return Receipt_Method;
	}
	public void setReceipt_Method(String receipt_Method) {
		Receipt_Method = receipt_Method;
	}
	public String getRemittance_Bank_Branch() {
		return Remittance_Bank_Branch;
	}
	public void setRemittance_Bank_Branch(String remittance_Bank_Branch) {
		Remittance_Bank_Branch = remittance_Bank_Branch;
	}
	public String getRemittance_Bank() {
		return Remittance_Bank;
	}
	public void setRemittance_Bank(String remittance_Bank) {
		Remittance_Bank = remittance_Bank;
	}
	public String getLockbox_Number() {
		return Lockbox_Number;
	}
	public void setLockbox_Number(String lockbox_Number) {
		Lockbox_Number = lockbox_Number;
	}
	public String getDeposit_Date() {
		return Deposit_Date;
	}
	public void setDeposit_Date(String deposit_Date) {
		Deposit_Date = deposit_Date;
	}
	public String getDeposit_Time() {
		return Deposit_Time;
	}
	public void setDeposit_Time(String deposit_Time) {
		Deposit_Time = deposit_Time;
	}
	public String getAnticipated_Clearing_Date() {
		return Anticipated_Clearing_Date;
	}
	public void setAnticipated_Clearing_Date(String anticipated_Clearing_Date) {
		Anticipated_Clearing_Date = anticipated_Clearing_Date;
	}
	public String getTransaction_Reference_1() {
		return Transaction_Reference_1;
	}
	public void setTransaction_Reference_1(String transaction_Reference_1) {
		Transaction_Reference_1 = transaction_Reference_1;
	}
	public String getTransaction_Installment_1() {
		return Transaction_Installment_1;
	}
	public void setTransaction_Installment_1(String transaction_Installment_1) {
		Transaction_Installment_1 = transaction_Installment_1;
	}
	public String getTransaction_Reference_Date_1() {
		return Transaction_Reference_Date_1;
	}
	public void setTransaction_Reference_Date_1(String transaction_Reference_Date_1) {
		Transaction_Reference_Date_1 = transaction_Reference_Date_1;
	}
	public String getTransaction_Currency_1() {
		return Transaction_Currency_1;
	}
	public void setTransaction_Currency_1(String transaction_Currency_1) {
		Transaction_Currency_1 = transaction_Currency_1;
	}
	public String getCross_Currency_Rate_1() {
		return Cross_Currency_Rate_1;
	}
	public void setCross_Currency_Rate_1(String cross_Currency_Rate_1) {
		Cross_Currency_Rate_1 = cross_Currency_Rate_1;
	}
	public String getApplied_Amount_1() {
		return Applied_Amount_1;
	}
	public void setApplied_Amount_1(String applied_Amount_1) {
		Applied_Amount_1 = applied_Amount_1;
	}
	public String getAllocated_Receipt_Amount_1() {
		return Allocated_Receipt_Amount_1;
	}
	public void setAllocated_Receipt_Amount_1(String allocated_Receipt_Amount_1) {
		Allocated_Receipt_Amount_1 = allocated_Receipt_Amount_1;
	}
	public String getCustomer_Reference_1() {
		return Customer_Reference_1;
	}
	public void setCustomer_Reference_1(String customer_Reference_1) {
		Customer_Reference_1 = customer_Reference_1;
	}
	public String getTransaction_Reference_2() {
		return Transaction_Reference_2;
	}
	public void setTransaction_Reference_2(String transaction_Reference_2) {
		Transaction_Reference_2 = transaction_Reference_2;
	}
	public String getTransaction_Installment_2() {
		return Transaction_Installment_2;
	}
	public void setTransaction_Installment_2(String transaction_Installment_2) {
		Transaction_Installment_2 = transaction_Installment_2;
	}
	public String getTransaction_Reference_Date_2() {
		return Transaction_Reference_Date_2;
	}
	public void setTransaction_Reference_Date_2(String transaction_Reference_Date_2) {
		Transaction_Reference_Date_2 = transaction_Reference_Date_2;
	}
	public String getTransaction_Currency_2() {
		return Transaction_Currency_2;
	}
	public void setTransaction_Currency_2(String transaction_Currency_2) {
		Transaction_Currency_2 = transaction_Currency_2;
	}
	public String getCross_Currency_Rate_2() {
		return Cross_Currency_Rate_2;
	}
	public void setCross_Currency_Rate_2(String cross_Currency_Rate_2) {
		Cross_Currency_Rate_2 = cross_Currency_Rate_2;
	}
	public String getApplied_Amount_2() {
		return Applied_Amount_2;
	}
	public void setApplied_Amount_2(String applied_Amount_2) {
		Applied_Amount_2 = applied_Amount_2;
	}
	public String getAllocated_Receipt_Amount_2() {
		return Allocated_Receipt_Amount_2;
	}
	public void setAllocated_Receipt_Amount_2(String allocated_Receipt_Amount_2) {
		Allocated_Receipt_Amount_2 = allocated_Receipt_Amount_2;
	}
	public String getCustomer_Reference_2() {
		return Customer_Reference_2;
	}
	public void setCustomer_Reference_2(String customer_Reference_2) {
		Customer_Reference_2 = customer_Reference_2;
	}
	public String getTransaction_Reference_3() {
		return Transaction_Reference_3;
	}
	public void setTransaction_Reference_3(String transaction_Reference_3) {
		Transaction_Reference_3 = transaction_Reference_3;
	}
	public String getTransaction_Installment_3() {
		return Transaction_Installment_3;
	}
	public void setTransaction_Installment_3(String transaction_Installment_3) {
		Transaction_Installment_3 = transaction_Installment_3;
	}
	public String getTransaction_Reference_Date_3() {
		return Transaction_Reference_Date_3;
	}
	public void setTransaction_Reference_Date_3(String transaction_Reference_Date_3) {
		Transaction_Reference_Date_3 = transaction_Reference_Date_3;
	}
	public String getTransaction_Currency_3() {
		return Transaction_Currency_3;
	}
	public void setTransaction_Currency_3(String transaction_Currency_3) {
		Transaction_Currency_3 = transaction_Currency_3;
	}
	public String getCross_Currency_Rate_3() {
		return Cross_Currency_Rate_3;
	}
	public void setCross_Currency_Rate_3(String cross_Currency_Rate_3) {
		Cross_Currency_Rate_3 = cross_Currency_Rate_3;
	}
	public String getApplied_Amount_3() {
		return Applied_Amount_3;
	}
	public void setApplied_Amount_3(String applied_Amount_3) {
		Applied_Amount_3 = applied_Amount_3;
	}
	public String getAllocated_Receipt_Amount_3() {
		return Allocated_Receipt_Amount_3;
	}
	public void setAllocated_Receipt_Amount_3(String allocated_Receipt_Amount_3) {
		Allocated_Receipt_Amount_3 = allocated_Receipt_Amount_3;
	}
	public String getCustomer_Reference_3() {
		return Customer_Reference_3;
	}
	public void setCustomer_Reference_3(String customer_Reference_3) {
		Customer_Reference_3 = customer_Reference_3;
	}
	public String getTransaction_Reference_4() {
		return Transaction_Reference_4;
	}
	public void setTransaction_Reference_4(String transaction_Reference_4) {
		Transaction_Reference_4 = transaction_Reference_4;
	}
	public String getTransaction_Installment_4() {
		return Transaction_Installment_4;
	}
	public void setTransaction_Installment_4(String transaction_Installment_4) {
		Transaction_Installment_4 = transaction_Installment_4;
	}
	public String getTransaction_Reference_Date_4() {
		return Transaction_Reference_Date_4;
	}
	public void setTransaction_Reference_Date_4(String transaction_Reference_Date_4) {
		Transaction_Reference_Date_4 = transaction_Reference_Date_4;
	}
	public String getTransaction_Currency_4() {
		return Transaction_Currency_4;
	}
	public void setTransaction_Currency_4(String transaction_Currency_4) {
		Transaction_Currency_4 = transaction_Currency_4;
	}
	public String getCross_Currency_Rate_4() {
		return Cross_Currency_Rate_4;
	}
	public void setCross_Currency_Rate_4(String cross_Currency_Rate_4) {
		Cross_Currency_Rate_4 = cross_Currency_Rate_4;
	}
	public String getApplied_Amount_4() {
		return Applied_Amount_4;
	}
	public void setApplied_Amount_4(String applied_Amount_4) {
		Applied_Amount_4 = applied_Amount_4;
	}
	public String getAllocated_Receipt_Amount_4() {
		return Allocated_Receipt_Amount_4;
	}
	public void setAllocated_Receipt_Amount_4(String allocated_Receipt_Amount_4) {
		Allocated_Receipt_Amount_4 = allocated_Receipt_Amount_4;
	}
	public String getCustomer_Reference_4() {
		return Customer_Reference_4;
	}
	public void setCustomer_Reference_4(String customer_Reference_4) {
		Customer_Reference_4 = customer_Reference_4;
	}
	public String getTransaction_Reference_5() {
		return Transaction_Reference_5;
	}
	public void setTransaction_Reference_5(String transaction_Reference_5) {
		Transaction_Reference_5 = transaction_Reference_5;
	}
	public String getTransaction_Installment_5() {
		return Transaction_Installment_5;
	}
	public void setTransaction_Installment_5(String transaction_Installment_5) {
		Transaction_Installment_5 = transaction_Installment_5;
	}
	public String getTransaction_Reference_Date_5() {
		return Transaction_Reference_Date_5;
	}
	public void setTransaction_Reference_Date_5(String transaction_Reference_Date_5) {
		Transaction_Reference_Date_5 = transaction_Reference_Date_5;
	}
	public String getTransaction_Currency_5() {
		return Transaction_Currency_5;
	}
	public void setTransaction_Currency_5(String transaction_Currency_5) {
		Transaction_Currency_5 = transaction_Currency_5;
	}
	public String getCross_Currency_Rate_5() {
		return Cross_Currency_Rate_5;
	}
	public void setCross_Currency_Rate_5(String cross_Currency_Rate_5) {
		Cross_Currency_Rate_5 = cross_Currency_Rate_5;
	}
	public String getApplied_Amount_5() {
		return Applied_Amount_5;
	}
	public void setApplied_Amount_5(String applied_Amount_5) {
		Applied_Amount_5 = applied_Amount_5;
	}
	public String getAllocated_Receipt_Amount_5() {
		return Allocated_Receipt_Amount_5;
	}
	public void setAllocated_Receipt_Amount_5(String allocated_Receipt_Amount_5) {
		Allocated_Receipt_Amount_5 = allocated_Receipt_Amount_5;
	}
	public String getCustomer_Reference_5() {
		return Customer_Reference_5;
	}
	public void setCustomer_Reference_5(String customer_Reference_5) {
		Customer_Reference_5 = customer_Reference_5;
	}
	public String getTransaction_Reference_6() {
		return Transaction_Reference_6;
	}
	public void setTransaction_Reference_6(String transaction_Reference_6) {
		Transaction_Reference_6 = transaction_Reference_6;
	}
	public String getTransaction_Installment_6() {
		return Transaction_Installment_6;
	}
	public void setTransaction_Installment_6(String transaction_Installment_6) {
		Transaction_Installment_6 = transaction_Installment_6;
	}
	public String getTransaction_Reference_Date_6() {
		return Transaction_Reference_Date_6;
	}
	public void setTransaction_Reference_Date_6(String transaction_Reference_Date_6) {
		Transaction_Reference_Date_6 = transaction_Reference_Date_6;
	}
	public String getTransaction_Currency_6() {
		return Transaction_Currency_6;
	}
	public void setTransaction_Currency_6(String transaction_Currency_6) {
		Transaction_Currency_6 = transaction_Currency_6;
	}
	public String getCross_Currency_Rate_6() {
		return Cross_Currency_Rate_6;
	}
	public void setCross_Currency_Rate_6(String cross_Currency_Rate_6) {
		Cross_Currency_Rate_6 = cross_Currency_Rate_6;
	}
	public String getApplied_Amount_6() {
		return Applied_Amount_6;
	}
	public void setApplied_Amount_6(String applied_Amount_6) {
		Applied_Amount_6 = applied_Amount_6;
	}
	public String getAllocated_Receipt_Amount_6() {
		return Allocated_Receipt_Amount_6;
	}
	public void setAllocated_Receipt_Amount_6(String allocated_Receipt_Amount_6) {
		Allocated_Receipt_Amount_6 = allocated_Receipt_Amount_6;
	}
	public String getCustomer_Reference_6() {
		return Customer_Reference_6;
	}
	public void setCustomer_Reference_6(String customer_Reference_6) {
		Customer_Reference_6 = customer_Reference_6;
	}
	public String getTransaction_Reference_7() {
		return Transaction_Reference_7;
	}
	public void setTransaction_Reference_7(String transaction_Reference_7) {
		Transaction_Reference_7 = transaction_Reference_7;
	}
	public String getTransaction_Installment_7() {
		return Transaction_Installment_7;
	}
	public void setTransaction_Installment_7(String transaction_Installment_7) {
		Transaction_Installment_7 = transaction_Installment_7;
	}
	public String getTransaction_Reference_Date_7() {
		return Transaction_Reference_Date_7;
	}
	public void setTransaction_Reference_Date_7(String transaction_Reference_Date_7) {
		Transaction_Reference_Date_7 = transaction_Reference_Date_7;
	}
	public String getTransaction_Currency_7() {
		return Transaction_Currency_7;
	}
	public void setTransaction_Currency_7(String transaction_Currency_7) {
		Transaction_Currency_7 = transaction_Currency_7;
	}
	public String getCross_Currency_Rate_7() {
		return Cross_Currency_Rate_7;
	}
	public void setCross_Currency_Rate_7(String cross_Currency_Rate_7) {
		Cross_Currency_Rate_7 = cross_Currency_Rate_7;
	}
	public String getApplied_Amount_7() {
		return Applied_Amount_7;
	}
	public void setApplied_Amount_7(String applied_Amount_7) {
		Applied_Amount_7 = applied_Amount_7;
	}
	public String getAllocated_Receipt_Amount_7() {
		return Allocated_Receipt_Amount_7;
	}
	public void setAllocated_Receipt_Amount_7(String allocated_Receipt_Amount_7) {
		Allocated_Receipt_Amount_7 = allocated_Receipt_Amount_7;
	}
	public String getCustomer_Reference_7() {
		return Customer_Reference_7;
	}
	public void setCustomer_Reference_7(String customer_Reference_7) {
		Customer_Reference_7 = customer_Reference_7;
	}
	public String getTransaction_Reference_8() {
		return Transaction_Reference_8;
	}
	public void setTransaction_Reference_8(String transaction_Reference_8) {
		Transaction_Reference_8 = transaction_Reference_8;
	}
	public String getTransaction_Installment_8() {
		return Transaction_Installment_8;
	}
	public void setTransaction_Installment_8(String transaction_Installment_8) {
		Transaction_Installment_8 = transaction_Installment_8;
	}
	public String getTransaction_Reference_Date_8() {
		return Transaction_Reference_Date_8;
	}
	public void setTransaction_Reference_Date_8(String transaction_Reference_Date_8) {
		Transaction_Reference_Date_8 = transaction_Reference_Date_8;
	}
	public String getTransaction_Currency_8() {
		return Transaction_Currency_8;
	}
	public void setTransaction_Currency_8(String transaction_Currency_8) {
		Transaction_Currency_8 = transaction_Currency_8;
	}
	public String getCross_Currency_Rate_8() {
		return Cross_Currency_Rate_8;
	}
	public void setCross_Currency_Rate_8(String cross_Currency_Rate_8) {
		Cross_Currency_Rate_8 = cross_Currency_Rate_8;
	}
	public String getApplied_Amount_8() {
		return Applied_Amount_8;
	}
	public void setApplied_Amount_8(String applied_Amount_8) {
		Applied_Amount_8 = applied_Amount_8;
	}
	public String getAllocated_Receipt_Amount_8() {
		return Allocated_Receipt_Amount_8;
	}
	public void setAllocated_Receipt_Amount_8(String allocated_Receipt_Amount_8) {
		Allocated_Receipt_Amount_8 = allocated_Receipt_Amount_8;
	}
	public String getCustomer_Reference_8() {
		return Customer_Reference_8;
	}
	public void setCustomer_Reference_8(String customer_Reference_8) {
		Customer_Reference_8 = customer_Reference_8;
	}
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		Comments = comments;
	}
	public String getPayment_Interface_Flexfield_Segment_1() {
		return Payment_Interface_Flexfield_Segment_1;
	}
	public void setPayment_Interface_Flexfield_Segment_1(String payment_Interface_Flexfield_Segment_1) {
		Payment_Interface_Flexfield_Segment_1 = payment_Interface_Flexfield_Segment_1;
	}
	public String getPayment_Interface_Flexfield_Segment_2() {
		return Payment_Interface_Flexfield_Segment_2;
	}
	public void setPayment_Interface_Flexfield_Segment_2(String payment_Interface_Flexfield_Segment_2) {
		Payment_Interface_Flexfield_Segment_2 = payment_Interface_Flexfield_Segment_2;
	}
	public String getPayment_Interface_Flexfield_Segment_3() {
		return Payment_Interface_Flexfield_Segment_3;
	}
	public void setPayment_Interface_Flexfield_Segment_3(String payment_Interface_Flexfield_Segment_3) {
		Payment_Interface_Flexfield_Segment_3 = payment_Interface_Flexfield_Segment_3;
	}
	public String getPayment_Interface_Flexfield_Segment_4() {
		return Payment_Interface_Flexfield_Segment_4;
	}
	public void setPayment_Interface_Flexfield_Segment_4(String payment_Interface_Flexfield_Segment_4) {
		Payment_Interface_Flexfield_Segment_4 = payment_Interface_Flexfield_Segment_4;
	}
	public String getPayment_Interface_Flexfield_Segment_5() {
		return Payment_Interface_Flexfield_Segment_5;
	}
	public void setPayment_Interface_Flexfield_Segment_5(String payment_Interface_Flexfield_Segment_5) {
		Payment_Interface_Flexfield_Segment_5 = payment_Interface_Flexfield_Segment_5;
	}
	public String getPayment_Interface_Flexfield_Segment_6() {
		return Payment_Interface_Flexfield_Segment_6;
	}
	public void setPayment_Interface_Flexfield_Segment_6(String payment_Interface_Flexfield_Segment_6) {
		Payment_Interface_Flexfield_Segment_6 = payment_Interface_Flexfield_Segment_6;
	}
	public String getPayment_Interface_Flexfield_Segment_7() {
		return Payment_Interface_Flexfield_Segment_7;
	}
	public void setPayment_Interface_Flexfield_Segment_7(String payment_Interface_Flexfield_Segment_7) {
		Payment_Interface_Flexfield_Segment_7 = payment_Interface_Flexfield_Segment_7;
	}
	public String getPayment_Interface_Flexfield_Segment_8() {
		return Payment_Interface_Flexfield_Segment_8;
	}
	public void setPayment_Interface_Flexfield_Segment_8(String payment_Interface_Flexfield_Segment_8) {
		Payment_Interface_Flexfield_Segment_8 = payment_Interface_Flexfield_Segment_8;
	}
	public String getPayment_Interface_Flexfield_Segment_9() {
		return Payment_Interface_Flexfield_Segment_9;
	}
	public void setPayment_Interface_Flexfield_Segment_9(String payment_Interface_Flexfield_Segment_9) {
		Payment_Interface_Flexfield_Segment_9 = payment_Interface_Flexfield_Segment_9;
	}
	public String getPayment_Interface_Flexfield_Segment_10() {
		return Payment_Interface_Flexfield_Segment_10;
	}
	public void setPayment_Interface_Flexfield_Segment_10(String payment_Interface_Flexfield_Segment_10) {
		Payment_Interface_Flexfield_Segment_10 = payment_Interface_Flexfield_Segment_10;
	}
	public String getPayment_Interface_Flexfield_Segment_11() {
		return Payment_Interface_Flexfield_Segment_11;
	}
	public void setPayment_Interface_Flexfield_Segment_11(String payment_Interface_Flexfield_Segment_11) {
		Payment_Interface_Flexfield_Segment_11 = payment_Interface_Flexfield_Segment_11;
	}
	public String getPayment_Interface_Flexfield_Segment_12() {
		return Payment_Interface_Flexfield_Segment_12;
	}
	public void setPayment_Interface_Flexfield_Segment_12(String payment_Interface_Flexfield_Segment_12) {
		Payment_Interface_Flexfield_Segment_12 = payment_Interface_Flexfield_Segment_12;
	}
	public String getPayment_Interface_Flexfield_Segment_13() {
		return Payment_Interface_Flexfield_Segment_13;
	}
	public void setPayment_Interface_Flexfield_Segment_13(String payment_Interface_Flexfield_Segment_13) {
		Payment_Interface_Flexfield_Segment_13 = payment_Interface_Flexfield_Segment_13;
	}
	public String getPayment_Interface_Flexfield_Segment_14() {
		return Payment_Interface_Flexfield_Segment_14;
	}
	public void setPayment_Interface_Flexfield_Segment_14(String payment_Interface_Flexfield_Segment_14) {
		Payment_Interface_Flexfield_Segment_14 = payment_Interface_Flexfield_Segment_14;
	}
	public String getPayment_Interface_Flexfield_Segment_15() {
		return Payment_Interface_Flexfield_Segment_15;
	}
	public void setPayment_Interface_Flexfield_Segment_15(String payment_Interface_Flexfield_Segment_15) {
		Payment_Interface_Flexfield_Segment_15 = payment_Interface_Flexfield_Segment_15;
	}
		
}

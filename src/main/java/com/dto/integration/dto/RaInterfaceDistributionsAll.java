package com.dto.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class RaInterfaceDistributionsAll {
	
	private String Business_Unit_Identifier = "";
	private String Account_Class = "";
	private String Amount = "";
	private String Percent = "";
	private String Accounted_Amount_in_Ledger_Currency = "";
	private String Line_Transactions_Flexfield_Context = "";
	private String Line_Transactions_Flexfield_Segment_1 = "";
	private String Line_Transactions_Flexfield_Segment_2 = "";
	private String Line_Transactions_Flexfield_Segment_3 = "";
	private String Line_Transactions_Flexfield_Segment_4 = "";
	private String Line_Transactions_Flexfield_Segment_5 = "";
	private String Line_Transactions_Flexfield_Segment_6 = "";
	private String Line_Transactions_Flexfield_Segment_7 = "";
	private String Line_Transactions_Flexfield_Segment_8 = "";
	private String Line_Transactions_Flexfield_Segment_9 = "";
	private String Line_Transactions_Flexfield_Segment_10 = "";
	private String Line_Transactions_Flexfield_Segment_11 = "";
	private String Line_Transactions_Flexfield_Segment_12 = "";
	private String Line_Transactions_Flexfield_Segment_13 = "";
	private String Line_Transactions_Flexfield_Segment_14 = "";
	private String Line_Transactions_Flexfield_Segment_15 = "";
	private String Accounting_Flexfield_Segment_1 = "";
	private String Accounting_Flexfield_Segment_2 = "";
	private String Accounting_Flexfield_Segment_3 = "";
	private String Accounting_Flexfield_Segment_4 = "";
	private String Accounting_Flexfield_Segment_5 = "";
	private String Accounting_Flexfield_Segment_6 = "";
	private String Accounting_Flexfield_Segment_7 = "";
	private String Accounting_Flexfield_Segment_8 = "";
	private String Accounting_Flexfield_Segment_9 = "";
	private String Accounting_Flexfield_Segment_10 = "";
	private String Accounting_Flexfield_Segment_11 = "";
	private String Accounting_Flexfield_Segment_12 = "";
	private String Accounting_Flexfield_Segment_13 = "";
	private String Accounting_Flexfield_Segment_14 = "";
	private String Accounting_Flexfield_Segment_15 = "";
	private String Accounting_Flexfield_Segment_16 = "";
	private String Accounting_Flexfield_Segment_17 = "";
	private String Accounting_Flexfield_Segment_18 = "";
	private String Accounting_Flexfield_Segment_19 = "";
	private String Accounting_Flexfield_Segment_20 = "";
	private String Accounting_Flexfield_Segment_21 = "";
	private String Accounting_Flexfield_Segment_22 = "";
	private String Accounting_Flexfield_Segment_23 = "";
	private String Accounting_Flexfield_Segment_24 = "";
	private String Accounting_Flexfield_Segment_25 = "";
	private String Accounting_Flexfield_Segment_26 = "";
	private String Accounting_Flexfield_Segment_27 = "";
	private String Accounting_Flexfield_Segment_28 = "";
	private String Accounting_Flexfield_Segment_29 = "";
	private String Accounting_Flexfield_Segment_30 = "";
	private String Comments = "";
	private String Interim_Tax_Segment_1 = "";
	private String Interim_Tax_Segment_2 = "";
	private String Interim_Tax_Segment_3 = "";
	private String Interim_Tax_Segment_4 = "";
	private String Interim_Tax_Segment_5 = "";
	private String Interim_Tax_Segment_6 = "";
	private String Interim_Tax_Segment_7 = "";
	private String Interim_Tax_Segment_8 = "";
	private String Interim_Tax_Segment_9 = "";
	private String Interim_Tax_Segment_10 = "";
	private String Interim_Tax_Segment_11 = "";
	private String Interim_Tax_Segment_12 = "";
	private String Interim_Tax_Segment_13 = "";
	private String Interim_Tax_Segment_14 = "";
	private String Interim_Tax_Segment_15 = "";
	private String Interim_Tax_Segment_16 = "";
	private String Interim_Tax_Segment_17 = "";
	private String Interim_Tax_Segment_18 = "";
	private String Interim_Tax_Segment_19 = "";
	private String Interim_Tax_Segment_20 = "";
	private String Interim_Tax_Segment_21 = "";
	private String Interim_Tax_Segment_22 = "";
	private String Interim_Tax_Segment_23 = "";
	private String Interim_Tax_Segment_24 = "";
	private String Interim_Tax_Segment_25 = "";
	private String Interim_Tax_Segment_26 = "";
	private String Interim_Tax_Segment_27 = "";
	private String Interim_Tax_Segment_28 = "";
	private String Interim_Tax_Segment_29 = "";
	private String Interim_Tax_Segment_30 = "";
	private String Interface_Distributions_Flexfield_Context = "";
	private String Interface_Distributions_Flexfield__Segment_1 = "";
	private String Interface_Distributions_Flexfield__Segment_2 = "";
	private String Interface_Distributions_Flexfield__Segment_3 = "";
	private String Interface_Distributions_Flexfield_Segment_4 = "";
	private String Interface_Distributions_Flexfield_Segment_5 = "";
	private String Interface_Distributions_Flexfield_Segment_6 = "";
	private String Interface_Distributions_Flexfield_Segment_7 = "";
	private String Interface_Distributions_Flexfield_Segment_8 = "";
	private String Interface_Distributions_Flexfield_Segment_9 = "";
	private String Interface_Distributions_Flexfield_Segment_10 = "";
	private String Interface_Distributions_Flexfield_Segment_11 = "";
	private String Interface_Distributions_Flexfield_Segment_12 = "";
	private String Interface_Distributions_Flexfield_Segment_13 = "";
	private String Interface_Distributions_Flexfield_Segment_14 = "";
	private String Interface_Distributions_Flexfield_Segment_15 = "";
	private String Business_Unit_Name = "";
	private String End = "";
	public String getBusiness_Unit_Identifier() {
		return Business_Unit_Identifier;
	}
	public void setBusiness_Unit_Identifier(String business_Unit_Identifier) {
		Business_Unit_Identifier = business_Unit_Identifier;
	}
	public String getAccount_Class() {
		return Account_Class;
	}
	public void setAccount_Class(String account_Class) {
		Account_Class = account_Class;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getPercent() {
		return Percent;
	}
	public void setPercent(String percent) {
		Percent = percent;
	}
	public String getAccounted_Amount_in_Ledger_Currency() {
		return Accounted_Amount_in_Ledger_Currency;
	}
	public void setAccounted_Amount_in_Ledger_Currency(String accounted_Amount_in_Ledger_Currency) {
		Accounted_Amount_in_Ledger_Currency = accounted_Amount_in_Ledger_Currency;
	}
	public String getLine_Transactions_Flexfield_Context() {
		return Line_Transactions_Flexfield_Context;
	}
	public void setLine_Transactions_Flexfield_Context(String line_Transactions_Flexfield_Context) {
		Line_Transactions_Flexfield_Context = line_Transactions_Flexfield_Context;
	}
	public String getLine_Transactions_Flexfield_Segment_1() {
		return Line_Transactions_Flexfield_Segment_1;
	}
	public void setLine_Transactions_Flexfield_Segment_1(String line_Transactions_Flexfield_Segment_1) {
		Line_Transactions_Flexfield_Segment_1 = line_Transactions_Flexfield_Segment_1;
	}
	public String getLine_Transactions_Flexfield_Segment_2() {
		return Line_Transactions_Flexfield_Segment_2;
	}
	public void setLine_Transactions_Flexfield_Segment_2(String line_Transactions_Flexfield_Segment_2) {
		Line_Transactions_Flexfield_Segment_2 = line_Transactions_Flexfield_Segment_2;
	}
	public String getLine_Transactions_Flexfield_Segment_3() {
		return Line_Transactions_Flexfield_Segment_3;
	}
	public void setLine_Transactions_Flexfield_Segment_3(String line_Transactions_Flexfield_Segment_3) {
		Line_Transactions_Flexfield_Segment_3 = line_Transactions_Flexfield_Segment_3;
	}
	public String getLine_Transactions_Flexfield_Segment_4() {
		return Line_Transactions_Flexfield_Segment_4;
	}
	public void setLine_Transactions_Flexfield_Segment_4(String line_Transactions_Flexfield_Segment_4) {
		Line_Transactions_Flexfield_Segment_4 = line_Transactions_Flexfield_Segment_4;
	}
	public String getLine_Transactions_Flexfield_Segment_5() {
		return Line_Transactions_Flexfield_Segment_5;
	}
	public void setLine_Transactions_Flexfield_Segment_5(String line_Transactions_Flexfield_Segment_5) {
		Line_Transactions_Flexfield_Segment_5 = line_Transactions_Flexfield_Segment_5;
	}
	public String getLine_Transactions_Flexfield_Segment_6() {
		return Line_Transactions_Flexfield_Segment_6;
	}
	public void setLine_Transactions_Flexfield_Segment_6(String line_Transactions_Flexfield_Segment_6) {
		Line_Transactions_Flexfield_Segment_6 = line_Transactions_Flexfield_Segment_6;
	}
	public String getLine_Transactions_Flexfield_Segment_7() {
		return Line_Transactions_Flexfield_Segment_7;
	}
	public void setLine_Transactions_Flexfield_Segment_7(String line_Transactions_Flexfield_Segment_7) {
		Line_Transactions_Flexfield_Segment_7 = line_Transactions_Flexfield_Segment_7;
	}
	public String getLine_Transactions_Flexfield_Segment_8() {
		return Line_Transactions_Flexfield_Segment_8;
	}
	public void setLine_Transactions_Flexfield_Segment_8(String line_Transactions_Flexfield_Segment_8) {
		Line_Transactions_Flexfield_Segment_8 = line_Transactions_Flexfield_Segment_8;
	}
	public String getLine_Transactions_Flexfield_Segment_9() {
		return Line_Transactions_Flexfield_Segment_9;
	}
	public void setLine_Transactions_Flexfield_Segment_9(String line_Transactions_Flexfield_Segment_9) {
		Line_Transactions_Flexfield_Segment_9 = line_Transactions_Flexfield_Segment_9;
	}
	public String getLine_Transactions_Flexfield_Segment_10() {
		return Line_Transactions_Flexfield_Segment_10;
	}
	public void setLine_Transactions_Flexfield_Segment_10(String line_Transactions_Flexfield_Segment_10) {
		Line_Transactions_Flexfield_Segment_10 = line_Transactions_Flexfield_Segment_10;
	}
	public String getLine_Transactions_Flexfield_Segment_11() {
		return Line_Transactions_Flexfield_Segment_11;
	}
	public void setLine_Transactions_Flexfield_Segment_11(String line_Transactions_Flexfield_Segment_11) {
		Line_Transactions_Flexfield_Segment_11 = line_Transactions_Flexfield_Segment_11;
	}
	public String getLine_Transactions_Flexfield_Segment_12() {
		return Line_Transactions_Flexfield_Segment_12;
	}
	public void setLine_Transactions_Flexfield_Segment_12(String line_Transactions_Flexfield_Segment_12) {
		Line_Transactions_Flexfield_Segment_12 = line_Transactions_Flexfield_Segment_12;
	}
	public String getLine_Transactions_Flexfield_Segment_13() {
		return Line_Transactions_Flexfield_Segment_13;
	}
	public void setLine_Transactions_Flexfield_Segment_13(String line_Transactions_Flexfield_Segment_13) {
		Line_Transactions_Flexfield_Segment_13 = line_Transactions_Flexfield_Segment_13;
	}
	public String getLine_Transactions_Flexfield_Segment_14() {
		return Line_Transactions_Flexfield_Segment_14;
	}
	public void setLine_Transactions_Flexfield_Segment_14(String line_Transactions_Flexfield_Segment_14) {
		Line_Transactions_Flexfield_Segment_14 = line_Transactions_Flexfield_Segment_14;
	}
	public String getLine_Transactions_Flexfield_Segment_15() {
		return Line_Transactions_Flexfield_Segment_15;
	}
	public void setLine_Transactions_Flexfield_Segment_15(String line_Transactions_Flexfield_Segment_15) {
		Line_Transactions_Flexfield_Segment_15 = line_Transactions_Flexfield_Segment_15;
	}
	public String getAccounting_Flexfield_Segment_1() {
		return Accounting_Flexfield_Segment_1;
	}
	public void setAccounting_Flexfield_Segment_1(String accounting_Flexfield_Segment_1) {
		Accounting_Flexfield_Segment_1 = accounting_Flexfield_Segment_1;
	}
	public String getAccounting_Flexfield_Segment_2() {
		return Accounting_Flexfield_Segment_2;
	}
	public void setAccounting_Flexfield_Segment_2(String accounting_Flexfield_Segment_2) {
		Accounting_Flexfield_Segment_2 = accounting_Flexfield_Segment_2;
	}
	public String getAccounting_Flexfield_Segment_3() {
		return Accounting_Flexfield_Segment_3;
	}
	public void setAccounting_Flexfield_Segment_3(String accounting_Flexfield_Segment_3) {
		Accounting_Flexfield_Segment_3 = accounting_Flexfield_Segment_3;
	}
	public String getAccounting_Flexfield_Segment_4() {
		return Accounting_Flexfield_Segment_4;
	}
	public void setAccounting_Flexfield_Segment_4(String accounting_Flexfield_Segment_4) {
		Accounting_Flexfield_Segment_4 = accounting_Flexfield_Segment_4;
	}
	public String getAccounting_Flexfield_Segment_5() {
		return Accounting_Flexfield_Segment_5;
	}
	public void setAccounting_Flexfield_Segment_5(String accounting_Flexfield_Segment_5) {
		Accounting_Flexfield_Segment_5 = accounting_Flexfield_Segment_5;
	}
	public String getAccounting_Flexfield_Segment_6() {
		return Accounting_Flexfield_Segment_6;
	}
	public void setAccounting_Flexfield_Segment_6(String accounting_Flexfield_Segment_6) {
		Accounting_Flexfield_Segment_6 = accounting_Flexfield_Segment_6;
	}
	public String getAccounting_Flexfield_Segment_7() {
		return Accounting_Flexfield_Segment_7;
	}
	public void setAccounting_Flexfield_Segment_7(String accounting_Flexfield_Segment_7) {
		Accounting_Flexfield_Segment_7 = accounting_Flexfield_Segment_7;
	}
	public String getAccounting_Flexfield_Segment_8() {
		return Accounting_Flexfield_Segment_8;
	}
	public void setAccounting_Flexfield_Segment_8(String accounting_Flexfield_Segment_8) {
		Accounting_Flexfield_Segment_8 = accounting_Flexfield_Segment_8;
	}
	public String getAccounting_Flexfield_Segment_9() {
		return Accounting_Flexfield_Segment_9;
	}
	public void setAccounting_Flexfield_Segment_9(String accounting_Flexfield_Segment_9) {
		Accounting_Flexfield_Segment_9 = accounting_Flexfield_Segment_9;
	}
	public String getAccounting_Flexfield_Segment_10() {
		return Accounting_Flexfield_Segment_10;
	}
	public void setAccounting_Flexfield_Segment_10(String accounting_Flexfield_Segment_10) {
		Accounting_Flexfield_Segment_10 = accounting_Flexfield_Segment_10;
	}
	public String getAccounting_Flexfield_Segment_11() {
		return Accounting_Flexfield_Segment_11;
	}
	public void setAccounting_Flexfield_Segment_11(String accounting_Flexfield_Segment_11) {
		Accounting_Flexfield_Segment_11 = accounting_Flexfield_Segment_11;
	}
	public String getAccounting_Flexfield_Segment_12() {
		return Accounting_Flexfield_Segment_12;
	}
	public void setAccounting_Flexfield_Segment_12(String accounting_Flexfield_Segment_12) {
		Accounting_Flexfield_Segment_12 = accounting_Flexfield_Segment_12;
	}
	public String getAccounting_Flexfield_Segment_13() {
		return Accounting_Flexfield_Segment_13;
	}
	public void setAccounting_Flexfield_Segment_13(String accounting_Flexfield_Segment_13) {
		Accounting_Flexfield_Segment_13 = accounting_Flexfield_Segment_13;
	}
	public String getAccounting_Flexfield_Segment_14() {
		return Accounting_Flexfield_Segment_14;
	}
	public void setAccounting_Flexfield_Segment_14(String accounting_Flexfield_Segment_14) {
		Accounting_Flexfield_Segment_14 = accounting_Flexfield_Segment_14;
	}
	public String getAccounting_Flexfield_Segment_15() {
		return Accounting_Flexfield_Segment_15;
	}
	public void setAccounting_Flexfield_Segment_15(String accounting_Flexfield_Segment_15) {
		Accounting_Flexfield_Segment_15 = accounting_Flexfield_Segment_15;
	}
	public String getAccounting_Flexfield_Segment_16() {
		return Accounting_Flexfield_Segment_16;
	}
	public void setAccounting_Flexfield_Segment_16(String accounting_Flexfield_Segment_16) {
		Accounting_Flexfield_Segment_16 = accounting_Flexfield_Segment_16;
	}
	public String getAccounting_Flexfield_Segment_17() {
		return Accounting_Flexfield_Segment_17;
	}
	public void setAccounting_Flexfield_Segment_17(String accounting_Flexfield_Segment_17) {
		Accounting_Flexfield_Segment_17 = accounting_Flexfield_Segment_17;
	}
	public String getAccounting_Flexfield_Segment_18() {
		return Accounting_Flexfield_Segment_18;
	}
	public void setAccounting_Flexfield_Segment_18(String accounting_Flexfield_Segment_18) {
		Accounting_Flexfield_Segment_18 = accounting_Flexfield_Segment_18;
	}
	public String getAccounting_Flexfield_Segment_19() {
		return Accounting_Flexfield_Segment_19;
	}
	public void setAccounting_Flexfield_Segment_19(String accounting_Flexfield_Segment_19) {
		Accounting_Flexfield_Segment_19 = accounting_Flexfield_Segment_19;
	}
	public String getAccounting_Flexfield_Segment_20() {
		return Accounting_Flexfield_Segment_20;
	}
	public void setAccounting_Flexfield_Segment_20(String accounting_Flexfield_Segment_20) {
		Accounting_Flexfield_Segment_20 = accounting_Flexfield_Segment_20;
	}
	public String getAccounting_Flexfield_Segment_21() {
		return Accounting_Flexfield_Segment_21;
	}
	public void setAccounting_Flexfield_Segment_21(String accounting_Flexfield_Segment_21) {
		Accounting_Flexfield_Segment_21 = accounting_Flexfield_Segment_21;
	}
	public String getAccounting_Flexfield_Segment_22() {
		return Accounting_Flexfield_Segment_22;
	}
	public void setAccounting_Flexfield_Segment_22(String accounting_Flexfield_Segment_22) {
		Accounting_Flexfield_Segment_22 = accounting_Flexfield_Segment_22;
	}
	public String getAccounting_Flexfield_Segment_23() {
		return Accounting_Flexfield_Segment_23;
	}
	public void setAccounting_Flexfield_Segment_23(String accounting_Flexfield_Segment_23) {
		Accounting_Flexfield_Segment_23 = accounting_Flexfield_Segment_23;
	}
	public String getAccounting_Flexfield_Segment_24() {
		return Accounting_Flexfield_Segment_24;
	}
	public void setAccounting_Flexfield_Segment_24(String accounting_Flexfield_Segment_24) {
		Accounting_Flexfield_Segment_24 = accounting_Flexfield_Segment_24;
	}
	public String getAccounting_Flexfield_Segment_25() {
		return Accounting_Flexfield_Segment_25;
	}
	public void setAccounting_Flexfield_Segment_25(String accounting_Flexfield_Segment_25) {
		Accounting_Flexfield_Segment_25 = accounting_Flexfield_Segment_25;
	}
	public String getAccounting_Flexfield_Segment_26() {
		return Accounting_Flexfield_Segment_26;
	}
	public void setAccounting_Flexfield_Segment_26(String accounting_Flexfield_Segment_26) {
		Accounting_Flexfield_Segment_26 = accounting_Flexfield_Segment_26;
	}
	public String getAccounting_Flexfield_Segment_27() {
		return Accounting_Flexfield_Segment_27;
	}
	public void setAccounting_Flexfield_Segment_27(String accounting_Flexfield_Segment_27) {
		Accounting_Flexfield_Segment_27 = accounting_Flexfield_Segment_27;
	}
	public String getAccounting_Flexfield_Segment_28() {
		return Accounting_Flexfield_Segment_28;
	}
	public void setAccounting_Flexfield_Segment_28(String accounting_Flexfield_Segment_28) {
		Accounting_Flexfield_Segment_28 = accounting_Flexfield_Segment_28;
	}
	public String getAccounting_Flexfield_Segment_29() {
		return Accounting_Flexfield_Segment_29;
	}
	public void setAccounting_Flexfield_Segment_29(String accounting_Flexfield_Segment_29) {
		Accounting_Flexfield_Segment_29 = accounting_Flexfield_Segment_29;
	}
	public String getAccounting_Flexfield_Segment_30() {
		return Accounting_Flexfield_Segment_30;
	}
	public void setAccounting_Flexfield_Segment_30(String accounting_Flexfield_Segment_30) {
		Accounting_Flexfield_Segment_30 = accounting_Flexfield_Segment_30;
	}
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		Comments = comments;
	}
	public String getInterim_Tax_Segment_1() {
		return Interim_Tax_Segment_1;
	}
	public void setInterim_Tax_Segment_1(String interim_Tax_Segment_1) {
		Interim_Tax_Segment_1 = interim_Tax_Segment_1;
	}
	public String getInterim_Tax_Segment_2() {
		return Interim_Tax_Segment_2;
	}
	public void setInterim_Tax_Segment_2(String interim_Tax_Segment_2) {
		Interim_Tax_Segment_2 = interim_Tax_Segment_2;
	}
	public String getInterim_Tax_Segment_3() {
		return Interim_Tax_Segment_3;
	}
	public void setInterim_Tax_Segment_3(String interim_Tax_Segment_3) {
		Interim_Tax_Segment_3 = interim_Tax_Segment_3;
	}
	public String getInterim_Tax_Segment_4() {
		return Interim_Tax_Segment_4;
	}
	public void setInterim_Tax_Segment_4(String interim_Tax_Segment_4) {
		Interim_Tax_Segment_4 = interim_Tax_Segment_4;
	}
	public String getInterim_Tax_Segment_5() {
		return Interim_Tax_Segment_5;
	}
	public void setInterim_Tax_Segment_5(String interim_Tax_Segment_5) {
		Interim_Tax_Segment_5 = interim_Tax_Segment_5;
	}
	public String getInterim_Tax_Segment_6() {
		return Interim_Tax_Segment_6;
	}
	public void setInterim_Tax_Segment_6(String interim_Tax_Segment_6) {
		Interim_Tax_Segment_6 = interim_Tax_Segment_6;
	}
	public String getInterim_Tax_Segment_7() {
		return Interim_Tax_Segment_7;
	}
	public void setInterim_Tax_Segment_7(String interim_Tax_Segment_7) {
		Interim_Tax_Segment_7 = interim_Tax_Segment_7;
	}
	public String getInterim_Tax_Segment_8() {
		return Interim_Tax_Segment_8;
	}
	public void setInterim_Tax_Segment_8(String interim_Tax_Segment_8) {
		Interim_Tax_Segment_8 = interim_Tax_Segment_8;
	}
	public String getInterim_Tax_Segment_9() {
		return Interim_Tax_Segment_9;
	}
	public void setInterim_Tax_Segment_9(String interim_Tax_Segment_9) {
		Interim_Tax_Segment_9 = interim_Tax_Segment_9;
	}
	public String getInterim_Tax_Segment_10() {
		return Interim_Tax_Segment_10;
	}
	public void setInterim_Tax_Segment_10(String interim_Tax_Segment_10) {
		Interim_Tax_Segment_10 = interim_Tax_Segment_10;
	}
	public String getInterim_Tax_Segment_11() {
		return Interim_Tax_Segment_11;
	}
	public void setInterim_Tax_Segment_11(String interim_Tax_Segment_11) {
		Interim_Tax_Segment_11 = interim_Tax_Segment_11;
	}
	public String getInterim_Tax_Segment_12() {
		return Interim_Tax_Segment_12;
	}
	public void setInterim_Tax_Segment_12(String interim_Tax_Segment_12) {
		Interim_Tax_Segment_12 = interim_Tax_Segment_12;
	}
	public String getInterim_Tax_Segment_13() {
		return Interim_Tax_Segment_13;
	}
	public void setInterim_Tax_Segment_13(String interim_Tax_Segment_13) {
		Interim_Tax_Segment_13 = interim_Tax_Segment_13;
	}
	public String getInterim_Tax_Segment_14() {
		return Interim_Tax_Segment_14;
	}
	public void setInterim_Tax_Segment_14(String interim_Tax_Segment_14) {
		Interim_Tax_Segment_14 = interim_Tax_Segment_14;
	}
	public String getInterim_Tax_Segment_15() {
		return Interim_Tax_Segment_15;
	}
	public void setInterim_Tax_Segment_15(String interim_Tax_Segment_15) {
		Interim_Tax_Segment_15 = interim_Tax_Segment_15;
	}
	public String getInterim_Tax_Segment_16() {
		return Interim_Tax_Segment_16;
	}
	public void setInterim_Tax_Segment_16(String interim_Tax_Segment_16) {
		Interim_Tax_Segment_16 = interim_Tax_Segment_16;
	}
	public String getInterim_Tax_Segment_17() {
		return Interim_Tax_Segment_17;
	}
	public void setInterim_Tax_Segment_17(String interim_Tax_Segment_17) {
		Interim_Tax_Segment_17 = interim_Tax_Segment_17;
	}
	public String getInterim_Tax_Segment_18() {
		return Interim_Tax_Segment_18;
	}
	public void setInterim_Tax_Segment_18(String interim_Tax_Segment_18) {
		Interim_Tax_Segment_18 = interim_Tax_Segment_18;
	}
	public String getInterim_Tax_Segment_19() {
		return Interim_Tax_Segment_19;
	}
	public void setInterim_Tax_Segment_19(String interim_Tax_Segment_19) {
		Interim_Tax_Segment_19 = interim_Tax_Segment_19;
	}
	public String getInterim_Tax_Segment_20() {
		return Interim_Tax_Segment_20;
	}
	public void setInterim_Tax_Segment_20(String interim_Tax_Segment_20) {
		Interim_Tax_Segment_20 = interim_Tax_Segment_20;
	}
	public String getInterim_Tax_Segment_21() {
		return Interim_Tax_Segment_21;
	}
	public void setInterim_Tax_Segment_21(String interim_Tax_Segment_21) {
		Interim_Tax_Segment_21 = interim_Tax_Segment_21;
	}
	public String getInterim_Tax_Segment_22() {
		return Interim_Tax_Segment_22;
	}
	public void setInterim_Tax_Segment_22(String interim_Tax_Segment_22) {
		Interim_Tax_Segment_22 = interim_Tax_Segment_22;
	}
	public String getInterim_Tax_Segment_23() {
		return Interim_Tax_Segment_23;
	}
	public void setInterim_Tax_Segment_23(String interim_Tax_Segment_23) {
		Interim_Tax_Segment_23 = interim_Tax_Segment_23;
	}
	public String getInterim_Tax_Segment_24() {
		return Interim_Tax_Segment_24;
	}
	public void setInterim_Tax_Segment_24(String interim_Tax_Segment_24) {
		Interim_Tax_Segment_24 = interim_Tax_Segment_24;
	}
	public String getInterim_Tax_Segment_25() {
		return Interim_Tax_Segment_25;
	}
	public void setInterim_Tax_Segment_25(String interim_Tax_Segment_25) {
		Interim_Tax_Segment_25 = interim_Tax_Segment_25;
	}
	public String getInterim_Tax_Segment_26() {
		return Interim_Tax_Segment_26;
	}
	public void setInterim_Tax_Segment_26(String interim_Tax_Segment_26) {
		Interim_Tax_Segment_26 = interim_Tax_Segment_26;
	}
	public String getInterim_Tax_Segment_27() {
		return Interim_Tax_Segment_27;
	}
	public void setInterim_Tax_Segment_27(String interim_Tax_Segment_27) {
		Interim_Tax_Segment_27 = interim_Tax_Segment_27;
	}
	public String getInterim_Tax_Segment_28() {
		return Interim_Tax_Segment_28;
	}
	public void setInterim_Tax_Segment_28(String interim_Tax_Segment_28) {
		Interim_Tax_Segment_28 = interim_Tax_Segment_28;
	}
	public String getInterim_Tax_Segment_29() {
		return Interim_Tax_Segment_29;
	}
	public void setInterim_Tax_Segment_29(String interim_Tax_Segment_29) {
		Interim_Tax_Segment_29 = interim_Tax_Segment_29;
	}
	public String getInterim_Tax_Segment_30() {
		return Interim_Tax_Segment_30;
	}
	public void setInterim_Tax_Segment_30(String interim_Tax_Segment_30) {
		Interim_Tax_Segment_30 = interim_Tax_Segment_30;
	}
	public String getInterface_Distributions_Flexfield_Context() {
		return Interface_Distributions_Flexfield_Context;
	}
	public void setInterface_Distributions_Flexfield_Context(String interface_Distributions_Flexfield_Context) {
		Interface_Distributions_Flexfield_Context = interface_Distributions_Flexfield_Context;
	}
	public String getInterface_Distributions_Flexfield__Segment_1() {
		return Interface_Distributions_Flexfield__Segment_1;
	}
	public void setInterface_Distributions_Flexfield__Segment_1(String interface_Distributions_Flexfield__Segment_1) {
		Interface_Distributions_Flexfield__Segment_1 = interface_Distributions_Flexfield__Segment_1;
	}
	public String getInterface_Distributions_Flexfield__Segment_2() {
		return Interface_Distributions_Flexfield__Segment_2;
	}
	public void setInterface_Distributions_Flexfield__Segment_2(String interface_Distributions_Flexfield__Segment_2) {
		Interface_Distributions_Flexfield__Segment_2 = interface_Distributions_Flexfield__Segment_2;
	}
	public String getInterface_Distributions_Flexfield__Segment_3() {
		return Interface_Distributions_Flexfield__Segment_3;
	}
	public void setInterface_Distributions_Flexfield__Segment_3(String interface_Distributions_Flexfield__Segment_3) {
		Interface_Distributions_Flexfield__Segment_3 = interface_Distributions_Flexfield__Segment_3;
	}
	public String getInterface_Distributions_Flexfield_Segment_4() {
		return Interface_Distributions_Flexfield_Segment_4;
	}
	public void setInterface_Distributions_Flexfield_Segment_4(String interface_Distributions_Flexfield_Segment_4) {
		Interface_Distributions_Flexfield_Segment_4 = interface_Distributions_Flexfield_Segment_4;
	}
	public String getInterface_Distributions_Flexfield_Segment_5() {
		return Interface_Distributions_Flexfield_Segment_5;
	}
	public void setInterface_Distributions_Flexfield_Segment_5(String interface_Distributions_Flexfield_Segment_5) {
		Interface_Distributions_Flexfield_Segment_5 = interface_Distributions_Flexfield_Segment_5;
	}
	public String getInterface_Distributions_Flexfield_Segment_6() {
		return Interface_Distributions_Flexfield_Segment_6;
	}
	public void setInterface_Distributions_Flexfield_Segment_6(String interface_Distributions_Flexfield_Segment_6) {
		Interface_Distributions_Flexfield_Segment_6 = interface_Distributions_Flexfield_Segment_6;
	}
	public String getInterface_Distributions_Flexfield_Segment_7() {
		return Interface_Distributions_Flexfield_Segment_7;
	}
	public void setInterface_Distributions_Flexfield_Segment_7(String interface_Distributions_Flexfield_Segment_7) {
		Interface_Distributions_Flexfield_Segment_7 = interface_Distributions_Flexfield_Segment_7;
	}
	public String getInterface_Distributions_Flexfield_Segment_8() {
		return Interface_Distributions_Flexfield_Segment_8;
	}
	public void setInterface_Distributions_Flexfield_Segment_8(String interface_Distributions_Flexfield_Segment_8) {
		Interface_Distributions_Flexfield_Segment_8 = interface_Distributions_Flexfield_Segment_8;
	}
	public String getInterface_Distributions_Flexfield_Segment_9() {
		return Interface_Distributions_Flexfield_Segment_9;
	}
	public void setInterface_Distributions_Flexfield_Segment_9(String interface_Distributions_Flexfield_Segment_9) {
		Interface_Distributions_Flexfield_Segment_9 = interface_Distributions_Flexfield_Segment_9;
	}
	public String getInterface_Distributions_Flexfield_Segment_10() {
		return Interface_Distributions_Flexfield_Segment_10;
	}
	public void setInterface_Distributions_Flexfield_Segment_10(String interface_Distributions_Flexfield_Segment_10) {
		Interface_Distributions_Flexfield_Segment_10 = interface_Distributions_Flexfield_Segment_10;
	}
	public String getInterface_Distributions_Flexfield_Segment_11() {
		return Interface_Distributions_Flexfield_Segment_11;
	}
	public void setInterface_Distributions_Flexfield_Segment_11(String interface_Distributions_Flexfield_Segment_11) {
		Interface_Distributions_Flexfield_Segment_11 = interface_Distributions_Flexfield_Segment_11;
	}
	public String getInterface_Distributions_Flexfield_Segment_12() {
		return Interface_Distributions_Flexfield_Segment_12;
	}
	public void setInterface_Distributions_Flexfield_Segment_12(String interface_Distributions_Flexfield_Segment_12) {
		Interface_Distributions_Flexfield_Segment_12 = interface_Distributions_Flexfield_Segment_12;
	}
	public String getInterface_Distributions_Flexfield_Segment_13() {
		return Interface_Distributions_Flexfield_Segment_13;
	}
	public void setInterface_Distributions_Flexfield_Segment_13(String interface_Distributions_Flexfield_Segment_13) {
		Interface_Distributions_Flexfield_Segment_13 = interface_Distributions_Flexfield_Segment_13;
	}
	public String getInterface_Distributions_Flexfield_Segment_14() {
		return Interface_Distributions_Flexfield_Segment_14;
	}
	public void setInterface_Distributions_Flexfield_Segment_14(String interface_Distributions_Flexfield_Segment_14) {
		Interface_Distributions_Flexfield_Segment_14 = interface_Distributions_Flexfield_Segment_14;
	}
	public String getInterface_Distributions_Flexfield_Segment_15() {
		return Interface_Distributions_Flexfield_Segment_15;
	}
	public void setInterface_Distributions_Flexfield_Segment_15(String interface_Distributions_Flexfield_Segment_15) {
		Interface_Distributions_Flexfield_Segment_15 = interface_Distributions_Flexfield_Segment_15;
	}
	public String getBusiness_Unit_Name() {
		return Business_Unit_Name;
	}
	public void setBusiness_Unit_Name(String business_Unit_Name) {
		Business_Unit_Name = business_Unit_Name;
	}
	public String getEnd() {
		return End;
	}
	public void setEnd(String end) {
		End = end;
	}

}

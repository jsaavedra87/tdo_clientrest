package com.dto.integration.dto;

import java.util.List;

import com.tdo.integration.client.localmodel.FileTransferControl;

public class GLInterface {
	
private String dataAccesSet ;
private String ledgerId;
private List<GLInterfaceV3> listGLInterface;
private FileTransferControl fileTransferControl;

public String getDataAccesSet() {
	return dataAccesSet;
}
public void setDataAccesSet(String dataAccesSet) {
	this.dataAccesSet = dataAccesSet;
}
public String getLedgerId() {
	return ledgerId;
}
public void setLedgerId(String ledgerId) {
	this.ledgerId = ledgerId;
}
public List<GLInterfaceV3> getListGLInterface() {
	return listGLInterface;
}
public void setListGLInterface(List<GLInterfaceV3> listGLInterface) {
	this.listGLInterface = listGLInterface;
}
public FileTransferControl getFileTransferControl() {
	return fileTransferControl;
}
public void setFileTransferControl(FileTransferControl fileTransferControl) {
	this.fileTransferControl = fileTransferControl;
}	


}

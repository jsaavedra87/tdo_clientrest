package com.dto.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class BaseResponse {

	@JsonProperty("status")
	 private String status;
	
	@JsonProperty("code")
	 private Integer code;
	
	@JsonProperty("content")
	 private String content;
	 
	 public String getStatus() {
	  return status;
	 }
	 public void setStatus(String status) {
	  this.status = status;
	 }
	 public Integer getCode() {
	  return code;
	 }
	 public void setCode(Integer code) {
	  this.code = code;
	 }
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}

package com.dto.integration.dto;

import java.util.List;

import com.tdo.integration.client.localmodel.FileTransferControl;
import com.tdo.integration.client.model.CancelacionVenta;
import com.tdo.integration.client.model.MovimientosCaja;
import com.tdo.integration.client.model.NotaCredito;
import com.tdo.integration.client.model.Sales;

public class SalesDTO {
	
	private static final long serialVersionUID = 1L;
	List<Sales> sales;		
	List<Sales> ventasCanceladas;	
	List<CancelacionVenta> cancelaciones;
	List<MovimientosCaja> movimientosCaja;
	List<NotaCredito> ncExtemporaneas;
	FileTransferControl fileTransferControl;
		
	public List<Sales> getSales() {
		return sales;
	}
	public void setSales(List<Sales> sales) {
		this.sales = sales;
	}
	public List<Sales> getVentasCanceladas() {
		return ventasCanceladas;
	}
	public void setVentasCanceladas(List<Sales> ventasCanceladas) {
		this.ventasCanceladas = ventasCanceladas;
	}
	public List<CancelacionVenta> getCancelaciones() {
		return cancelaciones;
	}
	public void setCancelaciones(List<CancelacionVenta> cancelaciones) {
		this.cancelaciones = cancelaciones;
	}
	public List<MovimientosCaja> getMovimientosCaja() {
		return movimientosCaja;
	}
	public void setMovimientosCaja(List<MovimientosCaja> movimientosCaja) {
		this.movimientosCaja = movimientosCaja;
	}	
	public List<NotaCredito> getNcExtemporaneas() {
		return ncExtemporaneas;
	}
	public void setNcExtemporaneas(List<NotaCredito> ncExtemporaneas) {
		this.ncExtemporaneas = ncExtemporaneas;
	}	
	public FileTransferControl getFileTransferControl() {
		return fileTransferControl;
	}
	public void setFileTransferControl(FileTransferControl fileTransferControl) {
		this.fileTransferControl = fileTransferControl;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
	
}

package com.dto.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ArticulosJson {
	
	@JsonProperty("incluyeImpuestos")
	private boolean incluyeImpuestos;
	
	@JsonProperty("articulo")
	Articulo articulo;

	public boolean isIncluyeImpuestos() {
		return incluyeImpuestos;
	}

	public void setIncluyeImpuestos(boolean incluyeImpuestos) {
		this.incluyeImpuestos = incluyeImpuestos;
	}

	public Articulo getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

}

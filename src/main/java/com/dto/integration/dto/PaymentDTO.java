package com.dto.integration.dto;

import java.util.List;

import com.tdo.integration.client.model.Abono;

public class PaymentDTO {

	private List<Abono> lstPayment;

	public List<Abono> getLstPayment() {
		return lstPayment;
	}

	public void setLstPayment(List<Abono> lstPayment) {
		this.lstPayment = lstPayment;
	}

}

package com.dto.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class RaInterfaceLinesAll {

	private String Business_Unit_Identifier = "";

	private String Transaction_Batch_Source_Name = "";

	private String Transaction_Type_Name = "";

	private String Payment_Terms = "";

	private String Transaction_Date = "";

	private String Accounting_Date = "";

	private String Transaction_Number = "";

	private String Original_System_Bill_to_Customer_Reference = "";

	private String Original_System_Bill_to_Customer_Address_Reference = "";

	private String Original_System_Bill_to_Customer_Contact_Reference = "";

	private String Original_System_Ship_to_Customer_Reference = "";

	private String Original_System_Ship_to_Customer_Address_Reference = "";

	private String Original_System_Ship_to_Customer_Contact_Reference = "";

	private String Original_System_Ship_to_Customer_Account_Reference = "";

	private String Original_System_Ship_to_Customer_Account_Address_Reference = "";

	private String Original_System_Ship_to_Customer_Account_Contact_Reference = "";

	private String Original_System_Sold_to_Customer_Reference = "";

	private String Original_System_Sold_to_Customer_Account_Reference = "";

	private String Bill_to_Customer_Account_Number = "";

	private String Bill_to_Customer_Site_Number = "";

	private String Bill_to_Contact_Party_Number = "";

	private String Ship_to_Customer_Account_Number = "";

	private String Ship_to_Customer_Site_Number = "";

	private String Ship_to_Contact_Party_Number = "";

	private String Sold_to_Customer_Account_Number = "";

	private String Transaction_Line_Type = "";

	private String Transaction_Line_Description = "";

	private String Currency_Code = "";

	private String Currency_Conversion_Type = "";

	private String Currency_Conversion_Date = "";

	private String Currency_Conversion_Rate = "";

	private String Transaction_Line_Amount = "";

	private String Transaction_Line_Quantity = "";

	private String Customer_Ordered_Quantity = "";

	private String Unit_Selling_Price = "";

	private String Unit_Standard_Price = "";

	private String Line_Transactions_Flexfield_Context = "";

	private String Line_Transactions_Flexfield_Segment_1 = "";

	private String Line_Transactions_Flexfield_Segment_2 = "";

	private String Line_Transactions_Flexfield_Segment_3 = "";

	private String Line_Transactions_Flexfield_Segment_4 = "";

	private String Line_Transactions_Flexfield_Segment_5 = "";

	private String Line_Transactions_Flexfield_Segment_6 = "";

	private String Line_Transactions_Flexfield_Segment_7 = "";

	private String Line_Transactions_Flexfield_Segment_8 = "";

	private String Line_Transactions_Flexfield_Segment_9 = "";

	private String Line_Transactions_Flexfield_Segment_10 = "";

	private String Line_Transactions_Flexfield_Segment_11 = "";

	private String Line_Transactions_Flexfield_Segment_12 = "";

	private String Line_Transactions_Flexfield_Segment_13 = "";

	private String Line_Transactions_Flexfield_Segment_14 = "";

	private String Line_Transactions_Flexfield_Segment_15 = "";

	private String Primary_Salesperson_Number = "";

	private String Tax_Classification_Code = "";

	private String Legal_Entity_Identifier = "";

	private String Accounted_Amount_in_Ledger_Currency = "";

	private String Sales_Order_Number = "";

	private String Sales_Order_Date = "";

	private String Actual_Ship_Date = "";

	private String Warehouse_Code = "";

	private String Unit_of_Measure_Code = "";

	private String Unit_of_Measure_Name = "";

	private String Invoicing_Rule_Name = "";

	private String Revenue_Scheduling_Rule_Name = "";

	private String Number_of_Revenue_Periods = "";

	private String Revenue_Scheduling_Rule_Start_Date = "";

	private String Revenue_Scheduling_Rule_End_Date = "";

	private String Reason_Code_Meaning = "";

	private String Last_Period_to_Credit = "";

	private String Transaction_Business_Category_Code = "";

	private String Product_Fiscal_Classification_Code = "";

	private String Product_Category_Code = "";

	private String Product_Type = "";

	private String Line_Intended_Use_Code = "";

	private String Assessable_Value = "";

	private String Document_Sub_Type = "";

	private String Default_Taxation_Country = "";

	private String User_Defined_Fiscal_Classification = "";

	private String Tax_Invoice_Number = "";

	private String Tax_Invoice_Date = "";

	private String Tax_Regime_Code = "";

	private String Tax = "";

	private String Tax_Status_Code = "";

	private String Tax_Rate_Code = "";

	private String Tax_Jurisdiction_Code = "";

	private String First_Party_Registration_Number = "";

	private String Third_Party_Registration_Number = "";

	private String Final_Discharge_Location = "";

	private String Taxable_Amount = "";

	private String Taxable_Flag = "";

	private String Tax_Exemption_Flag = "";

	private String Tax_Exemption_Reason_Code = "";

	private String Tax_Exemption_Reason_Code_Meaning = "";

	private String Tax_Exemption_Certificate_Number = "";

	private String Line_Amount_Includes_Tax_Flag = "";

	private String Tax_Precedence = "";

	private String Credit_Method_To_Be_Used_For_Lines_With_Revenue_Scheduling_Rules = "";

	private String Credit_Method_To_Be_Used_For_Transactions_With_Split_Payment_Terms = "";

	private String Reason_Code = "";

	private String Tax_Rate = "";

	private String FOB_Point = "";

	private String Carrier = "";

	private String Shipping_Reference = "";

	private String Sales_Order_Line_Number = "";

	private String Sales_Order_Source = "";

	private String Sales_Order_Revision_Number = "";

	private String Purchase_Order_Number = "";

	private String Purchase_Order_Revision_Number = "";

	private String Purchase_Order_Date = "";

	private String Agreement_Name = "";

	private String Memo_Line_Name = "";

	private String Document_Number = "";

	private String Original_System_Batch_Name = "";

	private String Link_to_Transactions_Flexfield_Context = "";

	private String Link_to_Transactions_Flexfield_Segment_1 = "";

	private String Link_to_Transactions_Flexfield_Segment_2 = "";

	private String Link_to_Transactions_Flexfield_Segment_3 = "";

	private String Link_to_Transactions_Flexfield_Segment_4 = "";

	private String Link_to_Transactions_Flexfield_Segment_5 = "";

	private String Link_to_Transactions_Flexfield_Segment_6 = "";

	private String Link_to_Transactions_Flexfield_Segment_7 = "";

	private String Link_to_Transactions_Flexfield_Segment_8 = "";

	private String Link_to_Transactions_Flexfield_Segment_9 = "";

	private String Link_to_Transactions_Flexfield_Segment_10 = "";

	private String Link_to_Transactions_Flexfield_Segment_11 = "";

	private String Link_to_Transactions_Flexfield_Segment_12 = "";

	private String Link_to_Transactions_Flexfield_Segment_13 = "";

	private String Link_to_Transactions_Flexfield_Segment_14 = "";

	private String Link_to_Transactions_Flexfield_Segment_15 = "";

	private String Reference_Transactions_Flexfield_Context = "";

	private String Reference_Transactions_Flexfield_Segment_1 = "";

	private String Reference_Transactions_Flexfield_Segment_2 = "";

	private String Reference_Transactions_Flexfield_Segment_3 = "";

	private String Reference_Transactions_Flexfield_Segment_4 = "";

	private String Reference_Transactions_Flexfield_Segment_5 = "";

	private String Reference_Transactions_Flexfield_Segment_6 = "";

	private String Reference_Transactions_Flexfield_Segment_7 = "";

	private String Reference_Transactions_Flexfield_Segment_8 = "";

	private String Reference_Transactions_Flexfield_Segment_9 = "";

	private String Reference_Transactions_Flexfield_Segment_10 = "";

	private String Reference_Transactions_Flexfield_Segment_11 = "";

	private String Reference_Transactions_Flexfield_Segment_12 = "";

	private String Reference_Transactions_Flexfield_Segment_13 = "";

	private String Reference_Transactions_Flexfield_Segment_14 = "";

	private String Reference_Transactions_Flexfield_Segment_15 = "";

	private String Link_To_Parent_Line_Context = "";

	private String Link_To_Parent_Line_Segment_1 = "";

	private String Link_To_Parent_Line_Segment_2 = "";

	private String Link_To_Parent_Line_Segment_3 = "";

	private String Link_To_Parent_Line_Segment_4 = "";

	private String Link_To_Parent_Line_Segment_5 = "";

	private String Link_To_Parent_Line_Segment_6 = "";

	private String Link_To_Parent_Line_Segment_7 = "";

	private String Link_To_Parent_Line_Segment_8 = "";

	private String Link_To_Parent_Line_Segment_9 = "";

	private String Link_To_Parent_Line_Segment_10 = "";

	private String Link_To_Parent_Line_Segment_11 = "";

	private String Link_To_Parent_Line_Segment_12 = "";

	private String Link_To_Parent_Line_Segment_13 = "";

	private String Link_To_Parent_Line_Segment_14 = "";

	private String Link_To_Parent_Line_Segment_15 = "";

	private String Receipt_Method_Name = "";

	private String Printing_Option = "";

	private String Related_Batch_Source_Name = "";

	private String Related_Transaction_Number = "";

	private String Inventory_Item_Number = "";

	private String Inventory_Item_Segment_2 = "";

	private String Inventory_Item_Segment_3 = "";

	private String Inventory_Item_Segment_4 = "";

	private String Inventory_Item_Segment_5 = "";

	private String Inventory_Item_Segment_6 = "";

	private String Inventory_Item_Segment_7 = "";

	private String Inventory_Item_Segment_8 = "";

	private String Inventory_Item_Segment_9 = "";

	private String Inventory_Item_Segment_10 = "";

	private String Inventory_Item_Segment_11 = "";

	private String Inventory_Item_Segment_12 = "";

	private String Inventory_Item_Segment_13 = "";

	private String Inventory_Item_Segment_14 = "";

	private String Inventory_Item_Segment_15 = "";

	private String Inventory_Item_Segment_16 = "";

	private String Inventory_Item_Segment_17 = "";

	private String Inventory_Item_Segment_18 = "";

	private String Inventory_Item_Segment_19 = "";

	private String Inventory_Item_Segment_20 = "";

	private String Bill_To_Customer_Bank_Account_Name = "";

	private String Reset_Transaction_Date_Flag = "";

	private String Payment_Server_Order_Number = "";

	private String Last_Transaction_on_Debit_Authorization = "";

	private String Approval_Code = "";

	private String Address_Verification_Code = "";

	private String Transaction_Line_Translated_Description = "";

	private String Consolidated_Billing_Number = "";

	private String Promised_Commitment_Amount = "";

	private String Payment_Set_Identifier = "";

	private String Original_Accounting_Date = "";

	private String Invoiced_Line_Accounting_Level = "";

	private String Override_AutoAccounting_Flag = "";

	private String Historical_Flag = "";

	private String Deferral_Exclusion_Flag = "";

	private String Payment_Attributes = "";

	private String Invoice_Billing_Date = "";

	private String Invoice_Lines_Flexfield_Context = "";

	private String Invoice_Lines_Flexfield_Segment_1 = "";

	private String Invoice_Lines_Flexfield_Segment_2 = "";

	private String Invoice_Lines_Flexfield_Segment_3 = "";

	private String Invoice_Lines_Flexfield_Segment_4 = "";

	private String Invoice_Lines_Flexfield_Segment_5 = "";

	private String Invoice_Lines_Flexfield_Segment_6 = "";

	private String Invoice_Lines_Flexfield_Segment_7 = "";

	private String Invoice_Lines_Flexfield_Segment_8 = "";

	private String Invoice_Lines_Flexfield_Segment_9 = "";

	private String Invoice_Lines_Flexfield_Segment_10 = "";

	private String Invoice_Lines_Flexfield_Segment_11 = "";

	private String Invoice_Lines_Flexfield_Segment_12 = "";

	private String Invoice_Lines_Flexfield_Segment_13 = "";

	private String Invoice_Lines_Flexfield_Segment_14 = "";

	private String Invoice_Lines_Flexfield_Segment_15 = "";

	private String Invoice_Transactions_Flexfield_Context = "";

	private String Invoice_Transactions_Flexfield_Segment_1 = "";

	private String Invoice_Transactions_Flexfield_Segment_2 = "";

	private String Invoice_Transactions_Flexfield_Segment_3 = "";

	private String Invoice_Transactions_Flexfield_Segment_4 = "";

	private String Invoice_Transactions_Flexfield_Segment_5 = "";

	private String Invoice_Transactions_Flexfield_Segment_6 = "";

	private String Invoice_Transactions_Flexfield_Segment_7 = "";

	private String Invoice_Transactions_Flexfield_Segment_8 = "";

	private String Invoice_Transactions_Flexfield_Segment_9 = "";

	private String Invoice_Transactions_Flexfield_Segment_10 = "";

	private String Invoice_Transactions_Flexfield_Segment_11 = "";

	private String Invoice_Transactions_Flexfield_Segment_12 = "";

	private String Invoice_Transactions_Flexfield_Segment_13 = "";

	private String Invoice_Transactions_Flexfield_Segment_14 = "";

	private String Invoice_Transactions_Flexfield_Segment_15 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Context = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_1 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_2 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_3 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_4 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_5 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_6 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_7 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_8 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_9 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_10 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_11 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_12 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_13 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_14 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_15 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_16 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_17 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_18 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_19 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_20 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_21 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_22 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_23 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_24 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_25 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_26 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_27 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_28 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_29 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Segment_30 = "";

	private String Line_Global_Descriptive_Flexfield_Attribute_Category = "";

	private String Line_Global_Descriptive_Flexfield_Segment_1 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_2 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_3 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_4 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_5 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_6 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_7 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_8 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_9 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_10 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_11 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_12 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_13 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_14 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_15 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_16 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_17 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_18 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_19 = "";

	private String Line_Global_Descriptive_Flexfield_Segment_20 = "";

	private String Business_Unit_Name = "";

	private String Comments = "";

	private String Notes_from_Source = "";

	private String Credit_Card_Token_Number = "";

	private String Credit_Card_Expiration_Date = "";

	private String First_Name_of_the_Credit_Card_Holder = "";

	private String Last_Name_of_the_Credit_Card_Holder = "";

	private String Credit_Card_Issuer_Code = "";

	private String Masked_Credit_Card_Number = "";

	private String Credit_Card_Authorization_Request_Identifier = "";

	private String Credit_Card_Voice_Authorization_Code = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_1 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_2 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_3 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_4 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_5 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_6 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_7 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_8 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_9 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_10 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_11 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Number_Segment_12 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Date_Segment_1 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Date_Segment_2 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Date_Segment_3 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Date_Segment_4 = "";

	private String Receivables_Transaction_Region_Information_Flexfield_Date_Segment_5 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_1 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_2 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_3 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_4 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_5 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_1 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_2 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_3 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_4 = "";

	private String Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_5 = "";

	private String Freight_Charge = "";

	private String Insurance_Charge = "";

	private String Packing_Charge = "";

	private String Miscellaneous_Charge = "";

	private String Commercial_Discount = "";

	private String Enforce_Chronological_Document_Sequencing = "";

	private String End = "";

	public String getBusiness_Unit_Identifier() {
		return Business_Unit_Identifier;
	}

	public void setBusiness_Unit_Identifier(String business_Unit_Identifier) {
		Business_Unit_Identifier = business_Unit_Identifier;
	}

	public String getTransaction_Batch_Source_Name() {
		return Transaction_Batch_Source_Name;
	}

	public void setTransaction_Batch_Source_Name(String transaction_Batch_Source_Name) {
		Transaction_Batch_Source_Name = transaction_Batch_Source_Name;
	}

	public String getTransaction_Type_Name() {
		return Transaction_Type_Name;
	}

	public void setTransaction_Type_Name(String transaction_Type_Name) {
		Transaction_Type_Name = transaction_Type_Name;
	}

	public String getPayment_Terms() {
		return Payment_Terms;
	}

	public void setPayment_Terms(String payment_Terms) {
		Payment_Terms = payment_Terms;
	}

	public String getTransaction_Date() {
		return Transaction_Date;
	}

	public void setTransaction_Date(String transaction_Date) {
		Transaction_Date = transaction_Date;
	}

	public String getAccounting_Date() {
		return Accounting_Date;
	}

	public void setAccounting_Date(String accounting_Date) {
		Accounting_Date = accounting_Date;
	}

	public String getTransaction_Number() {
		return Transaction_Number;
	}

	public void setTransaction_Number(String transaction_Number) {
		Transaction_Number = transaction_Number;
	}

	public String getOriginal_System_Bill_to_Customer_Reference() {
		return Original_System_Bill_to_Customer_Reference;
	}

	public void setOriginal_System_Bill_to_Customer_Reference(String original_System_Bill_to_Customer_Reference) {
		Original_System_Bill_to_Customer_Reference = original_System_Bill_to_Customer_Reference;
	}

	public String getOriginal_System_Bill_to_Customer_Address_Reference() {
		return Original_System_Bill_to_Customer_Address_Reference;
	}

	public void setOriginal_System_Bill_to_Customer_Address_Reference(
			String original_System_Bill_to_Customer_Address_Reference) {
		Original_System_Bill_to_Customer_Address_Reference = original_System_Bill_to_Customer_Address_Reference;
	}

	public String getOriginal_System_Bill_to_Customer_Contact_Reference() {
		return Original_System_Bill_to_Customer_Contact_Reference;
	}

	public void setOriginal_System_Bill_to_Customer_Contact_Reference(
			String original_System_Bill_to_Customer_Contact_Reference) {
		Original_System_Bill_to_Customer_Contact_Reference = original_System_Bill_to_Customer_Contact_Reference;
	}

	public String getOriginal_System_Ship_to_Customer_Reference() {
		return Original_System_Ship_to_Customer_Reference;
	}

	public void setOriginal_System_Ship_to_Customer_Reference(String original_System_Ship_to_Customer_Reference) {
		Original_System_Ship_to_Customer_Reference = original_System_Ship_to_Customer_Reference;
	}

	public String getOriginal_System_Ship_to_Customer_Address_Reference() {
		return Original_System_Ship_to_Customer_Address_Reference;
	}

	public void setOriginal_System_Ship_to_Customer_Address_Reference(
			String original_System_Ship_to_Customer_Address_Reference) {
		Original_System_Ship_to_Customer_Address_Reference = original_System_Ship_to_Customer_Address_Reference;
	}

	public String getOriginal_System_Ship_to_Customer_Contact_Reference() {
		return Original_System_Ship_to_Customer_Contact_Reference;
	}

	public void setOriginal_System_Ship_to_Customer_Contact_Reference(
			String original_System_Ship_to_Customer_Contact_Reference) {
		Original_System_Ship_to_Customer_Contact_Reference = original_System_Ship_to_Customer_Contact_Reference;
	}

	public String getOriginal_System_Ship_to_Customer_Account_Reference() {
		return Original_System_Ship_to_Customer_Account_Reference;
	}

	public void setOriginal_System_Ship_to_Customer_Account_Reference(
			String original_System_Ship_to_Customer_Account_Reference) {
		Original_System_Ship_to_Customer_Account_Reference = original_System_Ship_to_Customer_Account_Reference;
	}

	public String getOriginal_System_Ship_to_Customer_Account_Address_Reference() {
		return Original_System_Ship_to_Customer_Account_Address_Reference;
	}

	public void setOriginal_System_Ship_to_Customer_Account_Address_Reference(
			String original_System_Ship_to_Customer_Account_Address_Reference) {
		Original_System_Ship_to_Customer_Account_Address_Reference = original_System_Ship_to_Customer_Account_Address_Reference;
	}

	public String getOriginal_System_Ship_to_Customer_Account_Contact_Reference() {
		return Original_System_Ship_to_Customer_Account_Contact_Reference;
	}

	public void setOriginal_System_Ship_to_Customer_Account_Contact_Reference(
			String original_System_Ship_to_Customer_Account_Contact_Reference) {
		Original_System_Ship_to_Customer_Account_Contact_Reference = original_System_Ship_to_Customer_Account_Contact_Reference;
	}

	public String getOriginal_System_Sold_to_Customer_Reference() {
		return Original_System_Sold_to_Customer_Reference;
	}

	public void setOriginal_System_Sold_to_Customer_Reference(String original_System_Sold_to_Customer_Reference) {
		Original_System_Sold_to_Customer_Reference = original_System_Sold_to_Customer_Reference;
	}

	public String getOriginal_System_Sold_to_Customer_Account_Reference() {
		return Original_System_Sold_to_Customer_Account_Reference;
	}

	public void setOriginal_System_Sold_to_Customer_Account_Reference(
			String original_System_Sold_to_Customer_Account_Reference) {
		Original_System_Sold_to_Customer_Account_Reference = original_System_Sold_to_Customer_Account_Reference;
	}

	public String getBill_to_Customer_Account_Number() {
		return Bill_to_Customer_Account_Number;
	}

	public void setBill_to_Customer_Account_Number(String bill_to_Customer_Account_Number) {
		Bill_to_Customer_Account_Number = bill_to_Customer_Account_Number;
	}

	public String getBill_to_Customer_Site_Number() {
		return Bill_to_Customer_Site_Number;
	}

	public void setBill_to_Customer_Site_Number(String bill_to_Customer_Site_Number) {
		Bill_to_Customer_Site_Number = bill_to_Customer_Site_Number;
	}

	public String getBill_to_Contact_Party_Number() {
		return Bill_to_Contact_Party_Number;
	}

	public void setBill_to_Contact_Party_Number(String bill_to_Contact_Party_Number) {
		Bill_to_Contact_Party_Number = bill_to_Contact_Party_Number;
	}

	public String getShip_to_Customer_Account_Number() {
		return Ship_to_Customer_Account_Number;
	}

	public void setShip_to_Customer_Account_Number(String ship_to_Customer_Account_Number) {
		Ship_to_Customer_Account_Number = ship_to_Customer_Account_Number;
	}

	public String getShip_to_Customer_Site_Number() {
		return Ship_to_Customer_Site_Number;
	}

	public void setShip_to_Customer_Site_Number(String ship_to_Customer_Site_Number) {
		Ship_to_Customer_Site_Number = ship_to_Customer_Site_Number;
	}

	public String getShip_to_Contact_Party_Number() {
		return Ship_to_Contact_Party_Number;
	}

	public void setShip_to_Contact_Party_Number(String ship_to_Contact_Party_Number) {
		Ship_to_Contact_Party_Number = ship_to_Contact_Party_Number;
	}

	public String getSold_to_Customer_Account_Number() {
		return Sold_to_Customer_Account_Number;
	}

	public void setSold_to_Customer_Account_Number(String sold_to_Customer_Account_Number) {
		Sold_to_Customer_Account_Number = sold_to_Customer_Account_Number;
	}

	public String getTransaction_Line_Type() {
		return Transaction_Line_Type;
	}

	public void setTransaction_Line_Type(String transaction_Line_Type) {
		Transaction_Line_Type = transaction_Line_Type;
	}

	public String getTransaction_Line_Description() {
		return Transaction_Line_Description;
	}

	public void setTransaction_Line_Description(String transaction_Line_Description) {
		Transaction_Line_Description = transaction_Line_Description;
	}

	public String getCurrency_Code() {
		return Currency_Code;
	}

	public void setCurrency_Code(String currency_Code) {
		Currency_Code = currency_Code;
	}

	public String getCurrency_Conversion_Type() {
		return Currency_Conversion_Type;
	}

	public void setCurrency_Conversion_Type(String currency_Conversion_Type) {
		Currency_Conversion_Type = currency_Conversion_Type;
	}

	public String getCurrency_Conversion_Date() {
		return Currency_Conversion_Date;
	}

	public void setCurrency_Conversion_Date(String currency_Conversion_Date) {
		Currency_Conversion_Date = currency_Conversion_Date;
	}

	public String getCurrency_Conversion_Rate() {
		return Currency_Conversion_Rate;
	}

	public void setCurrency_Conversion_Rate(String currency_Conversion_Rate) {
		Currency_Conversion_Rate = currency_Conversion_Rate;
	}

	public String getTransaction_Line_Amount() {
		return Transaction_Line_Amount;
	}

	public void setTransaction_Line_Amount(String transaction_Line_Amount) {
		Transaction_Line_Amount = transaction_Line_Amount;
	}

	public String getTransaction_Line_Quantity() {
		return Transaction_Line_Quantity;
	}

	public void setTransaction_Line_Quantity(String transaction_Line_Quantity) {
		Transaction_Line_Quantity = transaction_Line_Quantity;
	}

	public String getCustomer_Ordered_Quantity() {
		return Customer_Ordered_Quantity;
	}

	public void setCustomer_Ordered_Quantity(String customer_Ordered_Quantity) {
		Customer_Ordered_Quantity = customer_Ordered_Quantity;
	}

	public String getUnit_Selling_Price() {
		return Unit_Selling_Price;
	}

	public void setUnit_Selling_Price(String unit_Selling_Price) {
		Unit_Selling_Price = unit_Selling_Price;
	}

	public String getUnit_Standard_Price() {
		return Unit_Standard_Price;
	}

	public void setUnit_Standard_Price(String unit_Standard_Price) {
		Unit_Standard_Price = unit_Standard_Price;
	}

	public String getLine_Transactions_Flexfield_Context() {
		return Line_Transactions_Flexfield_Context;
	}

	public void setLine_Transactions_Flexfield_Context(String line_Transactions_Flexfield_Context) {
		Line_Transactions_Flexfield_Context = line_Transactions_Flexfield_Context;
	}

	public String getLine_Transactions_Flexfield_Segment_1() {
		return Line_Transactions_Flexfield_Segment_1;
	}

	public void setLine_Transactions_Flexfield_Segment_1(String line_Transactions_Flexfield_Segment_1) {
		Line_Transactions_Flexfield_Segment_1 = line_Transactions_Flexfield_Segment_1;
	}

	public String getLine_Transactions_Flexfield_Segment_2() {
		return Line_Transactions_Flexfield_Segment_2;
	}

	public void setLine_Transactions_Flexfield_Segment_2(String line_Transactions_Flexfield_Segment_2) {
		Line_Transactions_Flexfield_Segment_2 = line_Transactions_Flexfield_Segment_2;
	}

	public String getLine_Transactions_Flexfield_Segment_3() {
		return Line_Transactions_Flexfield_Segment_3;
	}

	public void setLine_Transactions_Flexfield_Segment_3(String line_Transactions_Flexfield_Segment_3) {
		Line_Transactions_Flexfield_Segment_3 = line_Transactions_Flexfield_Segment_3;
	}

	public String getLine_Transactions_Flexfield_Segment_4() {
		return Line_Transactions_Flexfield_Segment_4;
	}

	public void setLine_Transactions_Flexfield_Segment_4(String line_Transactions_Flexfield_Segment_4) {
		Line_Transactions_Flexfield_Segment_4 = line_Transactions_Flexfield_Segment_4;
	}

	public String getLine_Transactions_Flexfield_Segment_5() {
		return Line_Transactions_Flexfield_Segment_5;
	}

	public void setLine_Transactions_Flexfield_Segment_5(String line_Transactions_Flexfield_Segment_5) {
		Line_Transactions_Flexfield_Segment_5 = line_Transactions_Flexfield_Segment_5;
	}

	public String getLine_Transactions_Flexfield_Segment_6() {
		return Line_Transactions_Flexfield_Segment_6;
	}

	public void setLine_Transactions_Flexfield_Segment_6(String line_Transactions_Flexfield_Segment_6) {
		Line_Transactions_Flexfield_Segment_6 = line_Transactions_Flexfield_Segment_6;
	}

	public String getLine_Transactions_Flexfield_Segment_7() {
		return Line_Transactions_Flexfield_Segment_7;
	}

	public void setLine_Transactions_Flexfield_Segment_7(String line_Transactions_Flexfield_Segment_7) {
		Line_Transactions_Flexfield_Segment_7 = line_Transactions_Flexfield_Segment_7;
	}

	public String getLine_Transactions_Flexfield_Segment_8() {
		return Line_Transactions_Flexfield_Segment_8;
	}

	public void setLine_Transactions_Flexfield_Segment_8(String line_Transactions_Flexfield_Segment_8) {
		Line_Transactions_Flexfield_Segment_8 = line_Transactions_Flexfield_Segment_8;
	}

	public String getLine_Transactions_Flexfield_Segment_9() {
		return Line_Transactions_Flexfield_Segment_9;
	}

	public void setLine_Transactions_Flexfield_Segment_9(String line_Transactions_Flexfield_Segment_9) {
		Line_Transactions_Flexfield_Segment_9 = line_Transactions_Flexfield_Segment_9;
	}

	public String getLine_Transactions_Flexfield_Segment_10() {
		return Line_Transactions_Flexfield_Segment_10;
	}

	public void setLine_Transactions_Flexfield_Segment_10(String line_Transactions_Flexfield_Segment_10) {
		Line_Transactions_Flexfield_Segment_10 = line_Transactions_Flexfield_Segment_10;
	}

	public String getLine_Transactions_Flexfield_Segment_11() {
		return Line_Transactions_Flexfield_Segment_11;
	}

	public void setLine_Transactions_Flexfield_Segment_11(String line_Transactions_Flexfield_Segment_11) {
		Line_Transactions_Flexfield_Segment_11 = line_Transactions_Flexfield_Segment_11;
	}

	public String getLine_Transactions_Flexfield_Segment_12() {
		return Line_Transactions_Flexfield_Segment_12;
	}

	public void setLine_Transactions_Flexfield_Segment_12(String line_Transactions_Flexfield_Segment_12) {
		Line_Transactions_Flexfield_Segment_12 = line_Transactions_Flexfield_Segment_12;
	}

	public String getLine_Transactions_Flexfield_Segment_13() {
		return Line_Transactions_Flexfield_Segment_13;
	}

	public void setLine_Transactions_Flexfield_Segment_13(String line_Transactions_Flexfield_Segment_13) {
		Line_Transactions_Flexfield_Segment_13 = line_Transactions_Flexfield_Segment_13;
	}

	public String getLine_Transactions_Flexfield_Segment_14() {
		return Line_Transactions_Flexfield_Segment_14;
	}

	public void setLine_Transactions_Flexfield_Segment_14(String line_Transactions_Flexfield_Segment_14) {
		Line_Transactions_Flexfield_Segment_14 = line_Transactions_Flexfield_Segment_14;
	}

	public String getLine_Transactions_Flexfield_Segment_15() {
		return Line_Transactions_Flexfield_Segment_15;
	}

	public void setLine_Transactions_Flexfield_Segment_15(String line_Transactions_Flexfield_Segment_15) {
		Line_Transactions_Flexfield_Segment_15 = line_Transactions_Flexfield_Segment_15;
	}

	public String getPrimary_Salesperson_Number() {
		return Primary_Salesperson_Number;
	}

	public void setPrimary_Salesperson_Number(String primary_Salesperson_Number) {
		Primary_Salesperson_Number = primary_Salesperson_Number;
	}

	public String getTax_Classification_Code() {
		return Tax_Classification_Code;
	}

	public void setTax_Classification_Code(String tax_Classification_Code) {
		Tax_Classification_Code = tax_Classification_Code;
	}

	public String getLegal_Entity_Identifier() {
		return Legal_Entity_Identifier;
	}

	public void setLegal_Entity_Identifier(String legal_Entity_Identifier) {
		Legal_Entity_Identifier = legal_Entity_Identifier;
	}

	public String getAccounted_Amount_in_Ledger_Currency() {
		return Accounted_Amount_in_Ledger_Currency;
	}

	public void setAccounted_Amount_in_Ledger_Currency(String accounted_Amount_in_Ledger_Currency) {
		Accounted_Amount_in_Ledger_Currency = accounted_Amount_in_Ledger_Currency;
	}

	public String getSales_Order_Number() {
		return Sales_Order_Number;
	}

	public void setSales_Order_Number(String sales_Order_Number) {
		Sales_Order_Number = sales_Order_Number;
	}

	public String getSales_Order_Date() {
		return Sales_Order_Date;
	}

	public void setSales_Order_Date(String sales_Order_Date) {
		Sales_Order_Date = sales_Order_Date;
	}

	public String getActual_Ship_Date() {
		return Actual_Ship_Date;
	}

	public void setActual_Ship_Date(String actual_Ship_Date) {
		Actual_Ship_Date = actual_Ship_Date;
	}

	public String getWarehouse_Code() {
		return Warehouse_Code;
	}

	public void setWarehouse_Code(String warehouse_Code) {
		Warehouse_Code = warehouse_Code;
	}

	public String getUnit_of_Measure_Code() {
		return Unit_of_Measure_Code;
	}

	public void setUnit_of_Measure_Code(String unit_of_Measure_Code) {
		Unit_of_Measure_Code = unit_of_Measure_Code;
	}

	public String getUnit_of_Measure_Name() {
		return Unit_of_Measure_Name;
	}

	public void setUnit_of_Measure_Name(String unit_of_Measure_Name) {
		Unit_of_Measure_Name = unit_of_Measure_Name;
	}

	public String getInvoicing_Rule_Name() {
		return Invoicing_Rule_Name;
	}

	public void setInvoicing_Rule_Name(String invoicing_Rule_Name) {
		Invoicing_Rule_Name = invoicing_Rule_Name;
	}

	public String getRevenue_Scheduling_Rule_Name() {
		return Revenue_Scheduling_Rule_Name;
	}

	public void setRevenue_Scheduling_Rule_Name(String revenue_Scheduling_Rule_Name) {
		Revenue_Scheduling_Rule_Name = revenue_Scheduling_Rule_Name;
	}

	public String getNumber_of_Revenue_Periods() {
		return Number_of_Revenue_Periods;
	}

	public void setNumber_of_Revenue_Periods(String number_of_Revenue_Periods) {
		Number_of_Revenue_Periods = number_of_Revenue_Periods;
	}

	public String getRevenue_Scheduling_Rule_Start_Date() {
		return Revenue_Scheduling_Rule_Start_Date;
	}

	public void setRevenue_Scheduling_Rule_Start_Date(String revenue_Scheduling_Rule_Start_Date) {
		Revenue_Scheduling_Rule_Start_Date = revenue_Scheduling_Rule_Start_Date;
	}

	public String getRevenue_Scheduling_Rule_End_Date() {
		return Revenue_Scheduling_Rule_End_Date;
	}

	public void setRevenue_Scheduling_Rule_End_Date(String revenue_Scheduling_Rule_End_Date) {
		Revenue_Scheduling_Rule_End_Date = revenue_Scheduling_Rule_End_Date;
	}

	public String getReason_Code_Meaning() {
		return Reason_Code_Meaning;
	}

	public void setReason_Code_Meaning(String reason_Code_Meaning) {
		Reason_Code_Meaning = reason_Code_Meaning;
	}

	public String getLast_Period_to_Credit() {
		return Last_Period_to_Credit;
	}

	public void setLast_Period_to_Credit(String last_Period_to_Credit) {
		Last_Period_to_Credit = last_Period_to_Credit;
	}

	public String getTransaction_Business_Category_Code() {
		return Transaction_Business_Category_Code;
	}

	public void setTransaction_Business_Category_Code(String transaction_Business_Category_Code) {
		Transaction_Business_Category_Code = transaction_Business_Category_Code;
	}

	public String getProduct_Fiscal_Classification_Code() {
		return Product_Fiscal_Classification_Code;
	}

	public void setProduct_Fiscal_Classification_Code(String product_Fiscal_Classification_Code) {
		Product_Fiscal_Classification_Code = product_Fiscal_Classification_Code;
	}

	public String getProduct_Category_Code() {
		return Product_Category_Code;
	}

	public void setProduct_Category_Code(String product_Category_Code) {
		Product_Category_Code = product_Category_Code;
	}

	public String getProduct_Type() {
		return Product_Type;
	}

	public void setProduct_Type(String product_Type) {
		Product_Type = product_Type;
	}

	public String getLine_Intended_Use_Code() {
		return Line_Intended_Use_Code;
	}

	public void setLine_Intended_Use_Code(String line_Intended_Use_Code) {
		Line_Intended_Use_Code = line_Intended_Use_Code;
	}

	public String getAssessable_Value() {
		return Assessable_Value;
	}

	public void setAssessable_Value(String assessable_Value) {
		Assessable_Value = assessable_Value;
	}

	public String getDocument_Sub_Type() {
		return Document_Sub_Type;
	}

	public void setDocument_Sub_Type(String document_Sub_Type) {
		Document_Sub_Type = document_Sub_Type;
	}

	public String getDefault_Taxation_Country() {
		return Default_Taxation_Country;
	}

	public void setDefault_Taxation_Country(String default_Taxation_Country) {
		Default_Taxation_Country = default_Taxation_Country;
	}

	public String getUser_Defined_Fiscal_Classification() {
		return User_Defined_Fiscal_Classification;
	}

	public void setUser_Defined_Fiscal_Classification(String user_Defined_Fiscal_Classification) {
		User_Defined_Fiscal_Classification = user_Defined_Fiscal_Classification;
	}

	public String getTax_Invoice_Number() {
		return Tax_Invoice_Number;
	}

	public void setTax_Invoice_Number(String tax_Invoice_Number) {
		Tax_Invoice_Number = tax_Invoice_Number;
	}

	public String getTax_Invoice_Date() {
		return Tax_Invoice_Date;
	}

	public void setTax_Invoice_Date(String tax_Invoice_Date) {
		Tax_Invoice_Date = tax_Invoice_Date;
	}

	public String getTax_Regime_Code() {
		return Tax_Regime_Code;
	}

	public void setTax_Regime_Code(String tax_Regime_Code) {
		Tax_Regime_Code = tax_Regime_Code;
	}

	public String getTax() {
		return Tax;
	}

	public void setTax(String tax) {
		Tax = tax;
	}

	public String getTax_Status_Code() {
		return Tax_Status_Code;
	}

	public void setTax_Status_Code(String tax_Status_Code) {
		Tax_Status_Code = tax_Status_Code;
	}

	public String getTax_Rate_Code() {
		return Tax_Rate_Code;
	}

	public void setTax_Rate_Code(String tax_Rate_Code) {
		Tax_Rate_Code = tax_Rate_Code;
	}

	public String getTax_Jurisdiction_Code() {
		return Tax_Jurisdiction_Code;
	}

	public void setTax_Jurisdiction_Code(String tax_Jurisdiction_Code) {
		Tax_Jurisdiction_Code = tax_Jurisdiction_Code;
	}

	public String getFirst_Party_Registration_Number() {
		return First_Party_Registration_Number;
	}

	public void setFirst_Party_Registration_Number(String first_Party_Registration_Number) {
		First_Party_Registration_Number = first_Party_Registration_Number;
	}

	public String getThird_Party_Registration_Number() {
		return Third_Party_Registration_Number;
	}

	public void setThird_Party_Registration_Number(String third_Party_Registration_Number) {
		Third_Party_Registration_Number = third_Party_Registration_Number;
	}

	public String getFinal_Discharge_Location() {
		return Final_Discharge_Location;
	}

	public void setFinal_Discharge_Location(String final_Discharge_Location) {
		Final_Discharge_Location = final_Discharge_Location;
	}

	public String getTaxable_Amount() {
		return Taxable_Amount;
	}

	public void setTaxable_Amount(String taxable_Amount) {
		Taxable_Amount = taxable_Amount;
	}

	public String getTaxable_Flag() {
		return Taxable_Flag;
	}

	public void setTaxable_Flag(String taxable_Flag) {
		Taxable_Flag = taxable_Flag;
	}

	public String getTax_Exemption_Flag() {
		return Tax_Exemption_Flag;
	}

	public void setTax_Exemption_Flag(String tax_Exemption_Flag) {
		Tax_Exemption_Flag = tax_Exemption_Flag;
	}

	public String getTax_Exemption_Reason_Code() {
		return Tax_Exemption_Reason_Code;
	}

	public void setTax_Exemption_Reason_Code(String tax_Exemption_Reason_Code) {
		Tax_Exemption_Reason_Code = tax_Exemption_Reason_Code;
	}

	public String getTax_Exemption_Reason_Code_Meaning() {
		return Tax_Exemption_Reason_Code_Meaning;
	}

	public void setTax_Exemption_Reason_Code_Meaning(String tax_Exemption_Reason_Code_Meaning) {
		Tax_Exemption_Reason_Code_Meaning = tax_Exemption_Reason_Code_Meaning;
	}

	public String getTax_Exemption_Certificate_Number() {
		return Tax_Exemption_Certificate_Number;
	}

	public void setTax_Exemption_Certificate_Number(String tax_Exemption_Certificate_Number) {
		Tax_Exemption_Certificate_Number = tax_Exemption_Certificate_Number;
	}

	public String getLine_Amount_Includes_Tax_Flag() {
		return Line_Amount_Includes_Tax_Flag;
	}

	public void setLine_Amount_Includes_Tax_Flag(String line_Amount_Includes_Tax_Flag) {
		Line_Amount_Includes_Tax_Flag = line_Amount_Includes_Tax_Flag;
	}

	public String getTax_Precedence() {
		return Tax_Precedence;
	}

	public void setTax_Precedence(String tax_Precedence) {
		Tax_Precedence = tax_Precedence;
	}

	public String getCredit_Method_To_Be_Used_For_Lines_With_Revenue_Scheduling_Rules() {
		return Credit_Method_To_Be_Used_For_Lines_With_Revenue_Scheduling_Rules;
	}

	public void setCredit_Method_To_Be_Used_For_Lines_With_Revenue_Scheduling_Rules(
			String credit_Method_To_Be_Used_For_Lines_With_Revenue_Scheduling_Rules) {
		Credit_Method_To_Be_Used_For_Lines_With_Revenue_Scheduling_Rules = credit_Method_To_Be_Used_For_Lines_With_Revenue_Scheduling_Rules;
	}

	public String getCredit_Method_To_Be_Used_For_Transactions_With_Split_Payment_Terms() {
		return Credit_Method_To_Be_Used_For_Transactions_With_Split_Payment_Terms;
	}

	public void setCredit_Method_To_Be_Used_For_Transactions_With_Split_Payment_Terms(
			String credit_Method_To_Be_Used_For_Transactions_With_Split_Payment_Terms) {
		Credit_Method_To_Be_Used_For_Transactions_With_Split_Payment_Terms = credit_Method_To_Be_Used_For_Transactions_With_Split_Payment_Terms;
	}

	public String getReason_Code() {
		return Reason_Code;
	}

	public void setReason_Code(String reason_Code) {
		Reason_Code = reason_Code;
	}

	public String getTax_Rate() {
		return Tax_Rate;
	}

	public void setTax_Rate(String tax_Rate) {
		Tax_Rate = tax_Rate;
	}

	public String getFOB_Point() {
		return FOB_Point;
	}

	public void setFOB_Point(String fOB_Point) {
		FOB_Point = fOB_Point;
	}

	public String getCarrier() {
		return Carrier;
	}

	public void setCarrier(String carrier) {
		Carrier = carrier;
	}

	public String getShipping_Reference() {
		return Shipping_Reference;
	}

	public void setShipping_Reference(String shipping_Reference) {
		Shipping_Reference = shipping_Reference;
	}

	public String getSales_Order_Line_Number() {
		return Sales_Order_Line_Number;
	}

	public void setSales_Order_Line_Number(String sales_Order_Line_Number) {
		Sales_Order_Line_Number = sales_Order_Line_Number;
	}

	public String getSales_Order_Source() {
		return Sales_Order_Source;
	}

	public void setSales_Order_Source(String sales_Order_Source) {
		Sales_Order_Source = sales_Order_Source;
	}

	public String getSales_Order_Revision_Number() {
		return Sales_Order_Revision_Number;
	}

	public void setSales_Order_Revision_Number(String sales_Order_Revision_Number) {
		Sales_Order_Revision_Number = sales_Order_Revision_Number;
	}

	public String getPurchase_Order_Number() {
		return Purchase_Order_Number;
	}

	public void setPurchase_Order_Number(String purchase_Order_Number) {
		Purchase_Order_Number = purchase_Order_Number;
	}

	public String getPurchase_Order_Revision_Number() {
		return Purchase_Order_Revision_Number;
	}

	public void setPurchase_Order_Revision_Number(String purchase_Order_Revision_Number) {
		Purchase_Order_Revision_Number = purchase_Order_Revision_Number;
	}

	public String getPurchase_Order_Date() {
		return Purchase_Order_Date;
	}

	public void setPurchase_Order_Date(String purchase_Order_Date) {
		Purchase_Order_Date = purchase_Order_Date;
	}

	public String getAgreement_Name() {
		return Agreement_Name;
	}

	public void setAgreement_Name(String agreement_Name) {
		Agreement_Name = agreement_Name;
	}

	public String getMemo_Line_Name() {
		return Memo_Line_Name;
	}

	public void setMemo_Line_Name(String memo_Line_Name) {
		Memo_Line_Name = memo_Line_Name;
	}

	public String getDocument_Number() {
		return Document_Number;
	}

	public void setDocument_Number(String document_Number) {
		Document_Number = document_Number;
	}

	public String getOriginal_System_Batch_Name() {
		return Original_System_Batch_Name;
	}

	public void setOriginal_System_Batch_Name(String original_System_Batch_Name) {
		Original_System_Batch_Name = original_System_Batch_Name;
	}

	public String getLink_to_Transactions_Flexfield_Context() {
		return Link_to_Transactions_Flexfield_Context;
	}

	public void setLink_to_Transactions_Flexfield_Context(String link_to_Transactions_Flexfield_Context) {
		Link_to_Transactions_Flexfield_Context = link_to_Transactions_Flexfield_Context;
	}

	public String getLink_to_Transactions_Flexfield_Segment_1() {
		return Link_to_Transactions_Flexfield_Segment_1;
	}

	public void setLink_to_Transactions_Flexfield_Segment_1(String link_to_Transactions_Flexfield_Segment_1) {
		Link_to_Transactions_Flexfield_Segment_1 = link_to_Transactions_Flexfield_Segment_1;
	}

	public String getLink_to_Transactions_Flexfield_Segment_2() {
		return Link_to_Transactions_Flexfield_Segment_2;
	}

	public void setLink_to_Transactions_Flexfield_Segment_2(String link_to_Transactions_Flexfield_Segment_2) {
		Link_to_Transactions_Flexfield_Segment_2 = link_to_Transactions_Flexfield_Segment_2;
	}

	public String getLink_to_Transactions_Flexfield_Segment_3() {
		return Link_to_Transactions_Flexfield_Segment_3;
	}

	public void setLink_to_Transactions_Flexfield_Segment_3(String link_to_Transactions_Flexfield_Segment_3) {
		Link_to_Transactions_Flexfield_Segment_3 = link_to_Transactions_Flexfield_Segment_3;
	}

	public String getLink_to_Transactions_Flexfield_Segment_4() {
		return Link_to_Transactions_Flexfield_Segment_4;
	}

	public void setLink_to_Transactions_Flexfield_Segment_4(String link_to_Transactions_Flexfield_Segment_4) {
		Link_to_Transactions_Flexfield_Segment_4 = link_to_Transactions_Flexfield_Segment_4;
	}

	public String getLink_to_Transactions_Flexfield_Segment_5() {
		return Link_to_Transactions_Flexfield_Segment_5;
	}

	public void setLink_to_Transactions_Flexfield_Segment_5(String link_to_Transactions_Flexfield_Segment_5) {
		Link_to_Transactions_Flexfield_Segment_5 = link_to_Transactions_Flexfield_Segment_5;
	}

	public String getLink_to_Transactions_Flexfield_Segment_6() {
		return Link_to_Transactions_Flexfield_Segment_6;
	}

	public void setLink_to_Transactions_Flexfield_Segment_6(String link_to_Transactions_Flexfield_Segment_6) {
		Link_to_Transactions_Flexfield_Segment_6 = link_to_Transactions_Flexfield_Segment_6;
	}

	public String getLink_to_Transactions_Flexfield_Segment_7() {
		return Link_to_Transactions_Flexfield_Segment_7;
	}

	public void setLink_to_Transactions_Flexfield_Segment_7(String link_to_Transactions_Flexfield_Segment_7) {
		Link_to_Transactions_Flexfield_Segment_7 = link_to_Transactions_Flexfield_Segment_7;
	}

	public String getLink_to_Transactions_Flexfield_Segment_8() {
		return Link_to_Transactions_Flexfield_Segment_8;
	}

	public void setLink_to_Transactions_Flexfield_Segment_8(String link_to_Transactions_Flexfield_Segment_8) {
		Link_to_Transactions_Flexfield_Segment_8 = link_to_Transactions_Flexfield_Segment_8;
	}

	public String getLink_to_Transactions_Flexfield_Segment_9() {
		return Link_to_Transactions_Flexfield_Segment_9;
	}

	public void setLink_to_Transactions_Flexfield_Segment_9(String link_to_Transactions_Flexfield_Segment_9) {
		Link_to_Transactions_Flexfield_Segment_9 = link_to_Transactions_Flexfield_Segment_9;
	}

	public String getLink_to_Transactions_Flexfield_Segment_10() {
		return Link_to_Transactions_Flexfield_Segment_10;
	}

	public void setLink_to_Transactions_Flexfield_Segment_10(String link_to_Transactions_Flexfield_Segment_10) {
		Link_to_Transactions_Flexfield_Segment_10 = link_to_Transactions_Flexfield_Segment_10;
	}

	public String getLink_to_Transactions_Flexfield_Segment_11() {
		return Link_to_Transactions_Flexfield_Segment_11;
	}

	public void setLink_to_Transactions_Flexfield_Segment_11(String link_to_Transactions_Flexfield_Segment_11) {
		Link_to_Transactions_Flexfield_Segment_11 = link_to_Transactions_Flexfield_Segment_11;
	}

	public String getLink_to_Transactions_Flexfield_Segment_12() {
		return Link_to_Transactions_Flexfield_Segment_12;
	}

	public void setLink_to_Transactions_Flexfield_Segment_12(String link_to_Transactions_Flexfield_Segment_12) {
		Link_to_Transactions_Flexfield_Segment_12 = link_to_Transactions_Flexfield_Segment_12;
	}

	public String getLink_to_Transactions_Flexfield_Segment_13() {
		return Link_to_Transactions_Flexfield_Segment_13;
	}

	public void setLink_to_Transactions_Flexfield_Segment_13(String link_to_Transactions_Flexfield_Segment_13) {
		Link_to_Transactions_Flexfield_Segment_13 = link_to_Transactions_Flexfield_Segment_13;
	}

	public String getLink_to_Transactions_Flexfield_Segment_14() {
		return Link_to_Transactions_Flexfield_Segment_14;
	}

	public void setLink_to_Transactions_Flexfield_Segment_14(String link_to_Transactions_Flexfield_Segment_14) {
		Link_to_Transactions_Flexfield_Segment_14 = link_to_Transactions_Flexfield_Segment_14;
	}

	public String getLink_to_Transactions_Flexfield_Segment_15() {
		return Link_to_Transactions_Flexfield_Segment_15;
	}

	public void setLink_to_Transactions_Flexfield_Segment_15(String link_to_Transactions_Flexfield_Segment_15) {
		Link_to_Transactions_Flexfield_Segment_15 = link_to_Transactions_Flexfield_Segment_15;
	}

	public String getReference_Transactions_Flexfield_Context() {
		return Reference_Transactions_Flexfield_Context;
	}

	public void setReference_Transactions_Flexfield_Context(String reference_Transactions_Flexfield_Context) {
		Reference_Transactions_Flexfield_Context = reference_Transactions_Flexfield_Context;
	}

	public String getReference_Transactions_Flexfield_Segment_1() {
		return Reference_Transactions_Flexfield_Segment_1;
	}

	public void setReference_Transactions_Flexfield_Segment_1(String reference_Transactions_Flexfield_Segment_1) {
		Reference_Transactions_Flexfield_Segment_1 = reference_Transactions_Flexfield_Segment_1;
	}

	public String getReference_Transactions_Flexfield_Segment_2() {
		return Reference_Transactions_Flexfield_Segment_2;
	}

	public void setReference_Transactions_Flexfield_Segment_2(String reference_Transactions_Flexfield_Segment_2) {
		Reference_Transactions_Flexfield_Segment_2 = reference_Transactions_Flexfield_Segment_2;
	}

	public String getReference_Transactions_Flexfield_Segment_3() {
		return Reference_Transactions_Flexfield_Segment_3;
	}

	public void setReference_Transactions_Flexfield_Segment_3(String reference_Transactions_Flexfield_Segment_3) {
		Reference_Transactions_Flexfield_Segment_3 = reference_Transactions_Flexfield_Segment_3;
	}

	public String getReference_Transactions_Flexfield_Segment_4() {
		return Reference_Transactions_Flexfield_Segment_4;
	}

	public void setReference_Transactions_Flexfield_Segment_4(String reference_Transactions_Flexfield_Segment_4) {
		Reference_Transactions_Flexfield_Segment_4 = reference_Transactions_Flexfield_Segment_4;
	}

	public String getReference_Transactions_Flexfield_Segment_5() {
		return Reference_Transactions_Flexfield_Segment_5;
	}

	public void setReference_Transactions_Flexfield_Segment_5(String reference_Transactions_Flexfield_Segment_5) {
		Reference_Transactions_Flexfield_Segment_5 = reference_Transactions_Flexfield_Segment_5;
	}

	public String getReference_Transactions_Flexfield_Segment_6() {
		return Reference_Transactions_Flexfield_Segment_6;
	}

	public void setReference_Transactions_Flexfield_Segment_6(String reference_Transactions_Flexfield_Segment_6) {
		Reference_Transactions_Flexfield_Segment_6 = reference_Transactions_Flexfield_Segment_6;
	}

	public String getReference_Transactions_Flexfield_Segment_7() {
		return Reference_Transactions_Flexfield_Segment_7;
	}

	public void setReference_Transactions_Flexfield_Segment_7(String reference_Transactions_Flexfield_Segment_7) {
		Reference_Transactions_Flexfield_Segment_7 = reference_Transactions_Flexfield_Segment_7;
	}

	public String getReference_Transactions_Flexfield_Segment_8() {
		return Reference_Transactions_Flexfield_Segment_8;
	}

	public void setReference_Transactions_Flexfield_Segment_8(String reference_Transactions_Flexfield_Segment_8) {
		Reference_Transactions_Flexfield_Segment_8 = reference_Transactions_Flexfield_Segment_8;
	}

	public String getReference_Transactions_Flexfield_Segment_9() {
		return Reference_Transactions_Flexfield_Segment_9;
	}

	public void setReference_Transactions_Flexfield_Segment_9(String reference_Transactions_Flexfield_Segment_9) {
		Reference_Transactions_Flexfield_Segment_9 = reference_Transactions_Flexfield_Segment_9;
	}

	public String getReference_Transactions_Flexfield_Segment_10() {
		return Reference_Transactions_Flexfield_Segment_10;
	}

	public void setReference_Transactions_Flexfield_Segment_10(String reference_Transactions_Flexfield_Segment_10) {
		Reference_Transactions_Flexfield_Segment_10 = reference_Transactions_Flexfield_Segment_10;
	}

	public String getReference_Transactions_Flexfield_Segment_11() {
		return Reference_Transactions_Flexfield_Segment_11;
	}

	public void setReference_Transactions_Flexfield_Segment_11(String reference_Transactions_Flexfield_Segment_11) {
		Reference_Transactions_Flexfield_Segment_11 = reference_Transactions_Flexfield_Segment_11;
	}

	public String getReference_Transactions_Flexfield_Segment_12() {
		return Reference_Transactions_Flexfield_Segment_12;
	}

	public void setReference_Transactions_Flexfield_Segment_12(String reference_Transactions_Flexfield_Segment_12) {
		Reference_Transactions_Flexfield_Segment_12 = reference_Transactions_Flexfield_Segment_12;
	}

	public String getReference_Transactions_Flexfield_Segment_13() {
		return Reference_Transactions_Flexfield_Segment_13;
	}

	public void setReference_Transactions_Flexfield_Segment_13(String reference_Transactions_Flexfield_Segment_13) {
		Reference_Transactions_Flexfield_Segment_13 = reference_Transactions_Flexfield_Segment_13;
	}

	public String getReference_Transactions_Flexfield_Segment_14() {
		return Reference_Transactions_Flexfield_Segment_14;
	}

	public void setReference_Transactions_Flexfield_Segment_14(String reference_Transactions_Flexfield_Segment_14) {
		Reference_Transactions_Flexfield_Segment_14 = reference_Transactions_Flexfield_Segment_14;
	}

	public String getReference_Transactions_Flexfield_Segment_15() {
		return Reference_Transactions_Flexfield_Segment_15;
	}

	public void setReference_Transactions_Flexfield_Segment_15(String reference_Transactions_Flexfield_Segment_15) {
		Reference_Transactions_Flexfield_Segment_15 = reference_Transactions_Flexfield_Segment_15;
	}

	public String getLink_To_Parent_Line_Context() {
		return Link_To_Parent_Line_Context;
	}

	public void setLink_To_Parent_Line_Context(String link_To_Parent_Line_Context) {
		Link_To_Parent_Line_Context = link_To_Parent_Line_Context;
	}

	public String getLink_To_Parent_Line_Segment_1() {
		return Link_To_Parent_Line_Segment_1;
	}

	public void setLink_To_Parent_Line_Segment_1(String link_To_Parent_Line_Segment_1) {
		Link_To_Parent_Line_Segment_1 = link_To_Parent_Line_Segment_1;
	}

	public String getLink_To_Parent_Line_Segment_2() {
		return Link_To_Parent_Line_Segment_2;
	}

	public void setLink_To_Parent_Line_Segment_2(String link_To_Parent_Line_Segment_2) {
		Link_To_Parent_Line_Segment_2 = link_To_Parent_Line_Segment_2;
	}

	public String getLink_To_Parent_Line_Segment_3() {
		return Link_To_Parent_Line_Segment_3;
	}

	public void setLink_To_Parent_Line_Segment_3(String link_To_Parent_Line_Segment_3) {
		Link_To_Parent_Line_Segment_3 = link_To_Parent_Line_Segment_3;
	}

	public String getLink_To_Parent_Line_Segment_4() {
		return Link_To_Parent_Line_Segment_4;
	}

	public void setLink_To_Parent_Line_Segment_4(String link_To_Parent_Line_Segment_4) {
		Link_To_Parent_Line_Segment_4 = link_To_Parent_Line_Segment_4;
	}

	public String getLink_To_Parent_Line_Segment_5() {
		return Link_To_Parent_Line_Segment_5;
	}

	public void setLink_To_Parent_Line_Segment_5(String link_To_Parent_Line_Segment_5) {
		Link_To_Parent_Line_Segment_5 = link_To_Parent_Line_Segment_5;
	}

	public String getLink_To_Parent_Line_Segment_6() {
		return Link_To_Parent_Line_Segment_6;
	}

	public void setLink_To_Parent_Line_Segment_6(String link_To_Parent_Line_Segment_6) {
		Link_To_Parent_Line_Segment_6 = link_To_Parent_Line_Segment_6;
	}

	public String getLink_To_Parent_Line_Segment_7() {
		return Link_To_Parent_Line_Segment_7;
	}

	public void setLink_To_Parent_Line_Segment_7(String link_To_Parent_Line_Segment_7) {
		Link_To_Parent_Line_Segment_7 = link_To_Parent_Line_Segment_7;
	}

	public String getLink_To_Parent_Line_Segment_8() {
		return Link_To_Parent_Line_Segment_8;
	}

	public void setLink_To_Parent_Line_Segment_8(String link_To_Parent_Line_Segment_8) {
		Link_To_Parent_Line_Segment_8 = link_To_Parent_Line_Segment_8;
	}

	public String getLink_To_Parent_Line_Segment_9() {
		return Link_To_Parent_Line_Segment_9;
	}

	public void setLink_To_Parent_Line_Segment_9(String link_To_Parent_Line_Segment_9) {
		Link_To_Parent_Line_Segment_9 = link_To_Parent_Line_Segment_9;
	}

	public String getLink_To_Parent_Line_Segment_10() {
		return Link_To_Parent_Line_Segment_10;
	}

	public void setLink_To_Parent_Line_Segment_10(String link_To_Parent_Line_Segment_10) {
		Link_To_Parent_Line_Segment_10 = link_To_Parent_Line_Segment_10;
	}

	public String getLink_To_Parent_Line_Segment_11() {
		return Link_To_Parent_Line_Segment_11;
	}

	public void setLink_To_Parent_Line_Segment_11(String link_To_Parent_Line_Segment_11) {
		Link_To_Parent_Line_Segment_11 = link_To_Parent_Line_Segment_11;
	}

	public String getLink_To_Parent_Line_Segment_12() {
		return Link_To_Parent_Line_Segment_12;
	}

	public void setLink_To_Parent_Line_Segment_12(String link_To_Parent_Line_Segment_12) {
		Link_To_Parent_Line_Segment_12 = link_To_Parent_Line_Segment_12;
	}

	public String getLink_To_Parent_Line_Segment_13() {
		return Link_To_Parent_Line_Segment_13;
	}

	public void setLink_To_Parent_Line_Segment_13(String link_To_Parent_Line_Segment_13) {
		Link_To_Parent_Line_Segment_13 = link_To_Parent_Line_Segment_13;
	}

	public String getLink_To_Parent_Line_Segment_14() {
		return Link_To_Parent_Line_Segment_14;
	}

	public void setLink_To_Parent_Line_Segment_14(String link_To_Parent_Line_Segment_14) {
		Link_To_Parent_Line_Segment_14 = link_To_Parent_Line_Segment_14;
	}

	public String getLink_To_Parent_Line_Segment_15() {
		return Link_To_Parent_Line_Segment_15;
	}

	public void setLink_To_Parent_Line_Segment_15(String link_To_Parent_Line_Segment_15) {
		Link_To_Parent_Line_Segment_15 = link_To_Parent_Line_Segment_15;
	}

	public String getReceipt_Method_Name() {
		return Receipt_Method_Name;
	}

	public void setReceipt_Method_Name(String receipt_Method_Name) {
		Receipt_Method_Name = receipt_Method_Name;
	}

	public String getPrinting_Option() {
		return Printing_Option;
	}

	public void setPrinting_Option(String printing_Option) {
		Printing_Option = printing_Option;
	}

	public String getRelated_Batch_Source_Name() {
		return Related_Batch_Source_Name;
	}

	public void setRelated_Batch_Source_Name(String related_Batch_Source_Name) {
		Related_Batch_Source_Name = related_Batch_Source_Name;
	}

	public String getRelated_Transaction_Number() {
		return Related_Transaction_Number;
	}

	public void setRelated_Transaction_Number(String related_Transaction_Number) {
		Related_Transaction_Number = related_Transaction_Number;
	}

	public String getInventory_Item_Number() {
		return Inventory_Item_Number;
	}

	public void setInventory_Item_Number(String inventory_Item_Number) {
		Inventory_Item_Number = inventory_Item_Number;
	}

	public String getInventory_Item_Segment_2() {
		return Inventory_Item_Segment_2;
	}

	public void setInventory_Item_Segment_2(String inventory_Item_Segment_2) {
		Inventory_Item_Segment_2 = inventory_Item_Segment_2;
	}

	public String getInventory_Item_Segment_3() {
		return Inventory_Item_Segment_3;
	}

	public void setInventory_Item_Segment_3(String inventory_Item_Segment_3) {
		Inventory_Item_Segment_3 = inventory_Item_Segment_3;
	}

	public String getInventory_Item_Segment_4() {
		return Inventory_Item_Segment_4;
	}

	public void setInventory_Item_Segment_4(String inventory_Item_Segment_4) {
		Inventory_Item_Segment_4 = inventory_Item_Segment_4;
	}

	public String getInventory_Item_Segment_5() {
		return Inventory_Item_Segment_5;
	}

	public void setInventory_Item_Segment_5(String inventory_Item_Segment_5) {
		Inventory_Item_Segment_5 = inventory_Item_Segment_5;
	}

	public String getInventory_Item_Segment_6() {
		return Inventory_Item_Segment_6;
	}

	public void setInventory_Item_Segment_6(String inventory_Item_Segment_6) {
		Inventory_Item_Segment_6 = inventory_Item_Segment_6;
	}

	public String getInventory_Item_Segment_7() {
		return Inventory_Item_Segment_7;
	}

	public void setInventory_Item_Segment_7(String inventory_Item_Segment_7) {
		Inventory_Item_Segment_7 = inventory_Item_Segment_7;
	}

	public String getInventory_Item_Segment_8() {
		return Inventory_Item_Segment_8;
	}

	public void setInventory_Item_Segment_8(String inventory_Item_Segment_8) {
		Inventory_Item_Segment_8 = inventory_Item_Segment_8;
	}

	public String getInventory_Item_Segment_9() {
		return Inventory_Item_Segment_9;
	}

	public void setInventory_Item_Segment_9(String inventory_Item_Segment_9) {
		Inventory_Item_Segment_9 = inventory_Item_Segment_9;
	}

	public String getInventory_Item_Segment_10() {
		return Inventory_Item_Segment_10;
	}

	public void setInventory_Item_Segment_10(String inventory_Item_Segment_10) {
		Inventory_Item_Segment_10 = inventory_Item_Segment_10;
	}

	public String getInventory_Item_Segment_11() {
		return Inventory_Item_Segment_11;
	}

	public void setInventory_Item_Segment_11(String inventory_Item_Segment_11) {
		Inventory_Item_Segment_11 = inventory_Item_Segment_11;
	}

	public String getInventory_Item_Segment_12() {
		return Inventory_Item_Segment_12;
	}

	public void setInventory_Item_Segment_12(String inventory_Item_Segment_12) {
		Inventory_Item_Segment_12 = inventory_Item_Segment_12;
	}

	public String getInventory_Item_Segment_13() {
		return Inventory_Item_Segment_13;
	}

	public void setInventory_Item_Segment_13(String inventory_Item_Segment_13) {
		Inventory_Item_Segment_13 = inventory_Item_Segment_13;
	}

	public String getInventory_Item_Segment_14() {
		return Inventory_Item_Segment_14;
	}

	public void setInventory_Item_Segment_14(String inventory_Item_Segment_14) {
		Inventory_Item_Segment_14 = inventory_Item_Segment_14;
	}

	public String getInventory_Item_Segment_15() {
		return Inventory_Item_Segment_15;
	}

	public void setInventory_Item_Segment_15(String inventory_Item_Segment_15) {
		Inventory_Item_Segment_15 = inventory_Item_Segment_15;
	}

	public String getInventory_Item_Segment_16() {
		return Inventory_Item_Segment_16;
	}

	public void setInventory_Item_Segment_16(String inventory_Item_Segment_16) {
		Inventory_Item_Segment_16 = inventory_Item_Segment_16;
	}

	public String getInventory_Item_Segment_17() {
		return Inventory_Item_Segment_17;
	}

	public void setInventory_Item_Segment_17(String inventory_Item_Segment_17) {
		Inventory_Item_Segment_17 = inventory_Item_Segment_17;
	}

	public String getInventory_Item_Segment_18() {
		return Inventory_Item_Segment_18;
	}

	public void setInventory_Item_Segment_18(String inventory_Item_Segment_18) {
		Inventory_Item_Segment_18 = inventory_Item_Segment_18;
	}

	public String getInventory_Item_Segment_19() {
		return Inventory_Item_Segment_19;
	}

	public void setInventory_Item_Segment_19(String inventory_Item_Segment_19) {
		Inventory_Item_Segment_19 = inventory_Item_Segment_19;
	}

	public String getInventory_Item_Segment_20() {
		return Inventory_Item_Segment_20;
	}

	public void setInventory_Item_Segment_20(String inventory_Item_Segment_20) {
		Inventory_Item_Segment_20 = inventory_Item_Segment_20;
	}

	public String getBill_To_Customer_Bank_Account_Name() {
		return Bill_To_Customer_Bank_Account_Name;
	}

	public void setBill_To_Customer_Bank_Account_Name(String bill_To_Customer_Bank_Account_Name) {
		Bill_To_Customer_Bank_Account_Name = bill_To_Customer_Bank_Account_Name;
	}

	public String getReset_Transaction_Date_Flag() {
		return Reset_Transaction_Date_Flag;
	}

	public void setReset_Transaction_Date_Flag(String reset_Transaction_Date_Flag) {
		Reset_Transaction_Date_Flag = reset_Transaction_Date_Flag;
	}

	public String getPayment_Server_Order_Number() {
		return Payment_Server_Order_Number;
	}

	public void setPayment_Server_Order_Number(String payment_Server_Order_Number) {
		Payment_Server_Order_Number = payment_Server_Order_Number;
	}

	public String getLast_Transaction_on_Debit_Authorization() {
		return Last_Transaction_on_Debit_Authorization;
	}

	public void setLast_Transaction_on_Debit_Authorization(String last_Transaction_on_Debit_Authorization) {
		Last_Transaction_on_Debit_Authorization = last_Transaction_on_Debit_Authorization;
	}

	public String getApproval_Code() {
		return Approval_Code;
	}

	public void setApproval_Code(String approval_Code) {
		Approval_Code = approval_Code;
	}

	public String getAddress_Verification_Code() {
		return Address_Verification_Code;
	}

	public void setAddress_Verification_Code(String address_Verification_Code) {
		Address_Verification_Code = address_Verification_Code;
	}

	public String getTransaction_Line_Translated_Description() {
		return Transaction_Line_Translated_Description;
	}

	public void setTransaction_Line_Translated_Description(String transaction_Line_Translated_Description) {
		Transaction_Line_Translated_Description = transaction_Line_Translated_Description;
	}

	public String getConsolidated_Billing_Number() {
		return Consolidated_Billing_Number;
	}

	public void setConsolidated_Billing_Number(String consolidated_Billing_Number) {
		Consolidated_Billing_Number = consolidated_Billing_Number;
	}

	public String getPromised_Commitment_Amount() {
		return Promised_Commitment_Amount;
	}

	public void setPromised_Commitment_Amount(String promised_Commitment_Amount) {
		Promised_Commitment_Amount = promised_Commitment_Amount;
	}

	public String getPayment_Set_Identifier() {
		return Payment_Set_Identifier;
	}

	public void setPayment_Set_Identifier(String payment_Set_Identifier) {
		Payment_Set_Identifier = payment_Set_Identifier;
	}

	public String getOriginal_Accounting_Date() {
		return Original_Accounting_Date;
	}

	public void setOriginal_Accounting_Date(String original_Accounting_Date) {
		Original_Accounting_Date = original_Accounting_Date;
	}

	public String getInvoiced_Line_Accounting_Level() {
		return Invoiced_Line_Accounting_Level;
	}

	public void setInvoiced_Line_Accounting_Level(String invoiced_Line_Accounting_Level) {
		Invoiced_Line_Accounting_Level = invoiced_Line_Accounting_Level;
	}

	public String getOverride_AutoAccounting_Flag() {
		return Override_AutoAccounting_Flag;
	}

	public void setOverride_AutoAccounting_Flag(String override_AutoAccounting_Flag) {
		Override_AutoAccounting_Flag = override_AutoAccounting_Flag;
	}

	public String getHistorical_Flag() {
		return Historical_Flag;
	}

	public void setHistorical_Flag(String historical_Flag) {
		Historical_Flag = historical_Flag;
	}

	public String getDeferral_Exclusion_Flag() {
		return Deferral_Exclusion_Flag;
	}

	public void setDeferral_Exclusion_Flag(String deferral_Exclusion_Flag) {
		Deferral_Exclusion_Flag = deferral_Exclusion_Flag;
	}

	public String getPayment_Attributes() {
		return Payment_Attributes;
	}

	public void setPayment_Attributes(String payment_Attributes) {
		Payment_Attributes = payment_Attributes;
	}

	public String getInvoice_Billing_Date() {
		return Invoice_Billing_Date;
	}

	public void setInvoice_Billing_Date(String invoice_Billing_Date) {
		Invoice_Billing_Date = invoice_Billing_Date;
	}

	public String getInvoice_Lines_Flexfield_Context() {
		return Invoice_Lines_Flexfield_Context;
	}

	public void setInvoice_Lines_Flexfield_Context(String invoice_Lines_Flexfield_Context) {
		Invoice_Lines_Flexfield_Context = invoice_Lines_Flexfield_Context;
	}

	public String getInvoice_Lines_Flexfield_Segment_1() {
		return Invoice_Lines_Flexfield_Segment_1;
	}

	public void setInvoice_Lines_Flexfield_Segment_1(String invoice_Lines_Flexfield_Segment_1) {
		Invoice_Lines_Flexfield_Segment_1 = invoice_Lines_Flexfield_Segment_1;
	}

	public String getInvoice_Lines_Flexfield_Segment_2() {
		return Invoice_Lines_Flexfield_Segment_2;
	}

	public void setInvoice_Lines_Flexfield_Segment_2(String invoice_Lines_Flexfield_Segment_2) {
		Invoice_Lines_Flexfield_Segment_2 = invoice_Lines_Flexfield_Segment_2;
	}

	public String getInvoice_Lines_Flexfield_Segment_3() {
		return Invoice_Lines_Flexfield_Segment_3;
	}

	public void setInvoice_Lines_Flexfield_Segment_3(String invoice_Lines_Flexfield_Segment_3) {
		Invoice_Lines_Flexfield_Segment_3 = invoice_Lines_Flexfield_Segment_3;
	}

	public String getInvoice_Lines_Flexfield_Segment_4() {
		return Invoice_Lines_Flexfield_Segment_4;
	}

	public void setInvoice_Lines_Flexfield_Segment_4(String invoice_Lines_Flexfield_Segment_4) {
		Invoice_Lines_Flexfield_Segment_4 = invoice_Lines_Flexfield_Segment_4;
	}

	public String getInvoice_Lines_Flexfield_Segment_5() {
		return Invoice_Lines_Flexfield_Segment_5;
	}

	public void setInvoice_Lines_Flexfield_Segment_5(String invoice_Lines_Flexfield_Segment_5) {
		Invoice_Lines_Flexfield_Segment_5 = invoice_Lines_Flexfield_Segment_5;
	}

	public String getInvoice_Lines_Flexfield_Segment_6() {
		return Invoice_Lines_Flexfield_Segment_6;
	}

	public void setInvoice_Lines_Flexfield_Segment_6(String invoice_Lines_Flexfield_Segment_6) {
		Invoice_Lines_Flexfield_Segment_6 = invoice_Lines_Flexfield_Segment_6;
	}

	public String getInvoice_Lines_Flexfield_Segment_7() {
		return Invoice_Lines_Flexfield_Segment_7;
	}

	public void setInvoice_Lines_Flexfield_Segment_7(String invoice_Lines_Flexfield_Segment_7) {
		Invoice_Lines_Flexfield_Segment_7 = invoice_Lines_Flexfield_Segment_7;
	}

	public String getInvoice_Lines_Flexfield_Segment_8() {
		return Invoice_Lines_Flexfield_Segment_8;
	}

	public void setInvoice_Lines_Flexfield_Segment_8(String invoice_Lines_Flexfield_Segment_8) {
		Invoice_Lines_Flexfield_Segment_8 = invoice_Lines_Flexfield_Segment_8;
	}

	public String getInvoice_Lines_Flexfield_Segment_9() {
		return Invoice_Lines_Flexfield_Segment_9;
	}

	public void setInvoice_Lines_Flexfield_Segment_9(String invoice_Lines_Flexfield_Segment_9) {
		Invoice_Lines_Flexfield_Segment_9 = invoice_Lines_Flexfield_Segment_9;
	}

	public String getInvoice_Lines_Flexfield_Segment_10() {
		return Invoice_Lines_Flexfield_Segment_10;
	}

	public void setInvoice_Lines_Flexfield_Segment_10(String invoice_Lines_Flexfield_Segment_10) {
		Invoice_Lines_Flexfield_Segment_10 = invoice_Lines_Flexfield_Segment_10;
	}

	public String getInvoice_Lines_Flexfield_Segment_11() {
		return Invoice_Lines_Flexfield_Segment_11;
	}

	public void setInvoice_Lines_Flexfield_Segment_11(String invoice_Lines_Flexfield_Segment_11) {
		Invoice_Lines_Flexfield_Segment_11 = invoice_Lines_Flexfield_Segment_11;
	}

	public String getInvoice_Lines_Flexfield_Segment_12() {
		return Invoice_Lines_Flexfield_Segment_12;
	}

	public void setInvoice_Lines_Flexfield_Segment_12(String invoice_Lines_Flexfield_Segment_12) {
		Invoice_Lines_Flexfield_Segment_12 = invoice_Lines_Flexfield_Segment_12;
	}

	public String getInvoice_Lines_Flexfield_Segment_13() {
		return Invoice_Lines_Flexfield_Segment_13;
	}

	public void setInvoice_Lines_Flexfield_Segment_13(String invoice_Lines_Flexfield_Segment_13) {
		Invoice_Lines_Flexfield_Segment_13 = invoice_Lines_Flexfield_Segment_13;
	}

	public String getInvoice_Lines_Flexfield_Segment_14() {
		return Invoice_Lines_Flexfield_Segment_14;
	}

	public void setInvoice_Lines_Flexfield_Segment_14(String invoice_Lines_Flexfield_Segment_14) {
		Invoice_Lines_Flexfield_Segment_14 = invoice_Lines_Flexfield_Segment_14;
	}

	public String getInvoice_Lines_Flexfield_Segment_15() {
		return Invoice_Lines_Flexfield_Segment_15;
	}

	public void setInvoice_Lines_Flexfield_Segment_15(String invoice_Lines_Flexfield_Segment_15) {
		Invoice_Lines_Flexfield_Segment_15 = invoice_Lines_Flexfield_Segment_15;
	}

	public String getInvoice_Transactions_Flexfield_Context() {
		return Invoice_Transactions_Flexfield_Context;
	}

	public void setInvoice_Transactions_Flexfield_Context(String invoice_Transactions_Flexfield_Context) {
		Invoice_Transactions_Flexfield_Context = invoice_Transactions_Flexfield_Context;
	}

	public String getInvoice_Transactions_Flexfield_Segment_1() {
		return Invoice_Transactions_Flexfield_Segment_1;
	}

	public void setInvoice_Transactions_Flexfield_Segment_1(String invoice_Transactions_Flexfield_Segment_1) {
		Invoice_Transactions_Flexfield_Segment_1 = invoice_Transactions_Flexfield_Segment_1;
	}

	public String getInvoice_Transactions_Flexfield_Segment_2() {
		return Invoice_Transactions_Flexfield_Segment_2;
	}

	public void setInvoice_Transactions_Flexfield_Segment_2(String invoice_Transactions_Flexfield_Segment_2) {
		Invoice_Transactions_Flexfield_Segment_2 = invoice_Transactions_Flexfield_Segment_2;
	}

	public String getInvoice_Transactions_Flexfield_Segment_3() {
		return Invoice_Transactions_Flexfield_Segment_3;
	}

	public void setInvoice_Transactions_Flexfield_Segment_3(String invoice_Transactions_Flexfield_Segment_3) {
		Invoice_Transactions_Flexfield_Segment_3 = invoice_Transactions_Flexfield_Segment_3;
	}

	public String getInvoice_Transactions_Flexfield_Segment_4() {
		return Invoice_Transactions_Flexfield_Segment_4;
	}

	public void setInvoice_Transactions_Flexfield_Segment_4(String invoice_Transactions_Flexfield_Segment_4) {
		Invoice_Transactions_Flexfield_Segment_4 = invoice_Transactions_Flexfield_Segment_4;
	}

	public String getInvoice_Transactions_Flexfield_Segment_5() {
		return Invoice_Transactions_Flexfield_Segment_5;
	}

	public void setInvoice_Transactions_Flexfield_Segment_5(String invoice_Transactions_Flexfield_Segment_5) {
		Invoice_Transactions_Flexfield_Segment_5 = invoice_Transactions_Flexfield_Segment_5;
	}

	public String getInvoice_Transactions_Flexfield_Segment_6() {
		return Invoice_Transactions_Flexfield_Segment_6;
	}

	public void setInvoice_Transactions_Flexfield_Segment_6(String invoice_Transactions_Flexfield_Segment_6) {
		Invoice_Transactions_Flexfield_Segment_6 = invoice_Transactions_Flexfield_Segment_6;
	}

	public String getInvoice_Transactions_Flexfield_Segment_7() {
		return Invoice_Transactions_Flexfield_Segment_7;
	}

	public void setInvoice_Transactions_Flexfield_Segment_7(String invoice_Transactions_Flexfield_Segment_7) {
		Invoice_Transactions_Flexfield_Segment_7 = invoice_Transactions_Flexfield_Segment_7;
	}

	public String getInvoice_Transactions_Flexfield_Segment_8() {
		return Invoice_Transactions_Flexfield_Segment_8;
	}

	public void setInvoice_Transactions_Flexfield_Segment_8(String invoice_Transactions_Flexfield_Segment_8) {
		Invoice_Transactions_Flexfield_Segment_8 = invoice_Transactions_Flexfield_Segment_8;
	}

	public String getInvoice_Transactions_Flexfield_Segment_9() {
		return Invoice_Transactions_Flexfield_Segment_9;
	}

	public void setInvoice_Transactions_Flexfield_Segment_9(String invoice_Transactions_Flexfield_Segment_9) {
		Invoice_Transactions_Flexfield_Segment_9 = invoice_Transactions_Flexfield_Segment_9;
	}

	public String getInvoice_Transactions_Flexfield_Segment_10() {
		return Invoice_Transactions_Flexfield_Segment_10;
	}

	public void setInvoice_Transactions_Flexfield_Segment_10(String invoice_Transactions_Flexfield_Segment_10) {
		Invoice_Transactions_Flexfield_Segment_10 = invoice_Transactions_Flexfield_Segment_10;
	}

	public String getInvoice_Transactions_Flexfield_Segment_11() {
		return Invoice_Transactions_Flexfield_Segment_11;
	}

	public void setInvoice_Transactions_Flexfield_Segment_11(String invoice_Transactions_Flexfield_Segment_11) {
		Invoice_Transactions_Flexfield_Segment_11 = invoice_Transactions_Flexfield_Segment_11;
	}

	public String getInvoice_Transactions_Flexfield_Segment_12() {
		return Invoice_Transactions_Flexfield_Segment_12;
	}

	public void setInvoice_Transactions_Flexfield_Segment_12(String invoice_Transactions_Flexfield_Segment_12) {
		Invoice_Transactions_Flexfield_Segment_12 = invoice_Transactions_Flexfield_Segment_12;
	}

	public String getInvoice_Transactions_Flexfield_Segment_13() {
		return Invoice_Transactions_Flexfield_Segment_13;
	}

	public void setInvoice_Transactions_Flexfield_Segment_13(String invoice_Transactions_Flexfield_Segment_13) {
		Invoice_Transactions_Flexfield_Segment_13 = invoice_Transactions_Flexfield_Segment_13;
	}

	public String getInvoice_Transactions_Flexfield_Segment_14() {
		return Invoice_Transactions_Flexfield_Segment_14;
	}

	public void setInvoice_Transactions_Flexfield_Segment_14(String invoice_Transactions_Flexfield_Segment_14) {
		Invoice_Transactions_Flexfield_Segment_14 = invoice_Transactions_Flexfield_Segment_14;
	}

	public String getInvoice_Transactions_Flexfield_Segment_15() {
		return Invoice_Transactions_Flexfield_Segment_15;
	}

	public void setInvoice_Transactions_Flexfield_Segment_15(String invoice_Transactions_Flexfield_Segment_15) {
		Invoice_Transactions_Flexfield_Segment_15 = invoice_Transactions_Flexfield_Segment_15;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Context() {
		return Receivables_Transaction_Region_Information_Flexfield_Context;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Context(
			String receivables_Transaction_Region_Information_Flexfield_Context) {
		Receivables_Transaction_Region_Information_Flexfield_Context = receivables_Transaction_Region_Information_Flexfield_Context;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_1() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_1;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_1(
			String receivables_Transaction_Region_Information_Flexfield_Segment_1) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_1 = receivables_Transaction_Region_Information_Flexfield_Segment_1;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_2() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_2;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_2(
			String receivables_Transaction_Region_Information_Flexfield_Segment_2) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_2 = receivables_Transaction_Region_Information_Flexfield_Segment_2;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_3() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_3;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_3(
			String receivables_Transaction_Region_Information_Flexfield_Segment_3) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_3 = receivables_Transaction_Region_Information_Flexfield_Segment_3;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_4() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_4;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_4(
			String receivables_Transaction_Region_Information_Flexfield_Segment_4) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_4 = receivables_Transaction_Region_Information_Flexfield_Segment_4;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_5() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_5;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_5(
			String receivables_Transaction_Region_Information_Flexfield_Segment_5) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_5 = receivables_Transaction_Region_Information_Flexfield_Segment_5;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_6() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_6;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_6(
			String receivables_Transaction_Region_Information_Flexfield_Segment_6) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_6 = receivables_Transaction_Region_Information_Flexfield_Segment_6;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_7() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_7;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_7(
			String receivables_Transaction_Region_Information_Flexfield_Segment_7) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_7 = receivables_Transaction_Region_Information_Flexfield_Segment_7;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_8() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_8;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_8(
			String receivables_Transaction_Region_Information_Flexfield_Segment_8) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_8 = receivables_Transaction_Region_Information_Flexfield_Segment_8;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_9() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_9;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_9(
			String receivables_Transaction_Region_Information_Flexfield_Segment_9) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_9 = receivables_Transaction_Region_Information_Flexfield_Segment_9;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_10() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_10;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_10(
			String receivables_Transaction_Region_Information_Flexfield_Segment_10) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_10 = receivables_Transaction_Region_Information_Flexfield_Segment_10;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_11() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_11;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_11(
			String receivables_Transaction_Region_Information_Flexfield_Segment_11) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_11 = receivables_Transaction_Region_Information_Flexfield_Segment_11;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_12() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_12;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_12(
			String receivables_Transaction_Region_Information_Flexfield_Segment_12) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_12 = receivables_Transaction_Region_Information_Flexfield_Segment_12;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_13() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_13;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_13(
			String receivables_Transaction_Region_Information_Flexfield_Segment_13) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_13 = receivables_Transaction_Region_Information_Flexfield_Segment_13;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_14() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_14;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_14(
			String receivables_Transaction_Region_Information_Flexfield_Segment_14) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_14 = receivables_Transaction_Region_Information_Flexfield_Segment_14;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_15() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_15;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_15(
			String receivables_Transaction_Region_Information_Flexfield_Segment_15) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_15 = receivables_Transaction_Region_Information_Flexfield_Segment_15;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_16() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_16;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_16(
			String receivables_Transaction_Region_Information_Flexfield_Segment_16) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_16 = receivables_Transaction_Region_Information_Flexfield_Segment_16;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_17() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_17;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_17(
			String receivables_Transaction_Region_Information_Flexfield_Segment_17) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_17 = receivables_Transaction_Region_Information_Flexfield_Segment_17;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_18() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_18;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_18(
			String receivables_Transaction_Region_Information_Flexfield_Segment_18) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_18 = receivables_Transaction_Region_Information_Flexfield_Segment_18;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_19() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_19;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_19(
			String receivables_Transaction_Region_Information_Flexfield_Segment_19) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_19 = receivables_Transaction_Region_Information_Flexfield_Segment_19;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_20() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_20;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_20(
			String receivables_Transaction_Region_Information_Flexfield_Segment_20) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_20 = receivables_Transaction_Region_Information_Flexfield_Segment_20;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_21() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_21;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_21(
			String receivables_Transaction_Region_Information_Flexfield_Segment_21) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_21 = receivables_Transaction_Region_Information_Flexfield_Segment_21;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_22() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_22;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_22(
			String receivables_Transaction_Region_Information_Flexfield_Segment_22) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_22 = receivables_Transaction_Region_Information_Flexfield_Segment_22;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_23() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_23;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_23(
			String receivables_Transaction_Region_Information_Flexfield_Segment_23) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_23 = receivables_Transaction_Region_Information_Flexfield_Segment_23;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_24() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_24;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_24(
			String receivables_Transaction_Region_Information_Flexfield_Segment_24) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_24 = receivables_Transaction_Region_Information_Flexfield_Segment_24;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_25() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_25;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_25(
			String receivables_Transaction_Region_Information_Flexfield_Segment_25) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_25 = receivables_Transaction_Region_Information_Flexfield_Segment_25;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_26() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_26;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_26(
			String receivables_Transaction_Region_Information_Flexfield_Segment_26) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_26 = receivables_Transaction_Region_Information_Flexfield_Segment_26;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_27() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_27;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_27(
			String receivables_Transaction_Region_Information_Flexfield_Segment_27) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_27 = receivables_Transaction_Region_Information_Flexfield_Segment_27;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_28() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_28;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_28(
			String receivables_Transaction_Region_Information_Flexfield_Segment_28) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_28 = receivables_Transaction_Region_Information_Flexfield_Segment_28;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_29() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_29;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_29(
			String receivables_Transaction_Region_Information_Flexfield_Segment_29) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_29 = receivables_Transaction_Region_Information_Flexfield_Segment_29;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Segment_30() {
		return Receivables_Transaction_Region_Information_Flexfield_Segment_30;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Segment_30(
			String receivables_Transaction_Region_Information_Flexfield_Segment_30) {
		Receivables_Transaction_Region_Information_Flexfield_Segment_30 = receivables_Transaction_Region_Information_Flexfield_Segment_30;
	}

	public String getLine_Global_Descriptive_Flexfield_Attribute_Category() {
		return Line_Global_Descriptive_Flexfield_Attribute_Category;
	}

	public void setLine_Global_Descriptive_Flexfield_Attribute_Category(
			String line_Global_Descriptive_Flexfield_Attribute_Category) {
		Line_Global_Descriptive_Flexfield_Attribute_Category = line_Global_Descriptive_Flexfield_Attribute_Category;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_1() {
		return Line_Global_Descriptive_Flexfield_Segment_1;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_1(String line_Global_Descriptive_Flexfield_Segment_1) {
		Line_Global_Descriptive_Flexfield_Segment_1 = line_Global_Descriptive_Flexfield_Segment_1;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_2() {
		return Line_Global_Descriptive_Flexfield_Segment_2;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_2(String line_Global_Descriptive_Flexfield_Segment_2) {
		Line_Global_Descriptive_Flexfield_Segment_2 = line_Global_Descriptive_Flexfield_Segment_2;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_3() {
		return Line_Global_Descriptive_Flexfield_Segment_3;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_3(String line_Global_Descriptive_Flexfield_Segment_3) {
		Line_Global_Descriptive_Flexfield_Segment_3 = line_Global_Descriptive_Flexfield_Segment_3;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_4() {
		return Line_Global_Descriptive_Flexfield_Segment_4;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_4(String line_Global_Descriptive_Flexfield_Segment_4) {
		Line_Global_Descriptive_Flexfield_Segment_4 = line_Global_Descriptive_Flexfield_Segment_4;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_5() {
		return Line_Global_Descriptive_Flexfield_Segment_5;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_5(String line_Global_Descriptive_Flexfield_Segment_5) {
		Line_Global_Descriptive_Flexfield_Segment_5 = line_Global_Descriptive_Flexfield_Segment_5;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_6() {
		return Line_Global_Descriptive_Flexfield_Segment_6;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_6(String line_Global_Descriptive_Flexfield_Segment_6) {
		Line_Global_Descriptive_Flexfield_Segment_6 = line_Global_Descriptive_Flexfield_Segment_6;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_7() {
		return Line_Global_Descriptive_Flexfield_Segment_7;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_7(String line_Global_Descriptive_Flexfield_Segment_7) {
		Line_Global_Descriptive_Flexfield_Segment_7 = line_Global_Descriptive_Flexfield_Segment_7;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_8() {
		return Line_Global_Descriptive_Flexfield_Segment_8;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_8(String line_Global_Descriptive_Flexfield_Segment_8) {
		Line_Global_Descriptive_Flexfield_Segment_8 = line_Global_Descriptive_Flexfield_Segment_8;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_9() {
		return Line_Global_Descriptive_Flexfield_Segment_9;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_9(String line_Global_Descriptive_Flexfield_Segment_9) {
		Line_Global_Descriptive_Flexfield_Segment_9 = line_Global_Descriptive_Flexfield_Segment_9;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_10() {
		return Line_Global_Descriptive_Flexfield_Segment_10;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_10(String line_Global_Descriptive_Flexfield_Segment_10) {
		Line_Global_Descriptive_Flexfield_Segment_10 = line_Global_Descriptive_Flexfield_Segment_10;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_11() {
		return Line_Global_Descriptive_Flexfield_Segment_11;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_11(String line_Global_Descriptive_Flexfield_Segment_11) {
		Line_Global_Descriptive_Flexfield_Segment_11 = line_Global_Descriptive_Flexfield_Segment_11;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_12() {
		return Line_Global_Descriptive_Flexfield_Segment_12;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_12(String line_Global_Descriptive_Flexfield_Segment_12) {
		Line_Global_Descriptive_Flexfield_Segment_12 = line_Global_Descriptive_Flexfield_Segment_12;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_13() {
		return Line_Global_Descriptive_Flexfield_Segment_13;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_13(String line_Global_Descriptive_Flexfield_Segment_13) {
		Line_Global_Descriptive_Flexfield_Segment_13 = line_Global_Descriptive_Flexfield_Segment_13;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_14() {
		return Line_Global_Descriptive_Flexfield_Segment_14;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_14(String line_Global_Descriptive_Flexfield_Segment_14) {
		Line_Global_Descriptive_Flexfield_Segment_14 = line_Global_Descriptive_Flexfield_Segment_14;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_15() {
		return Line_Global_Descriptive_Flexfield_Segment_15;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_15(String line_Global_Descriptive_Flexfield_Segment_15) {
		Line_Global_Descriptive_Flexfield_Segment_15 = line_Global_Descriptive_Flexfield_Segment_15;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_16() {
		return Line_Global_Descriptive_Flexfield_Segment_16;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_16(String line_Global_Descriptive_Flexfield_Segment_16) {
		Line_Global_Descriptive_Flexfield_Segment_16 = line_Global_Descriptive_Flexfield_Segment_16;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_17() {
		return Line_Global_Descriptive_Flexfield_Segment_17;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_17(String line_Global_Descriptive_Flexfield_Segment_17) {
		Line_Global_Descriptive_Flexfield_Segment_17 = line_Global_Descriptive_Flexfield_Segment_17;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_18() {
		return Line_Global_Descriptive_Flexfield_Segment_18;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_18(String line_Global_Descriptive_Flexfield_Segment_18) {
		Line_Global_Descriptive_Flexfield_Segment_18 = line_Global_Descriptive_Flexfield_Segment_18;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_19() {
		return Line_Global_Descriptive_Flexfield_Segment_19;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_19(String line_Global_Descriptive_Flexfield_Segment_19) {
		Line_Global_Descriptive_Flexfield_Segment_19 = line_Global_Descriptive_Flexfield_Segment_19;
	}

	public String getLine_Global_Descriptive_Flexfield_Segment_20() {
		return Line_Global_Descriptive_Flexfield_Segment_20;
	}

	public void setLine_Global_Descriptive_Flexfield_Segment_20(String line_Global_Descriptive_Flexfield_Segment_20) {
		Line_Global_Descriptive_Flexfield_Segment_20 = line_Global_Descriptive_Flexfield_Segment_20;
	}

	public String getBusiness_Unit_Name() {
		return Business_Unit_Name;
	}

	public void setBusiness_Unit_Name(String business_Unit_Name) {
		Business_Unit_Name = business_Unit_Name;
	}

	public String getComments() {
		return Comments;
	}

	public void setComments(String comments) {
		Comments = comments;
	}

	public String getNotes_from_Source() {
		return Notes_from_Source;
	}

	public void setNotes_from_Source(String notes_from_Source) {
		Notes_from_Source = notes_from_Source;
	}

	public String getCredit_Card_Token_Number() {
		return Credit_Card_Token_Number;
	}

	public void setCredit_Card_Token_Number(String credit_Card_Token_Number) {
		Credit_Card_Token_Number = credit_Card_Token_Number;
	}

	public String getCredit_Card_Expiration_Date() {
		return Credit_Card_Expiration_Date;
	}

	public void setCredit_Card_Expiration_Date(String credit_Card_Expiration_Date) {
		Credit_Card_Expiration_Date = credit_Card_Expiration_Date;
	}

	public String getFirst_Name_of_the_Credit_Card_Holder() {
		return First_Name_of_the_Credit_Card_Holder;
	}

	public void setFirst_Name_of_the_Credit_Card_Holder(String first_Name_of_the_Credit_Card_Holder) {
		First_Name_of_the_Credit_Card_Holder = first_Name_of_the_Credit_Card_Holder;
	}

	public String getLast_Name_of_the_Credit_Card_Holder() {
		return Last_Name_of_the_Credit_Card_Holder;
	}

	public void setLast_Name_of_the_Credit_Card_Holder(String last_Name_of_the_Credit_Card_Holder) {
		Last_Name_of_the_Credit_Card_Holder = last_Name_of_the_Credit_Card_Holder;
	}

	public String getCredit_Card_Issuer_Code() {
		return Credit_Card_Issuer_Code;
	}

	public void setCredit_Card_Issuer_Code(String credit_Card_Issuer_Code) {
		Credit_Card_Issuer_Code = credit_Card_Issuer_Code;
	}

	public String getMasked_Credit_Card_Number() {
		return Masked_Credit_Card_Number;
	}

	public void setMasked_Credit_Card_Number(String masked_Credit_Card_Number) {
		Masked_Credit_Card_Number = masked_Credit_Card_Number;
	}

	public String getCredit_Card_Authorization_Request_Identifier() {
		return Credit_Card_Authorization_Request_Identifier;
	}

	public void setCredit_Card_Authorization_Request_Identifier(String credit_Card_Authorization_Request_Identifier) {
		Credit_Card_Authorization_Request_Identifier = credit_Card_Authorization_Request_Identifier;
	}

	public String getCredit_Card_Voice_Authorization_Code() {
		return Credit_Card_Voice_Authorization_Code;
	}

	public void setCredit_Card_Voice_Authorization_Code(String credit_Card_Voice_Authorization_Code) {
		Credit_Card_Voice_Authorization_Code = credit_Card_Voice_Authorization_Code;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_1() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_1;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_1(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_1) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_1 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_1;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_2() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_2;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_2(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_2) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_2 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_2;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_3() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_3;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_3(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_3) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_3 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_3;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_4() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_4;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_4(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_4) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_4 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_4;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_5() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_5;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_5(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_5) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_5 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_5;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_6() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_6;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_6(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_6) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_6 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_6;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_7() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_7;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_7(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_7) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_7 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_7;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_8() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_8;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_8(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_8) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_8 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_8;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_9() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_9;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_9(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_9) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_9 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_9;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_10() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_10;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_10(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_10) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_10 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_10;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_11() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_11;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_11(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_11) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_11 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_11;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Number_Segment_12() {
		return Receivables_Transaction_Region_Information_Flexfield_Number_Segment_12;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Number_Segment_12(
			String receivables_Transaction_Region_Information_Flexfield_Number_Segment_12) {
		Receivables_Transaction_Region_Information_Flexfield_Number_Segment_12 = receivables_Transaction_Region_Information_Flexfield_Number_Segment_12;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Date_Segment_1() {
		return Receivables_Transaction_Region_Information_Flexfield_Date_Segment_1;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Date_Segment_1(
			String receivables_Transaction_Region_Information_Flexfield_Date_Segment_1) {
		Receivables_Transaction_Region_Information_Flexfield_Date_Segment_1 = receivables_Transaction_Region_Information_Flexfield_Date_Segment_1;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Date_Segment_2() {
		return Receivables_Transaction_Region_Information_Flexfield_Date_Segment_2;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Date_Segment_2(
			String receivables_Transaction_Region_Information_Flexfield_Date_Segment_2) {
		Receivables_Transaction_Region_Information_Flexfield_Date_Segment_2 = receivables_Transaction_Region_Information_Flexfield_Date_Segment_2;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Date_Segment_3() {
		return Receivables_Transaction_Region_Information_Flexfield_Date_Segment_3;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Date_Segment_3(
			String receivables_Transaction_Region_Information_Flexfield_Date_Segment_3) {
		Receivables_Transaction_Region_Information_Flexfield_Date_Segment_3 = receivables_Transaction_Region_Information_Flexfield_Date_Segment_3;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Date_Segment_4() {
		return Receivables_Transaction_Region_Information_Flexfield_Date_Segment_4;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Date_Segment_4(
			String receivables_Transaction_Region_Information_Flexfield_Date_Segment_4) {
		Receivables_Transaction_Region_Information_Flexfield_Date_Segment_4 = receivables_Transaction_Region_Information_Flexfield_Date_Segment_4;
	}

	public String getReceivables_Transaction_Region_Information_Flexfield_Date_Segment_5() {
		return Receivables_Transaction_Region_Information_Flexfield_Date_Segment_5;
	}

	public void setReceivables_Transaction_Region_Information_Flexfield_Date_Segment_5(
			String receivables_Transaction_Region_Information_Flexfield_Date_Segment_5) {
		Receivables_Transaction_Region_Information_Flexfield_Date_Segment_5 = receivables_Transaction_Region_Information_Flexfield_Date_Segment_5;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_1() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_1;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_1(
			String receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_1) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_1 = receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_1;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_2() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_2;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_2(
			String receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_2) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_2 = receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_2;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_3() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_3;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_3(
			String receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_3) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_3 = receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_3;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_4() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_4;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_4(
			String receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_4) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_4 = receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_4;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_5() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_5;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_5(
			String receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_5) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_5 = receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_5;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_1() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_1;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_1(
			String receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_1) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_1 = receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_1;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_2() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_2;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_2(
			String receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_2) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_2 = receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_2;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_3() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_3;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_3(
			String receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_3) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_3 = receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_3;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_4() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_4;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_4(
			String receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_4) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_4 = receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_4;
	}

	public String getReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_5() {
		return Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_5;
	}

	public void setReceivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_5(
			String receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_5) {
		Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_5 = receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_5;
	}

	public String getFreight_Charge() {
		return Freight_Charge;
	}

	public void setFreight_Charge(String freight_Charge) {
		Freight_Charge = freight_Charge;
	}

	public String getInsurance_Charge() {
		return Insurance_Charge;
	}

	public void setInsurance_Charge(String insurance_Charge) {
		Insurance_Charge = insurance_Charge;
	}

	public String getPacking_Charge() {
		return Packing_Charge;
	}

	public void setPacking_Charge(String packing_Charge) {
		Packing_Charge = packing_Charge;
	}

	public String getMiscellaneous_Charge() {
		return Miscellaneous_Charge;
	}

	public void setMiscellaneous_Charge(String miscellaneous_Charge) {
		Miscellaneous_Charge = miscellaneous_Charge;
	}

	public String getCommercial_Discount() {
		return Commercial_Discount;
	}

	public void setCommercial_Discount(String commercial_Discount) {
		Commercial_Discount = commercial_Discount;
	}

	public String getEnforce_Chronological_Document_Sequencing() {
		return Enforce_Chronological_Document_Sequencing;
	}

	public void setEnforce_Chronological_Document_Sequencing(String enforce_Chronological_Document_Sequencing) {
		Enforce_Chronological_Document_Sequencing = enforce_Chronological_Document_Sequencing;
	}

	public String getEnd() {
		return End;
	}

	public void setEnd(String end) {
		End = end;
	}

}

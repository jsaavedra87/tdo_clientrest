package com.dto.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class GLInterfaceV3 {
	
	@JsonProperty("Status_Code")
	private String Status_Code ="";
	@JsonProperty("Ledger_ID")
	private String Ledger_ID ="";
	@JsonProperty("Effective_Date_of_Transaction")
	private String Effective_Date_of_Transaction ="";
	@JsonProperty("Journal_Source")
	private String Journal_Source ="";
	@JsonProperty("Journal_Category")
	private String Journal_Category ="";
	@JsonProperty("Currency_Code")
	private String Currency_Code ="";
	@JsonProperty("Journal_Entry_Creation_Date")
	private String Journal_Entry_Creation_Date ="";
	@JsonProperty("Actual_Flag")
	private String Actual_Flag ="";
	@JsonProperty("Segment1")
	private String Segment1 ="";
	@JsonProperty("Segment2")
	private String Segment2 ="";
	@JsonProperty("Segment3")
	private String Segment3 ="";
	@JsonProperty("Segment4")
	private String Segment4 ="";
	@JsonProperty("Segment5")
	private String Segment5 ="";
	@JsonProperty("Segment6")
	private String Segment6 ="";
	@JsonProperty("Segment7")
	private String Segment7 ="";
	@JsonProperty("Segment8")
	private String Segment8 ="";
	@JsonProperty("Segment9")
	private String Segment9 ="";
	@JsonProperty("Segment10")
	private String Segment10 ="";
	@JsonProperty("Segment11")
	private String Segment11 ="";
	@JsonProperty("Segment12")
	private String Segment12 ="";
	@JsonProperty("Segment13")
	private String Segment13 ="";
	@JsonProperty("Segment14")
	private String Segment14 ="";
	@JsonProperty("Segment15")
	private String Segment15 ="";
	@JsonProperty("Segment16")
	private String Segment16 ="";
	@JsonProperty("Segment17")
	private String Segment17 ="";
	@JsonProperty("Segment18")
	private String Segment18 ="";
	@JsonProperty("Segment19")
	private String Segment19 ="";
	@JsonProperty("Segment20")
	private String Segment20 ="";
	@JsonProperty("Segment21")
	private String Segment21 ="";
	@JsonProperty("Segment22")
	private String Segment22 ="";
	@JsonProperty("Segment23")
	private String Segment23 ="";
	@JsonProperty("Segment24")
	private String Segment24 ="";
	@JsonProperty("Segment25")
	private String Segment25 ="";
	@JsonProperty("Segment26")
	private String Segment26 ="";
	@JsonProperty("Segment27")
	private String Segment27 ="";
	@JsonProperty("Segment28")
	private String Segment28 ="";
	@JsonProperty("Segment29")
	private String Segment29 ="";
	@JsonProperty("Segment30")
	private String Segment30 ="";
	@JsonProperty("Entered_Debit_Amount")
	private double Entered_Debit_Amount = 0.0d;
	@JsonProperty("Entered_Credit_Amount")
	private double Entered_Credit_Amount = 0.0d;
	@JsonProperty("Converted_Debit_Amount")
	private String Converted_Debit_Amount;
	@JsonProperty("Converted_Credit_Amount")
	private String Converted_Credit_Amount;
	@JsonProperty("REFERENCE1")
	private String REFERENCE1 ="";
	@JsonProperty("REFERENCE2")
	private String REFERENCE2 ="";
	@JsonProperty("REFERENCE3")
	private String REFERENCE3 ="";
	@JsonProperty("REFERENCE4")
	private String REFERENCE4 ="";
	@JsonProperty("REFERENCE5")
	private String REFERENCE5 ="";
	@JsonProperty("REFERENCE6")
	private String REFERENCE6 ="";
	@JsonProperty("REFERENCE7")
	private String REFERENCE7 ="";
	@JsonProperty("REFERENCE8")
	private String REFERENCE8 ="";
	@JsonProperty("REFERENCE9")
	private String REFERENCE9 ="";
	@JsonProperty("REFERENCE10")
	private String REFERENCE10 ="";
	@JsonProperty("Reference_column_1")
	private String Reference_column_1 ="";
	@JsonProperty("Reference_column_2")
	private String Reference_column_2 ="";
	@JsonProperty("Reference_column_3")
	private String Reference_column_3 ="";
	@JsonProperty("Reference_column_4")
	private String Reference_column_4 ="";
	@JsonProperty("Reference_column_5")
	private String Reference_column_5 ="";
	@JsonProperty("Reference_column_6")
	private String Reference_column_6 ="";
	@JsonProperty("Reference_column_7")
	private String Reference_column_7 ="";
	@JsonProperty("Reference_column_8")
	private String Reference_column_8 ="";
	@JsonProperty("Reference_column_9")
	private String Reference_column_9 ="";
	@JsonProperty("Reference_column_10")
	private String Reference_column_10 ="";
	@JsonProperty("Statistical_Amount")
	private String Statistical_Amount ="";
	@JsonProperty("Currency_Conversion_Type")
	private String Currency_Conversion_Type ="";
	@JsonProperty("Currency_Conversion_Date")
	private String Currency_Conversion_Date ="";
	@JsonProperty("Currency_Conversion_Rate")
	private String Currency_Conversion_Rate ="";
	@JsonProperty("Interface_Group_Identifier")
	private String Interface_Group_Identifier ="";
	@JsonProperty("Context_field_for_Journal_Entry_Line_DFF")
	private String Context_field_for_Journal_Entry_Line_DFF ="";
	@JsonProperty("ATTRIBUTE1")
	private String ATTRIBUTE1 ="";
	@JsonProperty("ATTRIBUTE2")
	private String ATTRIBUTE2 ="";
	@JsonProperty("ATTRIBUTE3")
	private String ATTRIBUTE3 ="";
	@JsonProperty("ATTRIBUTE4")
	private String ATTRIBUTE4 ="";
	@JsonProperty("ATTRIBUTE5")
	private String ATTRIBUTE5 ="";
	@JsonProperty("ATTRIBUTE6")
	private String ATTRIBUTE6 ="";
	@JsonProperty("ATTRIBUTE7")
	private String ATTRIBUTE7 ="";
	@JsonProperty("ATTRIBUTE8")
	private String ATTRIBUTE8 ="";
	@JsonProperty("ATTRIBUTE9")
	private String ATTRIBUTE9 ="";
	@JsonProperty("ATTRIBUTE10")
	private String ATTRIBUTE10 ="";
	@JsonProperty("ATTRIBUTE11")
	private String ATTRIBUTE11 ="";
	@JsonProperty("ATTRIBUTE12")
	private String ATTRIBUTE12 ="";
	@JsonProperty("ATTRIBUTE13")
	private String ATTRIBUTE13 ="";
	@JsonProperty("ATTRIBUTE14")
	private String ATTRIBUTE14 ="";
	@JsonProperty("ATTRIBUTE15")
	private String ATTRIBUTE15 ="";
	@JsonProperty("ATTRIBUTE16")
	private String ATTRIBUTE16 ="";
	@JsonProperty("ATTRIBUTE17")
	private String ATTRIBUTE17 ="";
	@JsonProperty("ATTRIBUTE18")
	private String ATTRIBUTE18 ="";
	@JsonProperty("ATTRIBUTE19")
	private String ATTRIBUTE19 ="";
	@JsonProperty("ATTRIBUTE20")
	private String ATTRIBUTE20 ="";
	@JsonProperty("Context_field_for_Captured_Information_DFF")
	private String Context_field_for_Captured_Information_DFF ="";
	@JsonProperty("Average_Journal_Flag")
	private String Average_Journal_Flag ="";
	@JsonProperty("Clearing_Company")
	private String Clearing_Company ="";
	@JsonProperty("Ledger_Name")
	private String Ledger_Name ="";
	@JsonProperty("Encumbrance_Type_ID")
	private String Encumbrance_Type_ID ="";
	@JsonProperty("Reconciliation_Reference")
	private String Reconciliation_Reference ="";
	@JsonProperty("end")
	private String end ="";

	
	public String getStatus_Code() {
		return Status_Code;
	}
	public void setStatus_Code(String status_Code) {
		Status_Code = status_Code;
	}
	public String getLedger_ID() {
		return Ledger_ID;
	}
	public void setLedger_ID(String ledger_ID) {
		Ledger_ID = ledger_ID;
	}
	public String getEffective_Date_of_Transaction() {
		return Effective_Date_of_Transaction;
	}
	public void setEffective_Date_of_Transaction(String effective_Date_of_Transaction) {
		Effective_Date_of_Transaction = effective_Date_of_Transaction;
	}
	public String getJournal_Source() {
		return Journal_Source;
	}
	public void setJournal_Source(String journal_Source) {
		Journal_Source = journal_Source;
	}
	public String getJournal_Category() {
		return Journal_Category;
	}
	public void setJournal_Category(String journal_Category) {
		Journal_Category = journal_Category;
	}
	public String getCurrency_Code() {
		return Currency_Code;
	}
	public void setCurrency_Code(String currency_Code) {
		Currency_Code = currency_Code;
	}
	public String getJournal_Entry_Creation_Date() {
		return Journal_Entry_Creation_Date;
	}
	public void setJournal_Entry_Creation_Date(String journal_Entry_Creation_Date) {
		Journal_Entry_Creation_Date = journal_Entry_Creation_Date;
	}
	public String getActual_Flag() {
		return Actual_Flag;
	}
	public void setActual_Flag(String actual_Flag) {
		Actual_Flag = actual_Flag;
	}
	public String getSegment1() {
		return Segment1;
	}
	public void setSegment1(String segment1) {
		Segment1 = segment1;
	}
	public String getSegment2() {
		return Segment2;
	}
	public void setSegment2(String segment2) {
		Segment2 = segment2;
	}
	public String getSegment3() {
		return Segment3;
	}
	public void setSegment3(String segment3) {
		Segment3 = segment3;
	}
	public String getSegment4() {
		return Segment4;
	}
	public void setSegment4(String segment4) {
		Segment4 = segment4;
	}
	public String getSegment5() {
		return Segment5;
	}
	public void setSegment5(String segment5) {
		Segment5 = segment5;
	}
	public String getSegment6() {
		return Segment6;
	}
	public void setSegment6(String segment6) {
		Segment6 = segment6;
	}
	public String getSegment7() {
		return Segment7;
	}
	public void setSegment7(String segment7) {
		Segment7 = segment7;
	}
	public String getSegment8() {
		return Segment8;
	}
	public void setSegment8(String segment8) {
		Segment8 = segment8;
	}
	public String getSegment9() {
		return Segment9;
	}
	public void setSegment9(String segment9) {
		Segment9 = segment9;
	}
	public String getSegment10() {
		return Segment10;
	}
	public void setSegment10(String segment10) {
		Segment10 = segment10;
	}
	public String getSegment11() {
		return Segment11;
	}
	public void setSegment11(String segment11) {
		Segment11 = segment11;
	}
	public String getSegment12() {
		return Segment12;
	}
	public void setSegment12(String segment12) {
		Segment12 = segment12;
	}
	public String getSegment13() {
		return Segment13;
	}
	public void setSegment13(String segment13) {
		Segment13 = segment13;
	}
	public String getSegment14() {
		return Segment14;
	}
	public void setSegment14(String segment14) {
		Segment14 = segment14;
	}
	public String getSegment15() {
		return Segment15;
	}
	public void setSegment15(String segment15) {
		Segment15 = segment15;
	}
	public String getSegment16() {
		return Segment16;
	}
	public void setSegment16(String segment16) {
		Segment16 = segment16;
	}
	public String getSegment17() {
		return Segment17;
	}
	public void setSegment17(String segment17) {
		Segment17 = segment17;
	}
	public String getSegment18() {
		return Segment18;
	}
	public void setSegment18(String segment18) {
		Segment18 = segment18;
	}
	public String getSegment19() {
		return Segment19;
	}
	public void setSegment19(String segment19) {
		Segment19 = segment19;
	}
	public String getSegment20() {
		return Segment20;
	}
	public void setSegment20(String segment20) {
		Segment20 = segment20;
	}
	public String getSegment21() {
		return Segment21;
	}
	public void setSegment21(String segment21) {
		Segment21 = segment21;
	}
	public String getSegment22() {
		return Segment22;
	}
	public void setSegment22(String segment22) {
		Segment22 = segment22;
	}
	public String getSegment23() {
		return Segment23;
	}
	public void setSegment23(String segment23) {
		Segment23 = segment23;
	}
	public String getSegment24() {
		return Segment24;
	}
	public void setSegment24(String segment24) {
		Segment24 = segment24;
	}
	public String getSegment25() {
		return Segment25;
	}
	public void setSegment25(String segment25) {
		Segment25 = segment25;
	}
	public String getSegment26() {
		return Segment26;
	}
	public void setSegment26(String segment26) {
		Segment26 = segment26;
	}
	public String getSegment27() {
		return Segment27;
	}
	public void setSegment27(String segment27) {
		Segment27 = segment27;
	}
	public String getSegment28() {
		return Segment28;
	}
	public void setSegment28(String segment28) {
		Segment28 = segment28;
	}
	public String getSegment29() {
		return Segment29;
	}
	public void setSegment29(String segment29) {
		Segment29 = segment29;
	}
	public String getSegment30() {
		return Segment30;
	}
	public void setSegment30(String segment30) {
		Segment30 = segment30;
	}
	public double getEntered_Debit_Amount() {
		return Entered_Debit_Amount;
	}
	public void setEntered_Debit_Amount(double entered_Debit_Amount) {
		Entered_Debit_Amount = entered_Debit_Amount;
	}
	public double getEntered_Credit_Amount() {
		return Entered_Credit_Amount;
	}
	public void setEntered_Credit_Amount(double entered_Credit_Amount) {
		Entered_Credit_Amount = entered_Credit_Amount;
	}
	public String getConverted_Debit_Amount() {
		return Converted_Debit_Amount;
	}
	public void setConverted_Debit_Amount(String converted_Debit_Amount) {
		Converted_Debit_Amount = converted_Debit_Amount;
	}
	public String getConverted_Credit_Amount() {
		return Converted_Credit_Amount;
	}
	public void setConverted_Credit_Amount(String converted_Credit_Amount) {
		Converted_Credit_Amount = converted_Credit_Amount;
	}
	public String getREFERENCE1() {
		return REFERENCE1;
	}
	public void setREFERENCE1(String rEFERENCE1) {
		REFERENCE1 = rEFERENCE1;
	}
	public String getREFERENCE2() {
		return REFERENCE2;
	}
	public void setREFERENCE2(String rEFERENCE2) {
		REFERENCE2 = rEFERENCE2;
	}
	public String getREFERENCE3() {
		return REFERENCE3;
	}
	public void setREFERENCE3(String rEFERENCE3) {
		REFERENCE3 = rEFERENCE3;
	}
	public String getREFERENCE4() {
		return REFERENCE4;
	}
	public void setREFERENCE4(String rEFERENCE4) {
		REFERENCE4 = rEFERENCE4;
	}
	public String getREFERENCE5() {
		return REFERENCE5;
	}
	public void setREFERENCE5(String rEFERENCE5) {
		REFERENCE5 = rEFERENCE5;
	}
	public String getREFERENCE6() {
		return REFERENCE6;
	}
	public void setREFERENCE6(String rEFERENCE6) {
		REFERENCE6 = rEFERENCE6;
	}
	public String getREFERENCE7() {
		return REFERENCE7;
	}
	public void setREFERENCE7(String rEFERENCE7) {
		REFERENCE7 = rEFERENCE7;
	}
	public String getREFERENCE8() {
		return REFERENCE8;
	}
	public void setREFERENCE8(String rEFERENCE8) {
		REFERENCE8 = rEFERENCE8;
	}
	public String getREFERENCE9() {
		return REFERENCE9;
	}
	public void setREFERENCE9(String rEFERENCE9) {
		REFERENCE9 = rEFERENCE9;
	}
	public String getREFERENCE10() {
		return REFERENCE10;
	}
	public void setREFERENCE10(String rEFERENCE10) {
		REFERENCE10 = rEFERENCE10;
	}
	public String getReference_column_1() {
		return Reference_column_1;
	}
	public void setReference_column_1(String reference_column_1) {
		Reference_column_1 = reference_column_1;
	}
	public String getReference_column_2() {
		return Reference_column_2;
	}
	public void setReference_column_2(String reference_column_2) {
		Reference_column_2 = reference_column_2;
	}
	public String getReference_column_3() {
		return Reference_column_3;
	}
	public void setReference_column_3(String reference_column_3) {
		Reference_column_3 = reference_column_3;
	}
	public String getReference_column_4() {
		return Reference_column_4;
	}
	public void setReference_column_4(String reference_column_4) {
		Reference_column_4 = reference_column_4;
	}
	public String getReference_column_5() {
		return Reference_column_5;
	}
	public void setReference_column_5(String reference_column_5) {
		Reference_column_5 = reference_column_5;
	}
	public String getReference_column_6() {
		return Reference_column_6;
	}
	public void setReference_column_6(String reference_column_6) {
		Reference_column_6 = reference_column_6;
	}
	public String getReference_column_7() {
		return Reference_column_7;
	}
	public void setReference_column_7(String reference_column_7) {
		Reference_column_7 = reference_column_7;
	}
	public String getReference_column_8() {
		return Reference_column_8;
	}
	public void setReference_column_8(String reference_column_8) {
		Reference_column_8 = reference_column_8;
	}
	public String getReference_column_9() {
		return Reference_column_9;
	}
	public void setReference_column_9(String reference_column_9) {
		Reference_column_9 = reference_column_9;
	}
	public String getReference_column_10() {
		return Reference_column_10;
	}
	public void setReference_column_10(String reference_column_10) {
		Reference_column_10 = reference_column_10;
	}
	public String getStatistical_Amount() {
		return Statistical_Amount;
	}
	public void setStatistical_Amount(String statistical_Amount) {
		Statistical_Amount = statistical_Amount;
	}
	public String getCurrency_Conversion_Type() {
		return Currency_Conversion_Type;
	}
	public void setCurrency_Conversion_Type(String currency_Conversion_Type) {
		Currency_Conversion_Type = currency_Conversion_Type;
	}
	public String getCurrency_Conversion_Date() {
		return Currency_Conversion_Date;
	}
	public void setCurrency_Conversion_Date(String currency_Conversion_Date) {
		Currency_Conversion_Date = currency_Conversion_Date;
	}
	public String getCurrency_Conversion_Rate() {
		return Currency_Conversion_Rate;
	}
	public void setCurrency_Conversion_Rate(String currency_Conversion_Rate) {
		Currency_Conversion_Rate = currency_Conversion_Rate;
	}
	public String getInterface_Group_Identifier() {
		return Interface_Group_Identifier;
	}
	public void setInterface_Group_Identifier(String interface_Group_Identifier) {
		Interface_Group_Identifier = interface_Group_Identifier;
	}
	public String getContext_field_for_Journal_Entry_Line_DFF() {
		return Context_field_for_Journal_Entry_Line_DFF;
	}
	public void setContext_field_for_Journal_Entry_Line_DFF(String context_field_for_Journal_Entry_Line_DFF) {
		Context_field_for_Journal_Entry_Line_DFF = context_field_for_Journal_Entry_Line_DFF;
	}
	public String getATTRIBUTE1() {
		return ATTRIBUTE1;
	}
	public void setATTRIBUTE1(String aTTRIBUTE1) {
		ATTRIBUTE1 = aTTRIBUTE1;
	}
	public String getATTRIBUTE2() {
		return ATTRIBUTE2;
	}
	public void setATTRIBUTE2(String aTTRIBUTE2) {
		ATTRIBUTE2 = aTTRIBUTE2;
	}
	public String getATTRIBUTE3() {
		return ATTRIBUTE3;
	}
	public void setATTRIBUTE3(String aTTRIBUTE3) {
		ATTRIBUTE3 = aTTRIBUTE3;
	}
	public String getATTRIBUTE4() {
		return ATTRIBUTE4;
	}
	public void setATTRIBUTE4(String aTTRIBUTE4) {
		ATTRIBUTE4 = aTTRIBUTE4;
	}
	public String getATTRIBUTE5() {
		return ATTRIBUTE5;
	}
	public void setATTRIBUTE5(String aTTRIBUTE5) {
		ATTRIBUTE5 = aTTRIBUTE5;
	}
	public String getATTRIBUTE6() {
		return ATTRIBUTE6;
	}
	public void setATTRIBUTE6(String aTTRIBUTE6) {
		ATTRIBUTE6 = aTTRIBUTE6;
	}
	public String getATTRIBUTE7() {
		return ATTRIBUTE7;
	}
	public void setATTRIBUTE7(String aTTRIBUTE7) {
		ATTRIBUTE7 = aTTRIBUTE7;
	}
	public String getATTRIBUTE8() {
		return ATTRIBUTE8;
	}
	public void setATTRIBUTE8(String aTTRIBUTE8) {
		ATTRIBUTE8 = aTTRIBUTE8;
	}
	public String getATTRIBUTE9() {
		return ATTRIBUTE9;
	}
	public void setATTRIBUTE9(String aTTRIBUTE9) {
		ATTRIBUTE9 = aTTRIBUTE9;
	}
	public String getATTRIBUTE10() {
		return ATTRIBUTE10;
	}
	public void setATTRIBUTE10(String aTTRIBUTE10) {
		ATTRIBUTE10 = aTTRIBUTE10;
	}
	public String getATTRIBUTE11() {
		return ATTRIBUTE11;
	}
	public void setATTRIBUTE11(String aTTRIBUTE11) {
		ATTRIBUTE11 = aTTRIBUTE11;
	}
	public String getATTRIBUTE12() {
		return ATTRIBUTE12;
	}
	public void setATTRIBUTE12(String aTTRIBUTE12) {
		ATTRIBUTE12 = aTTRIBUTE12;
	}
	public String getATTRIBUTE13() {
		return ATTRIBUTE13;
	}
	public void setATTRIBUTE13(String aTTRIBUTE13) {
		ATTRIBUTE13 = aTTRIBUTE13;
	}
	public String getATTRIBUTE14() {
		return ATTRIBUTE14;
	}
	public void setATTRIBUTE14(String aTTRIBUTE14) {
		ATTRIBUTE14 = aTTRIBUTE14;
	}
	public String getATTRIBUTE15() {
		return ATTRIBUTE15;
	}
	public void setATTRIBUTE15(String aTTRIBUTE15) {
		ATTRIBUTE15 = aTTRIBUTE15;
	}
	public String getATTRIBUTE16() {
		return ATTRIBUTE16;
	}
	public void setATTRIBUTE16(String aTTRIBUTE16) {
		ATTRIBUTE16 = aTTRIBUTE16;
	}
	public String getATTRIBUTE17() {
		return ATTRIBUTE17;
	}
	public void setATTRIBUTE17(String aTTRIBUTE17) {
		ATTRIBUTE17 = aTTRIBUTE17;
	}
	public String getATTRIBUTE18() {
		return ATTRIBUTE18;
	}
	public void setATTRIBUTE18(String aTTRIBUTE18) {
		ATTRIBUTE18 = aTTRIBUTE18;
	}
	public String getATTRIBUTE19() {
		return ATTRIBUTE19;
	}
	public void setATTRIBUTE19(String aTTRIBUTE19) {
		ATTRIBUTE19 = aTTRIBUTE19;
	}
	public String getATTRIBUTE20() {
		return ATTRIBUTE20;
	}
	public void setATTRIBUTE20(String aTTRIBUTE20) {
		ATTRIBUTE20 = aTTRIBUTE20;
	}
	public String getContext_field_for_Captured_Information_DFF() {
		return Context_field_for_Captured_Information_DFF;
	}
	public void setContext_field_for_Captured_Information_DFF(String context_field_for_Captured_Information_DFF) {
		Context_field_for_Captured_Information_DFF = context_field_for_Captured_Information_DFF;
	}
	public String getAverage_Journal_Flag() {
		return Average_Journal_Flag;
	}
	public void setAverage_Journal_Flag(String average_Journal_Flag) {
		Average_Journal_Flag = average_Journal_Flag;
	}
	public String getClearing_Company() {
		return Clearing_Company;
	}
	public void setClearing_Company(String clearing_Company) {
		Clearing_Company = clearing_Company;
	}
	public String getLedger_Name() {
		return Ledger_Name;
	}
	public void setLedger_Name(String ledger_Name) {
		Ledger_Name = ledger_Name;
	}
	public String getEncumbrance_Type_ID() {
		return Encumbrance_Type_ID;
	}
	public void setEncumbrance_Type_ID(String encumbrance_Type_ID) {
		Encumbrance_Type_ID = encumbrance_Type_ID;
	}
	public String getReconciliation_Reference() {
		return Reconciliation_Reference;
	}
	public void setReconciliation_Reference(String reconciliation_Reference) {
		Reconciliation_Reference = reconciliation_Reference;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}

}

package com.dto.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class Articulo {
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("clave")
	private String clave;
	
	@JsonProperty("claveAlterna")
	private String claveAlterna;
	
	@JsonProperty("descripcion")
	private String descripcion;
	
	@JsonProperty("servicio")
	private boolean servicio;
	
	@JsonProperty("localizacion")
	private String localizacion;
	
	@JsonProperty("invMin")
	private float invMin;
	
	@JsonProperty("invMax")
	private float invMax;
	
	@JsonProperty("factor")
	private float factor;
	
	@JsonProperty("precioCompra")
	private float precioCompra;
	
	@JsonProperty("precio1")
	private float precio1;
	
	@JsonProperty("precio2")
	private float precio2;
	
	@JsonProperty("precio3")
	private float precio3;
	@JsonProperty("precio4")
	private float precio4;
	
	@JsonProperty("mayoreo2")
	private float mayoreo2;
	
	@JsonProperty("mayoreo3")
	private float mayoreo3;
	
	@JsonProperty("mayoreo4")
	private float mayoreo4;
	
	@JsonProperty("caracteristicas")
	private String caracteristicas;
	
	@JsonProperty("receta")
	private boolean receta;
	
	@JsonProperty("granel")
	private boolean granel;
	
	@JsonProperty("peso")
	private float peso;
	
	@JsonProperty("pesoAut")
	private boolean pesoAut;
	
	@JsonProperty("claveProdServ")
	private String claveProdServ;
	
	@JsonProperty("unidadCompraId")
	private float unidadCompraId;
	
	@JsonProperty("unidadVentaId")
	private float unidadVentaId;
	
	@JsonProperty("categoriaId")
	private float categoriaId;
	
	@JsonProperty("impuestosId")
	private String[] impuestosId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getClaveAlterna() {
		return claveAlterna;
	}

	public void setClaveAlterna(String claveAlterna) {
		this.claveAlterna = claveAlterna;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isServicio() {
		return servicio;
	}

	public void setServicio(boolean servicio) {
		this.servicio = servicio;
	}

	public String getLocalizacion() {
		return localizacion;
	}

	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}

	public float getInvMin() {
		return invMin;
	}

	public void setInvMin(float invMin) {
		this.invMin = invMin;
	}

	public float getInvMax() {
		return invMax;
	}

	public void setInvMax(float invMax) {
		this.invMax = invMax;
	}

	public float getFactor() {
		return factor;
	}

	public void setFactor(float factor) {
		this.factor = factor;
	}

	public float getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(float precioCompra) {
		this.precioCompra = precioCompra;
	}

	public float getPrecio1() {
		return precio1;
	}

	public void setPrecio1(float precio1) {
		this.precio1 = precio1;
	}

	public float getPrecio2() {
		return precio2;
	}

	public void setPrecio2(float precio2) {
		this.precio2 = precio2;
	}

	public float getPrecio3() {
		return precio3;
	}

	public void setPrecio3(float precio3) {
		this.precio3 = precio3;
	}

	public float getPrecio4() {
		return precio4;
	}

	public void setPrecio4(float precio4) {
		this.precio4 = precio4;
	}

	public float getMayoreo2() {
		return mayoreo2;
	}

	public void setMayoreo2(float mayoreo2) {
		this.mayoreo2 = mayoreo2;
	}

	public float getMayoreo3() {
		return mayoreo3;
	}

	public void setMayoreo3(float mayoreo3) {
		this.mayoreo3 = mayoreo3;
	}

	public float getMayoreo4() {
		return mayoreo4;
	}

	public void setMayoreo4(float mayoreo4) {
		this.mayoreo4 = mayoreo4;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public boolean isReceta() {
		return receta;
	}

	public void setReceta(boolean receta) {
		this.receta = receta;
	}

	public boolean isGranel() {
		return granel;
	}

	public void setGranel(boolean granel) {
		this.granel = granel;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public boolean isPesoAut() {
		return pesoAut;
	}

	public void setPesoAut(boolean pesoAut) {
		this.pesoAut = pesoAut;
	}

	public String getClaveProdServ() {
		return claveProdServ;
	}

	public void setClaveProdServ(String claveProdServ) {
		this.claveProdServ = claveProdServ;
	}

	public float getUnidadCompraId() {
		return unidadCompraId;
	}

	public void setUnidadCompraId(float unidadCompraId) {
		this.unidadCompraId = unidadCompraId;
	}

	public float getUnidadVentaId() {
		return unidadVentaId;
	}

	public void setUnidadVentaId(float unidadVentaId) {
		this.unidadVentaId = unidadVentaId;
	}

	public float getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(float categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String[] getImpuestosId() {
		return impuestosId;
	}

	public void setImpuestosId(String[] impuestosId) {
		this.impuestosId = impuestosId;
	}

	}
